package com.play.notification.service

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.play.networking.api.sync.SyncApi

class ProcessRemoteData {

    private val TAG : String = "ProcessRemoteData"

    fun processData(context: Context, data: Any){
        if (data is Map<*, *>) {
            Log.d(TAG, "Message data payload: $data")
            var intent: Intent? = null
            when {
                data.containsValue(SyncApi.GROUP_PAYLOAD) -> {
                    intent = Intent(NotificationService.ACTION_GROUP_EVENT)
                }
                data.containsValue(SyncApi.SYNC_PAYLOAD) -> {
                    intent = Intent(NotificationService.ACTION_SYNC_EVENT)
                }
                data.containsValue(SyncApi.MEDIA_PAYLOAD) -> {
                    intent = Intent(NotificationService.ACTION_MEDIA_EVENT)
                }
                data.containsValue(SyncApi.MESSAGE_PAYLOAD) -> {
                    intent = Intent(NotificationService.ACTION_MESSAGE_EVENT)
                }
            }
            when {
                intent != null -> {
                    intent.putExtra(NotificationService.DATA_PAYLOAD, data.toString())
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
                else -> {Log.d(TAG, "No Intent Attached")}
            }
        } else
            Log.d(TAG, "No FCM Data")

    }

}