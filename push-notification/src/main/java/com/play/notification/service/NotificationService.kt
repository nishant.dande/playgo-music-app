package com.play.notification.service

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class NotificationService : FirebaseMessagingService() {

    private var processRemoteData: ProcessRemoteData = ProcessRemoteData()

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.d(TAG, "New Token : $token")
        val intent = Intent(ACTION_REFRESH_TOKEN)
        intent.putExtra(DATA_TOKEN, token)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        Log.d(TAG, "From: ${remoteMessage.from}")
        if (remoteMessage.data.isNotEmpty()) {
            processRemoteData.processData(this, remoteMessage.data)
        }
    }

    companion object{
        private val TAG : String = "NotificationService"

        const val ACTION_REFRESH_TOKEN = "refresh-token"
        const val DATA_TOKEN = "token"

        // Push Notification
        const val ACTION_GROUP_EVENT = "action_group_event"
        const val ACTION_SYNC_EVENT = "action_sync_event"
        const val ACTION_MEDIA_EVENT = "action_media_event"
        const val ACTION_MESSAGE_EVENT = "action_message_event"
        const val DATA_PAYLOAD = "payload"

        fun registerDevice(context: Context){
            FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }
                task.result.let {
                    Log.d(TAG,"In Register Device - $it")
                    val intent = Intent(ACTION_REFRESH_TOKEN)
                    intent.putExtra(DATA_TOKEN, it)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
            })
        }
    }
}