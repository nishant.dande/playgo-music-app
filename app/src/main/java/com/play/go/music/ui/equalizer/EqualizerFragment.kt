package com.play.go.music.ui.equalizer

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.media.audiofx.Equalizer
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.play.go.music.R
import com.play.go.music.graph
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.v1.ui.view.RadioGridGroup
import kotlinx.android.synthetic.main.fragment_equalizer.*
import java.util.*

class EqualizerFragment : BaseFragment() {

    data class SelectedMode(var mode: Int,var points: String)

    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }
    private var audioSession = 1
    private lateinit var points: FloatArray
    private var numberOfFrequencyBands: Int = 5
    private var selectedMode: SelectedMode? = null
    private var selectedCheckedMode: Int? = null

    val TAG = "EqualizerFragment"

    override fun getTab(): Int {
        return R.string.equalizer
    }

    private var mEqualizer: Equalizer? = null
    var presetSpinner: Spinner? = null
    var seekBarFinal = arrayOfNulls<SeekBar>(5)
    val equalizerPresetNames = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_equalizer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presetSpinner = view.findViewById<Spinner>(R.id.equalizer_preset_spinner)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                setEqualizer()

                genderRadioGroup.setOnCheckedChangeListener(object : RadioGridGroup.OnCheckedChangeListener{
                    override fun onCheckedChanged(group: RadioGridGroup?, checkedId: Int) {
                        selectedCheckedMode = checkedId
                        when(checkedId){
                            R.id.rbPop ->{
                                val filter = equalizerPresetNames.indexOf("Pop")
                                mEqualizer!!.usePreset((filter-1).toShort())
                            }
                            R.id.rbNormal ->{
                                val filter = equalizerPresetNames.indexOf("Normal")
                                mEqualizer!!.usePreset((filter-1).toShort())
                            }
                            R.id.rbJazz ->{
                                val filter = equalizerPresetNames.indexOf("Jazz")
                                mEqualizer!!.usePreset((filter-1).toShort())
                            }
                            R.id.rbRock ->{
                                val filter = equalizerPresetNames.indexOf("Rock")
                                mEqualizer!!.usePreset((filter-1).toShort())
                            }
                            R.id.rbHipHop ->{
                                val filter = equalizerPresetNames.indexOf("Hip Hop")
                                mEqualizer!!.usePreset((filter-1).toShort())
                            }
                        }

                        if (checkedId != R.id.rbCustomize) {
                            val numberOfFreqBands: Short = 5
                            val lowerEqualizerBandLevel = mEqualizer!!.bandLevelRange[0]
                            for (i in 0 until numberOfFreqBands) {
                                seekBarFinal[i]!!.progress = mEqualizer!!.getBandLevel(i.toShort()) - lowerEqualizerBandLevel
                                seekBarFinal[i]!!.invalidate()
                                points[i] =
                                        (mEqualizer!!.getBandLevel(i.toShort()) - lowerEqualizerBandLevel).toFloat()
                            }
                        } else {
                            if (points.isNotEmpty()){
                                points.forEachIndexed { i, it ->
                                    seekBarFinal[i]!!.progress = it.toInt()
                                    seekBarFinal[i]!!.invalidate()
                                }
                            }
                        }

                    }
                })
                updateUi()
            } catch (e : java.lang.Exception){
                Log.e(TAG, e.printStackTrace().toString())
            }
        }
    }

    fun updateUi(){
        sessionGraph.let {
            selectedMode = it.userPreferences.getEqualizerMode() ?: null
            if (selectedMode != null){
                points = Gson().fromJson(selectedMode!!.points,object : TypeToken<FloatArray>() {}.type)
                        as FloatArray
                genderRadioGroup.check(selectedMode?.mode!!)
            } else {
                genderRadioGroup.check(R.id.rbPop)
            }
        }
    }

    override fun onPause() {
        super.onPause()
        sessionGraph.let {
            selectedMode = SelectedMode(selectedCheckedMode!!, Gson().toJson(points))
            it.userPreferences.setEqualizerMode(selectedMode!!)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun setEqualizer() {
        sessionGraph.let {
            audioSession = it.userPreferences.getSessionId()!!
        }
        mEqualizer = Equalizer(0, audioSession)
        mEqualizer?.enabled = true


        val equalizerPresetSpinnerAdapter = ArrayAdapter(
            requireContext(),
            R.layout.spinner_item,
            equalizerPresetNames
        )
        equalizerPresetSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        equalizerPresetNames.add("Custom")
        for (i in 0 until mEqualizer!!.numberOfPresets) {
            equalizerPresetNames.add(mEqualizer!!.getPresetName(i.toShort()))
        }
        presetSpinner!!.adapter = equalizerPresetSpinnerAdapter
        presetSpinner!!.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                try {
                    if (position != 0) {
                        mEqualizer!!.usePreset((position - 1).toShort())
                        val numberOfFreqBands: Short = 5
                        val lowerEqualizerBandLevel = mEqualizer!!.bandLevelRange[0]
                        for (i in 0 until numberOfFreqBands) {
                            seekBarFinal[i]!!.progress = mEqualizer!!.getBandLevel(i.toShort()) - lowerEqualizerBandLevel
                            seekBarFinal[i]!!.invalidate()
                            points[i] =
                                (mEqualizer!!.getBandLevel(i.toShort()) - lowerEqualizerBandLevel).toFloat()
                        }
                    }
                } catch (e: Exception) {
                    Toast.makeText(
                        requireContext(),
                        "Error while updating Equalizer",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        })


        points = FloatArray(numberOfFrequencyBands)

        val lowerEqualizerBandLevel = mEqualizer!!.bandLevelRange[0]
        val upperEqualizerBandLevel = mEqualizer!!.bandLevelRange[1]
        for (i in 0 until numberOfFrequencyBands) {
            val equalizerBandIndex = i.toShort()
            val frequencyHeaderTextView = TextView(context)
            frequencyHeaderTextView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            frequencyHeaderTextView.gravity = Gravity.CENTER_HORIZONTAL
            frequencyHeaderTextView.text =
                (mEqualizer!!.getCenterFreq(equalizerBandIndex) / 1000).toString() + "Hz"
            val seekBarRowLayout = LinearLayout(context)
            seekBarRowLayout.orientation = LinearLayout.VERTICAL
            val lowerEqualizerBandLevelTextView = TextView(context)
            lowerEqualizerBandLevelTextView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            lowerEqualizerBandLevelTextView.text = (lowerEqualizerBandLevel / 100).toString() + "dB"
            val upperEqualizerBandLevelTextView = TextView(context)
            lowerEqualizerBandLevelTextView.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            upperEqualizerBandLevelTextView.text = (upperEqualizerBandLevel / 100).toString() + "dB"
            val layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            layoutParams.weight = 1f
            var seekBar = SeekBar(context)
            var textView = TextView(context)
            when (i) {
                0 -> {
                    seekBar = view!!.findViewById(R.id.seekBar1)
                    textView = view!!.findViewById(R.id.textView1)
                }
                1 -> {
                    seekBar = view!!.findViewById(R.id.seekBar2)
                    textView = view!!.findViewById(R.id.textView2)
                }
                2 -> {
                    seekBar = view!!.findViewById(R.id.seekBar3)
                    textView = view!!.findViewById(R.id.textView3)
                }
                3 -> {
                    seekBar = view!!.findViewById(R.id.seekBar4)
                    textView = view!!.findViewById(R.id.textView4)
                }
                4 -> {
                    seekBar = view!!.findViewById(R.id.seekBar5)
                    textView = view!!.findViewById(R.id.textView5)
                }
            }
            seekBarFinal[i] = seekBar
            seekBar.progressDrawable.colorFilter =
                PorterDuffColorFilter(Color.DKGRAY, PorterDuff.Mode.SRC_IN)
            seekBar.id = i
            seekBar.max = upperEqualizerBandLevel - lowerEqualizerBandLevel
            textView.text = frequencyHeaderTextView.text
            textView.textAlignment = View.TEXT_ALIGNMENT_CENTER
            seekBar.progress =
                mEqualizer!!.getBandLevel(equalizerBandIndex) - lowerEqualizerBandLevel
            seekBar.setOnTouchListener(object : View.OnTouchListener{
                override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                    genderRadioGroup.check(R.id.rbCustomize)
                    return false
                }
            })
            seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    mEqualizer!!.setBandLevel(
                        equalizerBandIndex,
                        (progress + lowerEqualizerBandLevel).toShort()
                    )
                    points[seekBar.id] =
                        (mEqualizer!!.getBandLevel(equalizerBandIndex) - lowerEqualizerBandLevel).toFloat()
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {
//                    presetSpinner!!.setSelection(0)
                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {}
            })
        }

//        presetSpinner!!.setSelection(0)
    }
}