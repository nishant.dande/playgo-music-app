package com.play.go.music.ui.adapter.my_music

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.play.go.music.R
import com.play.models.music.Artist
import com.play.models.music.Orientation
import com.play.models.music.Song
import kotlinx.android.synthetic.main.item_card_song.view.*

class SongVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onSongClickVH: ((Int, SongVH) -> Unit)? = null
    var onSongLongClickVH: ((SongVH) -> Unit)? = null
    var context: Context = context

    var entity: Pair<Int, Song>? = null
        set(value) {
            field = value
            value?.let { pair ->
                var item = pair.second
                itemView.tvTitle.text = item.title
                try{
                    var artist: ArrayList<Artist> = Gson().fromJson(item.artist,
                        object : TypeToken<List<Artist?>?>() {}.type)
                            as ArrayList<Artist>
                    var artistName = ""
                    if (artist.isNotEmpty()){
                        artist.forEach {artist ->
                            artistName += artist.name+ " "
                        }
                    }
                    itemView.tvDescription.text = artistName
                } catch (e: Exception){

                }
                if (!TextUtils.isEmpty(item.image)) {
                    Glide.with(context)
                        .load(item.image)
                        .into(itemView.ivTitleImage)
                }
                itemView.setOnClickListener { onSongClickVH?.invoke(pair.first, this) }
                itemView.setOnLongClickListener{
                    onSongLongClickVH?.invoke(this)
                    return@setOnLongClickListener true
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): SongVH {
            if (viewType == Orientation.VERTICAL){
                return SongVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_song,
                        parent,
                        false
                    ),parent.context
                )
            } else if (viewType == Orientation.HORIZONTAL_WIDE){
                return SongVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_card_wide_song,
                        parent,
                        false
                    ),parent.context
                )
            } else {
                return SongVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_card_song,
                        parent,
                        false
                    ),parent.context
                )
            }
        }
    }
}

