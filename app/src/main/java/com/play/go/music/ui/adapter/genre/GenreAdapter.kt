package com.play.go.music.ui.adapter.genre

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter


class GenreAdapter()  : ListAdapter<GenreModel, GenreVH>(DIFF_CALLBACK) {

    var clickListener: ((GenreVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreVH {
        return GenreVH.create(parent, viewType)
            .apply {
                this.onClickVH = clickListener
            }
    }

    override fun onBindViewHolder(holder: GenreVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<String>){
        var items = arrayListOf<GenreModel>()
        settings.forEach {
            items.add(GenreModel(it, false))
        }
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as GenreModel )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<GenreModel> )
            }
        }
        submitList(items)
    }

    fun getSelectedItem() : List<GenreModel> {
        if (currentList.isNotEmpty()){
            return currentList.filter {
                it.selection
            }
        }

        return arrayListOf()
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GenreModel>() {
    override fun areItemsTheSame(oldItem: GenreModel, newItem: GenreModel): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: GenreModel, newItem: GenreModel): Boolean {
        return oldItem.genre == newItem.genre
    }
}