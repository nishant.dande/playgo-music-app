package com.play.go.music.ui.base.controller

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import java.util.*

open class SyncTimerTask {

    private val TAG = "SyncTimerTask"

    private var timer: Timer? = null
    private val noDelay = 0L
    private val everyFiveSeconds = 10000L
    private lateinit var syncListener: SyncListener

    interface SyncListener {
        fun timerTask()
    }

    fun task(appCompatActivity: AppCompatActivity, syncListener: SyncListener){
        this.syncListener = syncListener
        val timerTask = object : TimerTask() {
            override fun run() {
                appCompatActivity.runOnUiThread {
                    syncListener.timerTask()
                }
            }
        }

        if (timer == null) {
            timer = Timer()
            timer!!.schedule(timerTask, noDelay, everyFiveSeconds)
        }
    }

    fun stopTimer(){
        if (timer != null){
            timer?.cancel()
            timer?.purge()
        }
    }

}