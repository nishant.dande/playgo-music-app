package com.play.go.music

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.play.go.music.auth.TokenExpiredCallback
import com.play.go.music.graphs.AppGraph
import com.play.go.music.graphs.SessionGraphImpl
import com.play.go.music.graphs.NetworkGraphImpl
import com.play.networking.auth.InvalidTokenReason

class PlayMusic : Application() {

    private val expiredCallback: TokenExpiredCallback = object : TokenExpiredCallback {
        override fun onTokenExpired(reason: InvalidTokenReason) {
            Log.d("Nishant", "IN application token exprire")
//            val sm = appGraph.sessionGraph.sessionManager
//            if (sm.)
//            appGraph.sessionGraph.sessionManager.removeUser {
//                startActivity(Intent(this@Guacamole, MainActivity::class.java).apply {
//                    flags = flags.or(Intent.FLAG_ACTIVITY_NEW_TASK).or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
//                })
//            }
        }
    }

    val appGraph by lazy{
        val sessionGraph = SessionGraphImpl(this)
        AppGraph(sessionGraph = sessionGraph,
            networkGraph = NetworkGraphImpl(
                appContext = this,
                tokenProvider = sessionGraph.tokenProvider
            )
        )
    }

    override fun onCreate() {
        super.onCreate()
        appGraph

        Firebase.database.setPersistenceEnabled(true)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
    }
}

fun Context.graph(): AppGraph {
    return (this.applicationContext as PlayMusic).appGraph
}
