package com.play.go.music.ui.adapter.user

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.play.go.music.R
import com.play.models.account.User
import kotlinx.android.synthetic.main.item_user.view.*


class UserVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(
    itemView
)   {

    var onClickVH: ((UserVH) -> Unit)? = null
    var context: Context = context

    var entity: User? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvUserName.text = item.name
                val drawable = TextDrawable.builder()
                    .buildRound(item.name.subSequence(0, 1).toString().toUpperCase(), Color.GRAY)
                itemView.ivUserPic.setImageDrawable(drawable)
                itemView.setOnClickListener {
                    onClickVH?.invoke(this)
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): UserVH {
            return UserVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_user,
                    parent,
                    false
                ), parent.context
            )
        }
    }
}