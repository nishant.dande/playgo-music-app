package com.play.go.music.ui.adapter.user

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.account.User


class UserAdapter()  : ListAdapter<User, UserVH>(DIFF_CALLBACK) {

    var clickListener: ((UserVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserVH {
        return UserVH.create(parent, viewType)
            .apply {
                this.onClickVH = clickListener
            }
    }

    override fun onBindViewHolder(holder: UserVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(users: ArrayList<User>){
        var items = arrayListOf<User>()
        items = users
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as User )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<User> )
            }
        }
        submitList(items)
    }

    fun populateList(users: User){
        val items = arrayListOf<User>()
        items.add(users)
        populateList(items)
    }

    fun getUsers(): List<User> {
        return currentList
    }

    fun clearList(){
        submitList(null)
        notifyDataSetChanged()
    }

}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.name == newItem.name
    }
}