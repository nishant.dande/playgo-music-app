package com.play.go.music.extensions

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import com.play.go.music.R
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.go.music.v1.ui.login.LoginFragment
import com.play.go.music.v1.ui.my_music.MyMusicFragment
import com.play.go.music.v1.ui.playing.NowPlayingFragment
import com.play.go.music.v1.ui.search.SearchFragment

class BaseActivityFactory {}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Activity.changeSystemBarColor() {
    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    window.statusBarColor = ContextCompat.getColor(this, android.R.color.transparent)
//        window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)
    window.setBackgroundDrawableResource(R.drawable.bg_color)
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Fragment.changeSystemBarColor(fragment : Fragment) {
    requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
    requireActivity().window.statusBarColor = ContextCompat.getColor(requireContext(), android.R.color.transparent)
//        window.navigationBarColor = ContextCompat.getColor(this, android.R.color.transparent)
    if (fragment is HomeFragment || fragment is SearchFragment){
        requireActivity().window.setBackgroundDrawableResource(R.drawable.bg_color_white)
    } else if(fragment is MyMusicFragment || fragment is LoginFragment) {
        requireActivity().window.setBackgroundDrawableResource(R.drawable.bg_color)
    } else if(fragment is NowPlayingFragment) {
        requireActivity().window.setBackgroundDrawableResource(R.drawable.bg_color)
    } else {
        requireActivity().window.setBackgroundDrawableResource(R.drawable.bg_color_white)
    }
}

fun Fragment.changeScreenBackground(fragment: Fragment, view: View){
    if (fragment is HomeFragment || fragment is SearchFragment){
        view.setBackgroundResource(R.drawable.bg_color_white)
    } else if(fragment is MyMusicFragment || fragment is LoginFragment
        || fragment is NowPlayingFragment) {
        view.setBackgroundResource(R.drawable.bg_color)
    } else {
        view.setBackgroundResource(R.drawable.bg_color_white)
    }
}

fun Fragment.changeToolbarBackground(fragment: Fragment, view: View){
    if (fragment is HomeFragment || fragment is SearchFragment){
        view.setBackgroundResource(R.drawable.bg_color_white)
        if (fragment is HomeFragment){
            showHideView(view, R.id.ivProfilePic, View.VISIBLE)
            showHideView(view, R.id.ivSearch, View.GONE)
            showHideView(view, R.id.ivSettings, View.GONE)
            showHideView(view, R.id.ivMenu, View.VISIBLE)
            showHideView(view, R.id.ivNotification, View.VISIBLE)
        }
    } else if(fragment is MyMusicFragment || fragment is LoginFragment) {
        view.setBackgroundResource(R.drawable.bg_color)
        if (fragment is MyMusicFragment){
            showHideView(view, R.id.ivProfilePic, View.VISIBLE)
            showHideView(view, R.id.ivSearch, View.VISIBLE)
            showHideView(view, R.id.ivSettings, View.VISIBLE)
            showHideView(view, R.id.ivMenu, View.GONE)
            showHideView(view, R.id.ivNotification, View.GONE)
        }
    } else if(fragment is NowPlayingFragment) {
        view.setBackgroundResource(R.drawable.bg_color)
        showHideView(view, R.id.ivProfilePic, View.GONE)
        showHideView(view, R.id.ivSettings, View.GONE)
        showHideView(view, R.id.ivMenu, View.GONE)
        showHideView(view, R.id.ivNotification, View.GONE)
    } else {
        view.setBackgroundResource(R.drawable.bg_color_white)
    }
}

fun showToolbarView(fragment: Fragment, toolbar: View){
    toolbar.isGone = fragment is NowPlayingFragment
}

fun showBottomBarView(fragment: Fragment, bottomBar: View){
    bottomBar.isGone = fragment is NowPlayingFragment
}

fun showHideView(view: View, id: Int, status: Int){
    var icon = view.findViewById<View>(id) as AppCompatImageView
    icon.visibility = status
}

val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

private fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}