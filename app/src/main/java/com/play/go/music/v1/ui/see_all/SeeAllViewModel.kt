package com.play.go.music.v1.ui.see_all

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.NetworkGraph
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.session.SyncManager
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.music_player.PlayerProducer
import com.play.networking.Outcome
import com.play.networking.api.youtube.YoutubeApi
import com.play.networking.error.APIError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SeeAllViewModel(
    val sessionGraph: SessionGraph,
    val networkGraph: NetworkGraph
) : SyncManager(sessionGraph.userPreferences, networkGraph.syncSource){

    var error = MutableLiveData<APIError>()

    fun getYoutubeSongsByLanguage(context: Context? = null,
                                  language: String, youtubeSongsByLanguage: MutableLiveData<ArrayList<Song>>){
        viewModelScope.launch {
            networkGraph.syncSource.getLatestSongByLanguage(context, language).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
//                        info { it.data }
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {item ->


                                var song_id = item?.id?.videoId
                                if (song_id != null) {
                                    var song = Song("",
                                        item?.snippet?.title!!,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        item?.snippet?.thumbnails?.medium?.url!!
                                    )
                                    songs.add(song)
                                }
                            }
                        }

                        youtubeSongsByLanguage.value = songs

                    }
                    is Outcome.Failure -> {
                        it.e.printStackTrace()
                        error.value = APIError(it.e, "${language} songs are not available")
                    }
                    else -> {
                    }
                }
            }
        }
    }

}
