package com.play.go.music.ui.adapter.my_music

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.music.Song


class TopSongAdapter()  : ListAdapter<Song, TopSongVH>(TOP_DIFF_CALLBACK) {

    var onClick: ((TopSongVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopSongVH {
                return TopSongVH.create(parent, viewType)
                    .apply {
                        this.onSongClickVH = onClick
                    }
    }

    override fun onBindViewHolder(holder: TopSongVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Song>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as Song )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<Song> )
            }
        }
        submitList(items)
    }

    fun getItemByPosition(position: Int) : Song{
        return currentList[position]
    }
}


val TOP_DIFF_CALLBACK = object : DiffUtil.ItemCallback<Song>() {
    override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem.title == newItem.title
    }
}