package com.play.go.music.ui.adapter.drawer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import kotlinx.android.synthetic.main.item_menu.view.*

class MenuAdapter(
    private val context: Context,
    private val data: ArrayList<Int>,
    private val itemClickListener: ClickListener
) : RecyclerView.Adapter<MenuAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Int, context: Context) {
            itemView.tvTitle.text = context.getString(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_menu, parent,
                false
            )
        )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val itemData = data[position];
        holder.bind(itemData, context)
        holder.itemView.setOnClickListener(View.OnClickListener {
            itemClickListener.onListItemClickListener(itemData);
        })
    }

    fun inflateMenuList(){
        val mMenus : ArrayList<Int> = arrayListOf()

        mMenus.add(R.string.home)
        mMenus.add(R.string.my_music)
        mMenus.add(R.string.settings)
        mMenus.add(R.string.logout)

        data.clear()
        data.addAll(0, mMenus)
        notifyDataSetChanged()
    }

    interface ClickListener {
        fun onListItemClickListener(item: Int)
    }
}