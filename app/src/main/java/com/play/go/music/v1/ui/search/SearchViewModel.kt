package com.play.go.music.v1.ui.search

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.utils.Utility
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.models.youtube.YoutubeParams
import com.play.music_player.PlayerProducer
import com.play.networking.Outcome
import com.play.networking.api.spotify.source.SpotifyAccountSource
import com.play.networking.api.spotify.source.SpotifySource
import com.play.networking.api.youtube.YoutubeApi
import com.play.networking.api.youtube.source.YoutubeSource
import kotlinx.coroutines.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.io.File


class SearchViewModel(
    val sessionGraph: SessionGraph,
    val spotifySource: SpotifySource,
    val youtubeSource: YoutubeSource,
    val spotifyAccountSource: SpotifyAccountSource,
) : CoroutineScope by CoroutineScope(Dispatchers.IO), ViewModel(), AnkoLogger {

    companion object{
        val OFFSET = "1"
        val LIMIT = "50"
    }

    var getAllPlaylist = MutableLiveData<List<Playlist>>()
    var localSongs = MutableLiveData<List<Song>>()
    var spotifySongs = MutableLiveData<ArrayList<Song>>()
    var youtubeSongs = MutableLiveData<ArrayList<Song>>()

    init {
        initializeLibrarySongs()
    }

    private fun initializeLibrarySongs() {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.selectAllPlaylist()
            info { "initial songs cache=${cached.count()}" }
            launch(Dispatchers.Main) { getAllPlaylist.value = cached  }

        }
    }

    fun initializeLocalSongs() {
        launch(Dispatchers.IO) {
//            val cached = sessionGraph.library.getRecentAdded()
//            info { "initial songs cache=${cached.count()}" }
//            launch(Dispatchers.Main) { recentlyAdded.value = cached  }

            sessionGraph.userPreferences.getLocalLibrary().forEach {
                var list = withContext(Dispatchers.IO){
                    val files = Utility.getFiles(File(it)) ?: return@withContext null

                    var songs = arrayListOf<Song>()
                    files.forEach {
                        var imageUrl = ""

                        var song = Song("", it.name,it.absolutePath,
                            PlayerProducer.LOCAL_PLAYER, imageUrl)
                        songs.add(song)
                    }
                    return@withContext songs
                }

                if (list != null) {
                    launch(Dispatchers.Main) {
                        localSongs.value = list
                    }
                }
            }
        }
    }

    fun createPlaylist(name: String){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.createPlaylist(Playlist(name = name))
                val cached = sessionGraph.library.selectAllPlaylist()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { getAllPlaylist.value = cached  }
            }
        }
    }

    fun getYoutubeSearchList(query:String){
        viewModelScope.launch {
            youtubeSource.searchBy(YoutubeApi.API_KEY, YoutubeParams(query = query)).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
//                        info { it.data }
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {
                                var song_id = it.id.channelId ?: it.id.videoId
                                if (song_id != null) {
                                    var song = Song("",
                                        it.snippet.title,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        it.snippet.thumbnails.medium.url
                                    )
                                    songs.add(song)
                                }
                            }
                        }

                        youtubeSongs.value = songs

                    }
                    is Outcome.Failure -> it.e.printStackTrace()
                    else -> {
                    }
                }
            }
        }
    }

    fun getSpotifySearchList(query:String, limit: String, offset: String){
        viewModelScope.launch {
            spotifySource.searchBy(query, "album", limit, offset).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                        info { it.data }
                        var songs = arrayListOf<Song>()
                        it.data.let { searchItems ->
                            searchItems!!.albums.items.forEach {
                                var imageUrl = ""
                                if (it.images.isNotEmpty() &&
                                    it.images.size > 2){
                                    imageUrl = it.images[1].url
                                }
                                var artistName = ""
                                it.artists.forEach {artist ->
                                    artistName = artist.name
                                }
                                var song = Song(artistName, it.name,it.uri, PlayerProducer.SPOTIFY_PLAYER, imageUrl)
                                songs.add(song)
                            }
                        }
                        spotifySongs.value = songs
                    }
                    is Outcome.Failure -> {
                        it.e.errorMessage
                        if (it.e.errorMessage.toString().toLowerCase().equals("Only valid bearer authentication supported".toLowerCase())){
//                            getSpotifyToken()
                        }
                    }
                    else -> {
                    }
                }
            }
        }
    }

    var youtubeRecommendedSongs = MutableLiveData<ArrayList<Song>>()
    fun getYoutubeRecommendedSongs(){
        viewModelScope.launch {
            youtubeSource.getRecommended(YoutubeApi.API_KEY).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
//                        info { it.data }
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {item ->


                                var song_id = item?.id
                                if (song_id != null) {
                                    var song = Song("",
                                        item?.snippet?.title!!,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        item?.snippet?.thumbnails?.medium?.url!!
                                    )
                                    songs.add(song)
                                }
                            }
                        }

                        youtubeRecommendedSongs.value = songs

                    }
                    is Outcome.Failure -> it.e.printStackTrace()
                    else -> {
                    }
                }
            }
        }
    }

    var youtubeNewSongs = MutableLiveData<ArrayList<Song>>()
    fun getYoutubeNewSongs(context : Context){
        viewModelScope.launch {
            youtubeSource.getSongsByType(context,
                YoutubeApi.API_KEY, filePath = "new_music.json", query = "").let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {item ->


                                var song_id = item?.id?.videoId
                                if (song_id != null) {
                                    var song = Song("",
                                        item?.snippet?.title!!,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        item?.snippet?.thumbnails?.medium?.url!!
                                    )
                                    songs.add(song)
                                }
                            }
                        }
                        youtubeNewSongs.value = songs
                    }
                    is Outcome.Failure -> it.e.printStackTrace()
                    else -> {
                    }
                }
            }
        }
    }

    var youtubePartySongs = MutableLiveData<ArrayList<Song>>()
    fun getYoutubePartySongs(context : Context){
        viewModelScope.launch {
            youtubeSource.getSongsByType(context,
                YoutubeApi.API_KEY, filePath = "party_songs.json", query = "").let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {item ->


                                var song_id = item?.id?.videoId
                                if (song_id != null) {
                                    var song = Song("",
                                        item?.snippet?.title!!,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        item?.snippet?.thumbnails?.medium?.url!!
                                    )
                                    songs.add(song)
                                }
                            }
                        }
                        youtubePartySongs.value = songs
                    }
                    is Outcome.Failure -> it.e.printStackTrace()
                    else -> {
                    }
                }
            }
        }
    }

    var youtubePopularSongs = MutableLiveData<ArrayList<Song>>()
    fun getYoutubePopularSongs(context : Context){
        viewModelScope.launch {
            youtubeSource.getSongsByType(context,
                YoutubeApi.API_KEY, filePath = "popular_songs.json", query = "").let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {item ->


                                var song_id = item?.id?.videoId
                                if (song_id != null) {
                                    var song = Song("",
                                        item?.snippet?.title!!,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        item?.snippet?.thumbnails?.medium?.url!!
                                    )
                                    songs.add(song)
                                }
                            }
                        }
                        youtubePopularSongs.value = songs
                    }
                    is Outcome.Failure -> it.e.printStackTrace()
                    else -> {
                    }
                }
            }
        }
    }


}
