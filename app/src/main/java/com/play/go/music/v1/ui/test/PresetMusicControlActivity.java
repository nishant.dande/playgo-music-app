package com.play.go.music.v1.ui.test;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.play.ble_service.headless.activity.ServiceActivity;
import com.play.ble_service.service.BluetoothService;
import com.play.go.music.R;
import com.play.go.music.v1.ui.gaia.PresetMusicControlFeatureGaiaManager;
import com.qualcomm.qti.libraries.gaia.GAIA;

/**
 * <p>This activity is for controlling the streamed sound of the audio device connected to the application as using a
 * remote control.</p>
 */
public class PresetMusicControlActivity extends ServiceActivity implements View.OnClickListener,
        PresetMusicControlFeatureGaiaManager.GaiaManagerListener {

    // ====== PRIVATE FIELDS =======================================================================

    /**
     * For debug mode, the tag to display for logs.
     */
    public static final String TAG = "v1PresetMusicControl";
    /**
     * To manage the GAIA packets which have been received from the device and which will be sent to the device.
     */
    private PresetMusicControlFeatureGaiaManager mGaiaManager;



    // ====== ACTIVITY METHODS =======================================================================

    @Override // Activity
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Nishant", "in discovery onCreate");
        setContentView(R.layout.activity_preset_music);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    // ====== SERVICE METHODS =======================================================================

    @Override // ServiceActivity
    protected void handleMessageFromService(Message msg) {
        if(msg != null && msg.obj != null) {
            Log.d(TAG, "In PresetMusicControlActivity " + msg.what + " "
                    + msg.obj.toString());
            //noinspection UnusedAssignment
            String handleMessage = "Handle a message from BLE service: ";

            switch (msg.what) {
                case BluetoothService.Messages.CONNECTION_STATE_HAS_CHANGED:
                    @BluetoothService.State int connectionState = (int) msg.obj;
                    String stateLabel = connectionState == BluetoothService.State.CONNECTED ? "CONNECTED"
                            : connectionState == BluetoothService.State.CONNECTING ? "CONNECTING"
                            : connectionState == BluetoothService.State.DISCONNECTING ? "DISCONNECTING"
                            : connectionState == BluetoothService.State.DISCONNECTED ? "DISCONNECTED"
                            : "UNKNOWN";
                    displayLongToast(getString(R.string.toast_device_information) + stateLabel);
                    if (DEBUG)
                        Log.d(TAG, handleMessage + "CONNECTION_STATE_HAS_CHANGED: " + stateLabel);
                    break;

                case BluetoothService.Messages.DEVICE_BOND_STATE_HAS_CHANGED:
                    int bondState = (int) msg.obj;
                    String bondStateLabel = bondState == BluetoothDevice.BOND_BONDED ? "BONDED"
                            : bondState == BluetoothDevice.BOND_BONDING ? "BONDING"
                            : "BOND NONE";
                    displayLongToast(getString(R.string.toast_device_information) + bondStateLabel);
                    if (DEBUG)
                        Log.d(TAG, handleMessage + "DEVICE_BOND_STATE_HAS_CHANGED: " + bondStateLabel);
                    break;

                case BluetoothService.Messages.GATT_SUPPORT:
                    if (DEBUG) Log.d(TAG, handleMessage + "GATT_SUPPORT");
                    break;

                case BluetoothService.Messages.GAIA_PACKET:
                    byte[] data = (byte[]) msg.obj;
                    mGaiaManager.onReceiveGAIAPacket(data);
                    break;

                case BluetoothService.Messages.GAIA_READY:
                    if (DEBUG) Log.d(TAG, handleMessage + "GAIA_READY");
                    break;

                case BluetoothService.Messages.GATT_READY:
                    if (DEBUG) Log.d(TAG, handleMessage + "GATT_READY");
                    break;

                default:
                    if (DEBUG)
                        Log.d(TAG, handleMessage + "UNKNOWN MESSAGE: " + msg.what);
                    break;
            }
        } else {
            Log.d(TAG, "msg object null");
        }
    }

    @Override // ServiceActivity
    protected void onServiceConnected() {
        @GAIA.Transport int transport = getTransport() == BluetoothService.Transport.BR_EDR ?
                GAIA.Transport.BR_EDR : GAIA.Transport.BLE;
        mGaiaManager = new PresetMusicControlFeatureGaiaManager(this, transport);
        mGaiaManager.getPresetMusic();
    }

    @Override // ServiceActivity
    protected void onServiceDisconnected() {
    }


    // ====== GAIA MANAGER METHODS =======================================================================

    @Override // InformationGaiaManager.GaiaManagerListener
    public boolean sendGAIAPacket(byte[] packet) {
        return mService!= null && mService.isGaiaReady() && mService.sendGAIAPacket(packet);
    }

    @Override
    public void onGetPreset(int preset) {
        Log.d(TAG, "In PresetMusicControlActivity onGetPreset "+ preset);
        checkControl(preset);
    }

    @Override
    public void onControlNotSupported(int control) {
        Log.d(TAG, "In PresetMusicControlActivity onControlNotSupported "+ control);
    }



    // ====== OVERRIDE METHODS =======================================================================

    @Override // View.OnClickListener
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnApply:

            break;

            case R.id.btnGetInformation:
                mGaiaManager.getPresetMusic();
            break;

        }

    }

    // ====== UI METHODS ======================================================================

    /**
     * To initialise objects used in this activity.
     */
    private void init() {
        // manage the action bar
//        this.setSupportActionBar((Toolbar) findViewById(R.id.tb_menu));
        //noinspection ConstantConditions
//        this.getSupportActionBar().setLogo(R.drawable.ic_remote_32dp);
//        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_24dp);

        // adding listener for each button
        findViewById(R.id.btnApply).setOnClickListener(this);
        findViewById(R.id.btnGetInformation).setOnClickListener(this);


        ((RadioGroup)findViewById(R.id.rbEvents)).clearCheck();
        ((RadioGroup)findViewById(R.id.rbEvents)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                int eventCheckId = ((RadioGroup)findViewById(R.id.rbEvents)).getCheckedRadioButtonId();
                switch (eventCheckId){
                    case R.id.rbNormalMode :{
                        int rbId = radioGroup.getCheckedRadioButtonId();
                        if (rbId != -1) {
                            RadioButton radioButton = (RadioButton) radioGroup.findViewById(id);
                            if (radioButton.isChecked()) {
                                mGaiaManager.setPresetMusic(0);
                            }
                        }
                        break;
                    }
                    case R.id.rbMusicMode : {
                        int rbId = radioGroup.getCheckedRadioButtonId();
                        if (rbId != -1) {
                            RadioButton radioButton = (RadioButton) radioGroup.findViewById(id);
                            if (radioButton.isChecked()) {
                                mGaiaManager.setPresetMusic(2);
                            }
                        }
                        break;
                    }
                    case R.id.rbBassBoastMode :{
                        int rbId = radioGroup.getCheckedRadioButtonId();
                        if (rbId != -1) {
                            RadioButton radioButton = (RadioButton) radioGroup.findViewById(id);
                            if (radioButton.isChecked()) {
                                mGaiaManager.setPresetMusic(1);
                            }
                        }
                        break;
                    }
                    case R.id.rbMovieMode :{
                        int rbId = radioGroup.getCheckedRadioButtonId();
                        if (rbId != -1) {
                            RadioButton radioButton = (RadioButton) radioGroup.findViewById(id);
                            if (radioButton.isChecked()) {
                                mGaiaManager.setPresetMusic(3);
                            }
                        }
                        break;
                    }
                    case R.id.rbPodcastMode :{
                        int rbId = radioGroup.getCheckedRadioButtonId();
                        if (rbId != -1) {
                            RadioButton radioButton = (RadioButton) radioGroup.findViewById(id);
                            if (radioButton.isChecked()) {
                                mGaiaManager.setPresetMusic(4);
                            }
                        }
                        break;
                    }
                }
            }
        });

    }

    private void checkControl(int position){
        ((RadioGroup)findViewById(R.id.rbEvents)).clearCheck();
        switch (position){
            case 0 : {
                ((RadioGroup)findViewById(R.id.rbEvents)).check(R.id.rbNormalMode);
                break;
            }
            case 1 : {
                ((RadioGroup)findViewById(R.id.rbEvents)).check(R.id.rbBassBoastMode);
                break;
            }

            case 2 : {
                ((RadioGroup)findViewById(R.id.rbEvents)).check(R.id.rbMusicMode);
                break;
            }

            case 3 : {
                ((RadioGroup)findViewById(R.id.rbEvents)).check(R.id.rbMovieMode);
                break;
            }

            case 4 : {
                ((RadioGroup)findViewById(R.id.rbEvents)).check(R.id.rbPodcastMode);
                break;
            }
        }
    }
}
