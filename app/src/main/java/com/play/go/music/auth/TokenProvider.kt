package com.play.go.music.auth

import android.content.Context
import android.util.Base64
import com.play.go.music.preferences.UserPreferences
import com.play.networking.auth.InvalidTokenReason
import com.play.networking.auth.TokenProviding

class TokenProvider private constructor(
    private val context: Context,
    private val prefs: UserPreferences,
    private val expiredCallback: TokenExpiredCallback? = null
): TokenProviding {

    override fun getDeveloperToken(): String = "context.strings[R.string.musickit_token]"

    override fun getUserToken(): String = "prefs.currentUser?.token ?:"

    // TODO - handle perf token
    override fun getSpotifyApiToken(): String? = prefs.apiToken ?: ""

    override fun getSpotifyAuthToken(): String? =
        Base64.encodeToString("5d8c45cbb8b84049b3b3dabe6bbb64c1:469d1e55b35f42e19ddcc494af885509".toByteArray(), Base64.NO_WRAP)

    companion object {
        fun with(context: Context, userPreferences: UserPreferences, expiredCallback: TokenExpiredCallback?): TokenProvider {
            return TokenProvider(context, userPreferences, expiredCallback)
        }
    }

    override fun onTokenExpired(reason: InvalidTokenReason) {
        expiredCallback?.onTokenExpired(reason)
    }
}

interface TokenExpiredCallback {
    fun onTokenExpired(reason: InvalidTokenReason)
}
