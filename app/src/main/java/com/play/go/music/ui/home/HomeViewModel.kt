package com.play.go.music.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.NetworkGraph
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.session.SyncManager
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.models.news.MusicNews
import com.play.models.youtube.YoutubeParams
import com.play.music_player.PlayerProducer
import com.play.networking.Outcome
import com.play.networking.api.youtube.YoutubeApi
import com.play.networking.utils.Utility
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.error
import org.jetbrains.anko.info


class HomeViewModel(
    val sessionGraph: SessionGraph,
    val networkGraph: NetworkGraph
) : SyncManager(sessionGraph.userPreferences, networkGraph.syncSource){

    companion object{
        val OFFSET = "1"
        val LIMIT = "50"
    }

    var getAllPlaylist = MutableLiveData<List<Playlist>>()
    var getPlaylist = MutableLiveData<Playlist>()
    var recentlyAdded = MutableLiveData<List<Song>>()
    var newReleaseSongs = MutableLiveData<ArrayList<Song>>()

    init {
//        initializeLibrarySongs()
//        initializeRecentlyAddedSongs()

    }

    fun getUserPlaylist() {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.selectAllPlaylist()
            info { "initial songs cache=${cached.count()}" }
            launch(Dispatchers.Main) { getAllPlaylist.value = cached  }

        }
    }

    fun getUserPlaylistById(id: Int) {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.getPlaylistById(id)
            launch(Dispatchers.Main) { getPlaylist.value = cached  }

        }
    }

    fun initializeRecentlyAddedSongs() {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.getRecentAdded()
            info { "initial songs cache=${cached.count()}" }
            launch(Dispatchers.Main) { recentlyAdded.value = cached  }
        }
    }

    fun createPlaylist(name: String){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.createPlaylist(Playlist(name = name))
                val cached = sessionGraph.library.selectAllPlaylist()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { getAllPlaylist.value = cached  }
            }
        }
    }

    fun managePlaylist(playlistId: Int, songId: Int){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.addSongToPlaylist(ManagePlaylist(playlistId, songId))
                val cached = sessionGraph.library.selectAllPlaylist()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { getAllPlaylist.value = cached  }
            }
        }
    }

    fun addRecentlyPlayedSong(song: Song){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.addRecentSong(song)
                val cached = sessionGraph.library.getRecentAdded()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { recentlyAdded.value = cached  }
            }
        }
    }

    fun getSpotifyNewRelease(limit: String, offset: String) {
        viewModelScope.launch {
            networkGraph.spotifySource.newRelease(limit, offset).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
//                        info { it.data }
                        var songs = arrayListOf<Song>()
                        it.data.let { searchItems ->
                            searchItems!!.albums.items.forEach {
                                var imageUrl = ""
                                if (it.images!=null &&
                                    it.images.size > 2){
                                    imageUrl = it.images[1].url
                                }
                                var artistName = ""
                                it.artists.forEach {artist ->
                                    artistName = artist.name
                                }

                                var song = Song(artistName, it.name,
                                    it.uri,
                                    PlayerProducer.SPOTIFY_PLAYER,
                                    imageUrl)
                                songs.add(song)
                            }
                        }
                        newReleaseSongs.value = songs
                    }
                    is Outcome.Failure -> {
                        it.e.printStackTrace()
                        if (it.e.errorMessage.toString().toLowerCase()
                                .equals("Only valid bearer authentication supported".toLowerCase())){
                            getSpotifyToken()
                        }
                    }
                    else -> {
                    }
                }
            }
        }
    }

    private val _authState = MutableLiveData<TokenState>()
    val authState: LiveData<TokenState> get() = _authState
    fun getSpotifyToken(){
        viewModelScope.launch {
            networkGraph.spotifyAccountSource.getToken().let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                        info { it.data }
                        it.data?.access_token?.let { token ->
                            sessionGraph.sessionManager.createUser(token)
                            if (sessionGraph.sessionManager.apiToken != null)
                                _authState.value = TokenState.Authenticated(token)
                            else
                                _authState.value = TokenState.InvalidAuthentication(Exception("Wrong with cache"))
                        }
                    }
                    is Outcome.Failure -> {
                        it.e.errorMessage
                        _authState.value = TokenState.InvalidAuthentication(it.e)
                    }
                    else -> {
                    }
                }
            }
        }
    }
    sealed class TokenState {
        data class Authenticated(val auth: String) : TokenState()
        data class InvalidAuthentication(val cause: Throwable?) : TokenState()
    }

    var groups = MutableLiveData<ArrayList<Group>>()
    fun getGroups(){
        viewModelScope.launch {
            networkGraph.accountSource.getGroups().let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        groups.value = it.data
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }

    var users = MutableLiveData<ArrayList<User>>()
    fun getUsersByGroup(currentUser: String, ids: ArrayList<String>){
        viewModelScope.launch {
            networkGraph.accountSource.getAllUsers().let {
                when(it){
                    is Outcome.Success ->{
                        var _user = arrayListOf<User>()
                        it.data?.forEach {
                            if (ids.contains(it.userId)){
                                _user.add(it)
                            }
                        }
                        users.value = _user
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }

    var lyrics = MutableLiveData<String>()
    fun getLyrics(provider: String, title: String, artist: String){
        viewModelScope.launch {
            networkGraph.lyricsSource.getLyrics(provider, title, artist).let {
                when(it){
                    is Outcome.Success -> {
//                        Log.d("Nishant", it.data?.get("lyrics").toString())
                        lyrics.value = it.data?.get("lyrics").toString()
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }

    var youtubeSongs = MutableLiveData<ArrayList<Song>>()
    fun getYoutubeRecommendation(){
        viewModelScope.launch {
            networkGraph.youtubeSource.getRecommended(YoutubeApi.API_KEY).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
//                        info { it.data }
                        var songs = arrayListOf<Song>()
                        it.data.let {searchItems ->
                            searchItems?.items?.forEach {item ->


                                var song_id = item?.id
                                if (song_id != null) {
                                    var song = Song("",
                                        item?.snippet?.title!!,
                                        song_id,
                                        PlayerProducer.YOUTUBE_PLAYER,
                                        item?.snippet?.thumbnails?.medium?.url!!
                                    )
                                    songs.add(song)
                                }
                            }
                        }

                        youtubeSongs.value = songs

                    }
                    is Outcome.Failure -> it.e.printStackTrace()
                    else -> {
                    }
                }
            }
        }
    }

}
