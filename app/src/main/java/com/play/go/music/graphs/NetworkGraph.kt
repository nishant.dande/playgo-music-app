package com.play.go.music.graphs

import android.content.Context
import com.play.go.music.auth.TokenProvider
import com.play.networking.BuildConfig
import com.play.networking.api.account.source.AccountSource
import com.play.networking.api.auth.AuthApi
import com.play.networking.api.auth.source.AuthSource
import com.play.networking.api.lyrics.LyricsApi
import com.play.networking.api.lyrics.source.LyricsSource
import com.play.networking.api.news.NewsApi
import com.play.networking.api.news.source.NewsSource
import com.play.networking.api.provideRetrofit
import com.play.networking.api.spotify.source.SpotifySource
import com.play.networking.api.spotify.SpotifyApi
import com.play.networking.api.spotify.source.SpotifyAccountSource
import com.play.networking.api.sync.SyncApi
import com.play.networking.api.sync.source.SyncSource
import com.play.networking.api.youtube.YoutubeApi
import com.play.networking.api.youtube.source.YoutubeSource
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor

interface NetworkGraph {
    val spotifySource: SpotifySource
    val spotifyAccountSource: SpotifyAccountSource
    val youtubeSource: YoutubeSource
    val accountSource: AccountSource
    val syncSource: SyncSource
    val lyricsSource: LyricsSource
    val newsSource: NewsSource
    val authSource: AuthSource
}

class NetworkGraphImpl(
    appContext: Context,
    tokenProvider: TokenProvider
) : NetworkGraph {

    private val spotifyApi by lazy {
        provideRetrofit(
            baseUrl = SpotifyApi.BASE_URL,
            tokenProvider = tokenProvider,
            httpClient = OkHttpClient.Builder().apply {


                // token in headers
                this.addInterceptor {
                    val builder: Request.Builder = it.request().newBuilder()
                    builder.addHeader("Authorization", "Bearer ${tokenProvider.getSpotifyApiToken()}")
                    it.proceed(builder.build())
                }
                // add logging interceptor last to view others interceptors
                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor().also {
                        it.level = HttpLoggingInterceptor.Level.BODY
                    }
                    this.addInterceptor(logging)
                }
            }.build()
        )
    }

    private val spotifyAccountApi by lazy {
        provideRetrofit(
            baseUrl = SpotifyApi.ACCOUNT_BASE_URL,
            tokenProvider = tokenProvider,
            httpClient = OkHttpClient.Builder().apply {

                // token in headers
                this.addInterceptor {
                    val builder: Request.Builder = it.request().newBuilder()
                    builder.addHeader("Authorization", "Basic ${tokenProvider.getSpotifyAuthToken()}")
                    it.proceed(builder.build())
                }
                // add logging interceptor last to view others interceptors
                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor().also {
                        it.level = HttpLoggingInterceptor.Level.BODY
                    }
                    this.addInterceptor(logging)
                }
            }.build()
        )
    }

    private val youtubeApi by lazy {
        provideRetrofit(
            baseUrl = YoutubeApi.BASE_URL,
            tokenProvider = tokenProvider
        )
    }

    private val syncApi by lazy {
        provideRetrofit(
            baseUrl = SyncApi.BASE_URL,
            tokenProvider = tokenProvider
        )
    }

    private val lyricsApi by lazy {
        provideRetrofit(
            baseUrl = LyricsApi.BASE_URL,
            tokenProvider = tokenProvider
        )
    }

    private val newsApi by lazy {
        provideRetrofit(
            baseUrl = NewsApi.BASE_URL,
            tokenProvider = tokenProvider
        )
    }

    private val authApi by lazy {
        provideRetrofit(
            baseUrl = AuthApi.BASE_URL,
            tokenProvider = tokenProvider
        )
    }


    override val spotifySource: SpotifySource = SpotifySource(spotifyApi)
    override val spotifyAccountSource: SpotifyAccountSource = SpotifyAccountSource(spotifyAccountApi)
    override val youtubeSource: YoutubeSource = YoutubeSource(youtubeApi)
    override val accountSource: AccountSource = AccountSource()
    override val syncSource: SyncSource = SyncSource(syncApi)
    override val lyricsSource: LyricsSource = LyricsSource(lyricsApi)
    override val newsSource: NewsSource = NewsSource(newsApi)
    override val authSource: AuthSource = AuthSource(authApi)
}
