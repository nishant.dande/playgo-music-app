package com.play.go.music.ui.adapter.devices

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.play.ble_service.model.CustomDevice
import com.play.ble_service.service.BluetoothService
import com.play.go.music.R
import kotlinx.android.synthetic.main.item_discovery_device.view.*

class DevicesAdapter(
    private val context: Context,
    private val data: ArrayList<CustomDevice>,
    private val itemClickListener: ClickListener
) : RecyclerView.Adapter<DevicesAdapter.DataViewHolder>() {

    class DataViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: CustomDevice, context: Context) {
            itemView.tvDeviceName.text = item.device.name
            itemView.tvDeviceAddress.text = item.device.address
            itemView.tvDeviceType.text = getBluetoothTransport(item.device.type)
            if (item.connected == 1){
                itemView.tvConnected.visibility = View.VISIBLE
                itemView.chDevice.isChecked = true
                itemView.tvConnected.text = "Connected"
            } else {
                itemView.tvConnected.visibility = View.GONE
                itemView.chDevice.isChecked = false
            }
        }

        private fun getBluetoothTransport(transport: Int): String {
            return when (transport) {
                BluetoothService.Transport.BR_EDR -> {
                    "BR/EDR Device"
                }
                BluetoothService.Transport.BLE -> {
                    "BLE Device"
                }
                else -> {
                    "UNKNOWN"
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_discovery_device, parent,
                false
            )
        )

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        val itemData = data[position];
        holder.bind(itemData, context)
        holder.itemView.setOnClickListener(View.OnClickListener {
            itemClickListener.onListItemClickListener(itemData);
        })
    }

    fun populateList(customDevice: CustomDevice){
        val filterIndexed = data.filterIndexed { index, playlist ->
            playlist.device.address == customDevice.device.address
        }
        if (filterIndexed.isNotEmpty()){
            filterIndexed[0].apply {
                connected = customDevice.connected
            }
            notifyDataSetChanged()
        } else {
            data.add(customDevice)
            notifyDataSetChanged()
        }
    }

    interface ClickListener {
        fun onListItemClickListener(item: CustomDevice)
    }


}