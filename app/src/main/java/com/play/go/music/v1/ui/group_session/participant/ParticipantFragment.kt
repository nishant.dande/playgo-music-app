package com.play.go.music.v1.ui.group_session.participant

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.awesomedialog.*
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.extensions.hideKeyboard
import com.play.go.music.graph
import com.play.go.music.ui.adapter.user.UserAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.base.controller.ControllerViewModel
import com.play.go.music.v1.ui.group_session.session.SessionViewModel
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.account.UserInfo
import com.play.networking.error.APIError
import kotlinx.android.synthetic.main.fragment_participant.*
import java.util.*
import kotlin.collections.ArrayList


class ParticipantFragment : BaseFragment() {

    val TAG = "ParticipantFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private val USERS_LIMIT = 5
    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived
    private lateinit var group: Group
    private var filteredUser: List<User> = arrayListOf()
    private var unFilteredUser: List<User> = arrayListOf()
    private lateinit var adapter: ArrayAdapter<User>

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { SessionViewModel(session, networkGraph) }
            }
        }
    }

    private val vmController by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { ControllerViewModel(session, networkGraph) }
            }
        }
    }


    override fun getTab(): Int {
        return R.string.participant
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.getAllUsers()

        // read argument
        arguments.let {
            if (it != null) {
                group = it.get("group") as Group
            } else {
                Log.d(TAG, "No argument received")
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_participant, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        observer()
        //Set list view for music list
        rvParticipant!!.layoutManager = LinearLayoutManager(requireContext())
        rvParticipant!!.adapter = userList

        setClickListener()
    }

    private fun userSearchUi() {

        adapter =
            ArrayAdapter<User>(requireContext(),android.R.layout.simple_list_item_1, filteredUser)
        edtParticipantSearch.setAdapter(adapter)
        edtParticipantSearch.threshold = 1
        edtParticipantSearch.setOnItemClickListener { parent, view, position, id ->
            group.let {
                val userIds = group.userInfo?.map {
                    it.userId
                }
                if (userIds?.size!! < USERS_LIMIT) {
                    val itemAtPosition = parent.getItemAtPosition(position) as User
                    if (!userIds?.contains(itemAtPosition.userId)!!) {

                        group.userInfo.apply {
                            this?.add(UserInfo(itemAtPosition.userId!!, UserInfo.NO_ACTION))
                        }

                        // update group
                        if (!TextUtils.isEmpty(group.name)) {
                            vm.assignGroup(group.name, group.ownerId!!, group.userInfo)
                        }
                        edtParticipantSearch.text.clear()
                        hideKeyboard()
                    }
                } else {
                    showSnackBar("You can invite up to 5 friends")
                }
            }
        }
    }

    private fun setClickListener() {
        iv_search.setOnClickListener {
            handleSearchUi(true)
        }

        iv_back.setOnClickListener {
            handleSearchUi(false)
        }

        btnInviteFriends.setOnClickListener {
            group.let {
                val userIds = group.userInfo?.map {
                    it.userId
                }
                if (userIds?.isNotEmpty()!!) {
                    sessionGraph.userPreferences.setSelectedGroupName(group.name)
                    vmController.requestUserToJoin(requireContext(), userIds as ArrayList<String>)
                }
                else
                    Log.e(TAG, "Users Id is empty")
            }
        }
    }

    private fun handleSearchUi(showSearchBar: Boolean){
        if (showSearchBar){
            tvWhoListening.visibility = View.GONE
            edtParticipantSearch.visibility = View.VISIBLE
        } else {
            tvWhoListening.visibility = View.VISIBLE
            edtParticipantSearch.visibility = View.GONE
            hideKeyboard()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeObserver()
        showToolbar()
    }

    private val userList by lazy {
        UserAdapter().apply {

            clickListener = {
                it.let {
                    if (it.entity?.userId == group.ownerId){
                        showSnackBar("You cannot delete owner of the group")
                        return@let
                    }

                    AwesomeDialog.build(requireActivity())
                        .title(title = getString(R.string.delete),
                            titleColor = ContextCompat.getColor(requireContext(), R.color.color_dialog))
                        .body(body = getString(R.string.delete_user_message, it.entity?.name!!),
                            color = ContextCompat.getColor(requireContext(), R.color.color_dialog))
                        .onPositive(getString(R.string.yes)) {
                            group.apply {
                                userInfo?.remove(UserInfo(it.entity?.userId!!, UserInfo.NO_ACTION))
                            }

                            // Delete User from the group
                            vm.assignGroup(group.name, group.ownerId!!, group.userInfo)
                        }.onNegative(getString(R.string.no)){

                        }
                }
            }
        }
    }

    private fun observer(){
        vm.users.observe(viewLifecycleOwner, usersObserver)
        vm.user.observe(viewLifecycleOwner, userObserver)
        vm.userAssigned.observe(viewLifecycleOwner, userAssignedObserver)
        vm.error.observe(viewLifecycleOwner, errorObserver)
    }

    private fun removeObserver(){
        vm.users.removeObserver(usersObserver)
        vm.user.removeObserver(userObserver)
        vm.userAssigned.removeObserver(userAssignedObserver)
        vm.error.removeObserver(errorObserver)
    }

    private val errorObserver = Observer<APIError> { it ->
        it.let { error ->
            hideKeyboard()
            showSnackBar(error.errorMessage.toString())
        }
    }

    private val userObserver = Observer<User> { it ->
        it.let { user ->
           Log.d(TAG, user.toString())
        }
    }

    private val usersObserver = Observer<ArrayList<User>> { it ->
        Log.d(TAG, it.toString())
        if (it.isNotEmpty()) {
            unFilteredUser = it
            filteredUser = it.filter {
                it.userId != group.ownerId
            }

            //handle user search
            userSearchUi()

            updateParticipantList()
            manageGroupFCMToken()
        } else {
            Log.d(TAG, "No User is available")
        }

    }

    // Manage Group FCM Token for Sync Communication
    private fun manageGroupFCMToken() {
        sessionGraph.userPreferences.clearGroupToken()
        val userIds = group.userInfo?.map {
            it.userId
        }
        userIds?.forEach { userId ->
            val filter = filteredUser.filter { it.userId == userId }
            if (filter.isNotEmpty()){
                filter[0].pushId?.let {
                    sessionGraph.userPreferences.setGroupFCMToken(it)
                }
            }
        }
    }

    private fun updateParticipantList(){
        var users = arrayListOf<User>()
        val userIds = group.userInfo?.map {
            it.userId
        }
        userIds?.forEach { userId ->
            val filter = unFilteredUser.filter { it.userId == userId }
            if (filter.isNotEmpty()){
                users.add(filter[0])
            }
        }
        userList.populateList(users)
    }


    private val userAssignedObserver = Observer<Pair<Boolean, Group>> { it ->
        if(it != null)
        {
            if (it.first){
                Log.d(TAG, group.toString())
                userList.clearList()
                updateParticipantList()
            } else{
                showSnackBar("Error - User unable to assign")
            }
        }

    }


}