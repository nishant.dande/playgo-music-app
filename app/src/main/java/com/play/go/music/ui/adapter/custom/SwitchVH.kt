package com.play.go.music.ui.adapter.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.widget.AppCompatTextView
import com.play.go.music.R
import com.play.models.device.CustomItem
import kotlinx.android.synthetic.main.item_switch.view.*

class SwitchVH private constructor(itemView: View, context: Context) : BaseVH(itemView, context)  {

    var onClick: (((SwitchVH), SwitchState) -> Unit)? = null
    var context: Context = context

    var entity: CustomItem? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.findViewById<AppCompatTextView>(R.id.tvTitle).text = item.name
                itemView.swFeature.isChecked = item.checked
                itemView.swFeature.isClickable = item.isClickable
                itemView.swFeature.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked){
                        onClick?.invoke(this, SwitchState.onChecked)
                    } else {
                        onClick?.invoke(this, SwitchState.onReset)
                    }

                }
                itemView.setOnClickListener {
                    onClick?.invoke(this, SwitchState.onClicked)
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): SwitchVH {
            return SwitchVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_switch,
                    parent,
                    false
                ),parent.context
            )
        }
    }

    sealed class SwitchState {
        object onClicked : SwitchState()
        object onChecked : SwitchState()
        object onReset : SwitchState()
    }
}

