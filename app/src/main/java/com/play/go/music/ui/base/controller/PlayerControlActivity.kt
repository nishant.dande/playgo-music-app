package com.play.go.music.ui.base.controller

import android.content.Context
import android.media.AudioManager
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.gson.Gson
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.base.MusicServiceActivity
import com.play.go.music.ui.my_music.MyMusicFragment
import com.play.go.music.utils.Utility
import com.play.models.music.Song
import com.play.models.sync.MediaControlPayload
import com.play.models.sync.SyncPayload
import com.play.music_player.PlayerProducer
import com.play.music_player.base.NotifyPlayer
import com.play.music_player.base.Player
import com.play.music_player.local.LocalPlayer
import com.play.music_player.spotify.SpotifyPlayer
import com.play.music_player.youtube.YoutubePlayer
import com.play.networking.api.sync.SyncApi
import com.play.networking.api.sync.source.SyncSource
import kotlinx.android.synthetic.main.layout_mini_player.*


open class PlayerControlActivity : MusicServiceActivity(), MyMusicFragment.OnCallbackReceived,
    NotifyPlayer.PlayerState, SyncTimerTask.SyncListener, SyncSource.FileProgressListener {
    private val TAG = "PlayerControlActivity"

    private val networkGraph  by lazy { graph()!!.networkGraph }
    private val sessionGraph by lazy { graph()!!.sessionGraph }

    private var owner : Boolean = true

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { ControllerViewModel(session, networkGraph) }
            }
        }
    }

    private var player: Player? = null
    private var mSong: Song? = null

    // Maintain Youtube view
    private lateinit var mYouTubePlayerView: YouTubePlayerView
    var mPlayerContainer : ConstraintLayout? = null

    override fun getLocalMusicPlayer(exoPlayer: SimpleExoPlayer?) {
        exoPlayer.let {
            video_view.player = exoPlayer
            player.let {
                it?.play()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)
        setClickListener()
        mYouTubePlayerView = YouTubePlayerView(this)
        lifecycle.addObserver(mYouTubePlayerView!!)
        mYouTubePlayerView!!.enableBackgroundPlayback(true)

        // Check Connection
        vm.checkConnection()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (player != null){
            player?.stop()
            stopMusicService()
        }

        SyncTimerTask().stopTimer()
    }

    private fun setClickListener() {
        iv_seek_minus_10.setOnClickListener {
            operation(null, SyncApi.CMD_SEEK_REVERSE)
        }
        iv_play_pause.tag = R.drawable.ic_pause
        iv_play_pause.setOnClickListener {
            val isPlaying = Utility.handleMediaStateDrawable(this, iv_play_pause)
            if (isPlaying){
                operation(command = SyncApi.CMD_PAUSE)
            } else {
                operation(command = SyncApi.CMD_RESUME)
            }
        }
        iv_seek_plus_10.setOnClickListener {
            operation(command = SyncApi.CMD_SEEK_TO)
        }
        iv_volume.setOnClickListener {
            (getSystemService(Context.AUDIO_SERVICE) as AudioManager).adjustStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_SAME,
                    AudioManager.FLAG_SHOW_UI
                )
        }
    }

    override fun operation(song: Song?, command: String) {
        Log.d(TAG, command)
        when(command){
            SyncApi.CMD_PLAY -> {
                // Update Ui
                tvSongName.text = song?.title
                tvSongName.isSelected = true
                tvProvider.text = song?.provider
                playSong(song)
            }
            SyncApi.CMD_PAUSE -> {
                player.let {
                    it?.pause()
                }
            }
            SyncApi.CMD_RESUME -> {
                player.let {
                    it?.resume()
                }
            }
            SyncApi.CMD_SEEK_TO -> {
                player.let {
                    it?.seekTo(10000)
                }
            }
            SyncApi.CMD_SEEK_REVERSE -> {
                player.let {
                    it?.seekTo(-10000)
                }
            }
        }
    }


    override fun setYoutubeView(youTubePlayerView: ConstraintLayout) {
        showMiniControl(false)
        mPlayerContainer = youTubePlayerView
        mPlayerContainer?.removeView(mYouTubePlayerView)
        mPlayerContainer!!.addView(mYouTubePlayerView)
    }

    override fun onDestroyView() {
        showMiniControl(true)
        if (mSong?.provider == PlayerProducer.YOUTUBE_PLAYER) {
            mPlayerContainer?.removeView(mYouTubePlayerView)
            cl_youtube_player_view?.visibility = View.VISIBLE
            cl_youtube_player_view?.removeView(mYouTubePlayerView)
            cl_youtube_player_view!!.addView(mYouTubePlayerView)
        }
    }

    override fun onResumeView(container: ConstraintLayout) {
        if (mPlayerContainer != null) {
            if (mSong?.provider == PlayerProducer.YOUTUBE_PLAYER) {
                mPlayerContainer = container
                cl_youtube_player_view?.removeView(mYouTubePlayerView)
                mPlayerContainer?.removeView(mYouTubePlayerView)
                mPlayerContainer!!.addView(mYouTubePlayerView)
            }
        }
    }

    override fun getCurrentSong(): Song? {
        return mSong
    }

    /**
     * Public Function
     */
    fun showMiniControl(show: Boolean){
        runOnUiThread {
            if (show){
                clMediaControl.visibility = View.VISIBLE
                clMediaControl.bringToFront()
            } else {
                clMediaControl.visibility = View.GONE
            }
        }
    }

    fun disableMediaControl(){
        iv_seek_minus_10.visibility = View.GONE
        iv_play_pause.visibility = View.GONE
        iv_seek_plus_10.visibility = View.GONE
    }

    fun processRemoteSong(payload: String){
        val data = Gson().fromJson(payload, MediaControlPayload.Message.Data::class.java)
        owner = data.owner == Utility.getUserId(this)
        var song = Gson().fromJson(data.song, Song::class.java)
        operation(song, data.command)
    }

    fun processMediaSync(syncData: String){
        val data = Gson().fromJson(syncData, SyncPayload.Message.Data::class.java)
        Log.d(TAG, data.toString())
        if (player == null)
            return

        player.let {
            when(data.provider){
                    PlayerProducer.YOUTUBE_PLAYER ->{
                        (player as YoutubePlayer).syncTime(data.millisec)
                    }

                    PlayerProducer.SPOTIFY_PLAYER ->{
                    }
                    PlayerProducer.LOCAL_PLAYER ->{
                        (player as LocalPlayer).syncTime(data.millisec)
                    }
            }
        }
    }

    private fun playSong(song: Song?){
        var abstractPlayer = PlayerProducer.getPlayer(this)
        song.let {
            Utility.changePlayImageResource(this, iv_play_pause, true)
            mSong = it
            player = abstractPlayer.getPlayer(it?.provider!!)
            player.let {player ->
                if (it.provider == PlayerProducer.YOUTUBE_PLAYER){
                    mYouTubePlayerView?.isGone = false
                    (player as YoutubePlayer).setYoutubeView(mYouTubePlayerView!!)
                    (player as YoutubePlayer).setPlayerStateListener(this)
                    player.setDataSource(song!!.uri)
                    player.play()
                } else
                    mYouTubePlayerView?.isGone = true

                if (it.provider == PlayerProducer.LOCAL_PLAYER){
                    startMusicService()
                    (player as LocalPlayer).setPlayerStateListener(this)
                    player.setDataSource(it.uri)
                    video_view.isGone = true
                    pieProgress.isGone = true

//                    vm.uploadMediaFile(it.uri, this)
//                    vm.downloadMediaFile("https://firebasestorage.googleapis.com/v0/b/music-world-6fc4a.appspot.com/o/audio%2FScam%201992%20Ringtone(PaglaSongs).mp3?alt=media&token=e654b34f-d2c6-4cb6-b633-7725288b56b8",
//                    "song1", this)
                } else {
                    pieProgress.isGone = true
                    video_view.isGone = true
                    stopMusicService()
                }

                if (it.provider == PlayerProducer.SPOTIFY_PLAYER){
                    player?.setDataSource(it.uri)
                    (player as SpotifyPlayer).setPlayerStateListener(this)
                    player.play()
                }
            }
        }
    }

    override fun state(command: String, isPlaying: Boolean) {
        Log.d(TAG, "State : $command $isPlaying")
//        showMiniControl(true)
        if (owner) {
            mSong?.let {
                vm.syncMedia(this, command, it)
            }

            // Timer Task
            if (command == SyncApi.CMD_PLAY){
                SyncTimerTask().task(this, this)
            }
        } else {
            disableMediaControl()
        }
    }

    override fun timerTask() {
        mSong.let {
            if (it != null) {
                when(it.provider){
                    PlayerProducer.YOUTUBE_PLAYER ->{
                        vm.syncMediaSession(
                            (player as YoutubePlayer).getCurrentDuration(),
                            provider = it.provider
                        )
                    }

                    PlayerProducer.SPOTIFY_PLAYER ->{
                    }
                    PlayerProducer.LOCAL_PLAYER ->{
                        vm.syncMediaSession(
                            getCurrentDuration().toString(),
                            provider = it.provider
                        )
//                        var intentService = Intent(requireContext(), MediaPlayerService::class.java)
//                        intentService.putExtra("5", "SYNC")
//                        requireActivity().startService(intentService)
                    }
                }
            }
        }
    }

    override fun progress(byteUploaded: Long, totalBytes: Long) {
        var percentage = byteUploaded * 100 /totalBytes
        Log.d(TAG, percentage.toString())
        pieProgress.setProgress(percentage.toFloat())
    }

    override fun status(status: String) {
        Log.d(TAG, "Media Url $status")
        pieProgress.isGone = true
    }

    override fun error(e: Exception) {
        Log.e(TAG, e.message, e)
    }

    /**
     * Interfaces
     */
//    interface OnCallbackReceived {
//        fun operation(song: Song?, command: String);
//    }
}