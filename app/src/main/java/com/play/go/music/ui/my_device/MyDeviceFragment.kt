package com.play.go.music.ui.my_device

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothProfile
import android.content.Context
import android.content.Intent
import android.database.ContentObserver
import android.media.AudioManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.karumi.dexter.listener.DexterError
import com.play.go.music.R
import com.play.go.music.ui.adapter.custom.ArrowVH
import com.play.go.music.ui.adapter.custom.CustomAdapter
import com.play.go.music.ui.adapter.custom.TextVH
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.v1.ui.devices.DiscoveryDevicesActivity
import com.play.models.device.CustomItem
import kotlinx.android.synthetic.main.fragment_my_device.*

val TAG = "MyDeviceFragment"

class MyDeviceFragment : BaseFragment() {

    private lateinit var settingsContentObserver: SettingsContentObserver


    override fun getTab(): Int {
        return R.string.my_device
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_device, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val devicefeatures : ArrayList<CustomItem> = ArrayList<CustomItem>()
        devicefeatures.add(0, CustomItem(getString(R.string.equalizer), ShowArrow = true))
        devicefeatures.add(1, CustomItem(getString(R.string.anc), showSwitch = true))
        devicefeatures.add(2, CustomItem(getString(R.string.game_mode), showSwitch = true))
        devicefeatures.add(3, CustomItem(getString(R.string.auto_play_pause), showSwitch = true))
        devicefeatures.add(4, CustomItem(getString(R.string.app_notification), showSwitch = true))
        devicefeatures.add(5, CustomItem(getString(R.string.call_notification), showSwitch = true))
        devicefeatures.add(6, CustomItem(getString(R.string.voice_assistance), showSwitch = true))
        devicefeatures.add(7, CustomItem(getString(R.string.touch_controls), ShowArrow = true))
        devicefeatures.add(8, CustomItem(getString(R.string.settings), ShowArrow = true))
        devicefeatures.add(9, CustomItem(getString(R.string.paired_devices), ShowText = true))
        features.populateList(devicefeatures)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvDeviceFeatures!!.layoutManager = LinearLayoutManager(requireContext())
        rvDeviceFeatures!!.adapter = features

        connectedDevice()

        settingsContentObserver = SettingsContentObserver(requireContext(), Handler(), seekBar)
        requireActivity().contentResolver
            .registerContentObserver(android.provider.Settings.System.CONTENT_URI, true,
                settingsContentObserver);

        val audioManager = (requireActivity().getSystemService(Context.AUDIO_SERVICE) as AudioManager)
        seekBar.max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
        seekBar.progress = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })

        tvBatteryStatus.visibility = View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().contentResolver.unregisterContentObserver(settingsContentObserver);
    }

    private val features by lazy {
        CustomAdapter().apply {
            onPlaylistClicked = {
                it.let {
                    if (it is ArrowVH) {
                        if (it.entity != null) {
                            when (it.entity!!.name) {
                                getString(R.string.equalizer) -> {
                                    checkAudioPermission(object : PermissionListener{
                                        override fun permissionGranted(granted: Boolean) {
                                            findNavController().navigate(R.id.action_myDeviceFragment_to_equalizerFragment)
                                        }

                                        override fun failed(dexterError: DexterError) {

                                        }
                                    })
                                }
                                getString(R.string.touch_controls) -> {
                                    checkLocationPermission(object : PermissionListener{
                                        override fun permissionGranted(granted: Boolean) {
                                            findNavController().navigate(R.id.action_myDeviceFragment_to_touchControlFragment)
                                        }

                                        override fun failed(dexterError: DexterError) {

                                        }

                                    })

                                }
                                getString(R.string.settings) -> {
                                    findNavController().navigate(R.id.action_myDeviceFragment_to_settingsFragment)
                                }
                            }
                        }
                    }

                    if (it is TextVH){
                        if (it.entity != null) {
                            when (it.entity!!.name) {
                                getString(R.string.paired_devices) -> {
                                    checkLocationPermission(object : PermissionListener{
                                        override fun permissionGranted(granted: Boolean) {
//                                            startActivity(
//                                                    Intent(
//                                                            requireActivity(),
//                                                            DiscoveryDevicesActivity::class.java
//                                                    )
//                                            )
                                            findNavController().navigate(MyDeviceFragmentDirections.actionMyDeviceFragmentToDiscoveryDevicesFragment())
                                        }

                                        override fun failed(dexterError: DexterError) {

                                        }

                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun connectedDevice(){

        val states = intArrayOf(
            BluetoothProfile.STATE_CONNECTED
        )
        BluetoothAdapter.getDefaultAdapter().getProfileProxy(
            requireContext(),
            object : BluetoothProfile.ServiceListener {
                override fun onServiceConnected(profile: Int, proxy: BluetoothProfile?) {
                    Log.d(TAG, "Device Connected " + profile + " " + proxy.toString())

                    val devicesMatchingConnectionStates =
                        proxy?.getDevicesMatchingConnectionStates(states)

                    if (devicesMatchingConnectionStates?.isNotEmpty()!!) {
                        devicesMatchingConnectionStates.forEach {
                            Log.d(TAG, it.name + " " + it.address)
                            tvDeviceName?.text = it.name
                            tvBatteryStatus?.visibility = View.VISIBLE
                            tvBatteryStatus?.text = getString(
                                R.string.battery_status_60, getBatteryLevel(
                                    it
                                ).toString()
                            )
                            tvConnectedStatus?.text = "Connected"
                        }
                    } else {
                        Log.d(TAG, "No Device Connected")
                        tvDeviceName?.text = "In-Build Device"
                        tvConnectedStatus?.text = "-"
                        tvBatteryStatus?.visibility = View.GONE
                        tvBatteryStatus?.text = getString(R.string.battery_status_60, "0")
                    }

                }

                override fun onServiceDisconnected(profile: Int) {
                    Log.d(TAG, "No Device Connected")
                }

            },
            BluetoothProfile.HEADSET
        );
    }


    fun getBatteryLevel(pairedDevice: BluetoothDevice?): Int {
        return pairedDevice?.let { bluetoothDevice ->
            (bluetoothDevice.javaClass.getMethod("getBatteryLevel"))
                .invoke(pairedDevice) as Int
        } ?: -1
    }

    class SettingsContentObserver(context: Context, handler: Handler?, private val seekBar: SeekBar) :
        ContentObserver(handler) {
        private val audioManager: AudioManager =
            context.getSystemService(Context.AUDIO_SERVICE) as AudioManager

        override fun deliverSelfNotifications(): Boolean {
            return false
        }

        override fun onChange(selfChange: Boolean) {
            val currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
            seekBar.progress = currentVolume
        }

    }
}