package com.play.go.music.v1.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.audiofx.Equalizer
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.awesomedialog.*
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.drawer.MenuAdapter
import com.play.go.music.ui.group.GroupViewModel
import com.play.go.music.ui.settings.SettingViewModel
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.base.controller.ControllerViewModel
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.group_session.session.SessionFragment
import com.play.go.music.v1.ui.playing.NowPlayingFragment
import com.play.models.account.UserInfo
import com.play.models.sync.Group
import com.play.models.sync.MessagePayload
import com.play.music_player.PlayerProducer
import com.play.networking.api.spotify.SpotifyApi
import com.play.notification.service.NotificationService
import kotlinx.android.synthetic.main.activity_music_v1.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MusicActivity : PlayerControlActivity(), MenuAdapter.ClickListener {

    private val TAG = "v1MusicActivity"

    private val sessionGraph by lazy { graph()!!.sessionGraph }
    private val networkGraph by lazy { graph()!!.networkGraph }
    private val localLibrary by lazy { graph().sessionGraph.userPreferences }

    private val settingVm by lazy {
        getViewModel { SettingViewModel.create(this, sessionGraph) }
    }

    private val vmGroupModel by lazy {
        getViewModel { GroupViewModel.create(sessionGraph, networkGraph.accountSource) }
    }

    private val vmController by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { ControllerViewModel(session, networkGraph) }
            }
        }
    }

    private lateinit var menuAdapter: MenuAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localLibrary.setLocalLibrary(Utility.defaultLocalPath())
        navController.let {
            if (it != null) {
                bottom_nav_view.setupWithNavController(it)
                nav_view.setupWithNavController(it)
            }
        }
        // Register Device for FCM
        NotificationService.registerDevice(this)
        registerListeners()

        setUi()
        setListener()

        // Clear Group Session pre app loading
        sessionGraph.userPreferences.clearGroupSession()
    }

    override fun onDestroy() {
        super.onDestroy()
        unRegisterListener()

        //Clear Group Session If App Destroy
        sessionGraph.userPreferences.clearGroupSession()
    }

    companion object {
        fun newIntent(caller: Context): Intent {
            return Intent(caller, MusicActivity::class.java)
        }
    }


    // Handle in setting fragment
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
//        info { intent }
        Log.d(TAG, "In onActivityResult")
        // Check if result comes from the correct activity
        if (requestCode == SpotifyApi.LOGIN_REQUEST_CODE) {
            settingVm.onSpotifyResultFromActivity(requestCode, resultCode, intent)
        }
        super.onActivityResult(requestCode, resultCode, intent)


    }

    /**
     * Listeners
     */
    private fun registerListeners(){
        LocalBroadcastManager.getInstance(this).registerReceiver(mTokenReceiver,
            IntentFilter(NotificationService.ACTION_REFRESH_TOKEN))
        LocalBroadcastManager.getInstance(this).registerReceiver(mAudioSessionReceiver,
            IntentFilter(PlayerProducer.ACTION_AUDIO_SESSION))
        LocalBroadcastManager.getInstance(this).registerReceiver(mGroupActionReceiver,
            IntentFilter(NotificationService.ACTION_GROUP_EVENT))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaActionReceiver,
            IntentFilter(NotificationService.ACTION_MEDIA_EVENT))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaSyncSessionActionReceiver,
            IntentFilter(NotificationService.ACTION_SYNC_EVENT))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageActionReceiver,
            IntentFilter(NotificationService.ACTION_MESSAGE_EVENT))
    }

    private fun unRegisterListener(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAudioSessionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mTokenReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGroupActionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaActionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaSyncSessionActionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageActionReceiver)
    }

    /**
     * Receiver Callback
     */
    private val mAudioSessionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val sessionId = intent?.getIntExtra(PlayerProducer.AUDIO_SESSION_ID, 0)
            Log.d(TAG, "In AudioSessionReceiver - $sessionId")
            sessionId.let {
                if (it != null) {
                    // Store it to preferences
                    sessionGraph.userPreferences.setSessionId(it)
                }
            }
        }
    }

    private val mTokenReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val token = intent.getStringExtra(NotificationService.DATA_TOKEN)
            if (token != null) {
                Log.d(TAG, "In mTokenReceiver - $token")
                // Store it to preferences
                localLibrary.setFCMToken(token)
            } else {
                Log.d(TAG, "Failed to receive token")
            }
        }
    }

    private val mGroupActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val groupPayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            if (groupPayload != null) {
                Log.d(TAG, "In mGroupActionReceiver - $groupPayload")
                val data = Gson().fromJson(groupPayload, Group::class.java)
                val alertDialog = AwesomeDialog.build(this@MusicActivity)
                    .title(
                        getString(R.string.group_play_music),
                        titleColor = ContextCompat.getColor(
                            this@MusicActivity,
                            R.color.color_dialog
                        )
                    )
                    .body(
                        getString(R.string.request_to_join, data.name),
                        color = ContextCompat.getColor(this@MusicActivity, R.color.color_dialog)
                    )
                    .onPositive(getString(R.string.accept)) {
                        Log.d(TAG, data.toString())
                        vmGroupModel.updateUserSessionStatus(
                            data.name!!,
                            Utility.getUserId(this@MusicActivity), UserInfo.JOIN,
                            {
                                Log.d(TAG, it)
                                // Update Local Cache
                                sessionGraph.userPreferences.setSelectedGroupName(data.name!!)
                                val groupStateCache = SessionFragment.GroupStateCache(
                                    data.name!!,
                                    SessionFragment.GroupStateCache.GROUP_PLAY_START
                                )
                                // Update Shared Preferences.. Group Play Status
                                sessionGraph.userPreferences.setGroupPlayStatus(groupStateCache)

                                // Update Owner
                                vmGroupModel.getUserById(data.owner!!, {
                                    vmController.requestSendMessage(
                                        payloadData = MessagePayload.UPDATE_GROUP_SESSION,
                                        pushIds = arrayListOf(it.pushId!!))
                                }, {
                                    Toast.makeText(this@MusicActivity, it.localizedMessage, Toast.LENGTH_LONG).show()
                                })

                                // Update Ui
                                val fragment = getForegroundFragment()
                                if(fragment is NowPlayingFragment) {
                                    (fragment as NowPlayingFragment).updateUIAfterGroupSessionStart()
                                }
                            }, {
                                Toast.makeText(this@MusicActivity, it.localizedMessage, Toast.LENGTH_LONG).show()
                            })
                    }.onNegative(getString(R.string.decline)) {
                        // Make single event live data
                        vmGroupModel.updateUserSessionStatus(
                            data.name!!,
                            Utility.getUserId(this@MusicActivity), UserInfo.CANCELLED,
                            {
                                Log.d(TAG, it)
                                // Update Local Cache
                                val groupStateCache = SessionFragment.GroupStateCache(
                                    data.name!!,SessionFragment.GroupStateCache.GROUP_PLAY_STOP)
                                // Update Shared Preferences.. Group Play Status
                                sessionGraph.userPreferences.setGroupPlayStatus(groupStateCache)

                                // Update Owner
                                vmGroupModel.getUserById(data.owner!!, {
                                    vmController.requestSendMessage(
                                        payloadData = MessagePayload.UPDATE_GROUP_SESSION,
                                        pushIds = arrayListOf(it.pushId!!))
                                }, {
                                    Toast.makeText(this@MusicActivity, it.localizedMessage, Toast.LENGTH_LONG).show()
                                })
                            }, {
                                Toast.makeText(this@MusicActivity, it.localizedMessage, Toast.LENGTH_LONG).show()
                            })
                    }
                if (alertDialog.isShowing){
                    // Update Database Group Session
                    vmGroupModel.updateGroupSessionStatus(data.name!!, UserInfo.WAITING, {
                        Log.d(TAG, it)
                    }, {
                        Toast.makeText(this@MusicActivity, it.localizedMessage, Toast.LENGTH_LONG).show()
                    })
                }
//                Events.groupServiceEvent.postValue(data)
            } else {
                Log.d(TAG, "Failed to receive groupPayload")
            }
        }
    }

    private val mMediaActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val mediaPayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            if (mediaPayload != null) {
                Log.d(TAG, "In mMediaActionReceiver - $mediaPayload")
                runOnUiThread {
                    processRemoteSong(mediaPayload)
                }
            } else {
                Log.d(TAG, "Failed to receive groupPayload")
            }
        }
    }

    private val mMediaSyncSessionActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val syncPayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            if (syncPayload != null) {
                Log.d(TAG, "In mMediaSyncSessionActionReceiver - $syncPayload")
                runOnUiThread {
                    processMediaSync(syncPayload)
                }
            } else {
                Log.d(TAG, "Failed to receive groupPayload")
            }
        }
    }

    private val mMessageActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val messagePayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            val data = Gson().fromJson(messagePayload, MessagePayload.Message.Data::class.java)
            if (data != null) {
                Log.d(TAG, "In mMessageActionReceiver - $messagePayload")
                when(data.payload){
                    MessagePayload.UPDATE_GROUP_SESSION ->{
                        sessionGraph.userPreferences.setClearGroupFCMToken()
                        // Update group fcm push ids as per user status
                        sessionGraph.userPreferences.getSelectedGroupName().let{
                            vmGroupModel.getJoinedUserInGroup(it!!, {
                                sessionGraph.userPreferences.setGroupFCMToken(it.pushId!!)

                                // Update Remote Device about current Song
                                startCurrentSongAgain()
                            }, {
                                Toast.makeText(this@MusicActivity, it.localizedMessage, Toast.LENGTH_LONG).show()
                            })
                        }
                    }

                    MessagePayload.STOP_GROUP_SESSION ->{

                        sessionGraph.userPreferences.getSelectedGroupName().let{
                            // Update Local Cache
                            val groupStateCache = SessionFragment.GroupStateCache(
                                it?:SessionFragment.GroupStateCache.NO_GROUP,
                                SessionFragment.GroupStateCache.GROUP_PLAY_STOP)
                            // Update Shared Preferences.. Group Play Status
                            sessionGraph.userPreferences.setGroupPlayStatus(groupStateCache)

                            // Update Ui
                            val fragment = getForegroundFragment()
                            if(fragment is NowPlayingFragment) {
                                (fragment as NowPlayingFragment).updateUIAfterGroupSessionStop()
                            }
                        }
                    }
                }
            } else {
                Log.d(TAG, "Failed to receive message payload")
            }
        }
    }


    private fun setListener() {
        ivMenu.setOnClickListener {
            forceOpenDrawer()
        }
    }

    private fun setUi() {
        rvMenu.layoutManager = LinearLayoutManager(this)
//        val divider = DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL)
//        divider.setDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.bg_white_divider)!!)
//        rvMenu.addItemDecoration(divider)
        menuAdapter = MenuAdapter(this, arrayListOf(), itemClickListener = this)
        rvMenu.adapter = menuAdapter
        menuAdapter.inflateMenuList()
    }

    private fun forceOpenDrawer(){
        if (drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        drawer_layout.openDrawer(GravityCompat.START)
    }

    private fun forceCloseDrawer(){
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    object Events {
        val groupServiceEvent: MutableLiveData<Group> by lazy {
            MutableLiveData<Group>()
        }
    }

    override fun onListItemClickListener(item: Int) {
        forceCloseDrawer()
        when(item){
            R.string.home ->{
                navController.let {
                    it?.navigate(R.id.homeFragment)
                }
            }
            R.string.my_music ->{
                navController.let {
                    it?.navigate(R.id.myMusicFragment)
//                    it?.navigate(MyMusicFragmentDirections.,
//                            NavOptions.Builder().setLaunchSingleTop(true).build())
                }
            }
            R.string.settings ->{
                var fragment = getForegroundFragment().toString()
                if (fragment.contains("HomeFragment")){
                    navController?.navigate(R.id.action_homeFragment_to_myDeviceFragment)
                } else if(fragment.contains("NowPlayingFragment")){
                    navController?.navigate(R.id.action_nowPlayingFragment_to_myDeviceFragment)
                } else if(fragment.contains("MusicFragment")){
                } else if(fragment.contains("SeeAllFragment")){
                    navController?.navigate(R.id.action_seeAllFragment_to_myDeviceFragment)
                }
            }
            R.string.logout ->{
                sessionGraph.userPreferences.setUserInfo(
                    sessionGraph.userPreferences.getUserInfo().apply {
                        this?.loggedIn = false
                    }!!
                )
                finish()
                startActivity(Intent(this, com.play.go.music.MainActivity::class.java))
            }
        }
    }

    class AudioSessionReceiver : BroadcastReceiver(){
        val TAG = "AudioSessionReceiver"
        override fun onReceive(context: Context?, intent: Intent?) {
            var id  = intent?.getIntExtra(Equalizer.EXTRA_AUDIO_SESSION, -1);
            var name = intent?.getStringExtra(Equalizer.EXTRA_PACKAGE_NAME);
            Log.d(TAG, "Audio Session : "+id.toString()+" "+name)
            val intent1: Intent = Intent(PlayerProducer.ACTION_AUDIO_SESSION)
            intent1.putExtra(PlayerProducer.AUDIO_SESSION_ID, id)
            LocalBroadcastManager.getInstance(context!!).sendBroadcast(intent1)
        }
    }

}