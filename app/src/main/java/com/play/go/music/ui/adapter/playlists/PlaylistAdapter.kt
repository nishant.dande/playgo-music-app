package com.play.go.music.ui.adapter.playlists

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.music.Playlist
import com.play.models.music.Song


class PlaylistAdapter  : ListAdapter<Playlist, PlaylistVH>(PLAYLIST_DIFF_CALLBACK) {

    var onPlaylistAdapClick: ((PlaylistVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistVH {
                return PlaylistVH.create(parent, viewType)
                    .apply {
                        this.onClick = onPlaylistAdapClick
                    }
    }

    override fun onBindViewHolder(holder: PlaylistVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Playlist>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as Playlist )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<Playlist> )
            }
        }
        submitList(items)
    }
}


val PLAYLIST_DIFF_CALLBACK = object : DiffUtil.ItemCallback<Playlist>() {
    override fun areItemsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem.name == newItem.name
    }
}