package com.play.go.music.ui.news

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.adapter.news.NewsAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.ui.home.HomeViewModel
import com.play.go.music.ui.my_music.MyMusicFragment
import com.play.models.news.MusicNews
import kotlinx.android.synthetic.main.fragment_my_music.*
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : BaseFragment() {

    val TAG = "NewsFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }


    private val vm by lazy {
        networkGraph.let { networkGraph ->
            getViewModel { NewsViewModel(networkGraph) }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun getTab(): Int {
        return R.string.news
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvNews!!.layoutManager = LinearLayoutManager(requireContext())
        rvNews!!.adapter = musicList

        observer()
        vm.getMusicNews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeObserver()
    }

    private fun observer(){
        vm.musicNews.observe(viewLifecycleOwner, musicNewsObserver)
    }

    private fun removeObserver(){
        vm.musicNews.removeObserver(musicNewsObserver)
    }

    private val musicNewsObserver = Observer<MusicNews> { it ->
        it.let {news ->
            Log.d(TAG, news.toString())
            musicList.populateList(news.articles as ArrayList<MusicNews.Article>)
        }
    }

    private val musicList by lazy {
        NewsAdapter().apply {
            clickListener = {
                Log.d(TAG, it.entity.toString())
            }
        }
    }
}