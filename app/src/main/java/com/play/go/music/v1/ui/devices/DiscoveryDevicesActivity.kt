package com.play.go.music.v1.ui.devices

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothProfile
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.play.ble_service.BluetoothWrapper
import com.play.ble_service.model.CustomDevice
import com.play.ble_service.utils.Consts
import com.play.go.music.R
import com.play.go.music.ui.adapter.devices.DevicesAdapter
import com.play.go.music.v1.ui.test.PresetMusicControlActivity
import kotlinx.android.synthetic.main.activity_discovery_devices.*

class DiscoveryDevicesActivity : AppCompatActivity(), DevicesAdapter.ClickListener {
    private val TAG = "v1DiscoveryDevices"
    lateinit var bluetoothWrapper: BluetoothWrapper
    private lateinit var menuAdapter: DevicesAdapter
    private lateinit var dialog: AlertDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discovery_devices)

        rvDevices!!.layoutManager = LinearLayoutManager(this)
        menuAdapter = DevicesAdapter(this, arrayListOf(), itemClickListener = this)
        rvDevices.adapter = menuAdapter

        // Bluetooth Layer
        bluetoothWrapper = BluetoothWrapper(this)
        bluetoothWrapper.registerReceiver()
        bluetoothWrapper.registerBondReceiver()

        bluetoothWrapper.getScannedDevices(object : BluetoothWrapper.DeviceListener {
            override fun getDevices(customDevice: CustomDevice) {
                Log.d(TAG, customDevice.device.name + " " + customDevice.bonded)
                menuAdapter.populateList(customDevice)
            }

            override fun onConnectionStateChange(device: BluetoothDevice?, state: String) {
                if (state == BluetoothDevice.ACTION_ACL_CONNECTED) {
                    dialog.hide()
                }

            }
        })

        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        builder.setCancelable(false) // if you want user to wait for some process to finish,

        builder.setView(R.layout.item_dialog)
        dialog = builder.create()

        connectedDevice()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (bluetoothWrapper != null){
            bluetoothWrapper.unregisterReceiver()
            bluetoothWrapper.unregisterBondReceiver()
        }
    }

    override fun onListItemClickListener(item: CustomDevice) {

        bluetoothWrapper.scanDevices(false)

        // keep information
        val sharedPref = getSharedPreferences(Consts.PREFERENCES_FILE, MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putInt(Consts.TRANSPORT_KEY, item.transport)
        editor.putString(Consts.BLUETOOTH_ADDRESS_KEY, item.device.address)
        editor.apply()

        if (item.connected == 0) {
            item.device.createBond()
            // Pair Device automatically
            item.device.setPin("1234".toByteArray())
            dialog.show()
        } else {
                    startActivity(
            Intent(
                this@DiscoveryDevicesActivity,
                PresetMusicControlActivity::class.java
            )
        )
        finish()
        }
    }

    private fun connectedDevice(){

        val states = intArrayOf(
            BluetoothProfile.STATE_CONNECTED
        )
        BluetoothAdapter.getDefaultAdapter().getProfileProxy(
            this,
            object : BluetoothProfile.ServiceListener {
                override fun onServiceConnected(profile: Int, proxy: BluetoothProfile?) {
                    Log.d(TAG, "Device Connected " + profile + " " + proxy.toString())

                    val devicesMatchingConnectionStates =
                        proxy?.getDevicesMatchingConnectionStates(states)

                    if (devicesMatchingConnectionStates?.isNotEmpty()!!){
                        devicesMatchingConnectionStates.forEach {
                            Log.d(TAG, it.name+" "+it.address)
                            menuAdapter.populateList(
                                CustomDevice(0, 1, it, bluetoothWrapper.getBluetoothTransport(
                                    it.type
                                ))
                            )
                        }
                    } else {
                        Log.d(TAG, "No Device Connected")
                    }
                }

                override fun onServiceDisconnected(profile: Int) {
                    Log.d(TAG, "No Device Connected")
                }

            },
            BluetoothProfile.HEADSET
        );
    }
}