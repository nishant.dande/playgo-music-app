package com.play.go.music.ui.adapter.group.v1

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import com.play.go.music.preferences.UserPreferences
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.group_session.session.SessionFragment
import com.play.models.account.Group
import kotlinx.android.synthetic.main.item_group.view.*
import kotlinx.android.synthetic.main.item_music_list.view.tvTitle

class GroupVH private constructor(itemView: View, var context: Context,
                                    val userPreferences: UserPreferences) : RecyclerView.ViewHolder(itemView)   {

    var onGClick: ((GroupVH) -> Unit)? = null
    var onGLongClick: ((GroupVH) -> Unit)? = null
    var onGStartClick: ((GroupVH) -> Unit)? = null
    var onGStopClick: ((GroupVH) -> Unit)? = null

    var entity: Group? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = item.name
                itemView.cbAssignGroup.visibility = View.GONE
                var state = userPreferences.getGroupPlayStatus()
                if (state.name == item.name){
                    if (state.status == SessionFragment.GroupStateCache.GROUP_PLAY_STOP){
                        itemView.btnStart.isEnabled = true
                        itemView.btnStop.isEnabled = false
                    } else if(state.status == SessionFragment.GroupStateCache.GROUP_PLAY_START){
                        itemView.btnStart.isEnabled = false
                        itemView.btnStop.isEnabled = true
                    }
                } else {
                    itemView.btnStart.isEnabled = true
                    itemView.btnStop.isEnabled = false
                }

                itemView.btnStart.setOnClickListener {
                    onGStartClick?.invoke(this)
                }

                itemView.btnStop.setOnClickListener {
                    onGStopClick?.invoke(this)
                }

                itemView.setOnClickListener {
                    onGClick?.invoke(this)
                }

                itemView.setOnLongClickListener {
                    onGLongClick?.invoke(this)
                    return@setOnLongClickListener true
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int, userPreferences: UserPreferences): GroupVH {
                return GroupVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_group,
                        parent,
                        false
                    ),parent.context, userPreferences
                )
        }
    }
}

