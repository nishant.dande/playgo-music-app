package com.play.go.music.ui.group

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.group.GroupAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.ui.my_music.MyMusicViewModel
import com.play.go.music.utils.Utility
import com.play.models.account.UserInfo
import kotlinx.android.synthetic.main.fragment_group.*

class GroupFragment : BaseFragment() {

    val networkGraph get() = context?.graph()?.networkGraph
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    val vm by lazy {
        networkGraph?.let { networkGraph ->
            getViewModel { GroupViewModel( sessionGraph,
                networkGraph.accountSource
            ) }
        }
    }


    override fun getTab(): Int {
        return R.string.group
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_group, container, false)
    }

    override fun onStart() {
        super.onStart()
        vm?.getGroups(null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvGroups!!.layoutManager = LinearLayoutManager(requireContext())
        rvGroups!!.adapter = features

        add_fab.setOnClickListener {
            val alert = AlertDialog.Builder(requireContext())
            val edittext = EditText(requireContext())
            alert.setMessage(getString(R.string.enter_group_name))
            alert.setView(edittext)
            alert.setPositiveButton(getString(R.string.yes)) { dialog, whichButton ->
                val groupName: String = edittext.text.toString()
                if (!TextUtils.isEmpty(groupName)){
                    vm?.createGroup(groupName, Utility.getUserId(requireContext()))
                }
            }
            alert.setNegativeButton(getString(R.string.no)) {
                    dialog, whichButton ->
                // what ever you want to do with No option.
            }
            alert.show()
        }

        fab_account.setOnClickListener {
            val alert = AlertDialog.Builder(requireContext())
            val edittext = EditText(requireContext())
            alert.setMessage(getString(R.string.enter_name))
            alert.setView(edittext)
            alert.setPositiveButton(getString(R.string.yes)) { dialog, whichButton ->
                val name: String = edittext.text.toString()
                if (!TextUtils.isEmpty(name)){
                    vm?.createAccount(name, Utility.getUserId(requireContext()))
                }
            }
            alert.setNegativeButton(getString(R.string.no)) {
                    dialog, whichButton ->
                // what ever you want to do with No option.
            }
            alert.show()
        }

        vm?.groups?.observe(viewLifecycleOwner, Observer {
            rvGroups.visibility = View.VISIBLE
            tvNoGroup.visibility = View.GONE
            features.populateList(it)
        })
    }

    private val features by lazy {
        GroupAdapter().apply {
            onClick = {
            }

            onChecked = { it , isChecked->
                var userId = Utility.getUserId(requireContext())
                if (isChecked){
                    it.entity.let {group ->
                        if (group?.userInfo == null)
                            group?.userInfo = arrayListOf()

                        if (!group?.userInfo!!.toString().contains(userId)){
                            group?.userInfo.apply {
                                this?.add(UserInfo(userId, UserInfo.NO_ACTION))
                            }
                        }
                    }
                } else {
                    it.entity.let{group ->
                        if (group?.userInfo != null){
                            if (group.userInfo!!.toString().contains(userId)){
                                group?.userInfo.apply {
                                    this?.remove(UserInfo(userId, UserInfo.NO_ACTION))
                                }
                            }
                        }
                    }
                }

                it.entity.let{
                    vm?.assignGroup(it?.name!!, it.ownerId!!, it.userInfo)
                }
            }
        }
    }
}