package com.play.go.music.ui.adapter.my_music

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import com.play.models.music.Orientation
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.networking.utils.Utility
import kotlinx.android.synthetic.main.item_music_list.view.*

class PlaylistVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onClick: ((PlaylistVH) -> Unit)? = null
    var onSeeAllClick: ((String, PlaylistVH) -> Unit)? = null
    var redirectSongListener: ((Int, Song, PlaylistVH) -> Unit)? = null
    var redirectSongLongListener: ((SongVH) -> Unit)? = null
    var context: Context = context

    var entity: Playlist? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = Utility.titleCase(item.name)
                // handle trending icon
                if (item.name.toLowerCase().contains("trending")){
                    itemView.ivTrending.isGone = false
                    itemView.tvSeeALl.isGone = true
                } else {
                    itemView.ivTrending.isGone = true
                    itemView.tvSeeALl.isGone = false
                }
                if (item.orientation == Orientation.VERTICAL){
                    itemView.rvMusic.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                } else {
                    itemView.rvMusic.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                }
                itemView.rvMusic.adapter = SongAdapter(item.orientation).apply {
                    this.populateList(item.songs as ArrayList<Song>)
                    songAdapterListener = { index, songVH ->
                        redirectSongListener?.invoke(index, songVH.entity?.second!!, this@PlaylistVH)
                    }

                    songAdapterLongListener = {
                        redirectSongLongListener?.invoke(it)
                    }
                }

                itemView.tvTitle.setOnClickListener {
                    onClick?.invoke(this)
                }

                itemView.tvSeeALl.setOnClickListener {
                    onSeeAllClick?.invoke(item.name, this)
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): PlaylistVH {
                return PlaylistVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_music_list,
                        parent,
                        false
                    ),parent.context
                )
        }
    }
}

