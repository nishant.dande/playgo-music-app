package com.play.go.music.ui.adapter.my_music

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.music.Playlist
import com.play.models.music.Song


class MusicListAdapter  : ListAdapter<Playlist, PlaylistVH>(MUSIC_DIFF_CALLBACK) {

    var onPlaylistClick: ((PlaylistVH) -> (Unit))? = null
    var onSeeAllPlaylistClick: ((String, PlaylistVH) -> (Unit))? = null
    var onSongClick: ((Int, Song, PlaylistVH) -> (Unit))? = null
    var onSongLongClick: ((SongVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistVH {
                return PlaylistVH.create(parent, viewType)
                    .apply {
                        this.onClick = onPlaylistClick
                        this.onSeeAllClick = onSeeAllPlaylistClick
                        this.redirectSongListener = onSongClick
                        this.redirectSongLongListener = onSongLongClick
                    }
    }

    override fun onBindViewHolder(holder: PlaylistVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Playlist>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as Playlist )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<Playlist> )
            }
        }
        submitList(items)
    }

    fun populateList(settings: Playlist){
        var items = arrayListOf<Playlist>()

        if (currentList.size > 0){
            if (currentList.size == 1){
                var playlist = currentList.toList()[0] as Playlist
                if (playlist.id != settings.id) {
                    items.add(0, playlist)
                }
                items.add(0, settings)
            } else{
                var playlist = currentList.toList() as ArrayList<Playlist>
                val filterIndexed = playlist.filterIndexed { index, playlist ->
                    playlist.id == settings.id
                }
                if (filterIndexed.isNotEmpty()){
                    filterIndexed[0].apply {
                        songs = settings.songs.reversed()
                    }
                } else {
                    items.add(0, settings)
                }
                items.addAll(0,playlist )
            }
        } else {
            items.add(0, settings)
        }
        var sortedList = items.sortedWith(compareBy({ it.id }))
        submitList(sortedList)
        notifyDataSetChanged()
    }

}


val MUSIC_DIFF_CALLBACK = object : DiffUtil.ItemCallback<Playlist>() {
    override fun areItemsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Playlist, newItem: Playlist): Boolean {
        return oldItem.name == newItem.name
    }
}