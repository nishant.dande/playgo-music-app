package com.play.go.music.v1.ui.base.controller

import android.app.PictureInPictureParams
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.media.AudioManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Rational
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.gson.Gson
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.base.MusicServiceActivity
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity.BluetoothConnectionStateReceiver.Companion.BLE_STATE
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.go.music.v1.ui.playing.NowPlayingFragment
import com.play.models.music.Song
import com.play.models.playlist.ManagePlaylist
import com.play.models.sync.MediaControlPayload
import com.play.models.sync.SyncPayload
import com.play.music_player.PlayerProducer
import com.play.music_player.base.NotifyPlayer
import com.play.music_player.base.Player
import com.play.music_player.local.LocalPlayer
import com.play.music_player.spotify.SpotifyPlayer
import com.play.music_player.youtube.YoutubePlayer
import com.play.networking.api.sync.SyncApi
import com.play.networking.api.sync.source.SyncSource
import com.play.notification.service.NotificationService
import kotlinx.android.synthetic.main.activity_music_v1.*
import kotlinx.android.synthetic.main.layout_mini_player.*
import kotlinx.android.synthetic.main.layout_mini_player.clMediaControl
import kotlinx.android.synthetic.main.layout_mini_player.iv_play_pause
import kotlinx.android.synthetic.main.layout_mini_player.iv_seek_minus_10
import kotlinx.android.synthetic.main.layout_mini_player.iv_seek_plus_10
import kotlinx.android.synthetic.main.layout_mini_player.video_view
import kotlinx.android.synthetic.main.layout_toolbar.*


open class PlayerControlActivity : MusicServiceActivity(), HomeFragment.OnCallbackReceived,
    NotifyPlayer.PlayerState, SyncTimerTask.SyncListener, SyncSource.FileProgressListener {
    private val TAG = "v1PlayerControlActivity"

    private val networkGraph  by lazy { graph()!!.networkGraph }
    private val sessionGraph by lazy { graph()!!.sessionGraph }

    private var owner : Boolean = true
    private var isPlaying : Boolean = false
    protected var navController: NavController? = null

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { ControllerViewModel(session, networkGraph) }
            }
        }
    }

    private var player: Player? = null
    private var mSong: Song? = null
    private var mPlaylist: ManagePlaylist? = null
    private var timerTask = SyncTimerTask()

    // Maintain Youtube view
    private lateinit var mYouTubePlayerView: YouTubePlayerView
    var mPlayerContainer : ConstraintLayout? = null

    override fun getLocalMusicPlayer(exoPlayer: SimpleExoPlayer?) {
        exoPlayer.let {
            video_view.player = exoPlayer
            player.let {
                it?.play()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_v1)
        setClickListener()
        val navFragmentContainer = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navFragmentContainer.findNavController()
        mYouTubePlayerView = YouTubePlayerView(this)
        lifecycle.addObserver(mYouTubePlayerView!!)
        mYouTubePlayerView!!.enableBackgroundPlayback(true)
        setYoutubeView(cl_youtube_player_view, false)

        // Check Connection
        vm.checkConnection()
        registerListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (player != null){
            player?.stop()
            stopMusicService()
        }

        SyncTimerTask().stopTimer()
        unRegisterListener()
    }

    private fun setClickListener() {
        iv_seek_minus_10.setOnClickListener {
            operation(null, SyncApi.CMD_SEEK_REVERSE)
        }

        iv_seek_minus_10.setOnLongClickListener {
            Log.d(TAG, mPlaylist?.getCurrentIndex().toString())
            mSong = mPlaylist?.getPreviousTrack()
            operation(mSong, command = SyncApi.CMD_PLAY)
            return@setOnLongClickListener true
        }
        iv_play_pause.tag = R.drawable.ic_pause
        iv_play_pause.setOnClickListener {
            val isPlaying = Utility.handleMediaStateDrawable(this, iv_play_pause)
            if (isPlaying){
                operation(command = SyncApi.CMD_PAUSE)
            } else {
                operation(command = SyncApi.CMD_RESUME)
            }
        }
        iv_seek_plus_10.setOnClickListener {
            operation(command = SyncApi.CMD_SEEK_TO)
        }
        iv_seek_plus_10.setOnLongClickListener {
            Log.d(TAG, mPlaylist?.getCurrentIndex().toString())
            mSong = mPlaylist?.getNextTrack()
            operation(mSong, command = SyncApi.CMD_PLAY)
            return@setOnLongClickListener true
        }
        iv_volume.setOnClickListener {
            (getSystemService(Context.AUDIO_SERVICE) as AudioManager).adjustStreamVolume(
                    AudioManager.STREAM_MUSIC,
                    AudioManager.ADJUST_SAME,
                    AudioManager.FLAG_SHOW_UI
                )
        }
        ivSettings.setOnClickListener {
            var fragment = getForegroundFragment().toString()
            if (fragment.contains("HomeFragment")){
                navController?.navigate(R.id.action_homeFragment_to_myDeviceFragment)
            } else if(fragment.contains("NowPlayingFragment")){
                navController?.navigate(R.id.action_nowPlayingFragment_to_myDeviceFragment)
            } else if(fragment.contains("MusicFragment")){
            } else if(fragment.contains("SeeAllFragment")){
                navController?.navigate(R.id.action_seeAllFragment_to_myDeviceFragment)
            }
        }
        ivSearch.setOnClickListener {
            var fragment = getForegroundFragment().toString()
            if (fragment.contains("HomeFragment")){
                navController?.navigate(R.id.action_homeFragment_to_searchFragment)
            } else if(fragment.contains("NowPlayingFragment")){
                navController?.navigate(R.id.action_nowPlayingFragment_to_searchFragment)
            }  else if(fragment.contains("SeeAllFragment")){
                navController?.navigate(R.id.action_seeAllFragment_to_searchFragment)
            } else if(fragment.contains("MusicFragment")){
            }
        }
        clClickableArea.setOnClickListener {
            var fragment = getForegroundFragment().toString()
            if (fragment.contains("HomeFragment")){
                navController?.navigate(R.id.action_homeFragment_to_nowPlayingFragment)
            } else if(fragment.contains("SeeAllFragment")){
                navController?.navigate(R.id.action_seeAllFragment_to_nowPlayingFragment)
            } else if(fragment.contains("MyMusicFragment")){
                navController?.navigate(R.id.action_myMusicFragment_to_nowPlayingFragment)
            }
        }
    }

    open fun getForegroundFragment(): Fragment? {
        val navHostFragment: Fragment? =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
        return navHostFragment?.childFragmentManager?.fragments?.get(0)
    }

    override fun operation(song: Song?, command: String) {
        Log.d(TAG, command)
        when(command){
            SyncApi.CMD_PLAY -> {
                // Update Ui
                tvSongName.text = song?.title
                tvSongName.isSelected = true
                tvProvider.text = song?.provider
                playSong(song)
            }
            SyncApi.CMD_PAUSE -> {
                player.let {
                    it?.pause()
                }
            }
            SyncApi.CMD_RESUME -> {
                player.let {
                    it?.resume()
                }
            }
            SyncApi.CMD_SEEK_TO -> {
                player.let {
                    it?.seekTo(10000)
                }
            }
            SyncApi.CMD_SEEK_REVERSE -> {
                player.let {
                    it?.seekTo(-10000)
                }
            }
        }
    }


    override fun setYoutubeView(youTubePlayerView: ConstraintLayout) {
        showMiniControl(false)
        cl_youtube_player_view?.removeView(mYouTubePlayerView)
        cl_youtube_player_view?.removeAllViews()
        mPlayerContainer?.removeView(cl_youtube_player_view)

        mPlayerContainer = youTubePlayerView
        mPlayerContainer?.removeView(mYouTubePlayerView)
        mPlayerContainer!!.addView(mYouTubePlayerView)
    }

    fun setYoutubeView(youTubePlayerView: ConstraintLayout, status: Boolean) {
        showMiniControl(status)
        mPlayerContainer = youTubePlayerView
        mPlayerContainer?.visibility = View.VISIBLE
        mPlayerContainer?.removeView(mYouTubePlayerView)
        mPlayerContainer?.removeAllViews()
        mPlayerContainer!!.addView(mYouTubePlayerView)
    }

    override fun onDestroyView() {
        if (mSong != null && player != null) {
            showMiniControl(true)
            if (mSong?.provider == PlayerProducer.YOUTUBE_PLAYER) {
                mPlayerContainer?.removeView(mYouTubePlayerView)
                cl_youtube_player_view?.visibility = View.VISIBLE
                cl_youtube_player_view?.removeView(mYouTubePlayerView)
                cl_youtube_player_view!!.addView(mYouTubePlayerView)
            }
        }
    }

    override fun onResumeView(container: ConstraintLayout) {
        if (mPlayerContainer != null) {
            if (mSong?.provider == PlayerProducer.YOUTUBE_PLAYER) {
                cl_youtube_player_view?.removeView(mYouTubePlayerView)
                cl_youtube_player_view?.removeAllViews()
                mPlayerContainer?.removeView(cl_youtube_player_view)
//                mPlayerContainer = container
//                mPlayerContainer?.removeView(mYouTubePlayerView)
//                mPlayerContainer!!.addView(mYouTubePlayerView)
                setYoutubeView(container, false)

            }
        }
    }

    override fun getCurrentSong(): Song? {
        return mSong
    }

    override fun getCurrentPlaylist(): ManagePlaylist? {
        return mPlaylist
    }

    override fun setCurrentPlaylist(managePlaylist: ManagePlaylist) {
        mPlaylist = managePlaylist
    }

    /**
     * Public Function
     */
    fun showMiniControl(show: Boolean){
        runOnUiThread {
            if (show){
                clMediaControl.visibility = View.VISIBLE
                clMediaControl.bringToFront()
            } else {
                clMediaControl.visibility = View.GONE
            }
        }
    }

    fun disableMediaControl(){
        iv_seek_minus_10.visibility = View.GONE
        iv_play_pause.visibility = View.GONE
        iv_seek_plus_10.visibility = View.GONE
    }

    fun processRemoteSong(payload: String){
        val data = Gson().fromJson(payload, MediaControlPayload.Message.Data::class.java)
        owner = data.owner == Utility.getUserId(this)
        var song = Gson().fromJson(data.song, Song::class.java)
        song.apply {
            artist = ""
        }
        operation(song, data.command)

        // Update Remote Device Ui except player screen
        if (data.command == SyncApi.CMD_PLAY &&
            getForegroundFragment() !is NowPlayingFragment){
            showMiniControl(true)
        }
    }

    fun processMediaSync(syncData: String){
        val data = Gson().fromJson(syncData, SyncPayload.Message.Data::class.java)
        Log.d(TAG, data.toString())
        if (player == null)
            return

        player.let {
            when(data.provider){
                    PlayerProducer.YOUTUBE_PLAYER ->{
                        (player as YoutubePlayer).syncTime(data.millisec)
                    }

                    PlayerProducer.SPOTIFY_PLAYER ->{
                    }
                    PlayerProducer.LOCAL_PLAYER ->{
                        (player as LocalPlayer).syncTime(data.millisec)
                    }
            }
        }
    }

    private fun playSong(song: Song?){
        var abstractPlayer = PlayerProducer.getPlayer(this)
        song.let {
            Utility.changePlayImageResource(this, iv_play_pause, true)
            mSong = it
            player = abstractPlayer.getPlayer(it?.provider!!)
            player.let {player ->
                if (it.provider == PlayerProducer.YOUTUBE_PLAYER){
                    mYouTubePlayerView?.isGone = false
                    mPlayerContainer?.isGone = false
                    (player as YoutubePlayer).setYoutubeView(mYouTubePlayerView!!)
                    (player as YoutubePlayer).setPlayerStateListener(this)
                    player.setDataSource(song!!.uri)
                    player.play()
                } else {
                    mYouTubePlayerView?.isGone = true
                    mPlayerContainer?.isGone = true
                    ivSongPic?.isGone = true
                }

                if (it.provider == PlayerProducer.LOCAL_PLAYER){
                    startMusicService()
                    (player as LocalPlayer).setPlayerStateListener(this)
                    player.setDataSource(it.uri)
                    video_view.isGone = true
                    pieProgress.isGone = true

//                    vm.uploadMediaFile(it.uri, this)
//                    vm.downloadMediaFile("https://firebasestorage.googleapis.com/v0/b/music-world-6fc4a.appspot.com/o/audio%2FScam%201992%20Ringtone(PaglaSongs).mp3?alt=media&token=e654b34f-d2c6-4cb6-b633-7725288b56b8",
//                    "song1", this)
                } else {
                    pieProgress.isGone = true
                    video_view.isGone = true
                    ivSongPic.isGone = true
                    stopMusicService()
                }

                if (it.provider == PlayerProducer.SPOTIFY_PLAYER){
                    ivSongPic.isGone = false
                    Glide.with(this)
                        .load(it.image)
                        .error(R.drawable.ic_placeholder)
                        .into(ivSongPic);
                    player?.setDataSource(it.uri)
                    (player as SpotifyPlayer).setPlayerStateListener(this)
                    player.play()
                }
            }
            vm.addRecentlyPlayedSong(it)
        }
    }

    override fun state(command: String, isPlaying: Boolean) {
        Log.d(TAG, "State : $command $isPlaying")
        this.isPlaying = isPlaying
//        showMiniControl(true)
        if (owner) {
            mSong?.let {
                vm.syncMedia(this, command, it)
            }

            // Timer Task
            if (command == SyncApi.CMD_PLAY){
                // Do not start time automatically
            }
        } else {
            disableMediaControl()
        }
    }

    /**
     * Start the timer task
     */
    fun startTimer(){
        timerTask.task(this, this)
    }

    /**
     * Stop the timer task
     */
    fun stopTimer(){
        timerTask.stopTimer()
    }

    /**
     * Start Current Song Again
     */
    fun startCurrentSongAgain(){
        if (mSong != null){
            operation(mSong, SyncApi.CMD_PLAY)
        }
    }

    override fun timerTask() {
        mSong.let {
            if (it != null) {
                when(it.provider){
                    PlayerProducer.YOUTUBE_PLAYER ->{
                        vm.syncMediaSession(
                            (player as YoutubePlayer).getCurrentDuration(),
                            provider = it.provider
                        )
                    }

                    PlayerProducer.SPOTIFY_PLAYER ->{
                    }
                    PlayerProducer.LOCAL_PLAYER ->{
                        vm.syncMediaSession(
                            getCurrentDuration().toString(),
                            provider = it.provider
                        )
//                        var intentService = Intent(requireContext(), MediaPlayerService::class.java)
//                        intentService.putExtra("5", "SYNC")
//                        requireActivity().startService(intentService)
                    }
                }
            }
        }
    }

    override fun progress(byteUploaded: Long, totalBytes: Long) {
        var percentage = byteUploaded * 100 /totalBytes
        Log.d(TAG, percentage.toString())
        pieProgress.setProgress(percentage.toFloat())
    }

    override fun status(status: String) {
        Log.d(TAG, "Media Url $status")
        pieProgress.isGone = true
    }

    override fun error(e: Exception) {
        Log.e(TAG, e.message, e)
    }

    class BluetoothConnectionStateReceiver() : BroadcastReceiver() {
        val TAG = "BluetoothStateReceiver"

        companion object {
            const val BLE_STATE = "ble_state"
            const val STATE = "state"
            const val PAUSE_MUSIC = 0
            const val RESUME_MUSIC = 1
        }

        override fun onReceive(context: Context, intent: Intent) {
            val intentAction = intent.action ?: return
            Log.d(TAG, "BluetoothConnectionStateReceiver In ${intentAction}")
            when(intentAction){
                "android.bluetooth.device.action.ACL_DISCONNECTED" ->{
                    val intent = Intent(BLE_STATE)
                    intent.putExtra(STATE, PAUSE_MUSIC)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
                "android.bluetooth.device.action.ACL_CONNECTED" ->{
                    val intent = Intent(BLE_STATE)
                    intent.putExtra(STATE, RESUME_MUSIC)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
            }
        }
    }

    /**
     * Listeners
     */
    private fun registerListeners(){
        LocalBroadcastManager.getInstance(this).registerReceiver(mStateReceiver,
            IntentFilter(BLE_STATE)
        )
    }

    private fun unRegisterListener(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mStateReceiver)
    }

    private val mStateReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val state = intent?.getIntExtra(BluetoothConnectionStateReceiver.STATE, 0)
            Log.d(TAG, "In mStateReceiver - $state")
            state.let {
                if (it != null) {
                   if (it == BluetoothConnectionStateReceiver.PAUSE_MUSIC){
                       // pause music
                       operation(getCurrentSong(), SyncApi.CMD_PAUSE)
                   } else if(it== BluetoothConnectionStateReceiver.RESUME_MUSIC){
                       // auto play music
                        operation(getCurrentSong(), SyncApi.CMD_RESUME)
                   }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (isPlaying) {
            var aspectRatio = Rational(500, 300)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                enterPictureInPictureMode(
                    PictureInPictureParams.Builder().setAspectRatio(aspectRatio).build()
                )
            }
        }
    }

    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean,
        newConfig: Configuration?
    ) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        if (isInPictureInPictureMode) {
            // Hide a few UI components
            toolbar.isGone = true
            bottom_nav_view.isGone = true
            if (getForegroundFragment() is NowPlayingFragment){
                clMediaControl.isGone = true
//                clGroupControl.isGone = true
//                tvLyricsText.isGone = true
//                tvLyrics.isGone = true
//                clNextTrack.isGone = true
//                view.isGone = true
//                rvRecentMusics.isGone = true
            } else {
                tvSongName.isGone = true
                tvProvider.isGone = true
                iv_seek_minus_10.isGone = true
                iv_play_pause.isGone = true
                iv_seek_plus_10.isGone = true
                progressControl.isGone = true
                nav_host_fragment.isGone = true
                nav_view.isGone = true
                cl_youtube_player_view.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                clClickableArea.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
                cl_youtube_player_view.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
                cl_youtube_player_view.invalidate()
            }
        } else {
            toolbar.isGone = false
            bottom_nav_view.isGone = false
            // Show them back
            if (getForegroundFragment() is NowPlayingFragment){
                clMediaControl.isGone = false
//                clGroupControl.isGone = false
//                tvLyricsText.isGone = false
//                tvLyrics.isGone = false
//                clNextTrack.isGone = false
//                view.isGone = false
//                rvRecentMusics.isGone = false
            } else {
                tvSongName.isGone = false
                tvProvider.isGone = false
                iv_seek_minus_10.isGone = false
                iv_play_pause.isGone = false
                iv_seek_plus_10.isGone = false
                progressControl.isGone = false
                nav_host_fragment.isGone = false
                nav_view.isGone = false
                cl_youtube_player_view.layoutParams.width = 100
                cl_youtube_player_view.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            }
        }
    }

    /**
     * Interfaces
     */
//    interface OnCallbackReceived {
//        fun operation(song: Song?, command: String);
//    }
}