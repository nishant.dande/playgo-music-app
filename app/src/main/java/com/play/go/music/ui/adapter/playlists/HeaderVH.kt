package com.play.go.music.ui.adapter.playlists

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import com.play.models.music.Orientation
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.networking.utils.Utility
import kotlinx.android.synthetic.main.item_music_list.view.*

class HeaderVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onRedirectClick: ((PlaylistVH) -> Unit)? = null
    var context: Context = context

    var entity: Pair<String, List<Playlist>>? = null
        set(value) {
            field = value
            value?.let { item ->

                itemView.tvTitle.text = Utility.titleCase(item.first)
                    itemView.rvMusic.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                itemView.rvMusic.adapter = PlaylistAdapter().apply {
                    this.populateList(item.second as ArrayList<Playlist>)
                    onPlaylistAdapClick = {
                        onRedirectClick?.invoke(it)
                    }
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): HeaderVH {
                return HeaderVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_music_list,
                        parent,
                        false
                    ),parent.context
                )
        }
    }
}

