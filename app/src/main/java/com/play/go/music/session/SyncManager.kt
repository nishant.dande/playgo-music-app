package com.play.go.music.session

import android.content.Context
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.play.go.music.preferences.UserPreferences
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.group_session.session.SessionFragment
import com.play.models.music.Song
import com.play.models.sync.*
import com.play.networking.Outcome
import com.play.networking.api.sync.SyncApi
import com.play.networking.api.sync.SyncApi.SYNC_PAYLOAD
import com.play.networking.api.sync.source.SyncSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info

open class SyncManager(
    private val userPreferences: UserPreferences,
    private val syncSource: SyncSource)
    : CoroutineScope by CoroutineScope(Dispatchers.IO), ViewModel(), AnkoLogger {

    fun checkConnection(){
        viewModelScope.launch{
            syncSource.checkConnection().let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                        info { it.data?.get("msg") }
                    }
                    is Outcome.Failure -> {
                        error { it.e.errorMessage }
                    }
                    else -> {
                    }
                }
            }
        }
    }

    fun requestUserToJoin(context: Context, userIds: ArrayList<String>){
        viewModelScope.launch {
            var group: Group = Group(Utility.getUserId(context),
                userPreferences.getSelectedGroupName(), userIds)
            val registrationIds: ArrayList<String> = userPreferences.getGroupFCMToken()
            if (registrationIds.size > 0) {
                var payload = GroupPayload(
                    message = GroupPayload.Message(
                        GroupPayload.Message.Data(
                            payload_type = SyncApi.GROUP_PAYLOAD,
                            owner = group.owner,
                            name = group.name,
                            userId = Gson().toJson(group.userId))),
                    registrationToken = registrationIds)
                syncSource.requestUserToJoin(payload).let {
                    when (it) {
                        is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                            info { it.data }

                        }
                        is Outcome.Failure -> {
                            error { it.e.errorMessage }
                        }
                        else -> {
                        }
                    }
                }
            } else {
                error { "No user in the group ${group.name}" }
            }
        }
    }

    fun syncMedia(context: Context, command: String, song: Song){
        if (userPreferences.getGroupPlayStatus().status ==
            SessionFragment.GroupStateCache.GROUP_PLAY_STOP)
            return

        viewModelScope.launch {
            val message = MediaControlPayload.Message.Data(
                payload_type = SyncApi.MEDIA_PAYLOAD,
                owner = Utility.getUserId(context),
                command = command,
                song = "\'"+Gson().toJson(song)+"\'")

            val registrationIds: ArrayList<String> = userPreferences.getGroupFCMToken()
            if (registrationIds.size > 0) {
                var payload = MediaControlPayload(
                    message = MediaControlPayload.Message(message),
                    registrationToken = registrationIds)

                syncSource.syncMedia(payload).let {
                    when (it) {
                        is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                            info { it.data }

                        }
                        is Outcome.Failure -> {
                            error { it.e.errorMessage }
                        }
                        else -> {
                        }
                    }
                }
            } else {
                error { "Please Select Group" }
            }
        }
    }

    fun syncMediaSession(millisec: String, provider: String?){
        if (userPreferences.getGroupPlayStatus().status ==
            SessionFragment.GroupStateCache.GROUP_PLAY_STOP)
            return

        if (!TextUtils.isEmpty(millisec)) {
            viewModelScope.launch {
                var registration_ids = arrayListOf<String>()
                registration_ids = userPreferences.getGroupFCMToken()
                if (registration_ids.size > 0) {
                    var payload = SyncPayload(
                        message = SyncPayload.Message(
                            SyncPayload.Message.Data(
                                SYNC_PAYLOAD, millisec, provider
                            )
                        ),
                        registrationToken = registration_ids
                    )
                    syncSource.syncMediaSession(payload).let {
                        when (it) {
                            is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                                info { it.data }

                            }
                            is Outcome.Failure -> {
                                error { it.e.errorMessage }
                            }
                            else -> {
                            }
                        }
                    }
                }
            }
        }
    }

    fun requestSendMessage(payloadData: String){
        requestSendMessage(payloadData, userPreferences.getGroupFCMToken())
    }

    fun requestSendMessage(payloadData: String, pushIds: ArrayList<String>){
        viewModelScope.launch {
            if (pushIds.size > 0) {
                val payload = MessagePayload(
                    message = MessagePayload.Message(
                        data = MessagePayload.Message.Data(
                            payload_type = SyncApi.MESSAGE_PAYLOAD,
                            payload = payloadData
                        )
                    ),
                    registrationToken = pushIds )
                syncSource.sendMessage(payload).let {
                    when (it) {
                        is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
                            info { it.data }
                        }
                        is Outcome.Failure -> {
                            error { it.e.errorMessage }
                        }
                        else -> {
                        }
                    }
                }
            } else {
                error { "Please select user to send data" }
            }
        }
    }

    fun uploadMediaFile(path: String, fileProgressListener: SyncSource.FileProgressListener){
        syncSource.upload(path, fileProgressListener)
    }

    fun downloadMediaFile(path: String, name: String,
                        fileProgressListener: SyncSource.FileProgressListener){
        syncSource.download(path, name, fileProgressListener)
    }
}