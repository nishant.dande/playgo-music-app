package com.play.go.music.ui.adapter.group.v1

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.go.music.preferences.UserPreferences
import com.play.models.account.Group


class GroupAdapter(val userPreferences: UserPreferences) : ListAdapter<Group, GroupVH>(DIFF_CALLBACK) {

    var onClick: ((GroupVH) -> (Unit))? = null
    var onLongClick: ((GroupVH) -> (Unit))? = null
    var onStartClick: ((GroupVH) -> Unit)? = null
    var onStopClick: ((GroupVH) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupVH {
        return GroupVH.create(parent, viewType, userPreferences)
            .apply {
                this.onGClick = onClick
                this.onGLongClick = onLongClick
                this.onGStartClick = onStartClick
                this.onGStopClick = onStopClick
            }
    }

    override fun onBindViewHolder(holder: GroupVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Group>){
        var items = settings
        submitList(items)
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Group>() {
    override fun areItemsTheSame(oldItem: Group, newItem: Group): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Group, newItem: Group): Boolean {
        return oldItem.name == newItem.name
    }
}