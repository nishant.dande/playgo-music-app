package com.play.go.music.v1.ui.playing

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.example.awesomedialog.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.MusicActivity
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.group_session.session.SessionFragment
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.music.Artist
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.models.playlist.ManagePlaylist
import com.play.music_player.PlayerProducer
import com.play.networking.api.sync.SyncApi
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.fragment_now_playing.*

class NowPlayingFragment : BaseFragment() {

    val TAG = "HomeFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { NowPlayingViewModel(session, networkGraph) }
            }
        }
    }

    private var mSong: Song? = null
    private var mSelectedPlaylist: ManagePlaylist? = null
    private var mManagePlaylist : Boolean = false

    override fun getTab(): Int {
        return R.string.home
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Add Immersive Ui Layout
//        vm.initializeRecentlyAddedSongs()
//        vm.getSpotifyNewRelease(NowPlayingViewModel.LIMIT, NowPlayingViewModel.OFFSET)
//        vm.getYoutubeRecommendation()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_now_playing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()

        //Set list view for music list
//        rvRecentMusics!!.layoutManager = LinearLayoutManager(requireContext())
//        rvRecentMusics!!.adapter = musicList


        setClickListener()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            onCallbackReceived = context as HomeFragment.OnCallbackReceived
            (context as PlayerControlActivity).showMiniControl(false)
        }

    }

    override fun onResume() {
        super.onResume()
        onCallbackReceived.let {
            if (it.getCurrentSong() != null){
                mSong = it.getCurrentSong()!!
                showMusicInfo(mSong!!)
                Log.d(TAG, mSong?.title+" "+mSong?.artist)

                if (mSong!!.provider == PlayerProducer.YOUTUBE_PLAYER)
                    it.onResumeView(youtube_player_view)
                else if (mSong!!.provider == PlayerProducer.SPOTIFY_PLAYER){
                    if (mSong?.artist != null && !TextUtils.isEmpty(mSong?.artist)) {
                        var artist: ArrayList<Artist> = Gson().fromJson(mSong?.artist,
                            object : TypeToken<List<Artist?>?>() {}.type
                        )
                                as ArrayList<Artist>
                        if (artist.isNotEmpty()) {
                            artist.forEach { artist ->
                                artist.id?.let { it1 -> vm?.getArtistAlbum(it1) }
                            }
                        }
                    }
                }
            }

            updateNextTrack()
        }

        // handle mini player
        if (requireActivity() != null && requireActivity() is PlayerControlActivity){
            (requireActivity() as PlayerControlActivity).showMiniControl(false)
        }

        // Make a call to fetch lyrics
//        tvLyricsText.performClick()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        onCallbackReceived.let {
            it.onDestroyView()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        removeObserver()
    }

    private fun setClickListener() {

//        ivPlaylist.setOnClickListener {
//            val alert = AlertDialog.Builder(requireContext())
//            val edittext = EditText(requireContext())
//            alert.setMessage(getString(R.string.enter_playlist_name))
//            alert.setView(edittext)
//            alert.setPositiveButton(getString(R.string.yes)) { dialog, whichButton ->
//                val name: String = edittext.text.toString()
//                if (!TextUtils.isEmpty(name)){
//                    vm.createPlaylist(name)
//                }
//            }
//            alert.setNegativeButton(getString(R.string.no)) {
//                    dialog, whichButton ->
//                // what ever you want to do with No option.
//            }
//            alert.show()
//        }
//
//        ivPlaylistList.setOnClickListener {
//            mManagePlaylist = true
//            vm.getUserPlaylist()
//        }
//
//        iv_group.setOnClickListener {
////            findNavController().navigate(R.id.action_homeFragment_to_groupFragment)
//        }
//
//        tvGroup.setOnClickListener {
////            vm.getGroups()
//        }

        swGroupPlay.setOnClickListener {
            findNavController().navigate(NowPlayingFragmentDirections.actionNowPlayingFragmentToSessionFragment())
        }

        iv_seek_minus_10.setOnClickListener {
            mSelectedPlaylist.let {
                mSong = it?.getPreviousTrack()
                onCallbackReceived.operation(mSong, SyncApi.CMD_PLAY)
                showMusicInfo(mSong!!)
                updateNextTrack()

                // Reset Lyrics
                hideLyricsContainer()
//                tvLyricsText.performClick()
            }
        }
        iv_seek_minus_10.setOnLongClickListener {
            mSong.let {
                onCallbackReceived.operation(command = SyncApi.CMD_SEEK_REVERSE)
                return@setOnLongClickListener true
            }
        }
        iv_seek_plus_10.setOnClickListener {
            mSelectedPlaylist.let {
                mSong = it?.getNextTrack()
                onCallbackReceived.operation(mSong, SyncApi.CMD_PLAY)
                showMusicInfo(mSong!!)
                updateNextTrack()

                // Reset Lyrics
                hideLyricsContainer()
//                tvLyricsText.performClick()
            }
        }
        iv_seek_plus_10.setOnLongClickListener {
            mSong.let {
                onCallbackReceived.operation(command = SyncApi.CMD_SEEK_TO)
                return@setOnLongClickListener true
            }
        }
        iv_play_pause.tag = R.drawable.ic_pause
        iv_play_pause.setOnClickListener {
            val isPlaying =
                Utility.handleMediaStateDrawable(requireContext(), iv_play_pause)

            if (isPlaying){
                mSong.let {
                    onCallbackReceived.operation(command = SyncApi.CMD_PAUSE)
                }
            } else {
                mSong.let {
                    onCallbackReceived.operation(command = SyncApi.CMD_RESUME)
                }
            }
        }

        ivLyricsArrow.setOnClickListener {
            if (sessionGraph.userPreferences.getGroupPlayStatus().status ==
                SessionFragment.GroupStateCache.GROUP_PLAY_STOP) {
                // Make a call to fetch lyrics
                vm.getLyrics(mSong?.provider!!, mSong?.title!!, mSong?.artist)

            } else {
                updateUIAfterGroupSessionStart()
            }
        }

        tvLyrics.setOnClickListener {
//            if (tvLyrics.layoutParams.height ==
//                ViewGroup.LayoutParams.WRAP_CONTENT){
//                tvLyrics.layoutParams.height = 263
//            } else {
//                tvLyrics.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
//            }
//            tvLyrics.invalidate()
//            tvLyrics.requestLayout()

        }

        tvLyrics.setOnClickListener {
            tvLyricsText.performClick()
        }

        tvLyricsText.setOnClickListener {
            if (nsLyricsScroll.layoutParams.height == 0){
                ivLyricsArrow.performClick()
                nsLyricsScroll.isGone = false
                nsLyricsScroll.layoutParams.height = 500
            } else {
                nsLyricsScroll.isGone = true
//                nsLyricsScroll.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                nsLyricsScroll.layoutParams.height = 0
            }
            nsLyricsScroll.invalidate()
            nsLyricsScroll.requestLayout()

        }

        iv_favourite.setOnClickListener {
            mSong.let {
                if (it?.id != null && it.id > 0) {
                    sessionGraph.userPreferences.setLikedSongId(it.id)
                } else {
                    vm.getSongDetails(it?.uri!!)
                }
            }
        }


        iv_favourite.setOnLongClickListener {
            mManagePlaylist = false
            vm.getUserPlaylist()
            return@setOnLongClickListener true
        }

//        clNextTrack.setOnClickListener {
//            iv_seek_plus_10.performClick()
//
//            hideLyricsContainer()
//        }

        iv_volume.setOnClickListener {
            (requireActivity().getSystemService(Context.AUDIO_SERVICE) as AudioManager).adjustStreamVolume(
                AudioManager.STREAM_MUSIC,
                AudioManager.ADJUST_SAME,
                AudioManager.FLAG_SHOW_UI
            )
        }

        iv_more.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type="text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT,mSong?.title+" - "+ mSong?.uri);
            startActivity(Intent.createChooser(shareIntent, mSong?.title))
        }
    }

    fun updateUIAfterGroupSessionStop(){
//        swGroupPlay.isChecked = false
        tvLyrics.isGone = false
    }

    fun updateUIAfterGroupSessionStart(){
//        swGroupPlay.isChecked = true
        tvLyrics.isGone = true
    }

    private val musicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    Log.d(TAG, "playlisy : "+it.toString())
                }
            }

            onSongClick = { index, song, playlistVH ->
                song.let {
//                    Log.d(TAG,"Song : "+ it.toString())
                    it.let { song ->
                        setTrack(song)
                        vm.addRecentlyPlayedSong(song)
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : ${it.entity}")

                }
            }
        }
    }

    private fun setTrack(song: Song){
        mSong = song
        showMusicInfo(song)

        // Set View for Youtube and Local Music
        when (song.provider) {
            PlayerProducer.YOUTUBE_PLAYER -> {
                onCallbackReceived.let {
                    onCallbackReceived.setYoutubeView(youtube_player_view)
                }
            }
            PlayerProducer.LOCAL_PLAYER -> { }
            PlayerProducer.SPOTIFY_PLAYER -> { }
        }

        // Sync Play
        mSong.let {
            onCallbackReceived.let {
                onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
            }
        }

        // Reset Lyrics
        hideLyricsContainer()
//        tvLyricsText.performClick()
    }

    private fun observer(){
        vm.groups.observe(viewLifecycleOwner, groupObserver)
        vm.newReleaseSongs.observe(viewLifecycleOwner, newReleaseSongObserver)
        vm.recentlyAdded.observe(viewLifecycleOwner, recentAddedObserver)
        vm.getAllPlaylist.observe(viewLifecycleOwner, playlistObserver)
        vm.getPlaylist.observe(viewLifecycleOwner, getPlaylist)
        vm.users.observe(viewLifecycleOwner, userObserver)
        vm.lyrics.observe(viewLifecycleOwner, lyricsObserver)
        vm.youtubeSongs.observe(viewLifecycleOwner, youtubeObserver)
        vm.artistAlbum.observe(viewLifecycleOwner, artistsAlbumObserver)
        vm.getSong.observe(viewLifecycleOwner, songObserver)
        vm.getSongForPlaylist.observe(viewLifecycleOwner, songForPlaylistObserver)

        // Check Sync Server Connection
        vm.checkConnection()


        // Listen Group Notification
        MusicActivity.Events.groupServiceEvent.observe(viewLifecycleOwner,
            Observer<com.play.models.sync.Group> { profile ->
                if (profile != null) {
                    Log.d(TAG, profile.toString())
                    AwesomeDialog.build(requireActivity())
                        .title(getString(R.string.group_play_music))
                        .body(getString(R.string.request_to_join, profile.name))
                        .onPositive(getString(R.string.accept)) {
//                            tvGroup!!.text = profile.name
                            // Make single event live data
                            MusicActivity.Events.groupServiceEvent.postValue(null)
                        }.onNegative(getString(R.string.decline)){
                            // Make single event live data
                            MusicActivity.Events.groupServiceEvent.postValue(null)
                        }
                }
            })
    }

    private fun removeObserver(){
        vm.groups.removeObserver(groupObserver)
        vm.newReleaseSongs.removeObserver(newReleaseSongObserver)
        vm.recentlyAdded.removeObserver(recentAddedObserver)
        vm.getAllPlaylist.removeObserver(playlistObserver)
        vm.getPlaylist.removeObserver(getPlaylist)
        vm.users.removeObserver(userObserver)
        vm.lyrics.removeObserver(lyricsObserver)
        vm.youtubeSongs.removeObserver(youtubeObserver)
        vm.artistAlbum.removeObserver(artistsAlbumObserver)
        vm.getSong.removeObserver(songObserver)
        vm.getSongForPlaylist.removeObserver(songForPlaylistObserver)
    }

    private val groupObserver = Observer<ArrayList<Group>> { it ->
        it.add(0, Group("None", Utility.getUserId(requireContext()), arrayListOf()))
        val map = it.map {
            it.name
        }

        var myDialog: AlertDialog
        val myBuilder = AlertDialog.Builder(requireContext())
        myBuilder.setTitle(getString(R.string.select_group))
            .setItems(map.toTypedArray()) { dialogInterface, position ->
//                Toast.makeText(requireContext(), it[position].toString(), Toast.LENGTH_SHORT).show()

                var groupName = it[position].name
                sessionGraph.userPreferences.setSelectedGroupName(groupName)
                if (groupName != "None") {
                    val userIds = it[position].userInfo?.map {
                        it.userId
                    }
                    userIds?.let { _userIds ->
                        if (_userIds.isNotEmpty()) {
                            vm.getUsersByGroup(Utility.getUserId(requireContext()), _userIds as ArrayList<String>)
                        } else {
                            showSnackBar("User not assign to group")
                        }
                    }
                }
//
//                if (tvGroup != null) {
//                    tvGroup!!.text = it[position].name
//                }
            }
        myDialog = myBuilder.create()
        myDialog.show()
    }

    private val newReleaseSongObserver = Observer<ArrayList<Song>> { it ->
        musicList.populateList(Playlist(-1, "Similar Songs", it))
    }

    private val recentAddedObserver = Observer<List<Song>> { it ->
        it.let {
            if (it.isNotEmpty()) {
                musicList.populateList(Playlist(2, "Recently Played", it))
            }
        }
    }

    private val getPlaylist = Observer<Playlist> { it ->
        it.let {
            Log.d(TAG, it.toString())
//            mSelectedPlaylist = it as ManagePlaylist

            musicList.populateList(it)
        }
    }

    private val playlistObserver = Observer<List<Playlist>> { it ->
        Log.d(TAG, it.toString())

        it.let { playlists ->

            val map = playlists.map { it.name }
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Choose an playlist")
            builder.setItems(map.toTypedArray()) { dialog, which ->
                if (mManagePlaylist) {
                    // Play Playlist
                    vm?.getUserPlaylistById(playlists.get(which).id)
                }
                else {
                    if (mSong != null) {
                        if (mSong!!.id > 0) {
                            // Add Song
                            vm?.managePlaylist(playlists.get(which).id, mSong!!.id)
                        } else {
                            vm?.getSongDetailsForPlaylist(playlists.get(which).id, mSong!!.uri)
                        }
                    } else {
                        Log.e(TAG, "Please select song to add to playlist")
                    }
                }
            }
            val dialog = builder.create()
            dialog.show()
        }
    }

    private val userObserver = Observer<ArrayList<User>> { it ->
        Log.d(TAG, it.toString())
        // Manage Group FCM Token for Sync Communication
        sessionGraph.userPreferences.clearGroupToken()
        var userIds = arrayListOf<String>()
        it.forEach {
            userIds.add(it.userId!!)
            if (it.userId != Utility.getUserId(requireContext())) {
                it.pushId?.let { pushId ->
                    sessionGraph.userPreferences.setGroupFCMToken(pushId)
                }
            }
        }

        if (userIds.isNotEmpty())
            vm.requestUserToJoin(requireContext(), userIds)
        else
            Log.e(TAG, "Users Id is empty")
    }

    private val lyricsObserver = Observer<String> { it ->
        if (it != null) {
            Log.d(TAG, it)
            it.let { lyrics ->
                lyrics.apply {
                    if (!TextUtils.isEmpty(this) && !this.equals("null")) {
                        updateUIAfterGroupSessionStop()
                        tvLyricsText.visibility = View.VISIBLE
                        tvLyrics.visibility = View.VISIBLE
                        nsLyricsScroll.visibility = View.VISIBLE
                        tvLyrics.text = this.replace("\\n", System.getProperty("line.separator"))
                    } else {
                        hideLyricsContainer()
                        showSnackBar("Lyrics Not Found")
                    }

                }
            }
        } else {
            hideLyricsContainer()
            showSnackBar("Lyrics Not Found")
        }
    }

    private val youtubeObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(-3, "Youtube Recommendation", songs))
        }
    }

    private val artistsAlbumObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(1, "More From the Artist", songs))
        }
    }

    private val songObserver = Observer<Song> { it ->
        it.let { song ->
            sessionGraph.userPreferences.setLikedSongId(song.id)
        }
    }

    private val songForPlaylistObserver = Observer<Pair<Int, Song>> { it ->
        it.let { item ->
            // Add Song
            vm?.managePlaylist(item.first, item.second.id)
        }
    }

    private fun showMusicInfo(song: Song){
//        setTitle(song.title)
        tvTitle.text = song.title
        tvArtistName.text = getArtistName(song.artist)


        when (song.provider) {
            PlayerProducer.YOUTUBE_PLAYER -> {
                youtube_player_view!!.visibility = View.VISIBLE
                ivTrackImage.visibility = View.GONE
                video_view.visibility = View.GONE
            }
            PlayerProducer.LOCAL_PLAYER -> {
                youtube_player_view.visibility = View.GONE
                ivTrackImage.visibility = View.GONE
                video_view.visibility = View.VISIBLE
            }
            PlayerProducer.SPOTIFY_PLAYER -> {
                youtube_player_view.visibility = View.GONE
                ivTrackImage.visibility = View.VISIBLE
                video_view.visibility = View.GONE
                Glide.with(requireActivity())
                    .load(song.image)
                    .error(R.drawable.ic_placeholder)
                    .into(ivTrackImage);
            }
        }

        Glide.with(this)
            .load(song.image)
            .apply(bitmapTransform(BlurTransformation(25, 3)))
            .into(ivBgSongImage)
    }

    private fun updateNextTrack(){
        onCallbackReceived.let {
            if (it.getCurrentPlaylist() != null){
                mSelectedPlaylist = (it.getCurrentPlaylist() as ManagePlaylist)
                var nextTrackIndex = mSelectedPlaylist?.getCurrentIndex()!!+1
                if (nextTrackIndex >= mSelectedPlaylist?.getCurrentPlaylist()?.songs!!.size)
                    return
                var song = mSelectedPlaylist?.getCurrentPlaylist()?.songs!![nextTrackIndex]
//                tvTitle.text = song?.title
//                if (!TextUtils.isEmpty(song?.image)) {
//                    Glide.with(requireActivity())
//                        .load(song?.image)
//                        .into(ivTitleImage)
//                }
                try{
                    var artist: ArrayList<Artist> = Gson().fromJson(song?.artist,
                        object : TypeToken<List<Artist?>?>() {}.type)
                            as ArrayList<Artist>
                    var artistName = ""
                    if (artist.isNotEmpty()){
                        artist.forEach {artist ->
                            artistName += artist.name+ " "
                        }
                    }
//                    tvArtistName.text = artistName
                } catch (e: Exception){

                }
            }
        }
    }

    private fun getArtistName(json: String) : String{
        var artistName = ""
        if (json != null && !TextUtils.isEmpty(json)){
            var artist: ArrayList<Artist> = Gson().fromJson(json,
            object : TypeToken<List<Artist?>?>() {}.type)
                    as ArrayList<Artist>
            if (artist.isNotEmpty()){
                artist.forEach {artist ->
                    artistName += artist.name+ " "
                }
            }
        }
        return artistName
    }

    private fun hideLyricsContainer(){
        // Hide Lyrics
        tvLyrics.text = ""
        tvLyrics.visibility = View.GONE
        nsLyricsScroll.visibility = View.GONE
        tvLyricsText.visibility = View.VISIBLE
    }
}