package com.play.go.music.ui.settings

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.SessionGraph
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationResponse
import kotlinx.coroutines.launch

class SettingViewModel private constructor(
    context: Context,
    private val sessionGraph: SessionGraph
) : ViewModel() {

    private val _authState = MutableLiveData<State>()
    val authState: LiveData<State> get() = _authState

    private var _localFolderPath: List<String> = listOf()
        set(value) {
            field = value
            localFolderPath.value = value
        }
    val localFolderPath = MutableLiveData<List<String>>()

    val settingConfig = MutableLiveData<List<String>>()

//    private var authManager = AuthenticationFactory.createAuthenticationManager(context)

//    val startActivityForResultEvent = LiveMessageEvent<ActivityNavigation>()

    init {
        refreshAuth()
    }

    fun refreshAuth() {
//        _authState.value = State.Unauthenticated
    }

    fun authenticate() {
        // TODO: Support webview callbacks for non-installed app
//        startActivityForResultEvent.sendEvent {
//            val intent = authManager.createIntentBuilder(sessionManager.devToken)
//                .setHideStartScreen(false)
//                .setStartScreenMessage("Connect with Apple Music!")
//                .build()
//            this.startActivityForResult(intent, MUSIC_KIT_AUTH)
//        }
    }

    fun onSpotifyResultFromActivity(requestCode: Int, resultCode: Int, data: Intent?) {
        val response = AuthenticationClient.getResponse(resultCode, data)
        when (response.type) {
            AuthenticationResponse.Type.TOKEN -> {
                sessionGraph.sessionManager.createUser(response.accessToken)
                _authState.value = State.Authenticated(response.accessToken)
            }
            AuthenticationResponse.Type.ERROR -> {                    // Handle error response
                Log.e("MainActivity", "Error! Yay!" + response.error)
                _authState.value = State.InvalidAuthentication(Exception(response.error))
            }
            else -> {
            }
        }
    }

    fun onFolderResultFromActivity(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null && data.extras != null) {
            sessionGraph.userPreferences.setLocalLibrary(data!!.extras!!.getString("data")!!)
        }
    }

    fun loadLocalLibraryFolderPath(){
        viewModelScope.launch {
            sessionGraph.userPreferences.getLocalLibrary().let{
                _localFolderPath = it
            }
        }
    }

    fun setSettingConfig(name: String, status: Boolean) {
        sessionGraph.userPreferences.setSettingConfig(name, status)
    }

    fun getSettingConfig(){
        viewModelScope.launch {
            sessionGraph.userPreferences.getSettingConfig().let{
                settingConfig.value = it
            }
        }
    }

    fun onSpotifyLogout(){
        sessionGraph.sessionManager.removeUser()
        _authState.value = State.Unauthenticated
    }

    companion object {
        const val MUSIC_KIT_AUTH = 1337

        fun create(context: Context, sessionGraph: SessionGraph): SettingViewModel {
            return SettingViewModel(context, sessionGraph)
        }
    }

    sealed class State {
        data class Authenticated(val auth: String) : State()
        object Unauthenticated : State()
        data class InvalidAuthentication(val cause: Throwable?) : State()
    }
}
