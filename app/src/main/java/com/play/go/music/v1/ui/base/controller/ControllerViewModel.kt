package com.play.go.music.v1.ui.base.controller

import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.NetworkGraph
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.session.SyncManager
import com.play.models.music.Song
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.info

class ControllerViewModel(
    val sessionGraph: SessionGraph,
    val networkGraph: NetworkGraph
) : SyncManager(sessionGraph.userPreferences, networkGraph.syncSource){


    fun addRecentlyPlayedSong(song: Song){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.addRecentSong(song)
                val cached = sessionGraph.library.getRecentAdded()
                info { "initial songs cache=${cached.count()}" }
            }
        }
    }

}
