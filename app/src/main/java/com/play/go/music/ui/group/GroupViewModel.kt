package com.play.go.music.ui.group

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.play.go.music.graphs.SessionGraph
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.account.UserInfo
import com.play.networking.Outcome
import com.play.networking.api.account.source.AccountSource
import com.play.networking.error.APIError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info

open class GroupViewModel(
    val mSessionGraph: SessionGraph,
    val accountSource: AccountSource
) : CoroutineScope by CoroutineScope(Dispatchers.IO), ViewModel(), AnkoLogger {

    companion object {
        fun create(sessionGraph: SessionGraph, accountSource: AccountSource): GroupViewModel {
            return GroupViewModel(sessionGraph, accountSource)
        }
    }

    var error = MutableLiveData<APIError>()
    var groupCreated = MutableLiveData<Pair<String, String>>()
    var userCreated = MutableLiveData<Boolean>()
    var userAssigned = MutableLiveData<Pair<Boolean, Group>>()
    var groups = MutableLiveData<ArrayList<Group>>()
    var users = MutableLiveData<ArrayList<User>>()
    var user = MutableLiveData<User>()
    var groupDeleted = MutableLiveData<Boolean>()


    fun getGroups(ownerId: String?){
        viewModelScope.launch {
            if (ownerId == null) {
                accountSource.getGroups().let {
                    when (it) {
                        is Outcome.Success -> {
                            info { it.data }
                            groups.value = it.data
                        }
                        is Outcome.Failure -> {
                            error { it.e }
                            error.value = APIError(it.e, it.e.errorMessage)
                        }
                        else -> {

                        }
                    }
                }
            } else {
                accountSource.getGroupsByOwner(ownerId).let {
                    when (it) {
                        is Outcome.Success -> {
                            info { it.data }
                            groups.value = it.data
                        }
                        is Outcome.Failure -> {
                            error { it.e }
                            error.value = APIError(it.e, it.e.errorMessage)
                        }
                        else -> {

                        }
                    }
                }
            }
        }
    }

    fun createGroup(groupName : String, ownerId: String){
        viewModelScope.launch {
            accountSource.createGroup(Group(groupName, ownerId, null)).let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        it.data.let {isGroupCreated ->
                            if (isGroupCreated != null &&
                                    isGroupCreated) {
                                groupCreated.value = Pair(groupName, ownerId)
                            } else {
                                error.value = APIError(IllegalStateException(), "Group Not Created")
                            }
                        }
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else ->{

                    }
                }
            }
        }
    }

    fun assignGroup(groupName: String, ownerId: String, userId: ArrayList<UserInfo>?){
        viewModelScope.launch {
            accountSource.assignGroup(groupName, ownerId, userId).let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        if (it.data != null)
                            userAssigned.value = Pair(it.data!!, Group(groupName, ownerId, userId))
                        else
                            error.value = APIError(IllegalArgumentException(), "Unable to assign user")
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else ->{

                    }
                }
            }
        }
    }

    fun createAccount(name: String, userId: String){
        viewModelScope.launch {
            var pushId = mSessionGraph.userPreferences.getFCMToken()
            accountSource.createUser(User(name, userId , pushId)).let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        userCreated.value = it.data
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else ->{

                    }
                }
            }
        }
    }

    fun getAllUsers(){
        viewModelScope.launch {
            accountSource.getAllUsers().let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        users.value = it.data
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else ->{

                    }
                }
            }
        }
    }

    fun getUserById(id: String, callback: (result: User) -> Unit, error: (exception: Exception) -> Unit){
        accountSource.getUserById(id, callback, error)
    }

    fun getGroupByName(name: String){
        viewModelScope.launch {
            accountSource.getGroupByName(name).let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else ->{

                    }
                }
            }
        }
    }

    fun deleteGroup(name: String){
        viewModelScope.launch {
            accountSource.deleteGroupByName(name).let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        groupDeleted.value = it.data
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else ->{

                    }
                }
            }
        }
    }


    fun updateUserSessionStatus(groupName: String, userId: String, status: String,
                                callback: (result: String) -> Unit, error: (exception: Exception) -> Unit){
        accountSource.updateUserSessionStatus(groupName, userId, status, callback, error)
    }

    fun updateGroupSessionStatus(groupName: String, status: String,
                                 callback: (result: String) -> Unit, error: (exception: Exception) -> Unit){
        accountSource.updateGroupSessionStatus(groupName, status, callback, error)
    }

    fun getJoinedUserInGroup(groupName: String,
                              callback: (result: User) -> Unit, error: (exception: Exception) -> Unit){
        accountSource.getGroupUserPerStatus(groupName, UserInfo.JOIN, callback, error)
    }

    fun clearUserAssigned(){
        userAssigned.value = null
    }

    fun clearGroupCreated(){
        groupCreated.value = null
    }
}
