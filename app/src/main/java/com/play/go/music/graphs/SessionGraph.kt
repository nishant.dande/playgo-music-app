package com.play.go.music.graphs

import android.content.Context
import androidx.room.Room
import com.play.go.music.auth.TokenExpiredCallback
import com.play.go.music.auth.TokenProvider
import com.play.go.music.preferences.UserPreferences
import com.play.go.music.session.SessionManager
import com.play.persistance.Library
import com.play.persistance.LibraryPlaylist
import com.play.persistance.MusicLibraryDatabase

interface SessionGraph {
    val tokenProvider: TokenProvider
    val sessionManager: SessionManager
    val userPreferences: UserPreferences
    val library: Library
}

class SessionGraphImpl(appContext: Context, expiredCallback: TokenExpiredCallback? = null) :
    SessionGraph {

    override val userPreferences: UserPreferences = UserPreferences(appContext)
    override val tokenProvider: TokenProvider =
        TokenProvider.with(appContext, userPreferences, expiredCallback)
    override val sessionManager: SessionManager = SessionManager(tokenProvider, userPreferences)

    private val libraryDao = LibraryPlaylist(
        Room.databaseBuilder(appContext, MusicLibraryDatabase::class.java, "music.db").build()
    )

    override val library: Library
        get() = Library(libraryDao)
}
