package com.play.go.music.v1.ui.gaia

import android.util.Log
import com.play.ble_service.manager.AGaiaManager
import com.qualcomm.qti.libraries.gaia.GAIA
import com.qualcomm.qti.libraries.gaia.GaiaException
import com.qualcomm.qti.libraries.gaia.packets.GaiaPacket
import java.util.*

class PresetMusicControlFeatureGaiaManager(
    private val mListener: GaiaManagerListener,
    @GAIA.Transport transportType: Int
) : AGaiaManager(
    transportType
) {
    // ====== PRIVATE FIELDS =======================================================================
    private val TAG = "PresetMusicControl"
    // ====== STATIC FIELDS =======================================================================
    /**
     * To know if we are using the application in the debug mode.
     */
    private val DEBUG: Boolean = true
    private val PRESET_MUSIC_CONTROL = byteArrayOf(0x00 , 0x01, 0x02, 0x03, 0x04, 0x05)

    // ====== PUBLIC METHODS =======================================================================
    fun setPresetMusic(preset: Int) {
        if (preset in 0..5) {
            val PAYLOAD_LENGTH = 1
            val PRESET_OFFSET = 0
            val payload = ByteArray(PAYLOAD_LENGTH)
            payload[PRESET_OFFSET] = PRESET_MUSIC_CONTROL[preset]
            createRequest(createPacket(GAIA.COMMAND_SET_EQ_CONTROL, payload))
        } else {
            Log.w(
                TAG,
                "setPreset used with parameter not between 0 and 1 value: " +
                        preset
            )
        }
    }

    fun getPresetMusic() {
        createRequest(createPacket(GAIA.COMMAND_GET_EQ_CONTROL))
    }

    override fun receiveSuccessfulAcknowledgement(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In PresetMusicControlActivity receiveSuccessfulAcknowledgement " +
                        packet!!.command + " " + Arrays.toString(packet.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
        when (packet!!.command) {
            GAIA.COMMAND_GET_EQ_CONTROL -> receiveGetEQControlACK(packet)
        }
    }

    override fun receiveUnsuccessfulAcknowledgement(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In PresetMusicControlActivity receiveUnsuccessfulAcknowledgement " +
                        packet!!.command + " " + Arrays.toString(packet.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
        when (packet!!.command) {
            GAIA.COMMAND_GET_EQ_CONTROL, GAIA.COMMAND_SET_EQ_CONTROL -> mListener.onControlNotSupported(
                1
            )
        }
    }

    override fun manageReceivedPacket(packet: GaiaPacket?): Boolean {
        try {
            Log.d(
                TAG,
                "In PresetMusicControlActivity manageReceivedPacket " + packet!!.command + " " + Arrays.toString(
                    packet.bytes
                )
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
        return false
    }

    override fun hasNotReceivedAcknowledgementPacket(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In PresetMusicControlActivity hasNotReceivedAcknowledgementPacket " +
                        packet!!.command + " " + Arrays.toString(packet.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
    }

    override fun onSendingFailed(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In PresetMusicControlActivity onSendingFailed " +
                        packet!!.command + " " + Arrays.toString(packet!!.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
    }

    override fun sendGAIAPacket(packet: ByteArray?): Boolean {
        return mListener.sendGAIAPacket(packet)
    }

    // ====== PRIVATE METHODS - RECEIVING =============================================================
    private fun receiveGetEQControlACK(packet: GaiaPacket) {
        val payload = packet.payload
        val PAYLOAD_VALUE_OFFSET = 1
        val PAYLOAD_VALUE_LENGTH = 1
        val PAYLOAD_MIN_LENGTH = PAYLOAD_VALUE_LENGTH + 1 // ACK status length is 1
        if (payload.size >= PAYLOAD_MIN_LENGTH) {
            val preset = payload[PAYLOAD_VALUE_OFFSET].toInt()
            mListener.onGetPreset(preset)
        }
    }

    // ====== INTERFACES ===========================================================================
    interface GaiaManagerListener {
        fun sendGAIAPacket(packet: ByteArray?): Boolean

        fun onGetPreset(preset: Int)

        fun onControlNotSupported(control: Int)
    }
}