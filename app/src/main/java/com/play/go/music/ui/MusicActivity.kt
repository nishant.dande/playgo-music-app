package com.play.go.music.ui

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.gson.Gson
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.base.controller.PlayerControlActivity
import com.play.go.music.ui.settings.SettingViewModel
import com.play.go.music.utils.Utility
import com.play.models.sync.Group
import com.play.models.sync.GroupPayload
import com.play.music_player.PlayerProducer
import com.play.networking.api.spotify.SpotifyApi
import com.play.notification.service.NotificationService
import kotlinx.android.synthetic.main.activity_music.*

class MusicActivity : PlayerControlActivity(){

    private val TAG = "MusicActivity"

    private val sessionGraph by lazy { graph()!!.sessionGraph }
    private val localLibrary by lazy { graph().sessionGraph.userPreferences }

    private val settingVm by lazy {
        getViewModel { SettingViewModel.create(this, sessionGraph) }
    }

    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        localLibrary.setLocalLibrary(Utility.defaultLocalPath())
        navController = findNavController(R.id.nav_host_fragment).also {
            nav_view.setupWithNavController(it)
        }
        // Register Device for FCM
        NotificationService.registerDevice(this)
        registerListeners()
    }

    override fun onDestroy() {
        super.onDestroy()
        unRegisterListener()

        //Clear Group Session If App Destroy
        sessionGraph.userPreferences.clearGroupSession()
    }

    companion object {
        fun newIntent(caller: Context): Intent {
            return Intent(caller, MusicActivity::class.java)
        }
    }


    // Handle in setting fragment
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
//        info { intent }
        Log.d(TAG, "In onActivityResult")
        // Check if result comes from the correct activity
        if (requestCode == SpotifyApi.LOGIN_REQUEST_CODE) {
            settingVm.onSpotifyResultFromActivity(requestCode, resultCode, intent)
        }
        super.onActivityResult(requestCode, resultCode, intent)


    }

    /**
     * Listeners
     */
    private fun registerListeners(){
        LocalBroadcastManager.getInstance(this).registerReceiver(mTokenReceiver,
            IntentFilter(NotificationService.ACTION_REFRESH_TOKEN))
        LocalBroadcastManager.getInstance(this).registerReceiver(mAudioSessionReceiver,
            IntentFilter(PlayerProducer.ACTION_AUDIO_SESSION))
        LocalBroadcastManager.getInstance(this).registerReceiver(mGroupActionReceiver,
            IntentFilter(NotificationService.ACTION_GROUP_EVENT))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaActionReceiver,
            IntentFilter(NotificationService.ACTION_MEDIA_EVENT))
        LocalBroadcastManager.getInstance(this).registerReceiver(mMediaSyncSessionActionReceiver,
            IntentFilter(NotificationService.ACTION_SYNC_EVENT))
    }

    private fun unRegisterListener(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAudioSessionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mTokenReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGroupActionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaActionReceiver)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMediaSyncSessionActionReceiver)
    }

    /**
     * Receiver Callback
     */
    private val mAudioSessionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val sessionId = intent?.getIntExtra(PlayerProducer.AUDIO_SESSION_ID, 0)
            Log.d(TAG, "In AudioSessionReceiver - $sessionId")
            sessionId.let {
                if (it != null) {
                    // Store it to preferences
                    sessionGraph.userPreferences.setSessionId(it)
                }
            }
        }
    }

    private val mTokenReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val token = intent.getStringExtra(NotificationService.DATA_TOKEN)
            if (token != null) {
                Log.d(TAG, "In mTokenReceiver - $token")
                // Store it to preferences
                localLibrary.setFCMToken(token)
            } else {
                Log.d(TAG, "Failed to receive token")
            }
        }
    }

    private val mGroupActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val groupPayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            if (groupPayload != null) {
                Log.d(TAG, "In mGroupActionReceiver - $groupPayload")
                val data = Gson().fromJson(groupPayload, Group::class.java)
                Events.groupServiceEvent.postValue(data)
            } else {
                Log.d(TAG, "Failed to receive groupPayload")
            }
        }
    }

    private val mMediaActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val mediaPayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            if (mediaPayload != null) {
                Log.d(TAG, "In mMediaActionReceiver - $mediaPayload")
                runOnUiThread {
                    processRemoteSong(mediaPayload)
                }
            } else {
                Log.d(TAG, "Failed to receive groupPayload")
            }
        }
    }

    private val mMediaSyncSessionActionReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            val syncPayload = intent.getStringExtra(NotificationService.DATA_PAYLOAD)
            if (syncPayload != null) {
                Log.d(TAG, "In mMediaSyncSessionActionReceiver - $syncPayload")
                runOnUiThread {
                    processMediaSync(syncPayload)
                }
            } else {
                Log.d(TAG, "Failed to receive groupPayload")
            }
        }
    }

    object Events {
        val groupServiceEvent: MutableLiveData<Group> by lazy {
            MutableLiveData<Group>()
        }
    }
}