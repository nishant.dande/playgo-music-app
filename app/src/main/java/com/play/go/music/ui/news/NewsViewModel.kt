package com.play.go.music.ui.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.NetworkGraph
import com.play.models.news.MusicNews
import com.play.networking.Outcome
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error


class NewsViewModel(
    val networkGraph: NetworkGraph
) : CoroutineScope by CoroutineScope(Dispatchers.IO), ViewModel(), AnkoLogger {

    var musicNews = MutableLiveData<MusicNews>()
    fun getMusicNews(){
        viewModelScope.launch {
            networkGraph.newsSource.getMusicNews(com.play.networking.utils.Utility.getCurrentDate()).let {
                when(it){
                    is Outcome.Success -> {
                        musicNews.value = it.data
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }

}
