package com.play.go.music.v1.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.play.go.music.R
import com.play.go.music.ui.adapter.genre.GenreAdapter
import com.play.go.music.ui.adapter.language.LanguageAdapter
import kotlinx.android.synthetic.main.fragment_genre.*
import kotlinx.android.synthetic.main.fragment_language.*

class LanguageFragment : Fragment() {

    private var languages = arrayListOf("Hindi", "English",
        "Bhojpuri", "Punjabi", "Tamil", "Telugu", "Bengali" ,"Malyalam", "Gujarati", "Rajasthani",
        "Marathi", "Oriya", "Assamese", "Kannada", "Haryanvi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        languageList.populateList(languages)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_language, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set list view for music list
        rvLanguageList!!.layoutManager = GridLayoutManager(requireContext(), 2)
        rvLanguageList!!.adapter = languageList
    }

    fun getSelectedItem() : List<String> {
        return languageList.getSelectedItem().map {
            it.language = it.language.replace("\n", " ");
            it.language = it.language.replace("\r", " ");
            it.language = it.language.toLowerCase()
            it.language
        }
    }

    private val languageList by lazy {
        LanguageAdapter().apply {
            clickListener = {
                it.let {

                }
            }
        }
    }
}