package com.play.go.music.v1.ui.see_all

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.models.account.Group
import com.play.models.music.Orientation
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_see_all.*

const val TAG = "v1SeeAllFragment"

class SeeAllFragment : BaseFragment() {

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived
    private lateinit var activity: PlayerControlActivity
    private var playlist: Playlist? = null

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { SeeAllViewModel(session, networkGraph) }
            }
        }
    }

    private var languages = arrayListOf("hindi", "marathi", "english", "punjabi", "tamil", "telugu", "kannada" ,"gujarati")
    private var songsBylanguages = arrayListOf<MutableLiveData<ArrayList<Song>>>()

    override fun getTab(): Int {
        return R.string.see_all
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // read argument
        arguments.let {
            if (it != null) {
                var data = it.get("playlist") as String
                playlist = Gson().fromJson(data, Playlist::class.java)
                Log.d(TAG, playlist.toString())
                musicList.populateList(
                    Playlist(1, playlist?.name!!, playlist?.songs!!,
                        orientation = Orientation.VERTICAL)
                )
            } else {
                Log.d(TAG, "No argument received")
                Log.d(TAG, "User Selection Languages :  "+sessionGraph.userPreferences.getUserInfo()?.languages)
                sessionGraph.userPreferences.getUserInfo().let { userInfo ->
                    if (userInfo?.languages?.isNotEmpty()!!){
                        languages.clear()
                        languages = userInfo.languages
                    }
                }

                languages.forEachIndexed { index, element ->
                    songsBylanguages.add(index, MutableLiveData<ArrayList<Song>>())
                    vm.getYoutubeSongsByLanguage(requireContext(),
                        element,songsBylanguages[index])
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_see_all, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            onCallbackReceived = context as HomeFragment.OnCallbackReceived
            activity = context
//            (context as PlayerControlActivity).showMiniControl(false)
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()

        //Set list view for music list
        rvSeeAllMusic!!.layoutManager = LinearLayoutManager(requireContext())
        rvSeeAllMusic!!.adapter = musicList
    }

    override fun onDestroy() {
        super.onDestroy()
        removeObserver()
    }

    private val musicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    onCallbackReceived.let {
                    }
                    Log.d(TAG, "playlist : "+it.toString())
                }
            }

            onSongClick = { index, song, playlistVH ->
                song.let {
                    it.let { song ->
                        setTrack(song)
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : ${it.entity}")

                }
            }
        }
    }

    private fun setTrack(song: Song){

        // Sync Play
        song.let {
            onCallbackReceived.let {
                onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
                activity.showMiniControl(true)
            }
        }
    }


    private fun observer(){
        songsBylanguages.forEachIndexed { index, element ->
            element.observe(viewLifecycleOwner, {
                Log.d(TAG, "${index} "+"Latest In ${languages[index]}")
                musicList.populateList(
                    Playlist(index, "Latest In ${languages[index]}", it,
                        orientation = Orientation.HORIZONTAL_WIDE)
                )
            })
        }

        vm.error.observe(viewLifecycleOwner, Observer {
            if (musicList.currentList.isEmpty()){
                showSnackBar(it.errorMessage!!)
            }
        })
    }

    private fun removeObserver(){
        songsBylanguages.forEachIndexed { index, element ->
//            element.removeObservers(viewLifecycleOwner)
        }
    }

}