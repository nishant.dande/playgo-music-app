package com.play.go.music.v1.ui.test

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.widget.AppCompatButton
import com.play.ble_service.headless.fragment.ServiceFragment
import com.play.ble_service.service.BluetoothService
import com.play.go.music.R
import com.play.go.music.v1.ui.gaia.PresetMusicControlFeatureGaiaManager
import com.qualcomm.qti.libraries.gaia.GAIA

class PresetMusicControlFragment : ServiceFragment(),
    PresetMusicControlFeatureGaiaManager.GaiaManagerListener, View.OnClickListener {

    val TAG = "PresetMusicControl"
    private var mGaiaManager: PresetMusicControlFeatureGaiaManager? = null
    lateinit var rgPresetMusic: RadioGroup

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_preset_music, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (view.findViewById(R.id.btnGetInformation) as AppCompatButton).setOnClickListener(this)

        rgPresetMusic = (view.findViewById(R.id.rbEvents) as RadioGroup)


        (view.findViewById(R.id.rbEvents) as RadioGroup).clearCheck()
        (view.findViewById(R.id.rbEvents) as RadioGroup).setOnCheckedChangeListener { radioGroup, id ->
            val eventCheckId =
                (view.findViewById(R.id.rbEvents) as RadioGroup).checkedRadioButtonId
            when (eventCheckId) {
                R.id.rbNormalMode -> {
                    val rbId = radioGroup.checkedRadioButtonId
                    if (rbId != -1) {
                        val radioButton =
                            radioGroup.findViewById<View>(id) as RadioButton
                        if (radioButton.isChecked) {
                            mGaiaManager?.setPresetMusic(0)
                        }
                    }
                }
                R.id.rbMusicMode -> {
                    val rbId = radioGroup.checkedRadioButtonId
                    if (rbId != -1) {
                        val radioButton =
                            radioGroup.findViewById<View>(id) as RadioButton
                        if (radioButton.isChecked) {
                            mGaiaManager?.setPresetMusic(2)
                        }
                    }
                }
                R.id.rbBassBoastMode -> {
                    val rbId = radioGroup.checkedRadioButtonId
                    if (rbId != -1) {
                        val radioButton =
                            radioGroup.findViewById<View>(id) as RadioButton
                        if (radioButton.isChecked) {
                            mGaiaManager?.setPresetMusic(1)
                        }
                    }
                }
                R.id.rbMovieMode -> {
                    val rbId = radioGroup.checkedRadioButtonId
                    if (rbId != -1) {
                        val radioButton =
                            radioGroup.findViewById<View>(id) as RadioButton
                        if (radioButton.isChecked) {
                            mGaiaManager?.setPresetMusic(3)
                        }
                    }
                }
                R.id.rbPodcastMode -> {
                    val rbId = radioGroup.checkedRadioButtonId
                    if (rbId != -1) {
                        val radioButton =
                            radioGroup.findViewById<View>(id) as RadioButton
                        if (radioButton.isChecked) {
                            mGaiaManager?.setPresetMusic(4)
                        }
                    }
                }
            }
        }
    }

    override fun handleMessageFromService(msg: Message?) {
        Log.d(
            TAG,
            "In MultipointConnectionFeatureActivity " + msg!!.what + " "
                    + msg.obj.toString()
        )
        //noinspection UnusedAssignment
        val handleMessage = "Handle a message from BLE service: "

        when (msg.what) {
            BluetoothService.Messages.CONNECTION_STATE_HAS_CHANGED -> {
                @BluetoothService.State val connectionState = msg.obj as Int
                val stateLabel =
                    if (connectionState == BluetoothService.State.CONNECTED) "CONNECTED" else if (connectionState == BluetoothService.State.CONNECTING) "CONNECTING" else if (connectionState == BluetoothService.State.DISCONNECTING) "DISCONNECTING" else if (connectionState == BluetoothService.State.DISCONNECTED) "DISCONNECTED" else "UNKNOWN"
                displayLongToast(getString(R.string.toast_device_information) + stateLabel)
                if (DEBUG) Log.d(
                    TAG,
                    handleMessage + "CONNECTION_STATE_HAS_CHANGED: " + stateLabel
                )
            }
            BluetoothService.Messages.DEVICE_BOND_STATE_HAS_CHANGED -> {
                val bondState = msg.obj as Int
                val bondStateLabel =
                    if (bondState == BluetoothDevice.BOND_BONDED) "BONDED" else if (bondState == BluetoothDevice.BOND_BONDING) "BONDING" else "BOND NONE"
                displayLongToast(getString(R.string.toast_device_information) + bondStateLabel)
                if (DEBUG) Log.d(
                    TAG,
                    handleMessage + "DEVICE_BOND_STATE_HAS_CHANGED: " + bondStateLabel
                )
            }
            BluetoothService.Messages.GATT_SUPPORT -> if (DEBUG) Log.d(
                TAG,
                handleMessage + "GATT_SUPPORT"
            )
            BluetoothService.Messages.GAIA_PACKET -> {
                val data = msg.obj as ByteArray
                mGaiaManager!!.onReceiveGAIAPacket(data)
            }
            BluetoothService.Messages.GAIA_READY -> if (DEBUG) Log.d(
                TAG,
                handleMessage + "GAIA_READY"
            )
            BluetoothService.Messages.GATT_READY -> if (DEBUG) Log.d(
                TAG,
                handleMessage + "GATT_READY"
            )
            else -> if (DEBUG) Log.d(
                TAG,
                handleMessage + "UNKNOWN MESSAGE: " + msg.what
            )
        }
    }

    override fun onServiceConnected() {
        @GAIA.Transport val transport =
            if (getTransport() === BluetoothService.Transport.BR_EDR) GAIA.Transport.BR_EDR else GAIA.Transport.BLE
        mGaiaManager = PresetMusicControlFeatureGaiaManager(this, transport)
        mGaiaManager?.getPresetMusic()
    }

    override fun onServiceDisconnected() {
    }

    override fun sendGAIAPacket(packet: ByteArray?): Boolean {
        return mService != null && mService!!.isGaiaReady && mService!!.sendGAIAPacket(packet)
    }

    override fun onGetPreset(preset: Int) {
        Log.d(
            TAG,
            "In MultipointConnectionFeatureActivity onGetPreset $preset"
        )
        checkControl(preset)
    }


    override fun onControlNotSupported(control: Int) {
        Log.d(
            TAG,
            "In MultipointConnectionFeatureActivity onGetPreset $control"
        )
    }

    override fun onClick(v: View?) {
        mGaiaManager?.getPresetMusic()
    }

    private fun checkControl(position: Int) {
        rgPresetMusic.clearCheck()
        when (position) {
            0 -> {
                rgPresetMusic.check(R.id.rbNormalMode)
            }
            1 -> {
                rgPresetMusic.check(R.id.rbBassBoastMode)
            }
            2 -> {
                rgPresetMusic.check(R.id.rbMusicMode)
            }
            3 -> {
                rgPresetMusic.check(R.id.rbMovieMode)
            }
            4 -> {
                rgPresetMusic.check(R.id.rbPodcastMode)
            }
        }
    }

}