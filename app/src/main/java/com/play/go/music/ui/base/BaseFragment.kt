package com.play.go.music.ui.base

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.google.android.material.snackbar.Snackbar
import com.play.go.music.R
import com.play.go.music.extensions.*

abstract class BaseFragment : Permission() {

    abstract fun getTab() : Int

    companion object{
//        private var toolbarColor: Int = R.drawable.bg_toolbar_home
    }

    fun setToolbarColor(color: Int){
//        toolbarColor = color
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (parentFragment != null) {
            changeSystemBarColor(this)
            showToolbarView(this, requireActivity().findViewById<View>(R.id.toolbar))
            showBottomBarView(this, requireActivity().findViewById<View>(R.id.bottom_nav_view))
            changeScreenBackground(this, requireActivity().findViewById<View>(R.id.nav_host_fragment))
            changeToolbarBackground(this, requireActivity().findViewById<View>(R.id.clToolbar))
            showToolbarItem()
        }
    }

    private fun showToolbarItem(){
        val parent: Fragment? = (parentFragment as NavHostFragment).parentFragment
//        parent?.view?.findViewById<View>(R.id.toolbar)?.background = ContextCompat.getDrawable(requireContext(), toolbarColor)
        parent?.view?.findViewById<View>(R.id.nav_view)?.visibility = View.VISIBLE

        when(getTab()){
            R.string.home, R.string.my_device, R.string.equalizer,R.string.see_all,
            R.string.settings, R.string.touch_controls, R.string.group, R.string.news,
            R.string.participant->{
                parent?.view?.findViewById<View>(R.id.title)?.visibility = View.VISIBLE
                setTitle(getTab())
            }
        }
    }

    private fun setTitle(title: Int){
        val tvTitle: AppCompatTextView = requireActivity().findViewById<View>(R.id.tvTitle) as AppCompatTextView
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = getString(title)
    }

    fun setTitle(title: String){
        val tvTitle: AppCompatTextView = requireActivity().findViewById<View>(R.id.tvTitle) as AppCompatTextView
        tvTitle.visibility = View.VISIBLE
        tvTitle.text = title
    }

    fun setProfileTitle(name: String){
        val tvTitle: AppCompatTextView =
            requireActivity().findViewById<View>(R.id.tvTitle) as AppCompatTextView
        if (!TextUtils.isEmpty(name)) {
            tvTitle.visibility = View.VISIBLE
            tvTitle.text = HtmlCompat.fromHtml("Hi ${name}, Let's <font color='#f04500'>PLAY!</font>",
                HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else {
            tvTitle.visibility = View.GONE
        }
    }

    fun showToolbar(){
        val view = requireActivity().findViewById<View>(R.id.toolbar)
        if (view != null) {
            val tvTitle: Toolbar = view as Toolbar
            tvTitle.visibility = View.VISIBLE
        }
    }

    fun hideToolbar(){
        val view = requireActivity().findViewById<View>(R.id.toolbar)
        if (view != null) {
            val tvTitle: Toolbar = view as Toolbar
            tvTitle.visibility = View.GONE
        }
    }

    fun popBackStack(){
        requireActivity().onBackPressed()
    }


    fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(
            requireActivity().findViewById<View>(android.R.id.content),
            message, Snackbar.LENGTH_SHORT
        )
        val sbView = snackbar.view
        val textView = sbView
            .findViewById<View>(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.color_snackbar_text
            )
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            textView.textAlignment = View.TEXT_ALIGNMENT_CENTER;
        } else {
            textView.gravity = Gravity.CENTER_HORIZONTAL;
        }
        snackbar.show()
    }

}