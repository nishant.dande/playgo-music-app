package com.play.go.music.ui.adapter.genre

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import kotlinx.android.synthetic.main.item_genre.view.*

class GenreVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onClickVH: ((GenreVH) -> Unit)? = null
    var context: Context = context
    private var SELECTED_COLOR = ContextCompat.getColor(context, R.color.color_selected_orange)
    private var SELECTED_TEXT_COLOR = ContextCompat.getColor(context, R.color.white)
    private var UN_SELECTED_TEXT_COLOR = ContextCompat.getColor(context, R.color.color_selected_orange)
    private var UN_SELECTED_COLOR = ContextCompat.getColor(context, R.color.white)

    var entity: GenreModel? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = item.genre
                if (item.selection) {
                    itemView.cardView.setCardBackgroundColor(SELECTED_COLOR)
                    itemView.tvTitle.setTextColor(SELECTED_TEXT_COLOR)
                } else {
                    itemView.cardView.setCardBackgroundColor(UN_SELECTED_COLOR)
                    itemView.tvTitle.setTextColor(UN_SELECTED_TEXT_COLOR)
                }


                itemView.setOnClickListener {
                    item.selection= !item.selection

                    if (item.selection) {
                        itemView.cardView.setCardBackgroundColor(SELECTED_COLOR)
                        itemView.tvTitle.setTextColor(SELECTED_TEXT_COLOR)
                    } else {
                        itemView.cardView.setCardBackgroundColor(UN_SELECTED_COLOR)
                        itemView.tvTitle.setTextColor(UN_SELECTED_TEXT_COLOR)
                    }


                    onClickVH?.invoke(this)
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): GenreVH {
            return GenreVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_genre,
                    parent,
                    false
                ),parent.context
            )
        }
    }
}

data class GenreModel(var genre: String, var selection: Boolean)

