package com.play.go.music.ui.adapter.custom

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import com.play.go.music.R
import com.play.models.device.CustomItem

class ArrowVH private constructor(itemView: View, context: Context) : BaseVH(itemView, context) {

    var onClick: ((ArrowVH) -> Unit)? = null
    var context: Context = context

    var entity: CustomItem? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.findViewById<AppCompatTextView>(R.id.tvFeatureName).text = item.name
//                itemView.findViewById<TextView>(R.id.tvProvider).text = context.getString(item.provider)
//
//                Glide.with(context)
//                    .load(item.image)
//                    .into(itemView.findViewById<ImageView>(R.id.ivTitleImage));
                itemView.setOnClickListener { onClick?.invoke(this) }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): ArrowVH {
            return ArrowVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_arrow,
                    parent,
                    false
                ),parent.context
            )
        }
    }
}

