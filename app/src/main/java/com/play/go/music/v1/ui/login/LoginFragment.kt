package com.play.go.music.v1.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.play.go.music.R
import com.play.go.music.extensions.dp
import com.play.go.music.extensions.getViewModel
import com.play.go.music.extensions.hideKeyboard
import com.play.go.music.graph
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.models.auth.Response
import com.play.networking.error.APIError
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.layout_logged_in.*
import kotlinx.android.synthetic.main.layout_login.*
import kotlinx.android.synthetic.main.layout_mini_player.*
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

class LoginFragment : BaseFragment() {

    val TAG = "LoginFragment"
    private val EMAIL = "email"
    private val RC_SIGN_IN = 1

    private lateinit var callbackManager: CallbackManager
    private lateinit var activity : Activity

    private val networkGraph  by lazy { context?.graph()?.networkGraph }
    private val sessionGraph by lazy { context?.graph()?.sessionGraph }


    private val vm by lazy {
        sessionGraph.let { sessionGraph ->
            networkGraph.let { networkGraph ->
                getViewModel { LoginViewModel(sessionGraph!!, networkGraph!!) }
            }
        }
    }


    override fun getTab(): Int {
        return R.string.login
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is LoginActivity){
            activity = context
        } else if(context is PlayerControlActivity){
            activity = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addPaddingWhenMiniControlVisible()
        hideToolbar()
        refreshLayout()

        observer()
        // Facebook - https://developers.facebook.com/apps/4291738464204338/fb-login/quickstart/
//        printHashKey(this)
        FacebookSdk.sdkInitialize(requireContext());

        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().registerCallback(
            callbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    // App code

                    val accessToken = AccessToken.getCurrentAccessToken()
                    val isLoggedIn = accessToken != null && !accessToken.isExpired
                    Log.d(TAG, "Login Manager - ${accessToken} - ${isLoggedIn}")
                    hideKeyboard()
                    vm.updateCache(Response(message = "logged in successfully via facebook", loggedIn = true))

                    val newMeRequest = GraphRequest.newMeRequest(accessToken) { profileJsonObject, _ ->
                        try {
                            profileJsonObject.let {
                                Log.d(TAG, it.toString())
                                val name = it!!.getString("name")
                                if (!TextUtils.isEmpty(name)) {
                                    vm.createAccount(name, Utility.getUserId(requireContext()))
                                }
                            }
                        } catch (e: Exception) {
                            Log.e(TAG, e.printStackTrace().toString())
                        }
                    }

                    newMeRequest.executeAsync()
                }

                override fun onCancel() {
                    // App code
                    hideKeyboard()
                }

                override fun onError(exception: FacebookException) {
                    hideKeyboard()
                    // App code
                    Log.d(TAG, "Login Manager Error - ${exception.localizedMessage}")
                }
            })

        // Google - https://developers.google.com/identity/sign-in/android/sign-in
//        Logo guideline - https://developers.google.com/identity/branding-guidelines
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        // Build a GoogleSignInClient with the options specified by gso.
        var mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)

        ivGoogle.setOnClickListener {
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
        ivFacebook.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList("email", "public_profile")
            );
        }

//        login_email.setOnClickListener {
//            handleLogin(LoginViewModel.EMAIL_LOGIN)
//        }

//        login_phone.setOnClickListener {
//            handleLogin(LoginViewModel.PHONE_NUMBER_LOGIN)
//        }
//        btnBack.setOnClickListener {
//            handleLogin(LoginViewModel.BACK)
//        }
        btnLogin.setOnClickListener {
            handleLogin(LoginViewModel.LOGIN)
        }
        btnLogout.setOnClickListener {
            vm.updateCache(Response(message = "logout in successfully", loggedIn = false))
        }
        tvContinueAsGuest.setOnClickListener {
            vm.updateCache(Response(continueAsGuest = true))
            startActivity(Intent(activity,
                com.play.go.music.MainActivity::class.java))
            activity.finish()
        }
    }

    private fun addPaddingWhenMiniControlVisible() {
        if (activity != null && activity is PlayerControlActivity){
            if (activity.clMediaControl.isVisible){
                // Add Padding
                clLoginContainer.setPadding(0, 0, 0, 80.dp)
                clLoginContainer.invalidate()
            } else {
                // Remove Padding
                clLoginContainer.setPadding(0, 0, 0, 0)
                clLoginContainer.invalidate()
            }
        }
    }

    private fun handleLogin(type: String){
        when(type){
            LoginViewModel.LOGIN -> {
                // Make login api call
                if (edtPhoneNumber.hint.toString().toLowerCase().contains(LoginViewModel.EMAIL_LOGIN)) {
                    vm?.login(LoginViewModel.EMAIL_LOGIN, edtPhoneNumber.text.toString(), edtOtp.text.toString())
                } else {
                    vm?.login(LoginViewModel.PHONE_NUMBER_LOGIN, edtPhoneNumber.text.toString(), edtOtp.text.toString())
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode === RC_SIGN_IN) {
            super.onActivityResult(requestCode, resultCode, data)
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
//            val account = completedTask.getResult(ApiException::class.java)
            Log.d(TAG, "Signed in successfully, show authenticated UI.")
            hideKeyboard()
            vm.updateCache(Response(message = "logged in successfully via google", loggedIn = true))
            completedTask.addOnSuccessListener {
                it.displayName.let { name ->
                    Log.d(TAG, name!!)
                    if (!TextUtils.isEmpty(name)) {
                        vm.createAccount(name, Utility.getUserId(requireContext()))
                    }
                }

            }.addOnFailureListener {
                Log.e(TAG, it.toString(), it)
            }

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode())
            hideKeyboard()
        }
    }

    fun printHashKey(pContext: Context) {
        try {
            val info: PackageInfo = pContext.getPackageManager().getPackageInfo(
                pContext.getPackageName(),
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey: String = String(Base64.encode(md.digest(), 0))
                Log.i(TAG, "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e(TAG, "printHashKey()", e)
        } catch (e: Exception) {
            Log.e(TAG, "printHashKey()", e)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        showToolbar()
        removeObserver()
    }

    private fun refreshLayout(){
        if (sessionGraph?.userPreferences?.getUserInfo() != null){
            if(sessionGraph?.userPreferences?.getUserInfo()!!.loggedIn){
                logged_in_screen.visibility = View.VISIBLE
                login_screen.visibility = View.GONE
            } else {
                handleLogin(LoginViewModel.BACK) // reset login screen state
                login_screen.visibility = View.VISIBLE
                logged_in_screen.visibility = View.GONE
            }
        }
    }

    private fun observer(){
        vm.error.observe(viewLifecycleOwner, errorObserver)
        vm.authResponse.observe(viewLifecycleOwner, authResponseObserver)
    }

    private fun removeObserver(){
        vm.error.removeObserver(errorObserver)
        vm.authResponse.removeObserver(authResponseObserver)
    }


    private val authResponseObserver = Observer<Response> { it ->
        it.let {
            hideKeyboard()
            showSnackBar(it.message.toString())
            if (activity is LoginActivity){
                // Please use below create account api till
                // dedicated server db api is not get ready
                val name: String = edtUserName.text.toString()
                if (!TextUtils.isEmpty(name)){
                    vm.createAccount(name, Utility.getUserId(requireContext()))
                }

                startActivity(Intent(activity,
                    com.play.go.music.MainActivity::class.java))
                activity.finish()
            } else {
                refreshLayout()
                val name: String = edtUserName.text.toString()
                if (!TextUtils.isEmpty(name)){
                    vm?.createAccount(name, Utility.getUserId(requireContext()))
                }
            }
        }
    }

    private val errorObserver = Observer<APIError> { it ->
        it.let { error ->
            hideKeyboard()
            showSnackBar(error.errorMessage.toString())
        }
    }
}