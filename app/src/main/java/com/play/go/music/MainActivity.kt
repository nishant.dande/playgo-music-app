package com.play.go.music

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    private val sessionGraph by lazy { applicationContext.graph().sessionGraph }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Check session is active or inactive and then take action
        sessionGraph.userPreferences.let {
            it.getUserInfo().let { userInfo ->
                // Case 1 : Login -> On Boarding -> Main Screen
                if (userInfo == null) { // First Install Condition
                    startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.login.LoginActivity::class.java))
                } else {
                    if (userInfo.continueAsGuest){
                        startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.MusicActivity::class.java))
                    } else if (!userInfo?.loggedIn!!){
                        startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.login.LoginActivity::class.java))
                    } else if (userInfo?.onboarding!!){
                        startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.MusicActivity::class.java))
//                startActivity(Intent(this@MainActivity, com.play.go.music.ui.MusicActivity::class.java))
                    }else {
                        startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.onboarding.OnBoardingActivity::class.java))
                    }
                }

                // Case 2 : OnBoarding -> Main Screen
                /*if (userInfo == null) { // First Install Condition
                    startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.onboarding.OnBoardingActivity::class.java))
                } else {
                    if (userInfo?.onboarding!!){
                        startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.MusicActivity::class.java))
                    }else {
                        startActivity(Intent(this@MainActivity, com.play.go.music.v1.ui.onboarding.OnBoardingActivity::class.java))
                    }
                }*/
            }
        }
        finish()
    }
}