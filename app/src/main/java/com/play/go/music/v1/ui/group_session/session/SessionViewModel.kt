package com.play.go.music.v1.ui.group_session.session

import com.play.go.music.graphs.NetworkGraph
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.ui.group.GroupViewModel

class SessionViewModel(
        val sessionGraph: SessionGraph,
        val networkGraph: NetworkGraph
) : GroupViewModel(sessionGraph, networkGraph.accountSource){


}