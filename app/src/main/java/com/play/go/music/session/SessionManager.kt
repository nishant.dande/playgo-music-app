package com.play.go.music.session

import com.play.go.music.auth.TokenProvider
import com.play.go.music.preferences.UserPreferences


class SessionManager(
    tokenProvider: TokenProvider,
    private val userPreferences: UserPreferences
) {
    var apiToken: String? = tokenProvider.getSpotifyApiToken()
//    var devToken: String = tokenProvider.developerToken

    fun isLoggedIn(): Boolean = userPreferences.apiToken != null

    fun removeUser() {
        userPreferences.removeUser()
    }

    fun createUser(token: String) {
        userPreferences.apiToken = token
    }

}
