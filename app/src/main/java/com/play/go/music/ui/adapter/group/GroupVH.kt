package com.play.go.music.ui.adapter.group

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import com.play.go.music.utils.Utility
import com.play.models.account.Group
import kotlinx.android.synthetic.main.item_group.view.*
import kotlinx.android.synthetic.main.item_music_list.view.tvTitle

class GroupVH private constructor(itemView: View, var context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onGClick: ((GroupVH) -> Unit)? = null
    var onGChecked: (((GroupVH), Boolean) -> Unit)? = null

    var entity: Group? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = item.name
                val userIds = item.userInfo?.map {
                    it.userId
                }
                if (userIds != null && userIds.isNotEmpty()) {
                    itemView.cbAssignGroup.isChecked =
                        userIds.contains(Utility.getUserId(context))!!
                } else
                    itemView.cbAssignGroup.isChecked = false

                itemView.setOnClickListener {
                    onGClick?.invoke(this)
                }

                itemView.cbAssignGroup.setOnCheckedChangeListener { buttonView, isChecked ->
                    onGChecked?.invoke(this, isChecked)
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): GroupVH {
                return GroupVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_group,
                        parent,
                        false
                    ),parent.context
                )
        }
    }
}

