package com.play.go.music.v1.ui.onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.play.go.music.R
import com.play.go.music.ui.adapter.genre.GenreAdapter
import kotlinx.android.synthetic.main.fragment_genre.*

class GenreFragment : Fragment() {

    private var genres = arrayListOf("Rock", "Hip-Hop", "Classical", "Dance", "Heavy Metal",
        "Drum & Bass", "Alternative" ,"Blues")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        genreList.populateList(genres)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_genre, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Set list view for music list
        rvGenreList!!.layoutManager = GridLayoutManager(requireContext(), 2)
        rvGenreList!!.adapter = genreList
    }

    fun getSelectedItem() : List<String> {
        return genreList.getSelectedItem().map {
            it.genre = it.genre.replace("\n", " ");
            it.genre = it.genre.replace("\r", " ");
            it.genre
        }
    }


    private val genreList by lazy {
        GenreAdapter().apply {

            clickListener = {
                it.let {

                }
            }
        }
    }
}