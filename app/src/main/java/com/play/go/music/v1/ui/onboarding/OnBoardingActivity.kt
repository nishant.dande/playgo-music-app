package com.play.go.music.v1.ui.onboarding

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.appintro.AppIntro
import com.github.appintro.AppIntroCustomLayoutFragment
import com.play.go.music.R
import com.play.go.music.graph
import com.play.go.music.v1.ui.MusicActivity
import com.play.models.auth.Response

class OnBoardingActivity : AppIntro() {

    val TAG = "OnBoardingActivity"

    private val sessionGraph by lazy { applicationContext.graph().sessionGraph }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addSlide(AppIntroCustomLayoutFragment.newInstance(R.layout.activity_onboarding))
        addSlide(GenreFragment())
        addSlide(LanguageFragment())


        setBarColor(ContextCompat.getColor(this, R.color.transparent));
        setSeparatorColor(ContextCompat.getColor(this, R.color.transparent))
    }

    public fun onGetStartedClick(view: View){
        goToNextSlide()
    }

    override fun onNextSlide() {
        super.onNextSlide()
        Log.d(TAG, "On Next Slide")
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        // Decide what to do when the user clicks on "Skip"
        Log.d(TAG, "onSkipPressed")
        goToNextSlide()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        if (currentFragment != null){
            if (currentFragment is LanguageFragment) {
                var languageFragment = currentFragment as LanguageFragment
                var languages = languageFragment.getSelectedItem()
                Log.d(TAG, languages.toString())
                sessionGraph.userPreferences.setUserInfo(
                    sessionGraph.userPreferences.getUserInfo().apply {
                        this?.languages = languages as ArrayList<String>
                    }!!
                )
            }
        }
        // Decide what to do when the user clicks on "Done"
        sessionGraph.userPreferences.setUserInfo(
                sessionGraph.userPreferences.getUserInfo().apply {
                    this?.onboarding = true
                }!!
            )
        startActivity(Intent(this, MusicActivity::class.java))
        finish()
    }

    override fun onSlideChanged(oldFragment: Fragment?, newFragment: Fragment?) {
        super.onSlideChanged(oldFragment, newFragment)
        Log.d(TAG, "${oldFragment.toString()} ${newFragment.toString()}")
        if (oldFragment != null){
            if (oldFragment is GenreFragment) {
                var genreFragment = oldFragment as GenreFragment
                var genres = genreFragment.getSelectedItem()
                Log.d(TAG, genres.toString())
                if (sessionGraph.userPreferences.getUserInfo() != null) {
                    sessionGraph.userPreferences.setUserInfo(
                            sessionGraph.userPreferences.getUserInfo().apply {
                                this?.genre = genres as ArrayList<String>
                            }!!
                    )
                } else {
                    sessionGraph.userPreferences.setUserInfo(Response(genre = genres as ArrayList<String>))
                }
            }
        }
        if (newFragment != null){
            if (newFragment is GenreFragment || newFragment is LanguageFragment){
                setNextArrowColor(ContextCompat.getColor(this, R.color.color_orange))
                setColorSkipButton(ContextCompat.getColor(this, R.color.color_orange))
                setColorDoneText(ContextCompat.getColor(this, R.color.color_orange))
                setIndicatorColor(ContextCompat.getColor(this, R.color.color_selected_orange),
                    ContextCompat.getColor(this, R.color.color_un_selected_orange))
            } else {
                setNextArrowColor(ContextCompat.getColor(this, R.color.white))
                setColorSkipButton(ContextCompat.getColor(this, R.color.white))
                setIndicatorColor(ContextCompat.getColor(this, R.color.white),
                    ContextCompat.getColor(this, R.color.light_gray))
            }
        }
    }
}