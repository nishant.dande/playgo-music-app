package com.play.go.music.ui.settings

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.karumi.dexter.listener.DexterError
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.permission.Permission
import com.play.go.music.ui.adapter.custom.CustomAdapter
import com.play.go.music.ui.adapter.custom.SwitchVH
import com.play.go.music.ui.adapter.custom.TextVH
import com.play.go.music.ui.base.BaseFragment
import com.play.models.device.CustomItem
import com.play.networking.api.spotify.SpotifyApi
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import kotlinx.android.synthetic.main.fragment_settings.*
import lib.folderpicker.FolderPicker
import org.jetbrains.anko.toast

class SettingsFragment : BaseFragment() {

    val TAG = "SettingsFragment"

    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }
    private val sessionManager by lazy { sessionGraph.sessionManager }
    private val settingVm by lazy {
        getViewModel { SettingViewModel.create(requireContext(), sessionGraph) }
    }

    private lateinit var permission : Permission

    override fun getTab(): Int {
        return R.string.settings
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkPackages()
        settingVm.getSettingConfig()
    }

    override fun onStart() {
        super.onStart()
        observe()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvSettings!!.layoutManager = LinearLayoutManager(requireContext())
        rvSettings!!.adapter = settings
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        permission = Permission(requireActivity())
        permission.setStoragePermissionListener(object : Permission.StoragePermissionListener{
            override fun permissionGranted(statue: Boolean) {
                if (statue){
                    openFolderPicker()
                }
            }

            override fun permissionDenied(dexterError: DexterError) {
                showSnackBar(dexterError.name)
            }
        })
    }


    private val settings by lazy {
        CustomAdapter().apply {
            onPlaylistClicked = {
                it.let {
                    if (it is TextVH){
                        when(it.entity?.name) {
                            getString(R.string.software_info) ->{

                            }
                            getString(R.string.check_updates) ->{

                            }
                        }
                    }
                }
            }


            onCheckClick = { switchVH: SwitchVH, b: SwitchVH.SwitchState ->
                when(switchVH.entity?.name){
                    getString(R.string.spotify_app) ->{
                        when(b){
                            SwitchVH.SwitchState.onClicked ->{
                                login()
                            }
                            SwitchVH.SwitchState.onChecked ->{
                                settingVm.setSettingConfig(getString(R.string.spotify_app), true)
                            }
                            SwitchVH.SwitchState.onReset ->{
                                settingVm.setSettingConfig(getString(R.string.spotify_app), false)
                            }
                        }
                    }

                    getString(R.string.local_music_app) ->{
                        when(b){
                            SwitchVH.SwitchState.onClicked ->{
                                permission.checkStoragePermission()
                            }
                            SwitchVH.SwitchState.onChecked ->{
                                settingVm.setSettingConfig(getString(R.string.local_music_app), true)
                            }
                            SwitchVH.SwitchState.onReset ->{
                                settingVm.setSettingConfig(getString(R.string.local_music_app), false)
                            }
                        }
                    }

                    getString(R.string.youtube_app) ->{
                        when(b){
                            SwitchVH.SwitchState.onClicked ->{

                            }
                            SwitchVH.SwitchState.onChecked ->{
                                settingVm.setSettingConfig(getString(R.string.youtube_app), true)
                            }
                            SwitchVH.SwitchState.onReset ->{
                                settingVm.setSettingConfig(getString(R.string.youtube_app), false)
                            }
                        }
                    }

                    getString(R.string.dark_mode_app) ->{
                        when(b){
                            SwitchVH.SwitchState.onClicked ->{

                            }
                            SwitchVH.SwitchState.onChecked ->{
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                            }
                            SwitchVH.SwitchState.onReset ->{
                                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                            }
                        }
                    }
                }
            }
        }
    }

    fun openFolderPicker(){
        val intent = Intent(requireContext(), FolderPicker::class.java)
        startActivityForResult(intent, 111)
    }


    private fun login(){
        val builder: AuthenticationRequest.Builder = AuthenticationRequest.Builder(
            SpotifyApi.CLIENT_ID,
            AuthenticationResponse.Type.TOKEN, SpotifyApi.REDIRECT_URI
        )

        builder.setScopes(arrayOf("streaming"))
        val request: AuthenticationRequest = builder.build()

        AuthenticationClient.openLoginActivity(
            requireActivity(), SpotifyApi.LOGIN_REQUEST_CODE,
            request
        )
    }

    // Handle in setting fragment
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        // Check if result comes from the correct activity
        if (requestCode == SpotifyApi.LOGIN_REQUEST_CODE) {
            settingVm.onSpotifyResultFromActivity(requestCode, resultCode, intent)
        } else if (requestCode == 111 && resultCode == Activity.RESULT_OK) {
            settingVm.onFolderResultFromActivity(requestCode, resultCode, intent)
            settingVm.loadLocalLibraryFolderPath()
        } else {
            Log.e("Error", "Not working")
        }
        super.onActivityResult(requestCode, resultCode, intent)


    }

    fun logout(){
        settingVm.onSpotifyLogout()
    }

    private fun observe() {
        settingVm.authState.observe(requireActivity(), Observer {
            when (it) {
                is SettingViewModel.State.Authenticated -> {
                    Log.d(TAG, "\"Authenticated successfully\"")
//                    finish()
//                    startActivity(MainActivity.newIntent(this))
                }
                is SettingViewModel.State.Unauthenticated -> {
                    Log.d(TAG, "Not authenticated")
                }
                is SettingViewModel.State.InvalidAuthentication -> {
                    Log.d(TAG, "Invalid authentication -- ${it.cause?.localizedMessage}")
                    context?.toast("${it.cause?.localizedMessage}")
                }
            }
        })

        settingVm?.localFolderPath?.observe(viewLifecycleOwner, Observer {
            it.let {
                var devicefeatures : ArrayList<CustomItem> = ArrayList<CustomItem>()
                it.forEach {
                    devicefeatures.add(CustomItem(it,ShowText = true))
                }
                settings.populateList(devicefeatures)
                if (devicefeatures.isNotEmpty()){
                    settingVm.setSettingConfig(getString(R.string.local_music_app), true)
                }
            }
        })

        settingVm?.settingConfig?.observe(viewLifecycleOwner, Observer {
            it.let {
                var devicefeatures : ArrayList<CustomItem> = ArrayList<CustomItem>()
                devicefeatures.add(0,CustomItem(getString(R.string.spotify_app),
                    showSwitch = true, checked = it.contains(getString(R.string.spotify_app)),
                    isClickable = false))

                devicefeatures.add(1,CustomItem(getString(R.string.youtube_app),showSwitch = true,
                checked = it.contains(getString(R.string.youtube_app)), isClickable = false))

                devicefeatures.add(2,CustomItem(getString(R.string.local_music_app),showSwitch = true,
                checked = it.contains(getString(R.string.local_music_app)), isClickable = false))

//                devicefeatures.add(2,CustomItem(getString(R.string.dark_mode_app),showSwitch = true,
//                checked = it.contains(getString(R.string.dark_mode_app)), isClickable = true))

                devicefeatures.add(CustomItem(getString(R.string.software_info),ShowText = true))
                devicefeatures.add(CustomItem(getString(R.string.check_updates),ShowText = true))
                settings.populateList(devicefeatures)

                settingVm.loadLocalLibraryFolderPath()
            }
        })
    }

    private fun checkPackages() {
        val packageManager = requireActivity().packageManager
        val list= packageManager.getInstalledApplications(PackageManager.GET_META_DATA)
        var isSpotifyAppAvailable =false
        var isYoutubeAppAvailable =false
        list.forEach {
            if (it.packageName == "com.spotify.music"){
                Log.d(TAG, "${it.packageName}")
                isSpotifyAppAvailable = true
            }
            if (it.packageName == "com.google.android.youtube"){
                Log.d(TAG, "${it.packageName}")
                isYoutubeAppAvailable = true
            }
        }
        sessionGraph.userPreferences.setSettingConfig(getString(R.string.spotify_app), isSpotifyAppAvailable)
        sessionGraph.userPreferences.setSettingConfig(getString(R.string.youtube_app), isYoutubeAppAvailable)
    }



}