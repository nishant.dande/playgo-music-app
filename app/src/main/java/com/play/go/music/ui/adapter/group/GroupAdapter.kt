package com.play.go.music.ui.adapter.group

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.account.Group


class GroupAdapter : ListAdapter<Group, GroupVH>(DIFF_CALLBACK) {

    var onClick: ((GroupVH) -> (Unit))? = null
    var onChecked: (((GroupVH), Boolean) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupVH {
        return GroupVH.create(parent, viewType)
            .apply {
                this.onGClick = onClick
                this.onGChecked = onChecked
            }
    }

    override fun onBindViewHolder(holder: GroupVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Group>){
        var items = settings
//        if (currentList.size > 0){
//            if (currentList.size == 1){
//                items.add(0, currentList.toList()[0] as Group )
//            } else{
//                items.addAll(0,currentList.toList() as ArrayList<Group> )
//            }
//        }
        submitList(items)
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Group>() {
    override fun areItemsTheSame(oldItem: Group, newItem: Group): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Group, newItem: Group): Boolean {
        return oldItem.name == newItem.name
    }
}