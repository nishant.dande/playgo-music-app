package com.play.go.music.ui.adapter.custom

import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.device.CustomItem
import com.play.models.music.Playlist


class CustomAdapter  : ListAdapter<CustomItem, BaseVH>(DIFF_CALLBACK) {

    var onPlaylistClicked: ((BaseVH) -> (Unit))? = null
    var onCheckClick: (((SwitchVH), SwitchVH.SwitchState)  -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseVH {
        when(viewType) {
            SHOW_ARROW -> {
                return ArrowVH.create(parent, viewType)
                    .apply { this.onClick = onPlaylistClicked }
            }
            SHOW_SETTING_ICON -> {
                return ArrowVH.create(parent, viewType)
                    .apply { this.onClick = onPlaylistClicked }
            }
            SHOW_SWITCH -> {
                return SwitchVH.create(parent, viewType)
                    .apply {
                        this.onClick = onCheckClick
                    }
            }
            SHOW_TEXT_ARROW -> {
                return TextArrowVH.create(parent, viewType)
                    .apply { this.onClick = onPlaylistClicked }
            }
            SHOW_TEXT -> {
                return TextVH.create(parent, viewType)
                    .apply { this.onClick = onPlaylistClicked }
            }
            else -> {
                return ArrowVH.create(parent, viewType)
                    .apply { this.onClick = onPlaylistClicked }
            }
        }

    }

    override fun onBindViewHolder(holder: BaseVH, position: Int) {
        when (holder) {
            is ArrowVH -> {
                holder.entity = getItem(position)
            }
            is SwitchVH -> {
                holder.entity = getItem(position)
            }
            is TextVH -> {
                holder.entity = getItem(position)
            }
            is TextArrowVH -> {
                holder.entity = getItem(position)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val viewStatus = getItem(position)
        return when {
            viewStatus.ShowArrow -> SHOW_ARROW
            viewStatus.showSettingIcon -> SHOW_SETTING_ICON
            viewStatus.showSwitch -> SHOW_SWITCH
            viewStatus.ShowText -> SHOW_TEXT
            viewStatus.ShowTextArrow -> SHOW_TEXT_ARROW
            else -> SHOW_ARROW
        }
    }

    fun populateList(settings: ArrayList<CustomItem>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as CustomItem )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<CustomItem> )
            }
        }
        submitList(items)
    }

    fun updateSubTitle(newItem : CustomItem){
        if (currentList.size > 0){
            var items = currentList.toList() as ArrayList<CustomItem>
            val filterIndexed = items.filterIndexed { index, item ->
                item.name == newItem.name
            }
            if (filterIndexed.isNotEmpty()){
                filterIndexed[0].apply {
                    this.subTitle = newItem.subTitle
                }
            }
        }
        notifyDataSetChanged()
    }

    companion object{
        const val SHOW_SWITCH = 0
        const val SHOW_ARROW = 1
        const val SHOW_SETTING_ICON = 2
        const val SHOW_TEXT = 3
        const val SHOW_TEXT_ARROW = 4
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<CustomItem>() {
    override fun areItemsTheSame(oldItem: CustomItem, newItem: CustomItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: CustomItem, newItem: CustomItem): Boolean {
        return oldItem.name == newItem.name
    }
}