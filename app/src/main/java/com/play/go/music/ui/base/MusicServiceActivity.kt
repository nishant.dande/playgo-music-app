package com.play.go.music.ui.base

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.SimpleExoPlayer
import com.play.music_player.local.MediaPlayerService

abstract class MusicServiceActivity : AppCompatActivity() {

    private val TAG = "MusicServiceActivity"

    // Variable for storing instance of our service class
    var mService: MediaPlayerService? = null

    // Boolean to check if our activity is bound to service or not
    var mIsBound: Boolean = false


    abstract fun getLocalMusicPlayer(exoPlayer: SimpleExoPlayer?)
    /**
     * Interface for getting the instance of binder from our service class
     * So client can get instance of our service class and can directly communicate with it.
     */
    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "ServiceConnection: connected to service.")
            // We've bound to MyService, cast the IBinder and get MyBinder instance
            val binder = iBinder as MediaPlayerService.PlayerServiceBinder
            mService = binder.service
            mIsBound = true

            getLocalMusicPlayer(mService?.getPlayer())
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            Log.d(TAG, "ServiceConnection: disconnected from service.")
            mIsBound = false
        }
    }

    /**
     * Used to bind to our service class
     */
    private fun bindService() {
        Intent(this, MediaPlayerService::class.java).also { intent ->
            this.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }


    /**
     * Used to unbind and stop our service class
     */
    private fun unbindService() {
        Intent(this, MediaPlayerService::class.java).also { intent ->
            this.unbindService(serviceConnection)
        }
    }

    fun startMusicService(){
        Log.d(TAG, "startMusicService")
        if (!mIsBound)
            bindService()
        else {
            mService.let {
                getLocalMusicPlayer(it?.getPlayer())
            }
        }
    }

    fun stopMusicService(){
        Log.d(TAG, "stopMusicService")
        if (mIsBound) {
            unbindService()
            mIsBound = false
        }
    }

    fun getCurrentDuration(): Long{
        if (mIsBound){
            mService.let {
                return it?.getCurrentDuration() ?: 0
            }
        }

        return 0
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mIsBound){
            mService?.stopSelf()
            mIsBound = false
        }
    }

}