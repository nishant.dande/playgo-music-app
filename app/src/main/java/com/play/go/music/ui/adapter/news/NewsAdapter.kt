package com.play.go.music.ui.adapter.news

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.news.MusicNews


class NewsAdapter()  : ListAdapter<MusicNews.Article, NewsVH>(DIFF_CALLBACK) {

    var clickListener: ((NewsVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsVH {
        return NewsVH.create(parent, viewType)
            .apply {
                this.onClickVH = clickListener
            }
    }

    override fun onBindViewHolder(holder: NewsVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<MusicNews.Article>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as MusicNews.Article )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<MusicNews.Article> )
            }
        }
        submitList(items)
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<MusicNews.Article>() {
    override fun areItemsTheSame(oldItem: MusicNews.Article, newItem: MusicNews.Article): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MusicNews.Article, newItem: MusicNews.Article): Boolean {
        return oldItem.title == newItem.title
    }
}