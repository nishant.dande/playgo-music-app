package com.play.go.music.permission

import android.Manifest
import android.app.Activity
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

class Permission(val activity: Activity) {

    private val storage_permissions = listOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
    )
    private val audio_permissions = listOf(
        Manifest.permission.RECORD_AUDIO
    )

    private var isStoragePermissionGranted: Boolean = false;
    private var isAudioPermissionGranted: Boolean = false;
    private var storagePermissionListener : StoragePermissionListener? = null
    private var audioPermissionListener : AudioPermissionListener? = null

    fun setStoragePermissionListener(listener: StoragePermissionListener){
        this.storagePermissionListener = listener
    }
    fun setAudioPermissionListener(listener: AudioPermissionListener){
        this.audioPermissionListener = listener
    }

    fun checkStoragePermission() : Boolean {
        Dexter.withActivity(activity)
            .withPermissions(storage_permissions)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        if (storagePermissionListener != null){
                            isStoragePermissionGranted = report.areAllPermissionsGranted()
                            storagePermissionListener?.permissionGranted(isStoragePermissionGranted)
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

            })
            .onSameThread()
            .withErrorListener {
                if (storagePermissionListener != null){
                    storagePermissionListener?.permissionDenied(it)
                }
            }
            .check()
        return isStoragePermissionGranted
    }

    fun checkAudioPermission() : Boolean {
        Dexter.withActivity(activity)
            .withPermissions(audio_permissions)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        if (audioPermissionListener != null){
                            isAudioPermissionGranted = report.areAllPermissionsGranted()
                            audioPermissionListener?.permissionGranted(isAudioPermissionGranted)
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

            })
            .onSameThread()
            .withErrorListener {
                if (audioPermissionListener != null){
                    audioPermissionListener?.permissionDenied(it)
                }
            }
            .check()
        return isAudioPermissionGranted
    }


    // TODO - Change with new kotlin featue
    interface StoragePermissionListener : Error{
        fun permissionGranted(statue: Boolean)
    }
    interface AudioPermissionListener : Error{
        fun permissionGranted(statue: Boolean)
    }

    interface Error{
        fun permissionDenied(dexterError: DexterError)
    }

}