package com.play.go.music.v1.ui.group_session.session

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.awesomedialog.*
import com.google.gson.Gson
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.extensions.hideKeyboard
import com.play.go.music.graph
import com.play.go.music.preferences.UserPreferences
import com.play.go.music.ui.MusicActivity
import com.play.go.music.ui.adapter.group.v1.GroupAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.utils.Utility
import com.play.go.music.v1.ui.base.controller.ControllerViewModel
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.account.UserInfo
import com.play.models.sync.MessagePayload
import com.play.networking.error.APIError
import kotlinx.android.synthetic.main.fragment_group.*
import kotlinx.android.synthetic.main.fragment_now_playing.*
import kotlinx.android.synthetic.main.fragment_sesion.*
import kotlinx.android.synthetic.main.fragment_sesion.rvGroups

class SessionFragment : BaseFragment() {
    val TAG = "SessionFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived
    private lateinit var mPlayerControlActivity: PlayerControlActivity
    private var unFilteredUser: List<User> = arrayListOf()


    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { SessionViewModel(session, networkGraph) }
            }
        }
    }

    private val vmController by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { ControllerViewModel(session, networkGraph) }
            }
        }
    }

    override fun getTab(): Int {
        return R.string.session
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.getAllUsers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            onCallbackReceived = context as HomeFragment.OnCallbackReceived
            mPlayerControlActivity = context as PlayerControlActivity
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sesion, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        rvGroups!!.layoutManager = LinearLayoutManager(requireContext())
        rvGroups!!.adapter = groupAdapter
        btnStartSession.setOnClickListener {
            createGroup()
        }
        fab_start_new_session.setOnClickListener {
            createGroup()
        }
        btnScanToJoin.setOnClickListener {
            vm.getGroups(Utility.getUserId(requireContext()))
        }
    }

    override fun onResume() {
        super.onResume()
        btnScanToJoin.performClick()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeObserver()
    }

    private fun observer(){
        vm.groups.observe(viewLifecycleOwner, groupObserver)
        vm.error.observe(viewLifecycleOwner, errorObserver)
        vm.groupCreated.observe(viewLifecycleOwner, createGroupObserver)
        vm.userAssigned.observe(viewLifecycleOwner, userAssignedObserver)
        vm.users.observe(viewLifecycleOwner, usersObserver)
        vm.groupDeleted.observe(viewLifecycleOwner, groupDeletedObserver)
    }

    private fun removeObserver(){
        vm.groups.removeObserver(groupObserver)
        vm.error.removeObserver(errorObserver)
        vm.groupCreated.removeObserver(createGroupObserver)
        vm.userAssigned.removeObserver(userAssignedObserver)
        vm.users.removeObserver(usersObserver)
        vm.groupDeleted.removeObserver(groupDeletedObserver)
    }

    private val errorObserver = Observer<APIError> { it ->
        it.let { error ->
            hideKeyboard()
            showSnackBar(error.errorMessage.toString())
        }
    }

    private val groupDeletedObserver = Observer<Boolean> { it ->
        if (it){
            btnScanToJoin.performClick()
        }
    }

    private val createGroupObserver = Observer<Pair<String, String>> { it ->
        if (it != null){
            // Group Created
            // Now assign current group owner to group
            vm.assignGroup(it.first, it.second,
                arrayListOf<UserInfo>(UserInfo(Utility.getUserId(requireContext()), UserInfo.NO_ACTION)))
            vm.clearGroupCreated()
        } else{
//            showSnackBar("Error while creating group")
        }
    }

    private val userAssignedObserver = Observer<Pair<Boolean, Group>> { it ->
        if(it != null)
        {
            if (it.first){
                // user Assigned to group
                val bundle = bundleOf("group" to it.second)
                findNavController().navigate(R.id.action_sessionFragment_to_participantFragment, bundle)
                vm.clearUserAssigned()
//            findNavController().navigate(
//                SessionFragmentDirections
//                    .actionSessionFragmentToParticipantFragment())
            } else{
                showSnackBar("Error - User unable to assign")
            }
        }

    }

    private val groupObserver = Observer<ArrayList<Group>> { it ->
        if (it.isNotEmpty()){
            rvGroups.visibility = View.VISIBLE
            fab_start_new_session.visibility = View.VISIBLE
            clNoGroupCreated.visibility = View.GONE
            groupAdapter.populateList(it)
        } else {
            rvGroups.visibility = View.GONE
            fab_start_new_session.visibility = View.GONE
            clNoGroupCreated.visibility = View.VISIBLE
            showSnackBar(getString(R.string.no_group_found))
        }
    }


    private val usersObserver = Observer<java.util.ArrayList<User>> { it ->
        Log.d(TAG, it.toString())
        if (it.isNotEmpty()) {
            unFilteredUser = it
        } else {
            Log.d(TAG, "No User is available")
        }

    }

    /**
     * Dialogs
     */
    private fun createGroup(){
        val alert = AlertDialog.Builder(requireContext())
        val edittext = EditText(requireContext())
        alert.setMessage(getString(R.string.enter_group_name))
        alert.setView(edittext)
        alert.setPositiveButton(getString(R.string.yes)) { dialog, whichButton ->
            val groupName: String = edittext.text.toString()
            if (!TextUtils.isEmpty(groupName)){
                vm.createGroup(groupName, Utility.getUserId(requireContext()))
            }
        }
        alert.setNegativeButton(getString(R.string.no)) {
            dialog, whichButton ->
            // what ever you want to do with No option.
        }
        alert.show()
    }

    private val groupAdapter by lazy {
        GroupAdapter(sessionGraph.userPreferences).apply {
            onClick = {
                Log.d(TAG, "On Click "+it.entity?.name)
                val bundle = bundleOf("group" to it.entity)
                findNavController().navigate(R.id.action_sessionFragment_to_participantFragment, bundle)
            }

            onStartClick = {
                Log.d(TAG, "On Start Click "+it.entity?.name)
                startTime(it.entity!!)

                notifyDataSetChanged()

                // Update Database Group Session
                vm.updateGroupSessionStatus(it.entity?.name!!, UserInfo.REQUESTED, {
                    Log.d(TAG, it)
                }, { })
            }

            onStopClick = { group ->
                Log.d(TAG, "On Stop Click "+group.entity?.name)
                mPlayerControlActivity.let {
                    it.stopTimer()

                    // Clear Group FCM Ids
                    sessionGraph.userPreferences.clearGroupToken()

                    val groupStateCache = GroupStateCache(group.entity?.name, GroupStateCache.GROUP_PLAY_STOP)
                    // Update Shared Preferences.. Group Play Status
                    sessionGraph.userPreferences.setGroupPlayStatus(groupStateCache)
                    notifyDataSetChanged()

                    // Update Database Group Session
                    vm.updateGroupSessionStatus(group.entity?.name!!, UserInfo.NO_ACTION, {
                        Log.d(TAG, it)
                        vmController.requestSendMessage(MessagePayload.STOP_GROUP_SESSION)
                    }, {})
                }
            }

            onLongClick = {

                AwesomeDialog.build(requireActivity())
                    .title(title = getString(R.string.delete),
                        titleColor = ContextCompat.getColor(requireContext(), R.color.color_dialog))
                    .body(body = getString(R.string.delete_message, it.entity?.name!!),
                        color = ContextCompat.getColor(requireContext(), R.color.color_dialog))
                    .onPositive(getString(R.string.yes)) {
                        // Delete Group
                        vm.deleteGroup(it.entity?.name!!)
                    }.onNegative(getString(R.string.no)){

                    }
            }
        }
    }


    // Manage Group FCM Token for Sync Communication
    private fun startTime(group: Group) {
        sessionGraph.userPreferences.clearGroupToken()
        Log.d(TAG, unFilteredUser.toString())
        val filteredUser = unFilteredUser.filter {
            it.userId != group.ownerId
        }
        val userIds = group.userInfo?.map {
            it.userId
        }
        userIds?.forEach { userId ->
            val filter = filteredUser.filter { it.userId == userId }
            if (filter.isNotEmpty()){
                filter[0].pushId?.let {
                    sessionGraph.userPreferences.setGroupFCMToken(it)
                }
            }
        }

        // Notify All the participant
        if (userIds?.isNotEmpty()!!) {
            sessionGraph.userPreferences.setSelectedGroupName(group.name)
            vmController.requestUserToJoin(requireContext(), userIds as ArrayList<String>)
        }
        else
            Log.e(TAG, "Users Id is empty")

        mPlayerControlActivity.let {
            it.startTimer()

            val groupStateCache = GroupStateCache(group.name!!, GroupStateCache.GROUP_PLAY_START)
            // Update Shared Preferences.. Group Play Status
            sessionGraph.userPreferences.setGroupPlayStatus(groupStateCache)
        }
    }

    data class GroupStateCache(val name: String?, val status: String){
        companion object{
            const val GROUP_PLAY_START = "group_play_start"
            const val GROUP_PLAY_STOP = "group_play_stop"
            const val NO_GROUP = "no_group"
        }
    }
}