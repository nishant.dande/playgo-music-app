package com.play.go.music.ui.my_music

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.karumi.dexter.listener.DexterError
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.extensions.hideKeyboard
import com.play.go.music.graph
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.ui.base.controller.PlayerControlActivity
import com.play.models.music.Orientation
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.music_player.base.Player
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_my_music.*

const val TAG = "MyMusicFragment"

class MyMusicFragment : BaseFragment(){

    val networkGraph get() = context?.graph()?.networkGraph
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }
    private var player: Player? = null
    private lateinit var onCallbackReceived: OnCallbackReceived
    private lateinit var activity: PlayerControlActivity


    val vm by lazy {
        networkGraph?.let { networkGraph ->
            getViewModel { MyMusicViewModel( sessionGraph,
                networkGraph.spotifySource,
                networkGraph.youtubeSource,
                networkGraph.spotifyAccountSource
            ) }
        }
    }

    var myMusicList : ArrayList<Playlist> = ArrayList<Playlist>()

    override fun getTab(): Int {
        return R.string.my_music
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkStoragePermission(object : PermissionListener{
            override fun permissionGranted(granted: Boolean) {
                if (granted)
                    vm?.initializeLocalSongs()
            }

            override fun failed(dexterError: DexterError) {
                Log.e(TAG, dexterError.name)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_music, container, false)
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvMusics!!.layoutManager = LinearLayoutManager(requireContext())
        rvMusics!!.adapter = musicList

        edtSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Log.d(TAG, "Search Click")
                if (TextUtils.isEmpty(edtSearch!!.text.toString()))
                    return@OnEditorActionListener true

                var string = edtSearch!!.text.toString()
                searchMusic(string)
                edtSearch.text?.clear()
                hideKeyboard()
            }
            false
        })
        edtSearch.setOnTouchListener { v, event ->
            val DRAWABLE_RIGHT = 2

            if(event.action == MotionEvent.ACTION_UP) {
                if(event.x >= (edtSearch.right - edtSearch.compoundDrawables
                            [DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    if (!TextUtils.isEmpty(edtSearch!!.text.toString())) {
                        var string = edtSearch!!.text.toString()
                        searchMusic(string)
                        edtSearch.text?.clear()
                        hideKeyboard()
                    }
                }
            }
            false
        }

        vm?.spotifySongs?.observe(viewLifecycleOwner, Observer {
            myMusicList.add(Playlist(3, "Spotify List", it, orientation = Orientation.VERTICAL))
            musicList.populateList(Playlist(3, "Spotify List", it, orientation = Orientation.VERTICAL))
        })
        vm?.youtubeSongs?.observe(viewLifecycleOwner, Observer {
            myMusicList.add(Playlist(2, "Youtube List", it, orientation = Orientation.VERTICAL))
            musicList.populateList(Playlist(2, "Youtube List", it, orientation = Orientation.VERTICAL))
        })
        vm?.localSongs?.observe(viewLifecycleOwner, Observer {
            myMusicList.add(Playlist(1, "My List", it))
            musicList.populateList(Playlist(1, "My List", it))
        })

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            activity = context as PlayerControlActivity
            onCallbackReceived = context as OnCallbackReceived
//            activity.showMiniControl(false)
        }

    }

    private val musicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    Log.d(TAG, "playlisy : $it")
                }
            }

            onSongClick = { index, song, playlistVH ->
                song.let {
                    it.let {
                        if (it != null) {
                            handleSongClick(it)
                        } else {
                            Log.d(TAG, "Song Not Available")
                        }
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : $it")

                }
            }

        }
    }

    private fun handleSongClick(song: Song){
        Log.d(TAG, song.toString())
        onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
        activity.showMiniControl(true)
    }

    private fun searchMusic(string: String){
        searchTag.addTag(string)

        vm?.getYoutubeSearchList(string)
        vm?.getSpotifySearchList(string, MyMusicViewModel.LIMIT, MyMusicViewModel.OFFSET)
    }
    interface OnCallbackReceived {
        fun operation(song: Song? = null, command: String);
        fun setYoutubeView(youTubePlayerView: ConstraintLayout);
        fun onDestroyView()
        fun onResumeView(container: ConstraintLayout)
        fun getCurrentSong() : Song?
    }
}