package com.play.go.music.graphs

data class AppGraph(
    val sessionGraph: SessionGraph,
    val networkGraph: NetworkGraph
)
