package com.play.go.music.ui.adapter.playlists

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.play.go.music.R
import com.play.models.music.Playlist
import com.play.networking.utils.Utility
import kotlinx.android.synthetic.main.item_playlist.view.*

class PlaylistVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onClick: ((PlaylistVH) -> Unit)? = null
    var context: Context = context

    var entity: Playlist? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = Utility.titleCase(item.name)
                itemView.tvNoOFSongs.text = "${item.songs.size} Songs"

                if (item.songs.isNotEmpty()) {
                    if (!TextUtils.isEmpty(item.songs[0].image)) {
                        Glide.with(context)
                            .load(item.songs[0].image)
                            .circleCrop()
                            .into(itemView.ivTitleImage)
                    }
                }

                itemView.setOnClickListener {
                    onClick?.invoke(this)
                }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): PlaylistVH {
                return PlaylistVH(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_playlist,
                        parent,
                        false
                    ),parent.context
                )
        }
    }
}

