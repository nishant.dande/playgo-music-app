package com.play.go.music.v1.ui.search

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.karumi.dexter.listener.DexterError
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.extensions.hideKeyboard
import com.play.go.music.graph
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.go.music.v1.ui.new_songs.NewSongsFragment
import com.play.go.music.v1.ui.party.PartyFragment
import com.play.go.music.v1.ui.popular.PopularFragment
import com.play.go.music.v1.ui.recommended.RecommendedFragment
import com.play.models.music.Orientation
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.music_player.base.Player
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_search_v1.*

const val TAG = "v1SearchFragment"

class SearchFragment : BaseFragment(), RecognitionListener {

    val networkGraph get() = context?.graph()?.networkGraph
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }
    private var player: Player? = null
    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived
    private lateinit var activity: PlayerControlActivity
    var speechRecognizer: SpeechRecognizer? = null
    var demoCollectionAdapter : DemoCollectionAdapter? = null


    val vm by lazy {
        networkGraph?.let { networkGraph ->
            getViewModel { SearchViewModel( sessionGraph,
                networkGraph.spotifySource,
                networkGraph.youtubeSource,
                networkGraph.spotifyAccountSource
            ) }
        }
    }

    var myMusicList : ArrayList<Playlist> = ArrayList<Playlist>()

    private var tab_title = arrayListOf("Recommended", "Popular", "New", "Party")

    override fun getTab(): Int {
        return R.string.my_music
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkStoragePermission(object : PermissionListener{
            override fun permissionGranted(granted: Boolean) {
                if (granted)
                    vm?.initializeLocalSongs()
            }

            override fun failed(dexterError: DexterError) {
                Log.e(TAG, dexterError.name)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_v1, container, false)
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideToolbar()
        rvMusics!!.layoutManager = LinearLayoutManager(requireContext())
        rvMusics!!.adapter = musicList

        demoCollectionAdapter = DemoCollectionAdapter(this)
        pager.adapter = demoCollectionAdapter
        TabLayoutMediator(tab_layout, pager) { tab, position ->
            tab.text = tab_title[position]
        }.attach()

        edtSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Log.d(TAG, "Search Click")
                if (TextUtils.isEmpty(edtSearch!!.text.toString()))
                    return@OnEditorActionListener true

                var string = edtSearch!!.text.toString()
                searchMusic(string)
                edtSearch.text?.clear()
                hideKeyboard()


                lvPresetMusic.visibility = View.GONE
                rvMusics.visibility = View.VISIBLE
            }
            false
        })
        ivSpeech.setOnClickListener {
            checkAudioPermission(object : PermissionListener{
                override fun permissionGranted(granted: Boolean) {
                    if (granted){
                        var recognizerIntent= Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                        recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "US-en");
                        recognizerIntent.putExtra(
                            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
                        );
                        recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
                        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(requireContext())
                        speechRecognizer!!.setRecognitionListener(this@SearchFragment)
                        speechRecognizer!!.startListening(recognizerIntent)
                    }
                }

                override fun failed(dexterError: DexterError) {

                }
            })
        }
        ivBack.setOnClickListener {
            if (lvPresetMusic.isVisible)
                popBackStack()
            else{
                lvPresetMusic.visibility = View.VISIBLE
                rvMusics.visibility = View.GONE
            }
        }

        vm?.spotifySongs?.observe(viewLifecycleOwner, Observer {
            myMusicList.add(Playlist(3, "Spotify List", it, orientation = Orientation.VERTICAL))
            musicList.populateList(Playlist(3, "Spotify List", it, orientation = Orientation.VERTICAL))
        })
        vm?.youtubeSongs?.observe(viewLifecycleOwner, Observer {
            myMusicList.add(Playlist(2, "Youtube List", it, orientation = Orientation.VERTICAL))
            musicList.populateList(Playlist(2, "Youtube List", it, orientation = Orientation.VERTICAL))
        })
        vm?.localSongs?.observe(viewLifecycleOwner, Observer {
            myMusicList.add(Playlist(1, "My List", it))
            musicList.populateList(Playlist(1, "My List", it))
        })

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            activity = context as PlayerControlActivity
            onCallbackReceived = context as HomeFragment.OnCallbackReceived
//            activity.showMiniControl(false)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        showToolbar()
    }

    private val musicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    Log.d(TAG, "playlisy : $it")
                }
            }

            onSongClick = { index, song, playlistVH ->
                song.let {
                    it.let {
                        if (it != null) {
                            handleSongClick(it)
                        } else {
                            Log.d(TAG, "Song Not Available")
                        }
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : $it")

                }
            }

        }
    }

    private fun handleSongClick(song: Song){
        Log.d(TAG, song.toString())
        onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
        activity.showMiniControl(true)
    }

    private fun searchMusic(string: String){
        searchTag.addTag(string)

        vm?.getYoutubeSearchList(string)
        vm?.getSpotifySearchList(string, SearchViewModel.LIMIT, SearchViewModel.OFFSET)
    }

    class DemoCollectionAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

        override fun getItemCount(): Int = 4

        override fun createFragment(position: Int): Fragment {
            when (position) {
                0 -> {
                    return RecommendedFragment()
                }
                1 -> {
                    return PopularFragment()
                }
                2 -> {
                    return NewSongsFragment()
                }
                3 -> {
                    return PartyFragment()
                }
            }
            return RecommendedFragment()
        }
    }

    /**
     * Speech Recognition API
     */
    override fun onReadyForSpeech(params: Bundle?) {

    }

    override fun onBeginningOfSpeech() {
    }

    override fun onRmsChanged(rmsdB: Float) {
    }

    override fun onBufferReceived(buffer: ByteArray?) {
    }

    override fun onEndOfSpeech() {
    }

    override fun onError(error: Int) {
        Log.d("Error", getErrorText(error))
        showSnackBar(getErrorText(error))
    }

    override fun onResults(results: Bundle?) {
        results.let {
            val stringArrayList = it?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            var text = ""
            if (!stringArrayList.isNullOrEmpty()){
                stringArrayList?.forEach {
                    text += it+" "
                }
            }

            Log.d(TAG, text)
            if (!TextUtils.isEmpty(text)){
                searchMusic(text)
                lvPresetMusic.visibility = View.GONE
                rvMusics.visibility = View.VISIBLE
            }
        }
    }

    override fun onPartialResults(partialResults: Bundle?) {
    }

    override fun onEvent(eventType: Int, params: Bundle?) {
    }

    fun getErrorText(errorCode: Int) : String {
        var message : String = ""
        when (errorCode) {
            SpeechRecognizer.ERROR_AUDIO ->
                message = "Audio recording error";

            SpeechRecognizer.ERROR_CLIENT ->
                message = "Client side error";

            SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS ->
                message = "Insufficient permissions";

            SpeechRecognizer.ERROR_NETWORK ->
                message = "Network error";

            SpeechRecognizer.ERROR_NETWORK_TIMEOUT ->
                message = "Network timeout";

            SpeechRecognizer.ERROR_NO_MATCH ->
                message = "No match";

            SpeechRecognizer.ERROR_RECOGNIZER_BUSY ->
                message = "RecognitionService busy";

            SpeechRecognizer.ERROR_SERVER ->
                message = "error from server";

            SpeechRecognizer.ERROR_SPEECH_TIMEOUT ->
                message = "No speech input";

            else ->
                message = "Didn't understand, please try"
        }
        return message;
    }

}