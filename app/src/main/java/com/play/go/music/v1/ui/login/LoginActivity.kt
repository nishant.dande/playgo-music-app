package com.play.go.music.v1.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.play.go.music.R
import com.play.go.music.extensions.changeSystemBarColor


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_v1)
        changeSystemBarColor()
    }
}