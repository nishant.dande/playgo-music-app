package com.play.go.music.ui.adapter.news

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.play.go.music.R
import com.play.models.news.MusicNews
import com.play.networking.utils.Utility
import kotlinx.android.synthetic.main.item_news.view.*

class NewsVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onClickVH: ((NewsVH) -> Unit)? = null
    var context: Context = context

    var entity: MusicNews.Article? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = item.title
                itemView.tvDescription.text = item.description
                item.publishedAt.let {
                    itemView.tvPublishAt.text = Utility.getDateMonthYear(it!!)
                }
                if (!TextUtils.isEmpty(item.urlToImage)) {
                    Glide.with(context)
                        .load(item.urlToImage)
                        .into(itemView.ivNews)
                }
                itemView.setOnClickListener { onClickVH?.invoke(this) }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): NewsVH {
            return NewsVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_news,
                    parent,
                    false
                ),parent.context
            )
        }
    }
}

