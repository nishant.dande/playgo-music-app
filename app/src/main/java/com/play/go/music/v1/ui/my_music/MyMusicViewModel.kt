package com.play.go.music.v1.ui.my_music

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.NetworkGraph
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.session.SyncManager
import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.info


class MyMusicViewModel(
    val sessionGraph: SessionGraph,
    val networkGraph: NetworkGraph
) : SyncManager(sessionGraph.userPreferences, networkGraph.syncSource){

    companion object{
        val OFFSET = "1"
        val LIMIT = "50"
    }

    var getAllPlaylist = MutableLiveData<List<Playlist>>()
    var getPlaylist = MutableLiveData<Playlist>()
    var getSong = MutableLiveData<Song>()
    var recentlyAdded = MutableLiveData<List<Song>>()

    fun getUserPlaylist() {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.selectAllPlaylist()
            info { "initial songs cache=${cached.count()}" }
            launch(Dispatchers.Main) { getAllPlaylist.value = cached  }

        }
    }

    fun getUserPlaylistById(id: Int) {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.getPlaylistById(id)
            launch(Dispatchers.Main) { getPlaylist.value = cached  }

        }
    }

    fun getSongById(id: Int) {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.getSongById(id)
            launch(Dispatchers.Main) { getSong.value = cached  }

        }
    }

    fun initializeRecentlyAddedSongs() {
        launch(Dispatchers.IO) {
            val cached = sessionGraph.library.getRecentAdded()
            info { "initial songs cache=${cached.count()}" }
            launch(Dispatchers.Main) { recentlyAdded.value = cached  }
        }
    }

    fun createPlaylist(name: String){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.createPlaylist(Playlist(name = name))
                val cached = sessionGraph.library.selectAllPlaylist()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { getAllPlaylist.value = cached  }
            }
        }
    }

    fun managePlaylist(playlistId: Int, songId: Int){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.addSongToPlaylist(ManagePlaylist(playlistId, songId))
                val cached = sessionGraph.library.selectAllPlaylist()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { getAllPlaylist.value = cached  }
            }
        }
    }

    fun addRecentlyPlayedSong(song: Song){
        viewModelScope.launch {
            launch(Dispatchers.IO) {
                sessionGraph.library.addRecentSong(song)
                val cached = sessionGraph.library.getRecentAdded()
                info { "initial songs cache=${cached.count()}" }
                launch(Dispatchers.Main) { recentlyAdded.value = cached  }
            }
        }
    }
}
