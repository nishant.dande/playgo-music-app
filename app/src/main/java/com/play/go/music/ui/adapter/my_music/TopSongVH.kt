package com.play.go.music.ui.adapter.my_music

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.play.go.music.R
import com.play.models.music.Song
import kotlinx.android.synthetic.main.item_top_song.view.*

class TopSongVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onSongClickVH: ((TopSongVH) -> Unit)? = null
    var context: Context = context

    var entity: Song? = null
        set(value) {
            field = value
            value?.let { song ->
                var item = song
                itemView.tvSongName.text = item.title
                itemView.setOnClickListener { onSongClickVH?.invoke(this) }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): TopSongVH {
            return TopSongVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_top_song,
                    parent,
                    false
                ),parent.context
            )
        }
    }
}

