package com.play.go.music.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.play.go.music.R
import kotlinx.android.synthetic.main.layout_mini_player.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

class Utility {

    companion object{

        var mediaExtension = arrayOf(".mp3", ".mp4")

        fun defaultLocalPath(): String{
            var gpath: String = Environment.getExternalStorageDirectory().absolutePath
            var spath = "Download"
            var fullpath = File(gpath + File.separator + spath)
            return fullpath.toString()
        }

        suspend fun getFiles(fullpath: File): ArrayList<File>?{
            return withContext(Dispatchers.IO){
                Log.w("fullpath", "" + fullpath)
                val fileList: ArrayList<File> = ArrayList()
                val listAllFiles = fullpath.listFiles()

                if (listAllFiles != null && listAllFiles.size > 0) {
                    for (currentFile in listAllFiles) {
                        if (checkMediaFile(currentFile)) {
                            // File absolute path
                            Log.e("downloadFilePath", currentFile.getAbsolutePath())
                            // File Name
                            Log.e("downloadFileName", currentFile.getName())
                            fileList.add(currentFile.absoluteFile)
                        }
                    }
                    Log.w("fileList", "" + fileList.size)
                }
                if (fileList.size > 0)
                    return@withContext fileList
                else
                    return@withContext null
            }
        }

        private fun checkMediaFile(file: File) : Boolean{
            mediaExtension.forEach {
                if(file.name.endsWith(it))
                    return true
            }
            return false
        }

        @SuppressLint("HardwareIds")
        fun getUserId(context: Context) : String{
            return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        }

        fun handleMediaStateDrawable(context: Context, imageView: ImageView) : Boolean{
            val tag = imageView.tag as Int
            if(tag == R.drawable.ic_play_arrow){
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_pause
                    )
                )
                imageView.tag = R.drawable.ic_pause
                // Is Stopped
                return false
            } else if(tag == R.drawable.ic_pause){
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.ic_play_arrow
                    )
                )
                imageView.tag = R.drawable.ic_play_arrow

                // Is playing
                return true
            }
            return false
        }



        fun changePlayImageResource(context: Context, imageView: ImageView, isPlayed: Boolean){
            if (isPlayed) {
                imageView.tag = R.drawable.ic_pause
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(context, R.drawable.ic_pause)
                )
            } else {
                imageView.tag = R.drawable.ic_play_arrow
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(context, R.drawable.ic_play_arrow)
                )
            }
        }

    }
}