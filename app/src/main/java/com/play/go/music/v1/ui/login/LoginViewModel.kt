package com.play.go.music.v1.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.play.go.music.graphs.NetworkGraph
import com.play.go.music.graphs.SessionGraph
import com.play.go.music.session.SyncManager
import com.play.models.account.User
import com.play.models.auth.Response
import com.play.networking.Outcome
import com.play.networking.error.APIError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import java.lang.Exception


class LoginViewModel(
    val sessionGraph: SessionGraph,
    val networkGraph: NetworkGraph
) : SyncManager(sessionGraph.userPreferences, networkGraph.syncSource){

    var error = MutableLiveData<APIError>()
    var authResponse = MutableLiveData<Response>()

    fun login(type: String, identity: String, password: String?){
        viewModelScope.launch {
            networkGraph.authSource.login(type, identity, password).let {
                when (it) {
                    is Outcome.Success -> viewModelScope.launch(Dispatchers.Main) {
//                        info { it.data }
                        if (it.data != null) {
                            it.data.apply {
                                this?.loggedIn = true
                            }

                            updateCache(it.data!!)
                        } else {
                            error.value = APIError(Exception(), "User not found")
                        }
                    }
                    is Outcome.Failure -> {
                        it.e.printStackTrace()
                        error.value = APIError(it.e, it.e.errorMessage)
                    }
                    else -> {
                    }
                }
            }
        }
    }

    fun updateCache(response: Response){
        // Save to shared preference
        if (sessionGraph.userPreferences.getUserInfo() != null){
            response.apply {
                this.onboarding = sessionGraph.userPreferences.getUserInfo()?.onboarding!!
                this.genre = sessionGraph.userPreferences.getUserInfo()?.genre!!
                this.languages = sessionGraph.userPreferences.getUserInfo()?.languages!!
            }
        }
        sessionGraph.userPreferences.setUserInfo(response)
        authResponse.value = response
    }

    fun createAccount(name: String, userId: String){
        viewModelScope.launch {
            var pushId = sessionGraph.userPreferences.getFCMToken()
            networkGraph.accountSource.createUser(User(name, userId , pushId)).let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }

    companion object{
        const val FACEBOOK_LOGIN = "facebook"
        const val GOOGLE_LOGIN = "facebook"
        const val EMAIL_LOGIN = "email"
        const val PHONE_NUMBER_LOGIN = "mobile"
        const val BACK = "back"
        const val LOGIN = "login"
    }
}
