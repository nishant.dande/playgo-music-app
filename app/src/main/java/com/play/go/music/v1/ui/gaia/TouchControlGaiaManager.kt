package com.play.go.music.v1.ui.gaia

import android.annotation.SuppressLint
import android.util.Log
import com.play.ble_service.manager.AGaiaManager
import com.qualcomm.qti.libraries.gaia.GAIA
import com.qualcomm.qti.libraries.gaia.GaiaException
import com.qualcomm.qti.libraries.gaia.GaiaUtils
import com.qualcomm.qti.libraries.gaia.packets.GaiaPacket
import java.util.*

class TouchControlGaiaManager(
    private val mListener: GaiaManagerListener,
    @GAIA.Transport transport: Int) : AGaiaManager(transport) {



    // ====== PRIVATE FIELDS =======================================================================
    /**
     *
     * The tag to display for logs.
     */
    private val TAG = "TouchControlGaiaManager"

    // ====== ENUM =================================================================================
    /**
     *
     * This enumeration represents all the controls which can be used with the command
     * [COMMAND_AV_REMOTE_CONTROL][GAIA.COMMAND_AV_REMOTE_CONTROL].
     */
    @SuppressLint("ShiftFlags") // it is more human readable this way
    annotation class Controls {
        companion object {
            const val SINGLE_TAP = 0x01
            var DOUBLE_TAP = 0x02
            var TRIPLE_TAP = 0x03
            var HOLD = 0x04
            var PPCC = 0x01
            var MM = 0x02
            var NPP = 0x03
            var VC = 0x04
        }
    }

    // ====== PUBLIC METHODS =======================================================================
    fun sendControlCommand(control: ByteArray?) {
        Log.d(
            TAG, "sendControlCommand : " +
                    GaiaUtils.getHexadecimalStringFromBytes(control)
        )
        createRequest(createPacket(GAIA.COMMAND_SET_LED_CONTROL, control))
    }

    fun getInformation() {
        createRequest(createPacket(GAIA.COMMAND_GET_LED_CONTROL))
    }

    override fun receiveSuccessfulAcknowledgement(packet: GaiaPacket?) {
        Log.d(
            TAG, "receiveSuccessfulAcknowledgement : " +
                    packet.toString() + " command : " + packet!!.command
        )
        mListener.getGAIAPacket(packet)
    }

    override fun receiveUnsuccessfulAcknowledgement(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In receiveUnsuccessfulAcknowledgement " +
                        packet!!.command + " " + Arrays.toString(packet.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
        when (packet!!.command) {
            GAIA.COMMAND_GET_LED_CONTROL -> mListener.onTouchControlNotSupported()
        }
    }

    override fun manageReceivedPacket(packet: GaiaPacket?): Boolean {
        try {
            Log.d(
                TAG,
                "In manageReceivedPacket " + packet!!.command + " " + Arrays.toString(
                    packet.bytes
                )
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
        return false
    }

    override fun hasNotReceivedAcknowledgementPacket(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In hasNotReceivedAcknowledgementPacket " +
                        packet!!.command + " " + Arrays.toString(packet.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
    }

    override fun onSendingFailed(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "In onSendingFailed " +
                        packet!!.command + " " + Arrays.toString(packet!!.bytes)
            )
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
    }

    override fun sendGAIAPacket(packet: ByteArray?): Boolean {
        return mListener.sendGAIAPacket(packet)
    }

    // ====== INTERFACES ===========================================================================
    /**
     *
     * This interface allows this manager to dispatch messages or events to a listener.
     */
    interface GaiaManagerListener {
        /**
         *
         * To send over a communication channel the bytes of a GAIA packet using the GAIA protocol.
         *
         * @param packet
         * The byte array to send to a device.
         * @return
         * true if the sending could be done.
         */
        fun sendGAIAPacket(packet: ByteArray?): Boolean
        fun onTouchControlNotSupported()
        fun getGAIAPacket(packet: GaiaPacket?)
    }
}