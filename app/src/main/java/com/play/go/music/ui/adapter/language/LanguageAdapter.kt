package com.play.go.music.ui.adapter.language

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter


class LanguageAdapter()  : ListAdapter<LanguageModel, LanguageVH>(DIFF_CALLBACK) {

    var clickListener: ((LanguageVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageVH {
        return LanguageVH.create(parent, viewType)
            .apply {
                this.onClickVH = clickListener
            }
    }

    override fun onBindViewHolder(holder: LanguageVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(languages: ArrayList<String>){
        var items = arrayListOf<LanguageModel>()
        languages.forEach {
            items.add(LanguageModel(it, false))
        }
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as LanguageModel )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<LanguageModel> )
            }
        }
        submitList(items)
    }

    fun getSelectedItem() : List<LanguageModel> {
        if (currentList.isNotEmpty()){
            return currentList.filter {
                it.selection
            }
        }
        return arrayListOf()
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<LanguageModel>() {
    override fun areItemsTheSame(oldItem: LanguageModel, newItem: LanguageModel): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: LanguageModel, newItem: LanguageModel): Boolean {
        return oldItem == newItem
    }
}