package com.play.go.music.ui.adapter.my_music

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.music.Song


class SongAdapter(private val orientation: Int)  : ListAdapter<Song, SongVH>(DIFF_CALLBACK) {

    var songAdapterListener: ((Int, SongVH) -> (Unit))? = null
    var songAdapterLongListener: ((SongVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongVH {
                return SongVH.create(parent, viewType)
                    .apply {
                        this.onSongClickVH = songAdapterListener
                        this.onSongLongClickVH = songAdapterLongListener
                    }
    }

    override fun onBindViewHolder(holder: SongVH, position: Int) {
        holder.entity = Pair(position, getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return orientation
    }

    fun populateList(settings: ArrayList<Song>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as Song )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<Song> )
            }
        }
        submitList(items)
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Song>() {
    override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem.title == newItem.title
    }
}