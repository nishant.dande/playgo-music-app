package com.play.go.music.v1.ui.new_songs

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.search_tab.SearchTabAdapter
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.go.music.v1.ui.search.SearchViewModel
import com.play.models.music.Song
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_new_songs.*

class NewSongsFragment : Fragment() {

    val TAG = "v1PartyFragment"

    val networkGraph get() = context?.graph()?.networkGraph
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived
    private lateinit var activity: PlayerControlActivity

    val vm by lazy {
        networkGraph?.let { networkGraph ->
            getViewModel { SearchViewModel( sessionGraph,
                networkGraph.spotifySource,
                networkGraph.youtubeSource,
                networkGraph.spotifyAccountSource
            ) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm?.getYoutubeNewSongs(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_songs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm?.youtubeNewSongs?.observe(viewLifecycleOwner, youtubeSongsByLanguageObserver)


        //Set list view for music list
        rvNewSongs!!.layoutManager = GridLayoutManager(requireContext(), 2)
        rvNewSongs!!.adapter = musicList
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            activity = context as PlayerControlActivity
            onCallbackReceived = context as HomeFragment.OnCallbackReceived
//            activity.showMiniControl(false)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        vm?.youtubeNewSongs?.removeObserver(youtubeSongsByLanguageObserver)
    }

    private val youtubeSongsByLanguageObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            Log.d(TAG, songs.toString())
            musicList.populateList(songs)
        }
    }

    private val musicList by lazy {
        SearchTabAdapter().apply {

            clickListener = {
                it.let {
                    it.entity.let {
                        if (it != null) {
                            handleSongClick(it)
                        } else {
                            Log.d(com.play.go.music.v1.ui.search.TAG, "Song Not Available")
                        }
                    }
                }
            }
        }
    }

    private fun handleSongClick(song: Song){
        Log.d(TAG, song.toString())
        onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
        activity.showMiniControl(true)
    }
}