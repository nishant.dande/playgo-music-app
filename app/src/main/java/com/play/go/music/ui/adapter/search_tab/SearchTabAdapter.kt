package com.play.go.music.ui.adapter.search_tab

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.music.Song
import com.play.models.news.MusicNews


class SearchTabAdapter()  : ListAdapter<Song, SearchTabVH>(DIFF_CALLBACK) {

    var clickListener: ((SearchTabVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchTabVH {
        return SearchTabVH.create(parent, viewType)
            .apply {
                this.onClickVH = clickListener
            }
    }

    override fun onBindViewHolder(holder: SearchTabVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Song>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as Song )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<Song> )
            }
        }
        submitList(items)
    }
}


val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Song>() {
    override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
        return oldItem.title == newItem.title
    }
}