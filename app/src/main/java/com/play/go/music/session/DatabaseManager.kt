package com.play.go.music.session

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.play.models.account.Group
import com.play.models.account.User
import com.play.networking.Outcome
import com.play.networking.api.account.source.AccountSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info

open class DatabaseManager(private val accountSource: AccountSource)
    : CoroutineScope by CoroutineScope(Dispatchers.IO), ViewModel(), AnkoLogger {

    var groups = MutableLiveData<ArrayList<Group>>()
    fun getGroups(){
        viewModelScope.launch {
            accountSource.getGroups().let {
                when(it){
                    is Outcome.Success ->{
                        info { it.data }
                        groups.value = it.data
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }

    var users = MutableLiveData<ArrayList<User>>()
    fun getUsersByGroup(currentUser: String, ids: ArrayList<String>){
        viewModelScope.launch {
            accountSource.getAllUsers().let {
                when(it){
                    is Outcome.Success ->{
                        var _user = arrayListOf<User>()
                        it.data?.forEach {
                            if (ids.contains(it.userId)){
                                _user.add(it)
                            }
                        }
                        users.value = _user
                    }
                    is Outcome.Failure ->{
                        error { it.e }
                    }
                    else ->{

                    }
                }
            }
        }
    }
}