package com.play.go.music.ui.base

import android.Manifest
import androidx.fragment.app.Fragment
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.DexterError
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener

open class Permission : Fragment() {


    private val STORAGE_PERMISSIONS = listOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
    )

    private val AUDIO_PERMISSIONS = listOf(
        Manifest.permission.RECORD_AUDIO
    )

    private val LOCATION_PERMISSIONS = listOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    fun checkStoragePermission(permissionListener: PermissionListener) {
        Dexter.withActivity(requireActivity())
            .withPermissions(STORAGE_PERMISSIONS)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        permissionListener.permissionGranted(report.areAllPermissionsGranted())
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

            })
            .onSameThread()
            .withErrorListener {
                permissionListener.failed(it)
            }
            .check()
    }

    fun checkAudioPermission(permissionListener: PermissionListener){
        Dexter.withActivity(requireActivity())
            .withPermissions(AUDIO_PERMISSIONS)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        permissionListener.permissionGranted(report.areAllPermissionsGranted())
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

            })
            .onSameThread()
            .withErrorListener {
                permissionListener.failed(it)
            }
            .check()
    }

    fun checkLocationPermission(permissionListener: PermissionListener){
        Dexter.withActivity(requireActivity())
            .withPermissions(LOCATION_PERMISSIONS)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    if (report != null) {
                        permissionListener.permissionGranted(report.areAllPermissionsGranted())
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }

            })
            .onSameThread()
            .withErrorListener {
                permissionListener.failed(it)
            }
            .check()
    }

    interface PermissionListener{
        fun permissionGranted(granted: Boolean)
        fun failed(dexterError: DexterError)
    }
}