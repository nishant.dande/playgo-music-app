package com.play.go.music.preferences

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.play.go.music.ui.equalizer.EqualizerFragment
import com.play.go.music.v1.ui.group_session.session.SessionFragment
import com.play.go.music.v1.ui.group_session.session.SessionFragment.GroupStateCache.Companion.GROUP_PLAY_STOP
import com.play.models.auth.Response


class UserPreferences(val context: Context) {

    companion object {
        const val PREFS = "user_prefs"
        const val CURRENT_USER = "current_user"
        const val LOCAL_LIBRARY = "local_library"
        const val SETTING_CONFIG = "setting_config"
        const val USER_INFO = "user_info"

        //Group
        const val FCM_TOKEN = "fcm_token"
        const val GROUP_FCM_TOKEN = "group_fcm_token"
        const val SELECTED_GROUP_NAME = "selected_group_name"

        // Media Session Id
        const val MEDIA_SESSION_ID = "session_id"
        const val MEDIA_EQUALIZER_MODE = "equalizer_mode"

        // Music Liked Songs
        const val LIKED_SONGS_ID = "liked_songs_id"

        // Group Play
        const val GROUP_PLAY_STATUS = "group_play_status"

    }

    private val gson = Gson()

    private val sharedPreferences = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE)

    private var _apiToken: String? = null
    private var _localLibrary: ArrayList<String> = arrayListOf()
    private var _settingConfigs: ArrayList<String> = arrayListOf()

    // Group
    private var _groupfcmToken: ArrayList<String> = arrayListOf()
    private var _selectedGroupName: String? = null
    private var _fcmToken: String? = null

    // Media
    private var _sessionId: Int? = null

    // Liked Songs Ids
    private var _liked_song_ids: ArrayList<Int> = arrayListOf()

    set(value) {
        field = value
        if (value == null) {
//            sharedPreferences.edit().remove(CURRENT_USER).apply()
        } else {
//            sharedPreferences.edit().clear()
            sharedPreferences.edit()
                .putString(CURRENT_USER, _apiToken)
                .apply()
        }
    }
    var apiToken: String? = null
    get() {
//        if (_apiToken != null) {
            _apiToken = sharedPreferences.getString(CURRENT_USER, null)
//        }
        return _apiToken
    }
    set(value) {
        _apiToken = value
        field = value
        if (value == null) {
//            sharedPreferences.edit().remove(CURRENT_USER).apply()
        } else {
//            sharedPreferences.edit().clear()
            sharedPreferences.edit()
                .putString(CURRENT_USER, _apiToken)
                .apply()
        }
    }

    fun setLocalLibrary(path: String) {
        val localLibrary = getLocalLibrary()
        if (localLibrary.indexOf(path) == -1) {
            _localLibrary.add(path)
            sharedPreferences!!.edit().putString(LOCAL_LIBRARY, Gson().toJson(_localLibrary))
                .apply()
        }
    }

    fun setSettingConfig(name: String, status: Boolean) {
        val settingConfigs = getSettingConfig()
        if (status && settingConfigs.indexOf(name) == -1) {
            _settingConfigs.add(name)
            sharedPreferences!!.edit().putString(SETTING_CONFIG, Gson().toJson(_settingConfigs))
                .apply()
        } else {
            if (!status) {
                _settingConfigs.remove(name)
                sharedPreferences!!.edit().putString(SETTING_CONFIG, Gson().toJson(_settingConfigs))
                    .apply()
            }
        }
    }

    fun getLocalLibrary() : ArrayList<String> {
        var list = sharedPreferences!!.getString(LOCAL_LIBRARY, null)
        if (list != null){
            _localLibrary = gson.fromJson(list, object : TypeToken<ArrayList<String?>?>() {}.type)
                    as ArrayList<String>
        }
        return _localLibrary
    }

    fun getSettingConfig() : ArrayList<String> {
        var list = sharedPreferences!!.getString(SETTING_CONFIG, null)
        if (list != null){
            _settingConfigs = gson.fromJson(list, object : TypeToken<ArrayList<String?>?>() {}.type)
                    as ArrayList<String>
        }
        return _settingConfigs
    }

    fun setGroupFCMToken(path: String) {
        val localLibrary = getGroupFCMToken()
        if (localLibrary.indexOf(path) == -1) {
            _groupfcmToken.add(path)
            sharedPreferences!!.edit().putString(GROUP_FCM_TOKEN, Gson().toJson(_groupfcmToken))
                .apply()
        }
    }

    fun setClearGroupFCMToken() {
        sharedPreferences!!.edit().putString(GROUP_FCM_TOKEN, Gson().toJson(arrayListOf<String>()))
            .apply()
    }

    fun getGroupFCMToken() : ArrayList<String> {
        var list = sharedPreferences!!.getString(GROUP_FCM_TOKEN, null)
        if (list != null){
            _groupfcmToken = gson.fromJson(list, object : TypeToken<ArrayList<String?>?>() {}.type)
                    as ArrayList<String>
        }
        return _groupfcmToken
    }

    fun setEqualizerMode(data: EqualizerFragment.SelectedMode){
        sharedPreferences!!.edit().putString(MEDIA_EQUALIZER_MODE, Gson().toJson(data))
                .apply()
    }

    fun getEqualizerMode() : EqualizerFragment.SelectedMode? {
        var mode = sharedPreferences!!.getString(MEDIA_EQUALIZER_MODE, null)
        if (mode != null){
            return gson.fromJson(mode,object : TypeToken<EqualizerFragment.SelectedMode?>() {}.type)
                    as EqualizerFragment.SelectedMode
        }
        return null
    }

    fun clearGroupToken(){
        sharedPreferences!!.edit().remove(GROUP_FCM_TOKEN).apply()
    }


    fun setSelectedGroupName(token: String) {
        sharedPreferences!!.edit().putString(SELECTED_GROUP_NAME, token)
            .apply()
    }

    fun getSelectedGroupName() : String? {
        _selectedGroupName = sharedPreferences!!.getString(SELECTED_GROUP_NAME, null)
        return _selectedGroupName
    }

    fun setGroupPlayStatus(status: SessionFragment.GroupStateCache) {
        sharedPreferences!!.edit().putString(GROUP_PLAY_STATUS, gson.toJson(status))
            .apply()
    }

    fun getGroupPlayStatus() : SessionFragment.GroupStateCache {
        var data = sharedPreferences!!.getString(GROUP_PLAY_STATUS, null)
        if (data != null){
            try {
                return gson.fromJson(data, object : TypeToken<SessionFragment.GroupStateCache?>() {}.type)
                        as SessionFragment.GroupStateCache
            } catch (e: Exception){
                Log.e("Error", e.toString(), e)
                return SessionFragment.GroupStateCache(null, GROUP_PLAY_STOP)
            }
        }
        return SessionFragment.GroupStateCache(null, GROUP_PLAY_STOP)
    }

    fun setFCMToken(token: String) {
        if (getFCMToken() != null) {
            sharedPreferences!!.edit().remove(FCM_TOKEN).apply()
        }
        sharedPreferences!!.edit().putString(FCM_TOKEN, token)
            .apply()
    }

    fun getFCMToken() : String? {
        _fcmToken = sharedPreferences!!.getString(FCM_TOKEN, null)
        return _fcmToken
    }


    fun clearGroupSession(){
        sharedPreferences!!.edit().remove(GROUP_FCM_TOKEN).apply()
        sharedPreferences.edit().remove(SELECTED_GROUP_NAME).apply()
        sharedPreferences.edit().remove(GROUP_PLAY_STATUS).apply()
    }

    fun setSessionId(sessionId: Int) {
        sharedPreferences!!.edit().putInt(MEDIA_SESSION_ID, sessionId)
            .apply()
    }

    fun getSessionId() : Int? {
        _sessionId = sharedPreferences!!.getInt(MEDIA_SESSION_ID, 0)
        return _sessionId
    }

    fun setLikedSongId(id: Int) {
        val localLibrary = getLikedSongIds()
        if (localLibrary.indexOf(id) == -1) {
            _liked_song_ids.add(id)
            sharedPreferences!!.edit().putString(LIKED_SONGS_ID, Gson().toJson(_liked_song_ids))
                .apply()
        }
    }

    fun getLikedSongIds() : ArrayList<Int> {
        var list = sharedPreferences!!.getString(LIKED_SONGS_ID, null)
        if (list != null){
            _liked_song_ids = gson.fromJson(list, object : TypeToken<ArrayList<Int?>?>() {}.type)
                    as ArrayList<Int>
        }
        return _liked_song_ids
    }


    fun setUserInfo(data: Response){
        sharedPreferences!!.edit().putString(USER_INFO, Gson().toJson(data))
            .apply()
    }

    fun getUserInfo() : Response? {
        var mode = sharedPreferences!!.getString(USER_INFO, null)
        if (mode != null){
            return gson.fromJson(mode,object : TypeToken<Response>() {}.type)
                    as Response
        }
        return null
    }



    fun removeUser() {
//        apiToken = null
//        _localLibrary.clear()
    }
}
