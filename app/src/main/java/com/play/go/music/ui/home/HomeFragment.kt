package com.play.go.music.ui.home

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.awesomedialog.*
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.MusicActivity
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.ui.base.controller.PlayerControlActivity
import com.play.go.music.ui.my_music.MyMusicFragment
import com.play.go.music.utils.Utility
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.account.UserInfo
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.music_player.PlayerProducer
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    val TAG = "HomeFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: MyMusicFragment.OnCallbackReceived

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { HomeViewModel(session, networkGraph) }
            }
        }
    }

    private var mSong: Song? = null
    private var mSelectedPlaylist: Playlist? = null
    private var mManagePlaylist : Boolean = false

    override fun getTab(): Int {
        return R.string.home
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.initializeRecentlyAddedSongs()
        vm.getSpotifyNewRelease(HomeViewModel.LIMIT, HomeViewModel.OFFSET)
        vm.getYoutubeRecommendation()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()

        //Set list view for music list
        rvRecentMusics!!.layoutManager = LinearLayoutManager(requireContext())
        rvRecentMusics!!.adapter = musicList


        setClickListener()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            onCallbackReceived = context as MyMusicFragment.OnCallbackReceived
            (context as PlayerControlActivity).showMiniControl(false)
        }

    }

    override fun onResume() {
        super.onResume()
        onCallbackReceived.let {
            if (it.getCurrentSong() != null){
                mSong = it.getCurrentSong()!!
                showMusicInfo(mSong!!)

                if (mSong!!.provider == PlayerProducer.YOUTUBE_PLAYER)
                    it.onResumeView(youtube_player_view)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        onCallbackReceived.let {
            it.onDestroyView()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        removeObserver()
    }

    private fun setClickListener() {

        ivPlaylist.setOnClickListener {
            val alert = AlertDialog.Builder(requireContext())
            val edittext = EditText(requireContext())
            alert.setMessage(getString(R.string.enter_playlist_name))
            alert.setView(edittext)
            alert.setPositiveButton(getString(R.string.yes)) { dialog, whichButton ->
                val name: String = edittext.text.toString()
                if (!TextUtils.isEmpty(name)){
                    vm.createPlaylist(name)
                }
            }
            alert.setNegativeButton(getString(R.string.no)) {
                    dialog, whichButton ->
                // what ever you want to do with No option.
            }
            alert.show()
        }

        ivPlaylistList.setOnClickListener {
            mManagePlaylist = true
            vm.getUserPlaylist()
        }

        iv_group.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_groupFragment)
        }

        tvGroup.setOnClickListener {
            vm.getGroups()
        }

        iv_seek_minus_10.setOnClickListener {
            mSong.let {
                onCallbackReceived.operation(command = SyncApi.CMD_SEEK_REVERSE)
            }
        }
        iv_seek_plus_10.setOnClickListener {
            mSong.let {
                onCallbackReceived.operation(command = SyncApi.CMD_SEEK_TO)
            }
        }
        iv_play_pause.tag = R.drawable.ic_pause
        iv_play_pause.setOnClickListener {
            val isPlaying =
                Utility.handleMediaStateDrawable(requireContext(), iv_play_pause)

            if (isPlaying){
                mSong.let {
                    onCallbackReceived.operation(command = SyncApi.CMD_PAUSE)
                }
            } else {
                mSong.let {
                    onCallbackReceived.operation(command = SyncApi.CMD_RESUME)
                }
            }
        }

        iv_lyrics.setOnClickListener {
            if (!TextUtils.isEmpty(mSong?.artist)) {
                vm.getLyrics(mSong?.provider!!, mSong?.artist!!, mSong?.title!!)
            } else {
                showSnackBar("Artist Not Found")
            }
        }

        iv_favourite.setOnClickListener {
            mManagePlaylist = false
            vm.getUserPlaylist()
        }
    }

    private val musicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    Log.d(TAG, "playlisy : "+it.toString())
                }
            }

            onSongClick = { index, song, playlistVH ->
                song.let {
//                    Log.d(TAG,"Song : "+ it.toString())
                    it.let { song ->
                        setTrack(song)
                        vm.addRecentlyPlayedSong(song)
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : ${it.entity}")

                }
            }
        }
    }

    private fun setTrack(song: Song){
        mSong = song
        showMusicInfo(song)

        // Set View for Youtube and Local Music
        when (song.provider) {
            PlayerProducer.YOUTUBE_PLAYER -> {
                onCallbackReceived.let {
                    onCallbackReceived.setYoutubeView(youtube_player_view)
                }
            }
            PlayerProducer.LOCAL_PLAYER -> { }
            PlayerProducer.SPOTIFY_PLAYER -> { }
        }

        // Sync Play
        mSong.let {
            onCallbackReceived.let {
                onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
            }
        }
    }

    private fun observer(){
        vm.groups.observe(viewLifecycleOwner, groupObserver)
        vm.newReleaseSongs.observe(viewLifecycleOwner, newReleaseSongObserver)
        vm.recentlyAdded.observe(viewLifecycleOwner, recentAddedObserver)
        vm.getAllPlaylist.observe(viewLifecycleOwner, playlistObserver)
        vm.getPlaylist.observe(viewLifecycleOwner, getPlaylist)
        vm.users.observe(viewLifecycleOwner, userObserver)
        vm.lyrics.observe(viewLifecycleOwner, lyricsObserver)
        vm.youtubeSongs.observe(viewLifecycleOwner, youtubeObserver)

        // Check Sync Server Connection
        vm.checkConnection()


        // Listen Group Notification
        MusicActivity.Events.groupServiceEvent.observe(viewLifecycleOwner,
            Observer<com.play.models.sync.Group> { profile ->
                if (profile != null) {
                    Log.d(TAG, profile.toString())
                    AwesomeDialog.build(requireActivity())
                        .title(getString(R.string.group_play_music))
                        .body(getString(R.string.request_to_join, profile.name))
                        .onPositive(getString(R.string.accept)) {
                            tvGroup!!.text = profile.name
                            // Make single event live data
                            MusicActivity.Events.groupServiceEvent.postValue(null)
                        }.onNegative(getString(R.string.decline)){
                            // Make single event live data
                            MusicActivity.Events.groupServiceEvent.postValue(null)
                        }
                }
            })
    }

    private fun removeObserver(){
        vm.groups.removeObserver(groupObserver)
        vm.newReleaseSongs.removeObserver(newReleaseSongObserver)
        vm.recentlyAdded.removeObserver(recentAddedObserver)
        vm.getAllPlaylist.removeObserver(playlistObserver)
        vm.getPlaylist.removeObserver(getPlaylist)
        vm.users.removeObserver(userObserver)
        vm.lyrics.removeObserver(lyricsObserver)
        vm.youtubeSongs.removeObserver(youtubeObserver)
    }

    private val groupObserver = Observer<ArrayList<Group>> { it ->
        it.add(0, Group("None", Utility.getUserId(requireContext()), arrayListOf()))
        val map = it.map {
            it.name
        }

        var myDialog: AlertDialog
        val myBuilder = AlertDialog.Builder(requireContext())
        myBuilder.setTitle(getString(R.string.select_group))
            .setItems(map.toTypedArray()) { dialogInterface, position ->
//                Toast.makeText(requireContext(), it[position].toString(), Toast.LENGTH_SHORT).show()

                var groupName = it[position].name
                sessionGraph.userPreferences.setSelectedGroupName(groupName)
                if (groupName != "None") {
                    val userIds = it[position].userInfo?.map {
                        it.userId
                    }
                    userIds?.let { _userIds ->
                        if (_userIds.isNotEmpty()) {
                            vm.getUsersByGroup(Utility.getUserId(requireContext()), _userIds as ArrayList<String>)
                        } else {
                            showSnackBar("User not assign to group")
                        }
                    }
                }
//
                if (tvGroup != null) {
                    tvGroup!!.text = it[position].name
                }
            }
        myDialog = myBuilder.create()
        myDialog.show()
    }

    private val newReleaseSongObserver = Observer<ArrayList<Song>> { it ->
        musicList.populateList(Playlist(-1, "New Release", it))
    }

    private val recentAddedObserver = Observer<List<Song>> { it ->
        it.let {
            if (it.isNotEmpty()) {
                musicList.populateList(Playlist(-2, "Recently Added", it))
            }
        }
    }

    private val getPlaylist = Observer<Playlist> { it ->
        it.let {
            Log.d(TAG, it.toString())
            mSelectedPlaylist = it

            musicList.populateList(it)
        }
    }

    private val playlistObserver = Observer<List<Playlist>> { it ->
        Log.d(TAG, it.toString())

        it.let { playlists ->

            val map = playlists.map { it.name }
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Choose an playlist")
            builder.setItems(map.toTypedArray()) { dialog, which ->
                if (mManagePlaylist) {
                    // Play Playlist
                    vm?.getUserPlaylistById(playlists.get(which).id)
                }
                else {
                    if (mSong != null) {
                        // Add Song
                        vm?.managePlaylist(playlists.get(which).id, mSong!!.id)
                    } else {
                        Log.e(TAG, "Please select song to add to playlist")
                    }
                }
            }
            val dialog = builder.create()
            dialog.show()
        }
    }

    private val userObserver = Observer<ArrayList<User>> { it ->
        Log.d(TAG, it.toString())
        // Manage Group FCM Token for Sync Communication
        sessionGraph.userPreferences.clearGroupToken()
        var userIds = arrayListOf<String>()
        it.forEach {
            userIds.add(it.userId!!)
            if (it.userId != Utility.getUserId(requireContext())) {
                it.pushId?.let { pushId ->
                    sessionGraph.userPreferences.setGroupFCMToken(pushId)
                }
            }
        }

        if (userIds.isNotEmpty())
            vm.requestUserToJoin(requireContext(), userIds)
        else
            Log.e(TAG, "Users Id is empty")
    }

    private val lyricsObserver = Observer<String> { it ->
        Log.d(TAG, it)
        it.let {lyrics ->
            lyrics.apply {
                val alert = AlertDialog.Builder(requireContext())
                val textview = TextView(requireContext())
                textview.text = this.replace("\\n", System.getProperty("line.separator"))
                alert.setMessage("Lyrics : ")
                alert.setView(textview)
                alert.show()
            }
        }
    }

    private val youtubeObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(-3, "Youtube Recommendation", songs))
        }
    }

    private fun showMusicInfo(song: Song){
        setTitle(song.title)
        Glide.with(requireActivity())
            .load(song.image)
            .error(R.drawable.ic_placeholder)
            .into(ivTrackImage);
        when (song.provider) {
            PlayerProducer.YOUTUBE_PLAYER -> {
                youtube_player_view!!.visibility = View.VISIBLE
                ivTrackImage.visibility = View.GONE
                video_view.visibility = View.GONE
            }
            PlayerProducer.LOCAL_PLAYER -> {
                youtube_player_view.visibility = View.GONE
                ivTrackImage.visibility = View.GONE
                video_view.visibility = View.VISIBLE
            }
            PlayerProducer.SPOTIFY_PLAYER -> {
                youtube_player_view.visibility = View.GONE
                ivTrackImage.visibility = View.VISIBLE
                video_view.visibility = View.GONE
            }
        }
    }
}