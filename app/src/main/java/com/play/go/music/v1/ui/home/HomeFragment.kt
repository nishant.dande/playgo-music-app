package com.play.go.music.v1.ui.home

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.play.go.music.R
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.base.BaseFragment
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.models.music.*
import com.play.models.playlist.ManagePlaylist
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    val TAG = "v1HomeFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: OnCallbackReceived
    private lateinit var activity: PlayerControlActivity

    private val vm by lazy {
        sessionGraph.let { session ->
            networkGraph.let { networkGraph ->
                getViewModel { HomeViewModel(session, networkGraph) }
            }
        }
    }

    private var mSong: Song? = null
    private var mSelectedPlaylist: Playlist? = null
    private var mManagePlaylist : Boolean = false

    override fun getTab(): Int {
        return R.string.home
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.getYoutubeRecommendation()
        vm.getYoutubeTrendingSongs()
        vm.getYoutubeSongsByLanguage(null, "hindi")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_v1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProfileTitle("John")
        observer()

        //Set list view for music list
        rvRecentMusics!!.layoutManager = LinearLayoutManager(requireContext())
        rvRecentMusics!!.adapter = musicList


        setClickListener()
    }

    override fun onResume() {
        super.onResume()
        vm.getSpotifyTrending()
        vm.getSpotifyTopPicks()
        vm.getLatestInHindi()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            onCallbackReceived = context as OnCallbackReceived
            activity = context
//            (context as PlayerControlActivity).showMiniControl(false)
        }

    }


    override fun onDestroyView() {
        super.onDestroyView()
        if (onCallbackReceived != null) {
            onCallbackReceived.let {
//                it.onDestroyView()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        removeObserver()
    }

    private fun setClickListener() {


    }

    private val musicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    mSelectedPlaylist = it.entity
                    onCallbackReceived.let {
                        it.setCurrentPlaylist(ManagePlaylist(mSelectedPlaylist!!, 0))
                    }
                    Log.d(TAG, "playlisy : "+it.toString())
                }
            }

            onSeeAllPlaylistClick = { name , it ->
                navigateToSeeAll(it.entity!!)
            }

            onSongClick = { index, song, playlistVH ->
                Log.d(TAG, "Index : ${index} -> ${song.title}")
                playlistVH.let {item ->
                    Log.d(TAG, item.entity?.name!!)
                    onCallbackReceived.let {
                        it.setCurrentPlaylist(ManagePlaylist(item.entity!!, index))
                    }
                }

                song.let {
//                    Log.d(TAG,"Song : "+ it.toString())
                    it.let { item ->
                        setTrack(item)
//                        vm.addRecentlyPlayedSong(song)
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : ${it.entity}")

                }
            }
        }
    }

    private fun observer(){
        vm.youtubeSongs.observe(viewLifecycleOwner, youtubeObserver)
        vm.youtubeTrendingSongs.observe(viewLifecycleOwner, youtubeTrendingObserver)
        vm.topPicks.observe(viewLifecycleOwner, topPicksObserver)
        vm.trending.observe(viewLifecycleOwner, trendingObserver)
        vm.latestInHindi.observe(viewLifecycleOwner, latestInHindiObserver)
        vm.youtubeSongsByLanguage.observe(viewLifecycleOwner, youtubeSongsByLanguageObserver)

        // Check Sync Server Connection
//        vm.checkConnection()

    }

    private fun removeObserver(){
        vm.youtubeSongs.removeObserver(youtubeObserver)
        vm.youtubeTrendingSongs.removeObserver(youtubeTrendingObserver)
        vm.topPicks.removeObserver(topPicksObserver)
        vm.trending.removeObserver(trendingObserver)
        vm.latestInHindi.removeObserver(latestInHindiObserver)
        vm.youtubeSongsByLanguage.removeObserver(youtubeSongsByLanguageObserver)
    }

    private val youtubeObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(2, getString(R.string.recommendations), songs,
                orientation = Orientation.HORIZONTAL_WIDE))
        }
    }
    private val youtubeTrendingObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(3, getString(R.string.trending), songs,
                orientation = Orientation.HORIZONTAL_SQUARE))
        }
    }
    private val topPicksObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(1, getString(R.string.top_picks), songs))
        }
    }
    private val trendingObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(4, getString(R.string.trending), songs,
                orientation = Orientation.HORIZONTAL_SQUARE))
        }
    }

    private val latestInHindiObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(6, getString(R.string.latest_in_hindi), songs, orientation = Orientation.HORIZONTAL_SQUARE))
        }
    }

    private val youtubeSongsByLanguageObserver = Observer<ArrayList<Song>> { it ->
        it.let {songs ->
            musicList.populateList(Playlist(5, getString(R.string.latest_in_hindi), songs,
                orientation = Orientation.HORIZONTAL_SQUARE))
        }
    }

    private fun setTrack(song: Song){
        mSong = song

        // Sync Play
        mSong.let {
            onCallbackReceived.let {
                onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
                activity.showMiniControl(true)
            }
        }
    }

    interface OnCallbackReceived {
        fun operation(song: Song? = null, command: String);
        fun setYoutubeView(youTubePlayerView: ConstraintLayout);
        fun onDestroyView()
        fun onResumeView(container: ConstraintLayout)
        fun getCurrentSong() : Song?
        fun getCurrentPlaylist() : ManagePlaylist?
        fun setCurrentPlaylist(managePlaylist: ManagePlaylist)
    }


    private fun navigateToSeeAll(playlist: Playlist){
        if (playlist.name.toLowerCase().contains("latest")){
            findNavController().navigate(R.id.action_homeFragment_to_seeAllFragment)
        } else {
            val bundle = bundleOf("playlist" to Gson().toJson(playlist))
            findNavController().navigate(R.id.action_homeFragment_to_seeAllFragment, bundle)
        }
    }
}