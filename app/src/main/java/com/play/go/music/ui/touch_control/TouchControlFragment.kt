package com.play.go.music.ui.touch_control

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.play.ble_service.headless.fragment.ServiceFragment
import com.play.ble_service.service.BluetoothService
import com.play.go.music.R
import com.play.go.music.ui.adapter.custom.CustomAdapter
import com.play.go.music.ui.adapter.custom.SwitchVH
import com.play.go.music.ui.adapter.custom.TextArrowVH
import com.play.go.music.ui.adapter.custom.TextVH
import com.play.go.music.v1.ui.gaia.TouchControlGaiaManager
import com.play.models.device.CustomItem
import com.qualcomm.qti.libraries.gaia.GAIA
import com.qualcomm.qti.libraries.gaia.GaiaException
import com.qualcomm.qti.libraries.gaia.GaiaUtils
import com.qualcomm.qti.libraries.gaia.packets.GaiaPacket
import kotlinx.android.synthetic.main.fragment_touch_control.*

class TouchControlFragment : ServiceFragment(), TouchControlGaiaManager.GaiaManagerListener {

    val TAG = "TouchControlFragment"

//    override fun getTab(): Int {
//        return R.string.touch_controls
//    }

    private var mGaiaManager: TouchControlGaiaManager? = null
    // Initialize an array of controls
    val controlSet = arrayOf(
            "Play/Pause/Call Receive/Call Disconnect",
            "Music Modes",
            "Next Song/Prev Song/Pairing",
            "Voice Assistant/Call Reject"
    )

    var eventPackets = ByteArray(4)
    var controlPackets = ByteArray(4)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_touch_control, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var devicefeatures : ArrayList<CustomItem> = ArrayList<CustomItem>()
        devicefeatures.add(CustomItem(getString(R.string.tap_sensor), showSwitch = true))
        devicefeatures.add(
            CustomItem(
                getString(R.string.single_tap),
                ShowTextArrow = true,
                subTitle = "Play/Pause"
            )
        )
        devicefeatures.add(
            CustomItem(
                getString(R.string.double_tap),
                ShowTextArrow = true,
                subTitle = "Call Attends"
            )
        )
        devicefeatures.add(
            CustomItem(
                getString(R.string.triple_tap),
                ShowTextArrow = true,
                subTitle = "Next Track"
            )
        )
        devicefeatures.add(
            CustomItem(
                getString(R.string.hold_tap),
                ShowTextArrow = true,
                subTitle = "Next Track"
            )
        )
        devicefeatures.add(CustomItem(getString(R.string.reset_to_default), ShowText = true))
        customs.populateList(devicefeatures)
    }

    override fun handleMessageFromService(msg: Message?) {
        if(msg != null && msg.obj != null) {
            Log.d(TAG,
                    "In MultipointConnectionFeatureActivity " + msg!!.what + " "+ msg.obj.toString())
            //noinspection UnusedAssignment
            val handleMessage = "Handle a message from BLE service: "

            when (msg.what) {
                BluetoothService.Messages.CONNECTION_STATE_HAS_CHANGED -> {
                    @BluetoothService.State val connectionState = msg.obj as Int
                    val stateLabel =
                            if (connectionState == BluetoothService.State.CONNECTED) "CONNECTED" else if (connectionState == BluetoothService.State.CONNECTING) "CONNECTING" else if (connectionState == BluetoothService.State.DISCONNECTING) "DISCONNECTING" else if (connectionState == BluetoothService.State.DISCONNECTED) "DISCONNECTED" else "UNKNOWN"
                    displayLongToast(getString(R.string.toast_device_information) + stateLabel)
                    if (DEBUG) Log.d(
                            TAG,
                            handleMessage + "CONNECTION_STATE_HAS_CHANGED: " + stateLabel
                    )
                }
                BluetoothService.Messages.DEVICE_BOND_STATE_HAS_CHANGED -> {
                    val bondState = msg.obj as Int
                    val bondStateLabel =
                            if (bondState == BluetoothDevice.BOND_BONDED) "BONDED" else if (bondState == BluetoothDevice.BOND_BONDING) "BONDING" else "BOND NONE"
                    displayLongToast(getString(R.string.toast_device_information) + bondStateLabel)
                    if (DEBUG) Log.d(
                            TAG,
                            handleMessage + "DEVICE_BOND_STATE_HAS_CHANGED: " + bondStateLabel
                    )
                }
                BluetoothService.Messages.GATT_SUPPORT -> if (DEBUG) Log.d(
                        TAG,
                        handleMessage + "GATT_SUPPORT"
                )
                BluetoothService.Messages.GAIA_PACKET -> {
                    val data = msg.obj as ByteArray
                    mGaiaManager!!.onReceiveGAIAPacket(data)
                }
                BluetoothService.Messages.GAIA_READY -> if (DEBUG) Log.d(
                        TAG,
                        handleMessage + "GAIA_READY"
                )
                BluetoothService.Messages.GATT_READY -> if (DEBUG) Log.d(
                        TAG,
                        handleMessage + "GATT_READY"
                )
                else -> if (DEBUG) Log.d(
                        TAG,
                        handleMessage + "UNKNOWN MESSAGE: " + msg.what
                )
            }
        } else {
            Log.d(TAG, "msg object null")
        }
    }

    override fun onServiceConnected() {
        @GAIA.Transport val transport =
            if (getTransport() === BluetoothService.Transport.BR_EDR) GAIA.Transport.BR_EDR else GAIA.Transport.BLE
        mGaiaManager = TouchControlGaiaManager(this, transport)
        mGaiaManager?.getInformation()
    }

    override fun onServiceDisconnected() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvTouchControl!!.layoutManager = LinearLayoutManager(requireContext())
        rvTouchControl!!.adapter = customs
//        mock()
    }

    private fun mock() {
        // Mock
        val packets = byteArrayOf(
            0xFF.toByte(),
            0x01.toByte(),
            0x00.toByte(),
            0x0A.toByte(),
            0x00.toByte(),
            0x0A.toByte(),
            0x02.toByte(),
            0x87.toByte(),
            0x00.toByte(),
            0x00,
            TouchControlGaiaManager.Controls.SINGLE_TAP.toByte(),
            TouchControlGaiaManager.Controls.PPCC.toByte(),
            TouchControlGaiaManager.Controls.DOUBLE_TAP.toByte(),
            TouchControlGaiaManager.Controls.MM.toByte(),
            TouchControlGaiaManager.Controls.TRIPLE_TAP.toByte(),
            TouchControlGaiaManager.Controls.VC.toByte(),
            TouchControlGaiaManager.Controls.HOLD.toByte(),
            TouchControlGaiaManager.Controls.NPP.toByte()
        )
        onServiceConnected();
        mGaiaManager?.onReceiveGAIAPacket(packets);
    }


    private val customs by lazy {
        CustomAdapter().apply {
            onPlaylistClicked = {
                it.let {
                    if (it is TextArrowVH){
                        var item = it as TextArrowVH
                        Log.d(TAG, item.toString())
                        showDialog(item.entity!!)
                    }

                    if (it is TextVH){
                        resetSetting()
                    }
                }
            }

            onCheckClick = { it, switchState ->
                if (switchState == SwitchVH.SwitchState.onChecked){
                    requireContext().toast("Tap Sensor Enabled")
                } else if (switchState == SwitchVH.SwitchState.onReset){
                    requireContext().toast("Tap Sensor Disabled")
                } else {

                }
            }
        }
    }

    private fun resetSetting() {
        mGaiaManager.let {
            val payload = ByteArray(8)
            payload[0] = TouchControlGaiaManager.Controls.SINGLE_TAP.toByte()
            payload[1] = TouchControlGaiaManager.Controls.PPCC.toByte()
            payload[2] = TouchControlGaiaManager.Controls.DOUBLE_TAP.toByte()
            payload[3] = TouchControlGaiaManager.Controls.MM.toByte()
            payload[4] = TouchControlGaiaManager.Controls.TRIPLE_TAP.toByte()
            payload[5] = TouchControlGaiaManager.Controls.NPP.toByte()
            payload[6] = TouchControlGaiaManager.Controls.HOLD.toByte()
            payload[7] = TouchControlGaiaManager.Controls.VC.toByte()
            it?.sendControlCommand(payload)
        }
    }

    override fun sendGAIAPacket(packet: ByteArray?): Boolean {
        return mService != null && mService!!.isGaiaReady && mService!!.sendGAIAPacket(packet)
    }

    override fun onTouchControlNotSupported() {
        Log.d(TAG, "The touch control feature is not supported")
    }

    override fun getGAIAPacket(packet: GaiaPacket?) {
        try {
            Log.d(
                TAG, "getGAIAPacket : " + GaiaUtils.getHexadecimalStringFromBytes(packet!!.bytes)
            )
            if (packet!!.bytes.size == 18) {
                val payload = ByteArray(9)
                //            payload = packet.getBytes();
                System.arraycopy(packet!!.bytes, 9, payload, 0, 9)
                var eventCount = 0
                var controlCount = 0
                eventPackets[0] = payload[1]
                ++eventCount
                for (i in 2 until payload.size) {
                    if (i % 2 == 0) {
                        controlPackets[controlCount] = payload[i]
                        ++controlCount
                    } else {
                        eventPackets[eventCount] = payload[i]
                        ++eventCount
                    }
                }
                printControl()

                // Update Ui
                updateUi()
            } else {
                Log.d(TAG, "Packet are not supported")
            }
        } catch (e: GaiaException) {
            e.printStackTrace()
        }
    }

    private fun updateUi() {
        if (controlPackets.isNotEmpty()){
            controlPackets.forEachIndexed { index, byte ->
//                Log.d(TAG, "index - ${index}, byte - ${controlSet[byte.toInt()-1]}")
                val subTitle = controlSet[byte.toInt()-1]
                when(index){
                    0->{
                        customs.updateSubTitle(CustomItem(
                                getString(R.string.single_tap),
                                ShowTextArrow = true,
                                subTitle = subTitle
                        ))
                    }
                    1->{
                        customs.updateSubTitle(CustomItem(
                                getString(R.string.double_tap),
                                ShowTextArrow = true,
                                subTitle = subTitle
                        ))
                    }
                    2->{
                        customs.updateSubTitle(CustomItem(
                                getString(R.string.triple_tap),
                                ShowTextArrow = true,
                                subTitle = subTitle
                        ))
                    }
                    3->{
                        customs.updateSubTitle(CustomItem(
                                getString(R.string.hold_tap),
                                ShowTextArrow = true,
                                subTitle = subTitle
                        ))
                    }
                }

            }
        }
    }

    private fun printControl() {
        Log.d(TAG, "Event Packet : " + GaiaUtils.getHexadecimalStringFromBytes(eventPackets))
        Log.d(TAG, "Control Packet : " + GaiaUtils.getHexadecimalStringFromBytes(controlPackets))
    }

    // Method to show an alert dialog with single choice list items
    private fun showDialog(event: CustomItem){
        var controlByte = getControlByte(event.subTitle!!)-1 ?: 0


        lateinit var dialog:AlertDialog
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Choose a control.")
        builder.setSingleChoiceItems(controlSet, controlByte) { _, which ->
            // Get the dialog selected item
            val control = controlSet[which]
            try {
                requireContext().toast("$event $control control selected.")
                when(event.name){
                    getString(R.string.single_tap) -> {
                        controlPackets[0] = getControlByte(control).toByte()
                    }
                    getString(R.string.double_tap) -> {
                        controlPackets[1] = getControlByte(control).toByte()
                    }
                    getString(R.string.triple_tap) -> {
                        controlPackets[2] = getControlByte(control).toByte()
                    }
                    getString(R.string.hold_tap) -> {
                        controlPackets[3] = getControlByte(control).toByte()
                    }
                }

                sendCommand()

                // update ui
                mGaiaManager?.getInformation()
            } catch (e: IllegalArgumentException) {
                requireContext().toast("$control control not supported.")
            }
            dialog.dismiss()
        }
        dialog = builder.create()
        dialog.show()
    }

    // Extension function to show toast message
    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun sendCommand(){
        mGaiaManager.let {
            printControl()
            val payload = ByteArray(8)
            payload[0] = eventPackets[0]
            payload[1] = controlPackets[0]
            payload[2] = eventPackets[1]
            payload[3] = controlPackets[1]
            payload[4] = eventPackets[2]
            payload[5] = controlPackets[2]
            payload[6] = eventPackets[3]
            payload[7] = controlPackets[3]
            it?.sendControlCommand(payload)
        }
    }

    private fun getControlByte(radioGroup: String): Int {
        when (radioGroup) {
            controlSet[0] -> {
                return TouchControlGaiaManager.Controls.PPCC
            }
            controlSet[1] -> {
                return TouchControlGaiaManager.Controls.MM
            }
            controlSet[2] -> {
                return TouchControlGaiaManager.Controls.NPP
            }
            controlSet[3] -> {
                return TouchControlGaiaManager.Controls.VC
            }
        }
        return 0
    }
}