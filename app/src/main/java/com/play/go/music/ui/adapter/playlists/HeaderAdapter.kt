package com.play.go.music.ui.adapter.playlists

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.play.models.music.Playlist


class HeaderAdapter  : ListAdapter<Pair<String, List<Playlist>>?, HeaderVH>(MUSIC_DIFF_CALLBACK) {

    var onPlaylistClick: ((PlaylistVH) -> (Unit))? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeaderVH {
                return HeaderVH.create(parent, viewType)
                    .apply {
                        this.onRedirectClick = onPlaylistClick
                    }
    }

    override fun onBindViewHolder(holder: HeaderVH, position: Int) {
        holder.entity = getItem(position)
    }

    fun populateList(settings: ArrayList<Pair<String, List<Playlist>>?>){
        var items = settings
        if (currentList.size > 0){
            if (currentList.size == 1){
                items.add(0, currentList.toList()[0] as Pair<String, List<Playlist>>? )
            } else{
                items.addAll(0,currentList.toList() as ArrayList<Pair<String, List<Playlist>>?> )
            }
        }
        submitList(items)
    }

    fun populateList(settings: Pair<String, List<Playlist>>){
        var items = arrayListOf<Pair<String, List<Playlist>>>()
        if (currentList.size > 0){
            val data = currentList.toList()
            data.forEach {
                if (it?.first == settings.first){
                    var playlists = it.second as ArrayList
                    val filter = playlists.filter {playlist ->
                        !settings.second.contains(playlist)
                    }
                    playlists.apply {
                        addAll(filter)
                    }
                }
            }
            items.add(data[0]!!)
        } else {
            items.add(0, settings)
        }
        submitList(items as List<Pair<String, List<Playlist>>?>?)
        notifyDataSetChanged()
    }

}


val MUSIC_DIFF_CALLBACK = object : DiffUtil.ItemCallback<Pair<String, List<Playlist>>?>() {

    override fun areItemsTheSame(oldItem: Pair<String, List<Playlist>>,
                                 newItem: Pair<String, List<Playlist>>): Boolean {
        return oldItem.first == oldItem.first
    }

    override fun areContentsTheSame(oldItem: Pair<String, List<Playlist>>,
                                    newItem: Pair<String, List<Playlist>>): Boolean {
        return oldItem.first == newItem.first
    }

}