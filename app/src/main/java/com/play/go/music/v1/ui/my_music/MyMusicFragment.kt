package com.play.go.music.v1.ui.my_music;

import android.content.Context
import android.os.Bundle;
import android.text.TextUtils
import android.util.Log

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson

import com.play.go.music.R;
import com.play.go.music.extensions.getViewModel
import com.play.go.music.graph
import com.play.go.music.ui.adapter.my_music.MusicListAdapter
import com.play.go.music.ui.adapter.my_music.TopSongAdapter
import com.play.go.music.ui.adapter.playlists.HeaderAdapter
import com.play.go.music.ui.base.BaseFragment;
import com.play.go.music.v1.ui.base.controller.PlayerControlActivity
import com.play.go.music.v1.ui.home.HomeFragment
import com.play.models.music.Orientation
import com.play.models.music.Playlist
import com.play.models.music.Song
import com.play.models.playlist.ManagePlaylist
import com.play.networking.api.sync.SyncApi
import kotlinx.android.synthetic.main.fragment_v1_my_music.*
import ru.tinkoff.scrollingpagerindicator.ViewPagerAttacher

class MyMusicFragment : BaseFragment() {

    val TAG = "v1MyMusicFragment"

    private val networkGraph  by lazy { context?.graph()!!.networkGraph }
    private val sessionGraph by lazy { context?.graph()!!.sessionGraph }

    private lateinit var onCallbackReceived: HomeFragment.OnCallbackReceived
    private lateinit var activity: PlayerControlActivity
    var songs = arrayListOf<Song>()
    private lateinit var song : Song
    var playlists = arrayListOf<Playlist>()
    var recentlyAddedPlaylist: Playlist? = null

    private val vm by lazy {
            sessionGraph.let { session ->
                    networkGraph.let { networkGraph ->
                    getViewModel { MyMusicViewModel(session, networkGraph) }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.initializeRecentlyAddedSongs()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView")
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_v1_my_music, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is PlayerControlActivity){
            onCallbackReceived = context as HomeFragment.OnCallbackReceived
            activity = context
//            (context as PlayerControlActivity).showMiniControl(false)
        }

    }


    override fun getTab(): Int {
        return R.string.my_music;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setProfileTitle("")
        observer()

        //Set list view for music list
        rvRecentMusics!!.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvRecentMusics!!.adapter = topPlayedSong
        indicator.attachToRecyclerView(rvRecentMusics)
        PagerSnapHelper().attachToRecyclerView(rvRecentMusics)
        rvRecentMusics.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val offset = recyclerView.computeHorizontalScrollOffset();
                val myCellWidth = recyclerView.getChildAt(0).measuredWidth
                if (offset % myCellWidth == 0) {
                    val position = offset / myCellWidth ;
                    updateSelectedItem(topPlayedSong.getItemByPosition(position))
                }
            }
        })

        rvPlaylistMusic!!.layoutManager = LinearLayoutManager(requireContext())
        rvPlaylistMusic!!.adapter = myPlaylistList

        rvLikedMusic!!.layoutManager = LinearLayoutManager(requireContext())
        rvLikedMusic!!.adapter = likedMusicList

        vm.getUserPlaylist()
        setClickListener()
    }

    private fun setClickListener() {
        tvCreatePlaylist.setOnClickListener {
            val alert = AlertDialog.Builder(requireContext())
            val edittext = EditText(requireContext())
            alert.setMessage(getString(R.string.enter_playlist_name))
            alert.setView(edittext)
            alert.setPositiveButton(getString(R.string.yes)) { dialog, whichButton ->
                val name: String = edittext.text.toString()
                if (!TextUtils.isEmpty(name)){
                    vm.createPlaylist(name)
                }
            }
            alert.setNegativeButton(getString(R.string.no)) {
                    dialog, whichButton ->
                // what ever you want to do with No option.
            }
            alert.show()
        }

        ivSongImage.setOnClickListener {
            if (song != null)
                setTrack(song)
        }

        tvSeeAll.setOnClickListener {
            if (recentlyAddedPlaylist != null) {
                navigateToSeeAll(recentlyAddedPlaylist!!)
            }
       }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        removeObserver()
    }


    private fun observer(){
        vm.recentlyAdded.observe(viewLifecycleOwner, recentAddedObserver)
        vm.getAllPlaylist.observe(viewLifecycleOwner, allPlaylistObserver)
        vm.getPlaylist.observe(viewLifecycleOwner, playlistObserver)
        vm.getSong.observe(viewLifecycleOwner, songObserver)

        val likedSongIds = sessionGraph.userPreferences.getLikedSongIds()
        if (likedSongIds.isNotEmpty()){
            likedSongIds.forEach {
                vm.getSongById(it)
            }
        }
    }

    private fun removeObserver(){
        vm.recentlyAdded.removeObserver(recentAddedObserver)
        vm.getAllPlaylist.removeObserver(allPlaylistObserver)
        vm.getPlaylist.removeObserver(playlistObserver)
        vm.getSong.removeObserver(songObserver)
    }

    private val topPlayedSong by lazy {
        TopSongAdapter().apply {
            onClick = {
                if (it != null){
                    updateSelectedItem(song = it.entity!!)

                    // Play Track
                    setTrack(it.entity!!)
                }
            }
        }
    }

    private fun updateSelectedItem(song: Song){
        this.song = song
        Glide.with(requireActivity())
            .load(song.image)
            .into(ivSongImage)
    }

    private val myPlaylistList by lazy {
        HeaderAdapter().apply {
            onPlaylistClick = {item ->
                onCallbackReceived.let {
                    if (item.entity!!.songs.isNotEmpty()) {
                        it.setCurrentPlaylist(ManagePlaylist(item.entity!!, 0))
                        setTrack(item.entity!!.songs.get(0))
                    }
                }
            }
        }
    }

    private val likedMusicList by lazy {
        MusicListAdapter().apply {
            onPlaylistClick = {
                it.let {
                    onCallbackReceived.let {
                    }
                    Log.d(TAG, "playlisy : "+it.toString())
                }
            }

            onSeeAllPlaylistClick = { name , it ->
                Log.d(TAG, "See All : "+it)
                if (it.entity != null) {
                   navigateToSeeAll(it.entity!!)
                }
            }

            onSongClick = { index, song, playlistVH ->
                song.let {
//                    Log.d(TAG,"Song : "+ it.toString())
                    it.let { song ->
                        setTrack(song)
                    }
                }
            }

            onSongLongClick = {
                it.let {
                    Log.d(TAG, "Song Long : ${it.entity}")

                }
            }
        }
    }


    private val recentAddedObserver = Observer<List<Song>> { it ->
        it.let {
            if (it.isNotEmpty()) {

                recentlyAddedPlaylist = Playlist(1, getString(R.string.recently_played), it,
                    orientation = Orientation.VERTICAL)
//                musicList.populateList(Playlist(1, "Recently Played", it,
//                    orientation = Orientation.HORIZONTAL_SQUARE))

                topPlayedSong.populateList(it as ArrayList<Song>)
                updateSelectedItem(topPlayedSong.getItemByPosition(0))
            }
        }
    }

    private val allPlaylistObserver = Observer<List<Playlist>> { it ->
        Log.d(TAG, it.toString())
        if (it.isNotEmpty()){
            it.forEach {
                vm.getUserPlaylistById(it.id)

            }
        }
    }


    private val playlistObserver = Observer<Playlist> { it ->
        it.let {playlistItem ->
            Log.d(TAG, playlistItem.toString())
            val filterItem = playlists.filter { playlist ->
                playlist.id == playlistItem.id
            }
            if (filterItem.isNotEmpty()){
                // Exist
                filterItem[0].apply {
                    this.songs = playlistItem.songs
                }
            } else {
                // New
                playlists.add(playlistItem)
            }
        }
        var item = Pair<String, List<Playlist>>("My Playlists", playlists)
        myPlaylistList.populateList(item)
    }


    private val songObserver = Observer<Song> { it ->
        it.let {
            val any = songs.any {song ->
                song.uri == it.uri
            }
            if (!any)
                songs.add(it)
        }

        likedMusicList.populateList(Playlist(1, "Liked Songs", songs,
            orientation = Orientation.HORIZONTAL_SQUARE))
    }

    private fun setTrack(song: Song){
        // Sync Play
        song.let {
            onCallbackReceived.let {
                onCallbackReceived.operation(song, SyncApi.CMD_PLAY)
                activity.showMiniControl(true)
            }
        }
    }

    private fun navigateToSeeAll(playlist: Playlist){
        val bundle = bundleOf("playlist" to Gson().toJson(playlist))
        findNavController().navigate(R.id.action_myMusicFragment_to_seeAllFragment, bundle)
    }

}