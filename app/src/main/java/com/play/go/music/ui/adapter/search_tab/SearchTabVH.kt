package com.play.go.music.ui.adapter.search_tab

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.play.go.music.R
import com.play.models.music.Song
import kotlinx.android.synthetic.main.item_search_tab.view.*

class SearchTabVH private constructor(itemView: View, context: Context) : RecyclerView.ViewHolder(itemView)   {

    var onClickVH: ((SearchTabVH) -> Unit)? = null
    var context: Context = context

    var entity: Song? = null
        set(value) {
            field = value
            value?.let { item ->
                itemView.tvTitle.text = item.title
//                itemView.tvDescription.text = item.description
//                item.publishedAt.let {
//                    itemView.tvPublishAt.text = Utility.getDateMonthYear(it!!)
//                }
                if (!TextUtils.isEmpty(item.image)) {
                    Glide.with(context)
                        .load(item.image)
                        .into(itemView.ivTrackImage)
                }
                itemView.setOnClickListener { onClickVH?.invoke(this) }
            }
        }

    companion object Factory {
        fun create(parent: ViewGroup, viewType: Int): SearchTabVH {
            return SearchTabVH(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_search_tab,
                    parent,
                    false
                ),parent.context
            )
        }
    }
}

