package com.play.networking.error

data class SpotifyError(
    val error: Error
) {
    data class Error(
        val message: String,
        val status: Int
    )
}