package com.play.networking.error

import com.google.gson.Gson
import com.play.models.auth.Response
import retrofit2.HttpException

class ErrorHandler {

    companion object{

        fun error(throwable: Throwable): APIError {
            if (throwable is HttpException){
                if (throwable.code() == APIError.ERROR_BAD_GATEWAY)
                    return APIError(Exception(), "Unable to connect to Server")
                var errorBody = throwable.response()?.errorBody()
                var errorMsg = Gson().fromJson<SpotifyError>(errorBody?.string(), SpotifyError::class.java)
                return APIError(Exception(), errorMsg)
            }
            return APIError(Exception(), throwable.message)
        }

        fun serverError(throwable: Throwable): APIError {
            if (throwable is HttpException){
                if (throwable.code() == APIError.ERROR_BAD_GATEWAY)
                    return APIError(Exception(), "Unable to connect to Server")

                if (throwable.code() == APIError.ERROR_NOT_FOUND)
                    return APIError(Exception(), "Server Not Found")
                var errorBody = throwable.response()?.errorBody()
                return APIError(Exception(), errorBody.toString())
            }
            return APIError(Exception(), throwable.message)
        }

        fun authServerError(throwable: Throwable): APIError {
            if (throwable is HttpException){
                if (throwable.code() == APIError.ERROR_BAD_GATEWAY)
                    return APIError(Exception(), "Unable to connect to Server")

                if (throwable.code() == APIError.ERROR_NOT_FOUND)
                    return APIError(Exception(), "Server Not Found")
                if (throwable.code() == APIError.ERROR_PAYMENT_REQUIRED)
                    return APIError(Exception(), "Server Error - Please restart")
                var errorBody = throwable.response()?.errorBody()
                var errorMsg = Gson().fromJson<Response>(errorBody?.string(), Response::class.java)
                return APIError(Exception(), errorMsg.message)
            }
            return APIError(Exception(), throwable.message)
        }
    }
}