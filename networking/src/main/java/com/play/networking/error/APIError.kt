package com.play.networking.error

import android.util.Log
import java.lang.Exception

class APIError(var exception: Exception, val errorMessage: String?) :
    Exception(errorMessage, exception) {

    constructor(exception: Exception, errorMessage: SpotifyError) :
            this(exception, errorMessage.error.message)
    init {
        Log.e("APIError", errorMessage, exception )
    }

    companion object{
        const val ERROR_BAD_GATEWAY = 502
        const val ERROR_NOT_FOUND = 404
        const val ERROR_PAYMENT_REQUIRED = 402
    }
}