package com.play.networking.utils

import android.os.Environment
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class Utility {
    companion object{
        var DATE_MONTH_YEAR : String = "dd MMM yyyy"
        var YEAR_MONTH_DATE : String = "yyyy-MM-dd"
        var YEAR_MONTH_DATE_THH_MM_mm_ss_Z : String = "yyyy-MM-dd'T'HH:mm:ss'Z'"

        fun defaultLocalPath(): String{
            var gpath: String = Environment.getExternalStorageDirectory().absolutePath
            var spath = "Download"
            var fullpath = File(gpath + File.separator + spath)
            return fullpath.toString()
        }

        /**
         * Get Current Date in YYYY-MM-DD
         */
        fun getCurrentDate() : String{
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            return parseDate(YEAR_MONTH_DATE, year, month, day)
        }

        /**
         * Get the String Date format ('dd MMM yyyy')
         */
        fun getDateMonthYear(dateString: String) : String{
            val format2 = SimpleDateFormat(YEAR_MONTH_DATE)
            val dateFormatter = SimpleDateFormat(DATE_MONTH_YEAR)
            val date = format2.parse(dateString)
            return dateFormatter.format(date)
        }

        /**
         * Parse Date to get string date
         * parserString - date format string
         */
        fun parseDate(parserString: String, year: Int, month: Int, day: Int) : String{
            val calendar = Calendar.getInstance()
            calendar[year, month] = day
            val dateFormatter = SimpleDateFormat(parserString)
            return  dateFormatter.format(calendar.time)
        }

        /**
         * Get Current Date in yyyy-MM-dd'T'HH:mm:ss'Z'
         */
        fun getCurrentDateTimeZone() : String{
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            return parseDate(YEAR_MONTH_DATE_THH_MM_mm_ss_Z, year, month, day)
        }

        /**
         *
         */
        fun getPreviousThreeMonthDateTime() : String{
            val c = Calendar.getInstance()
            c.add(Calendar.MONTH, -3)
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            return parseDate(YEAR_MONTH_DATE_THH_MM_mm_ss_Z, year, month, day)
        }

        /**
         * Get Title Case for String
         */
        fun titleCase(string: String?) : String {
            if (string == null)
                return "NA"

            if (string == "NA")
                return string

            var text = string;
            if (text.contains("_")){
                text = text.replace("_", " ")
            }

            val words = text.split(" ").toMutableList()
            var output = ""
            for(word in words){
                output += word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase()+" "
            }
            return output.trim()
        }
    }
}