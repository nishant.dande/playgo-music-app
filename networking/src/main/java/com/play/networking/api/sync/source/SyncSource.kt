package com.play.networking.api.sync.source

import android.content.Context
import android.net.Uri
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.play.models.sync.*
import com.play.models.youtube.LatestSongs
import com.play.networking.Outcome
import com.play.networking.api.provideFirebaseStorage
import com.play.networking.api.provideSyncService
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import com.play.networking.utils.Utility.Companion.defaultLocalPath
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import java.io.File

class SyncSource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val sync by lazy {
        provideSyncService(retrofit)
    }


    private val storage by lazy {
        provideFirebaseStorage()
    }

    private val storageReference by lazy{
        storage.reference
    }

    suspend fun syncMedia(payload: MediaControlPayload): Outcome<SyncResponse> {
        var ret: Outcome<SyncResponse>
        val req = sync.sendMediaControlData(payload)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun checkConnection(): Outcome<JsonObject> {
        var ret: Outcome<JsonObject>
        val req = sync.checkConnection()
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun requestUserToJoin(groupPayload: GroupPayload): Outcome<SyncResponse> {
        var ret: Outcome<SyncResponse>
        val req = sync.requestUserToJoin(groupPayload)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun syncMediaSession(syncPayload: SyncPayload): Outcome<SyncResponse> {
        var ret: Outcome<SyncResponse>
        val req = sync.syncMediaSession(syncPayload)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    fun upload(path: String, fileProgressListener: FileProgressListener){
        var file = Uri.fromFile(File(path))
        val media = storageReference.child("audio/${file.lastPathSegment}")
        media.putFile(file).addOnProgressListener {
            it.let {
                fileProgressListener.progress(it.bytesTransferred, it.totalByteCount)
            }
        }.addOnCompleteListener {
            media.downloadUrl.addOnSuccessListener {
                it.let {uri ->
                    fileProgressListener.status(uri.toString())
                }
            }
        }.addOnFailureListener {
            fileProgressListener.error(it)
        }
    }

    fun download(path: String, name : String, fileProgressListener: FileProgressListener){
        val httpsReference = storage.getReferenceFromUrl(path)
        val file = File(defaultLocalPath(), name)
        httpsReference.getFile(file).addOnProgressListener {
            it.let {
                fileProgressListener.progress(it.bytesTransferred, it.totalByteCount)
            }
        }.addOnSuccessListener {
            it.let {
                fileProgressListener.status(file.absolutePath)
            }
        }.addOnFailureListener{
            fileProgressListener.error(it)
        }
    }

    interface FileProgressListener{
        fun progress(byteUploaded: Long, totalBytes: Long)
        fun status(status: String)
        fun error(e: Exception)
    }

    suspend fun getLatestSongByLanguage(context: Context? = null, language: String): Outcome<LatestSongs> {
        var ret: Outcome<LatestSongs>
        val req =
                sync.getSongsByLanguage(language)
        try {
            req.await().run {
                ret = if (this != null) {
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(java.lang.NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            ret = Outcome.failure(APIError(e, e.message))
        }
        return ret
    }

    suspend fun sendMessage(messagePayload: MessagePayload): Outcome<SyncResponse> {
        var ret: Outcome<SyncResponse>
        val req = sync.sendMessage(messagePayload)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }
}
