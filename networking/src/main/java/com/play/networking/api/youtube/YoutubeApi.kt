package com.play.networking.api.youtube

import com.play.networking.BuildConfig

object YoutubeApi {
    const val BASE_URL = BuildConfig.YOUTUBE_BASE_URL
//    const val API_KEY: String = "AIzaSyA0ZJ6kgy3RyKT-CxzvjUOqhFKN46lOPIk"
    const val API_KEY: String = BuildConfig.YOUTUBE_API_KEY
}
