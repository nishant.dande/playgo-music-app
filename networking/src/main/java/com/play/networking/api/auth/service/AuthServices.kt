package com.play.networking.api.auth.service

import com.play.models.auth.Request
import com.play.models.auth.Response
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthServices {


    @POST("/auth")
    fun login(@Body request: Request): Deferred<Response>

}