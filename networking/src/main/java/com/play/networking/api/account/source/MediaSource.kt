package com.play.networking.api.account.source

import android.net.Uri
import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.UploadTask
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.spotify.Token
import com.play.networking.Outcome
import com.play.networking.api.account.AccountApi.GROUP
import com.play.networking.api.account.AccountApi.USER
import com.play.networking.api.provideFirebaseDatabase
import com.play.networking.api.provideFirebaseStorage
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.File

class MediaSource() : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val storage by lazy {
        provideFirebaseStorage()
    }

    private val storageReference by lazy{
        storage.reference
    }

    fun upload(path: String): UploadTask {
        var file = Uri.fromFile(File(path))
        val media = storageReference.child("audio/${file.lastPathSegment}")
        return media.putFile(file)
    }


}
