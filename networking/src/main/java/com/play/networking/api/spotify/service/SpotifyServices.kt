package com.play.networking.api.spotify.service

import com.play.models.spotify.*
import kotlinx.coroutines.Deferred
import retrofit2.http.*

//https://developer.spotify.com/console/
interface SpotifyServices {

//    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/browse/new-releases")
    fun getNewReleases(
        @Query("limit") limit: String?, @Query("offset") offset: String?
    ): Deferred<Albums>

    //
    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/search")
    fun getSearch(@Query("q") q: String?,
        @Query("type") type: String?,
        @Query("limit") limit: String?,
        @Query("offset") offset: String?
    ): Deferred<Albums?>

    @FormUrlEncoded
    @POST("api/token")
    fun getRefreshToken(@Field("grant_type") grant_type: String = "client_credentials"): Deferred<Token?>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/browse/categories/toplists/playlists")
    fun getTopPick(@Query("country") country: String? = "IN"): Deferred<TopPicks?>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/playlists/37i9dQZEVXbMDoHDwVN2tF")
    fun getTrending(): Deferred<Trending?>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/playlists/37i9dQZF1DXd8cOUiye1o2")
    fun getLatestHindi(): Deferred<Trending?>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/browse/categories/bollywood/playlists?country=IN")
    fun getBollywodHindiSong(): Deferred<BollywoodCategory?>

    @Headers("Content-Type: application/json;charset=UTF-8")
    @GET("/v1/artists/{artist_id}/albums?market=IN")
    fun getAlbumsByArtists(@Path("artist_id") artist_id: String,
                           @Query("country") country: String? = "IN"): Deferred<ArtistAlbum?>
}