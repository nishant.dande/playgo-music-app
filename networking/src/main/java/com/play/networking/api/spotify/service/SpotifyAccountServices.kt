package com.play.networking.api.spotify.service

import com.play.models.spotify.Token
import kotlinx.coroutines.Deferred
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface SpotifyAccountServices {

    @FormUrlEncoded
    @POST("api/token")
    fun getRefreshToken(@Field("grant_type") grant_type: String = "client_credentials"): Deferred<Token?>
}