package com.play.networking.api.auth

import com.play.networking.BuildConfig

object AuthApi {
    const val BASE_URL = BuildConfig.PLAY_SERVER_URL
}
