package com.play.networking.api.sync

import com.play.networking.BuildConfig

object SyncApi {
    const val BASE_URL = BuildConfig.PLAY_SERVER_URL

    // Payload Type
    const val GROUP_PAYLOAD = "group_payload"
    const val MEDIA_PAYLOAD = "media_payload"
    const val SYNC_PAYLOAD = "sync_payload"
    const val MESSAGE_PAYLOAD = "message_payload"

    //Command
    const val CMD_SET_URI = "uri"
    const val CMD_PLAY = "play"
    const val CMD_PAUSE = "pause"
    const val CMD_STOP = "stop"
    const val CMD_RESUME= "resume"
    const val CMD_SEEK_TO = "seek_to"
    const val CMD_SEEK_REVERSE = "seek_reverse"
    const val CMD_DOWNLOAD = "download"
    const val CMD_DOWNLOAD_COMPLETED = "download_completed"
    const val CMD_CLOSE = "close"
}
