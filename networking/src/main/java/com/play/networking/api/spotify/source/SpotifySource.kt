package com.play.networking.api.spotify.source

import com.play.models.spotify.*
import com.play.networking.Outcome
import com.play.networking.api.provideSpotifyService
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import java.lang.NullPointerException

class SpotifySource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val search by lazy {
        provideSpotifyService(retrofit)
    }

    suspend fun newRelease(limit: String?= "", offset: String? =""): Outcome<Albums> {
        var ret: Outcome<Albums>
        val req = search.getNewReleases(limit, offset)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            ret = Outcome.failure(APIError(e, e.message))
        }
        return ret
    }

    suspend fun searchBy(query: String, type: String, limit: String?= "", offset: String? =""): Outcome<Albums> {
        var ret: Outcome<Albums>
        val req = search.getSearch(query, type, limit, offset)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun getToken(): Outcome<Token> {
        var ret: Outcome<Token>
        val req = search.getRefreshToken()
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
//            e.printStackTrace()
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun topPicks(country: String?): Outcome<TopPicks> {
        var ret: Outcome<TopPicks>
        val req = search.getTopPick(country)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun trending(): Outcome<Trending> {
        var ret: Outcome<Trending>
        val req = search.getTrending()
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun latestInHindi(): Outcome<Trending> {
        var ret: Outcome<Trending>
        val req = search.getLatestHindi()
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun bollywoodHindiSongs(): Outcome<BollywoodCategory> {
        var ret: Outcome<BollywoodCategory>
        val req = search.getBollywodHindiSong()
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun getAlbumsByArtists(artist_id: String): Outcome<ArtistAlbum> {
        var ret: Outcome<ArtistAlbum>
        val req = search.getAlbumsByArtists(artist_id)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }


}
