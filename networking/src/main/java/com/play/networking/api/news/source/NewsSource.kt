package com.play.networking.api.news.source

import android.util.Log
import com.google.gson.JsonObject
import com.play.models.news.MusicNews
import com.play.models.sync.SyncResponse
import com.play.networking.Outcome
import com.play.networking.api.provideLyricsService
import com.play.networking.api.provideNewsService
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit

class NewsSource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val news by lazy {
        provideNewsService(retrofit)
    }

    suspend fun getMusicNews(from: String?, sortBy: String = "publishedAt"): Outcome<MusicNews> {
        var ret: Outcome<MusicNews>
        val req = news.getMusicNews(from = from, sortBy = sortBy)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }
}
