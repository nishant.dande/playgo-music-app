package com.play.networking.api.news

import com.play.networking.BuildConfig

object NewsApi {
    const val BASE_URL = BuildConfig.NEWS_BASE_URL
    const val API_KEY = BuildConfig.NEWS_API_KEY
}
