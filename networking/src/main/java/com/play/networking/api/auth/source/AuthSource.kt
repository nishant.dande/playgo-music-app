package com.play.networking.api.auth.source

import android.util.Log
import com.play.models.auth.Request
import com.play.models.auth.Response
import com.play.networking.Outcome
import com.play.networking.api.provideAuthService
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit

class AuthSource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val auth by lazy {
        provideAuthService(retrofit)
    }

    suspend fun login(type: String, identity: String, password: String?): Outcome<Response> {
        var ret: Outcome<Response>
        val req = auth.login(Request(identity, password, type))
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.authServerError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }
}
