package com.play.networking.api.spotify

import com.play.networking.BuildConfig

object SpotifyApi {
    const val BASE_URL = BuildConfig.SPOTIFY_BASE_URL
    const val ACCOUNT_BASE_URL = BuildConfig.SPOTIFY_ACCOUNT_BASE_URL
    val CLIENT_ID: String? = BuildConfig.SPOTIFY_CLIENT_ID
    val REDIRECT_URI = BuildConfig.SPOTIFY_REDIRECT_URL
    val LOGIN_REQUEST_CODE = 1337
}
