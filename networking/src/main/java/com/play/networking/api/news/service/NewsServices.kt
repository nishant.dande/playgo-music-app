package com.play.networking.api.news.service

import com.google.gson.JsonObject
import com.play.models.news.MusicNews
import com.play.models.sync.GroupPayload
import com.play.models.sync.MediaControlPayload
import com.play.models.sync.SyncPayload
import com.play.models.sync.SyncResponse
import com.play.networking.api.news.NewsApi
import kotlinx.coroutines.Deferred
import org.json.JSONObject
import retrofit2.http.*

interface NewsServices {

    @GET("everything")
    fun getMusicNews(@Query("q") q: String = "music",
                     @Query("sortBy") sortBy: String?,
                     @Query("apiKey") apiKey: String? = NewsApi.API_KEY,
                     @Query("from") from: String?): Deferred<MusicNews>

}