package com.play.networking.api.spotify.source

import com.play.models.spotify.Token
import com.play.networking.Outcome
import com.play.networking.api.provideSpotifyAccountServices
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit

class SpotifyAccountSource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val account by lazy {
        provideSpotifyAccountServices(retrofit)
    }

    suspend fun getToken(): Outcome<Token> {
        var ret: Outcome<Token>
        val req = account.getRefreshToken()
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
//            e.printStackTrace()
            ret = Outcome.failure(apiError)
        }
        return ret
    }


}
