package com.play.networking.api.youtube.service

import com.play.models.youtube.LatestSongs
import com.play.models.youtube.Recommendation
import com.play.models.youtube.TrendingSongs
import com.play.models.youtube.YoutubeSearch
import com.play.networking.utils.Utility
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

//https://developers.google.com/youtube/v3/docs/?apix=true
//https://console.cloud.google.com/apis/dashboard
// Quota - https://console.cloud.google.com/iam-admin/quotas/
            //https://developers.google.com/youtube/v3/determine_quota_cost
interface YoutubeServices {

    @GET("youtube/v3/search")
    fun getYoutubeSearch(
        @Query("key") key: String?,
        @Query("q") query: String?,
        @Query("part") part: String?,
        @Query("maxResults") maxResults: Int? = 50
    ): Deferred<YoutubeSearch>

    @GET("youtube/v3/videos")
    fun getRecommended(
        @Query("part") part: String = "snippet,contentDetails,statistics",
        @Query("chart") chart: String = "mostPopular",
        @Query("regionCode") regionCode: String = "IN",
        @Query("type") type: String = "video",
        @Query("videoCategoryId") videoCategoryId: String = "10",
        @Query("key") key: String?,
        @Query("maxResults") maxResults: Int? = 50,
        @Query("publishedBefore") publishedBefore: String? = Utility.getCurrentDateTimeZone(),
        @Query("publishedAfter") publishedAfter: String? = Utility.getPreviousThreeMonthDateTime()
    ): Deferred<Recommendation>

    @GET("youtube/v3/search")
    fun getLatestSongByLanguage(
        @Query("q") query: String,
        @Query("part") part: String = "snippet",
        @Query("regionCode") regionCode: String = "IN",
        @Query("publishedAfter") publishedAfter: String = Utility.getPreviousThreeMonthDateTime(),
        @Query("publishedBefore") publishedBefore: String = Utility.getCurrentDateTimeZone(),
        @Query("key") key: String?,
        @Query("maxResults") maxResults: Int? = 50
    ): Deferred<LatestSongs>

    @GET("youtube/v3/playlistItems")
    fun getTrendingSongs(
        @Query("part") part: String = "snippet,contentDetails",
        @Query("key") key: String?,
        @Query("maxResults") maxResults: String? = "100",
        @Query("playlistId") playlistId: String? = "PL3-sRm8xAzY9gpXTMGVHJWy_FMD67NBed"
    ): Deferred<TrendingSongs>

}