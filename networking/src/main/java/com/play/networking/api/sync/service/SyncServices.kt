package com.play.networking.api.sync.service

import com.google.gson.JsonObject
import com.play.models.sync.*
import com.play.models.youtube.LatestSongs
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface SyncServices {


    @POST("firebase/notification")
    fun sendMediaControlData(@Body mediaControlPayload: MediaControlPayload): Deferred<SyncResponse>

    @GET("/")
    fun checkConnection(): Deferred<JsonObject>

    @POST("firebase/notification")
    fun requestUserToJoin(@Body groupPayload: GroupPayload): Deferred<SyncResponse>

    @POST("firebase/notification")
    fun syncMediaSession(@Body syncPayload: SyncPayload): Deferred<SyncResponse>

    // Handle Youtube language Songs coming from server
    @GET("/songs/language")
    fun getSongsByLanguage(@Query(value = "language", encoded = true) language: String?): Deferred<LatestSongs>

    @POST("firebase/notification")
    fun sendMessage(@Body messagePayload: MessagePayload): Deferred<SyncResponse>
}