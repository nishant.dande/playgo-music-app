package com.play.networking.api.youtube.source

import android.content.Context
import android.text.TextUtils
import com.google.gson.Gson
import com.play.models.youtube.*
import com.play.networking.Outcome
import com.play.networking.api.provideYoutubeService
import com.play.networking.error.APIError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import java.lang.NullPointerException

class YoutubeSource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val search by lazy {
        provideYoutubeService(retrofit)
    }

    suspend fun searchBy(key: String, youtubeParams: YoutubeParams): Outcome<YoutubeSearch> {
        var ret: Outcome<YoutubeSearch>
        val req = search.getYoutubeSearch(key, youtubeParams.query, youtubeParams.part)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            ret = Outcome.failure(APIError(e, e.message))
        }
        return ret
    }

    suspend fun getRecommended(key: String): Outcome<Recommendation> {
        var ret: Outcome<Recommendation>
        val req = search.getRecommended(key = key)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            ret = Outcome.failure(APIError(e, e.message))
        }
        return ret
    }

    suspend fun getTrendingSongs(key: String): Outcome<TrendingSongs> {
        var ret: Outcome<TrendingSongs>
        val req = search.getTrendingSongs(key = key)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            ret = Outcome.failure(APIError(e, e.message))
        }
        return ret
    }

    suspend fun getLatestSongByLanguage(context: Context? = null, key: String, language: String): Outcome<LatestSongs> {
        if (context == null) {
            var ret: Outcome<LatestSongs>
            val req =
                search.getLatestSongByLanguage(key = key, query = "latest+song+in+${language}")
            try {
                req.await().run {
                    ret = if (this != null) {
                        Outcome.success(this)
                    } else {
                        Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                ret = Outcome.failure(APIError(e, e.message))
            }
            return ret
        } else {
            var ret: Outcome<LatestSongs>
            try {
                val content = context.assets.open("latest_songs_in_${language}.json")
                    .bufferedReader().use{ it.readText() }
                var songs = Gson().fromJson<LatestSongs>(content, LatestSongs::class.java)
                ret = Outcome.success(songs)
            } catch (e: Exception) {
                e.printStackTrace()
                ret = Outcome.failure(APIError(e, e.message))
            }
            return ret
        }
    }

    suspend fun getSongsByType(context: Context, key: String, filePath : String, query: String) : Outcome<LatestSongs> {
        if (TextUtils.isEmpty(filePath)){
            var ret: Outcome<LatestSongs>
            val req = search.getLatestSongByLanguage(key = key, query = query)
            try {
                req.await().run {
                    ret = if (this != null){
                        Outcome.success(this)
                    } else {
                        Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                ret = Outcome.failure(APIError(e, e.message))
            }
            return ret
        } else {
            var ret: Outcome<LatestSongs>
            try {
                val content = context.assets.open(filePath)
                    .bufferedReader().use{ it.readText() }
                var songs = Gson().fromJson<LatestSongs>(content, LatestSongs::class.java)
                ret = Outcome.success(songs)
            } catch (e: Exception) {
                e.printStackTrace()
                ret = Outcome.failure(APIError(e, e.message))
            }
            return ret
        }
    }
}
