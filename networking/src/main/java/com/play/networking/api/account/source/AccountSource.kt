package com.play.networking.api.account.source

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.play.models.account.Group
import com.play.models.account.User
import com.play.models.account.UserInfo
import com.play.networking.Outcome
import com.play.networking.api.account.AccountApi.GROUP
import com.play.networking.api.account.AccountApi.USER
import com.play.networking.api.provideFirebaseDatabase
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import okhttp3.internal.wait
import java.lang.IllegalArgumentException

class AccountSource() : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val database by lazy {
        provideFirebaseDatabase()
    }

    private val groupReference by lazy{
        database.getReference(GROUP)
    }

    private val userReference by lazy{
        database.getReference(USER)
    }

    val NAME_ENTITY = "name"
    val PUSH_ID_ENTITY = "pushId"
    val OWNER_ID_ENTITY = "ownerId"
    val STATUS_ENTITY = "status"
    val USER_ID_ENTITY = "userId"
    val USER_INFO_ENTITY = "userInfo"

    suspend fun createGroup(group: Group): Outcome<Boolean> {
        var ret: Outcome<Boolean> = Outcome.loading(true)
        ret = try {
            val await = groupReference.child(group.name).setValue(group).await()
            Outcome.success(true)
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun createUser(user: User): Outcome<Boolean> {
        var ret: Outcome<Boolean> = Outcome.loading(true)
        ret = try {
            userReference.child(user.userId.toString()).setValue(user).await()
            Outcome.success(true)
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun assignGroup(groupName: String, ownerId: String, userId: ArrayList<UserInfo>?): Outcome<Boolean> {
        var ret: Outcome<Boolean> = Outcome.loading(true)
        ret = try {
            groupReference.child(groupName).setValue(Group(groupName, ownerId, userId)).await()
            Outcome.success(true)
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun getGroups() : Outcome<ArrayList<Group>>{
        var ret: Outcome<ArrayList<Group>> = Outcome.loading(true)
        try {
            val dataSnapshot = groupReference.get().await()
            groupReference.keepSynced(true)
            val _groups = arrayListOf<Group>()
                    for (ds in dataSnapshot.children) {
//                    var group: Group = ds.getValue(Group::class.java) as Group
//                    Log.d("Nishant", group.toString())
                        val name = ds.child(NAME_ENTITY).getValue(String::class.java)
                        val ownerId = ds.child(OWNER_ID_ENTITY).getValue(String::class.java)
                        val userInfo = arrayListOf<UserInfo>()
                        for (childDs in ds.child(USER_INFO_ENTITY).children){
                            val userId = childDs.child(USER_ID_ENTITY).getValue(String::class.java)
                            val status = childDs.child(STATUS_ENTITY).getValue(String::class.java)
                            userInfo.add(UserInfo(userId, status))
                        }
                        _groups.add(Group(name!!, ownerId, userInfo))
                    }
                    ret = if (_groups.isNotEmpty()) {
                        Outcome.success(_groups)
                    } else
                        Outcome.success(arrayListOf())
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun getGroupsByOwner(owner: String) : Outcome<ArrayList<Group>>{
        var ret: Outcome<ArrayList<Group>> = Outcome.loading(true)
        try {
            val dataSnapshot = groupReference.get().await()
            groupReference.keepSynced(true)
            val _groups = arrayListOf<Group>()
                    for (ds in dataSnapshot.children) {
                        val ownerId = ds.child(OWNER_ID_ENTITY).getValue(String::class.java)
                        if (ownerId == owner){
                            val name = ds.child(NAME_ENTITY).getValue(String::class.java)
                            val userInfo = arrayListOf<UserInfo>()
                            for (childDs in ds.child(USER_INFO_ENTITY).children){
                                val userId = childDs.child(USER_ID_ENTITY).getValue(String::class.java)
                                val status = childDs.child(STATUS_ENTITY).getValue(String::class.java)
                                userInfo.add(UserInfo(userId, status))
                            }
                            _groups.add(Group(name!!, ownerId, userInfo = userInfo))
                        }
                    }
                    ret = if (_groups.isNotEmpty()) {
                        Outcome.success(_groups)
                    } else
                        Outcome.success(arrayListOf())
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun getAllUsers(): Outcome<ArrayList<User>> {
        var ret: Outcome<ArrayList<User>> = Outcome.loading(true)
        try {
            val dataSnapshot = userReference.get().await()
            userReference.keepSynced(true)
            var _users = arrayListOf<User>()
            for (ds in dataSnapshot.children) {
                val userId = ds.child(USER_ID_ENTITY).getValue(String::class.java)
                val name = ds.child(NAME_ENTITY).getValue(String::class.java)
                val pushId = ds.child(PUSH_ID_ENTITY).getValue(String::class.java)

                _users.add(User(name!!, userId = userId, pushId = pushId))
            }

            if (_users.isNotEmpty()) {
                ret = Outcome.success(_users)
            } else
                ret = Outcome.failure(APIError(NullPointerException(), "No Users Data Found"))
        } catch (e: Exception) {
            val apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    fun getUserById(id: String, callback: (result: User) -> Unit, error: (exception: Exception) -> Unit){
        val dataSnapshot = userReference.orderByChild(USER_ID_ENTITY).equalTo(id)
        userReference.keepSynced(true)
        dataSnapshot.addListenerForSingleValueEvent( object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (ds in snapshot.children) {
                        val name = ds.child("name").getValue(String::class.java)
                        val userId = ds.child("userId").getValue(String::class.java)
                        val pushId = ds.child("pushId").getValue(String::class.java)

                        callback.invoke(User(name!!, userId, pushId))
                        break
                    }
                } else {
                    error.invoke(APIError(NullPointerException(), "User Not Found"))
                }
            }

            override fun onCancelled(it: DatabaseError) {
                error.invoke(APIError(it.toException(), it.message))
            }

        })
    }

    suspend fun getGroupByName(name: String) : Outcome<ArrayList<Group>> {
        var ret: Outcome<ArrayList<Group>> = Outcome.loading(true)
        try {
            val dataSnapshot = groupReference.orderByChild(NAME_ENTITY).equalTo(name)
            groupReference.keepSynced(true)
            val _groups = arrayListOf<Group>()
            dataSnapshot.addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(snapshot: DataSnapshot) {
                    ret = if (snapshot.exists()){
                        for (ds in snapshot.children) {
                            val name = ds.child(NAME_ENTITY).getValue(String::class.java)
                            val ownerId = ds.child(OWNER_ID_ENTITY).getValue(String::class.java)
                            val userInfo = arrayListOf<UserInfo>()
                            for (childDs in ds.child(USER_INFO_ENTITY).children){
                                val userId = childDs.child(USER_ID_ENTITY).getValue(String::class.java)
                                val status = childDs.child(STATUS_ENTITY).getValue(String::class.java)
                                userInfo.add(UserInfo(userId, status))
                            }
                            _groups.add(Group(name!!, ownerId, userInfo))
                        }
                        Outcome.success(_groups)
                    } else {
                        Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                   ret = Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }

            })
        } catch (e: Exception) {
            var apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    suspend fun deleteGroupByName(name: String): Outcome<Boolean>{
        var ret: Outcome<Boolean> = Outcome.loading(true)
        try {
            groupReference.child(name).removeValue().await()
            ret = Outcome.success(true)
        } catch (e: Exception) {
            val apiError = ErrorHandler.error(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }

    fun updateGroupSessionStatus(groupName: String, status: String,
                                 callback: (result: String) -> Unit, error: (exception: Exception) -> Unit){
        val dataSnapshot = groupReference.orderByChild(NAME_ENTITY).equalTo(groupName)
        groupReference.keepSynced(true)
        dataSnapshot.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var group: Group? = null
                if (snapshot.exists()){
                    for (ds in snapshot.children) {
                        val name = ds.child(NAME_ENTITY).getValue(String::class.java)
                        val ownerId = ds.child(OWNER_ID_ENTITY).getValue(String::class.java)
                        val userInfo = arrayListOf<UserInfo>()
                        for (childDs in ds.child(USER_INFO_ENTITY).children){
                            val _userId = childDs.child(USER_ID_ENTITY).getValue(String::class.java)
                            userInfo.add(UserInfo(_userId, status))
                        }

                        group = Group(name!!, ownerId, userInfo)
                        if (group != null) {
                            groupReference.child(groupName).setValue(group).addOnSuccessListener {
                                callback.invoke("User Status Updated")
                            }.addOnFailureListener {
                                error.invoke(APIError(IllegalArgumentException(), "User Status Not Updated"))
                            }
                        }
                    }

                } else {
                    error.invoke(APIError(NullPointerException(), "No Data Found"))
                }
            }

            override fun onCancelled(dbError: DatabaseError) {
                error.invoke(APIError(NullPointerException(), dbError.message))
            }
        })
    }

    fun updateUserSessionStatus(groupName: String, userId: String, status: String,
                                callback: (result: String) -> Unit, error: (exception: Exception) -> Unit){
        val dataSnapshot = groupReference.orderByChild(NAME_ENTITY).equalTo(groupName)
        groupReference.keepSynced(true)
        dataSnapshot.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                var group: Group? = null
                if (snapshot.exists()){
                    for (ds in snapshot.children) {
                        val name = ds.child(NAME_ENTITY).getValue(String::class.java)
                        val ownerId = ds.child(OWNER_ID_ENTITY).getValue(String::class.java)
                        val userInfo = arrayListOf<UserInfo>()
                        for (childDs in ds.child(USER_INFO_ENTITY).children){
                            val _userId = childDs.child(USER_ID_ENTITY).getValue(String::class.java)
                            if (_userId == userId){
                                userInfo.add(UserInfo(_userId, status))
                            } else {
                                val _status = childDs.child(STATUS_ENTITY).getValue(String::class.java)
                                userInfo.add(UserInfo(_userId, _status))
                            }
                        }

                        group = Group(name!!, ownerId, userInfo)
                    }
                    if (group != null) {
                        groupReference.child(groupName).setValue(group).addOnSuccessListener {
                            callback.invoke("User Status Updated")
                        }.addOnFailureListener {
                            error.invoke(APIError(IllegalArgumentException(), "User Status Not Updated"))
                        }
                    }
                } else {
                    error.invoke(APIError(NullPointerException(), "No Data Found"))
                }
            }

            override fun onCancelled(dbError: DatabaseError) {
                error.invoke(APIError(NullPointerException(), dbError.message))
            }
        })
    }

    fun getGroupUserPerStatus(groupName: String, status: String,
                                 callback: (result: User) -> Unit, error: (exception: Exception) -> Unit){
        val dataSnapshot = groupReference.orderByChild(NAME_ENTITY).equalTo(groupName)
        groupReference.keepSynced(true)
        dataSnapshot.addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()){
                    for (ds in snapshot.children) {
                        for (childDs in ds.child(USER_INFO_ENTITY).children){
                            val _userId = childDs.child(USER_ID_ENTITY).getValue(String::class.java)
                            val _status = childDs.child(STATUS_ENTITY).getValue(String::class.java)
                            if (status == _status){
                                getUserById(_userId!!,{
                                    callback.invoke(it)
                                    Log.d("viMusicActivity", it.toString())
                                }, {
                                    error.invoke(APIError(NullPointerException(), "No Data Found ${_userId}"))
                                })
                            }
                        }
                    }

                } else {
                    error.invoke(APIError(NullPointerException(), "No Data Found"))
                }
            }

            override fun onCancelled(dbError: DatabaseError) {
                error.invoke(APIError(NullPointerException(), dbError.message))
            }
        })
    }
}
