package com.play.networking.api.lyrics.service

import com.google.gson.JsonObject
import com.play.models.sync.GroupPayload
import com.play.models.sync.MediaControlPayload
import com.play.models.sync.SyncPayload
import com.play.models.sync.SyncResponse
import kotlinx.coroutines.Deferred
import org.json.JSONObject
import retrofit2.http.*

interface LyricsServices {


    @GET("/lyrics")
    fun getLyrics(@Query(value = "provider", encoded = true) provider: String?,
                  @Query(value = "title", encoded = true) title: String?,
                  @Query(value = "artist", encoded = true) artist: String?): Deferred<JsonObject>

}