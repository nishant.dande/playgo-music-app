package com.play.networking.api.lyrics.source

import android.util.Log
import com.google.gson.JsonObject
import com.play.models.sync.SyncResponse
import com.play.networking.Outcome
import com.play.networking.api.provideLyricsService
import com.play.networking.error.APIError
import com.play.networking.error.ErrorHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit

class LyricsSource(
    private val retrofit: Retrofit
) : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    private val sync by lazy {
        provideLyricsService(retrofit)
    }

    suspend fun getLyrics(provider: String, title: String, artist: String?): Outcome<JsonObject> {
        var ret: Outcome<JsonObject>
        var proxyTitle = title
        if (provider == "youtube_player"){
            proxyTitle = title.split("|")[0]
        }
        val req = sync.getLyrics(provider, proxyTitle, artist)
        try {
            req.await().run {
                ret = if (this != null){
                    Outcome.success(this)
                } else {
                    Outcome.failure(APIError(NullPointerException(), "No Data Found"))
                }
            }
        } catch (e: Exception) {
            Log.e("Error", e.message, e)
            var apiError = ErrorHandler.serverError(e)
            ret = Outcome.failure(apiError)
        }
        return ret
    }
}
