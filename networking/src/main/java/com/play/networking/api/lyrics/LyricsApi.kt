package com.play.networking.api.lyrics

import com.play.networking.BuildConfig

object LyricsApi {
    const val BASE_URL = BuildConfig.PLAY_SERVER_URL
}
