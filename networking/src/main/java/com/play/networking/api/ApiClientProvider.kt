package com.play.networking.api

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.play.networking.api.auth.service.AuthServices
import com.play.networking.api.lyrics.service.LyricsServices
import com.play.networking.api.news.service.NewsServices
import com.play.networking.api.spotify.service.SpotifyAccountServices
import com.play.networking.api.spotify.service.SpotifyServices
import com.play.networking.api.sync.service.SyncServices
import com.play.networking.api.youtube.service.YoutubeServices
import com.play.networking.auth.TokenProviding
import com.play.networking.auth.toTokenReason
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun provideGson(): Gson {
    return GsonBuilder().create()
}

fun provideSyncService(retrofit: Retrofit): SyncServices {
    return retrofit.create(SyncServices::class.java)
}

fun provideLyricsService(retrofit: Retrofit): LyricsServices {
    return retrofit.create(LyricsServices::class.java)
}

fun provideAuthService(retrofit: Retrofit): AuthServices {
    return retrofit.create(AuthServices::class.java)
}

fun provideNewsService(retrofit: Retrofit): NewsServices {
    return retrofit.create(NewsServices::class.java)
}

fun provideFirebaseDatabase() : FirebaseDatabase {
    return Firebase.database
}

fun provideFirebaseStorage() : FirebaseStorage {
    return Firebase.storage
}

fun provideSpotifyService(retrofit: Retrofit): SpotifyServices {
    return retrofit.create(SpotifyServices::class.java)
}
fun provideSpotifyAccountServices(retrofit: Retrofit): SpotifyAccountServices {
    return retrofit.create(SpotifyAccountServices::class.java)
}

fun provideYoutubeService(retrofit: Retrofit): YoutubeServices {
    return retrofit.create(YoutubeServices::class.java)
}

fun provideOkHttpClientBuilder(tokenProvider: TokenProviding): OkHttpClient.Builder {
    return OkHttpClient.Builder().apply {
        // token in headers
//        this.addInterceptor {
//            val request = it.request().newBuilder()
//                .addHeader("Authorization", "Bearer ${tokenProvider.getSpotifyApiToken()}")
//                .build()
//            it.proceed(request)
//        }

        // token expired reauth
        this.addInterceptor {
            val request = it.request()
            val response = it.proceed(request)
            if (response.code == 403 || response.code == 401) {
                tokenProvider.onTokenExpired(response.code.toTokenReason())
            }
            response
        }

//        // add logging interceptor last to view others interceptors
//        if (BuildConfig.DEBUG) {
//            val logging = HttpLoggingInterceptor().also {
//                it.level = HttpLoggingInterceptor.Level.BODY
//            }
//            this.addInterceptor(logging)
//        }
    }
}

fun provideOkHttpClient(tokenProvider: TokenProviding): OkHttpClient {
    return provideOkHttpClientBuilder(
        tokenProvider
    ).build()
}

fun provideRetrofit(
    gson: Gson = provideGson(),
    tokenProvider: TokenProviding,
    httpClient: OkHttpClient = provideOkHttpClient(
        tokenProvider
    ),
    baseUrl: String
): Retrofit {
    return Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .client(httpClient)
        .build()
}
