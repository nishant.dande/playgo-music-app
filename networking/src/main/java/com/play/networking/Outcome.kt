package com.play.networking

import com.play.networking.error.APIError

sealed class Outcome<T> {
    data class Progress<T>(var loading: Boolean) : Outcome<T>()
    data class Success<T>(var data: T?) : Outcome<T>()
    data class Failure<T>(val e: APIError) : Outcome<T>()

    companion object {
        fun <T> loading(isLoading: Boolean): Outcome<T> =
            Progress(isLoading)

        fun <T> success(data: T?): Outcome<T> =
            Success(data)

        fun <T> failure(e: APIError): Outcome<T> =
            Failure(e)
    }
}
