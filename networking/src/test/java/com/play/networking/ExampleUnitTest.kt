package com.play.networking

import com.play.networking.utils.Utility
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun testCurrentDateTime(){
        var string = Utility.getCurrentDateTimeZone()
        assertEquals("2021-01-15T00:00:00Z", string)
    }

    @Test
    fun testPreviousDateTime(){
        var string = Utility.getPreviousThreeMonthDateTime()
        assertEquals("2021-01-15T00:00:00Z", string)
    }

    @Test
    fun testGetTitleCase(){
        var string = Utility.titleCase("Latest in hindi")
        assertEquals("Latest In Hindi", string)
    }
}