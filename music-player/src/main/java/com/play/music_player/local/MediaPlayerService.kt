package com.play.music_player.local

import android.app.*
import android.content.Intent
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.audio.AudioAttributes
import com.play.music_player.PlayerProducer
import com.play.music_player.R
import com.play.music_player.base.NotifyPlayer
import java.io.File


class MediaPlayerService : Service() {
    private val TAG = "MediaPlayerService"

    companion object{
        const val ACTION_URI = "ACTION_URI"
        const val ACTION_PLAY = "ACTION_PLAY"
        const val ACTION_PAUSE = "ACTION_PAUSE"
        const val ACTION_RESUME = "ACTION_RESUME"
        const val ACTION_STOP = "ACTION_STOP"
        const val ACTION_SEEK_TO = "ACTION_SEEK_TO"
        const val ACTION_REVERSE_SEEK_TO = "ACTION_REVERSE_SEEK_TO"
        const val ACTION_SYNC = "ACTION_SYNC"

        const val PARAM_EXTRA_URI = "PARAM_EXTRA_URI"
        const val PARAM_EXTRA_SEEK = "PARAM_EXTRA_SEEK"

    }

    /**
     * Private Variables
     */
    private lateinit var player: SimpleExoPlayer
    private var binder = PlayerServiceBinder()

    private var uri: String? = null
    private lateinit var playerOperation: NotifyPlayer.PlayerState

    override fun onCreate() {
        super.onCreate()
        initializePlayer()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent.let {
            if (it?.action == ACTION_URI) {
                uri = it?.getStringExtra(PARAM_EXTRA_URI)
                Log.d(TAG, "onStartCommand : Uri - $uri")
            }
            if (it?.action == ACTION_PLAY) {
                play()

            }
            if (it?.action == ACTION_PAUSE) {
                pause()
                makeNotification()
            }
            if (it?.action == ACTION_RESUME) {
                resume()
                makeNotification()
            }
            if (it?.action == ACTION_STOP) {
                stop()
            }
            if (it?.action == ACTION_SEEK_TO) {
                val millisecond = it.getLongExtra(PARAM_EXTRA_SEEK, 0)
                seekTo(millisecond)
            }
            if (it?.action == ACTION_REVERSE_SEEK_TO) {
                val millisecond = it.getLongExtra(PARAM_EXTRA_SEEK, 0)
                reverseSeekTo(millisecond)
            }

            if (it?.action == ACTION_SYNC) {
                val millisecond = it.getLongExtra(PARAM_EXTRA_SEEK, 0)
                syncTo(millisecond)
            }

        }
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    /**
     * Public Function
     */
    fun getPlayer() : SimpleExoPlayer? {
        return player
    }

    fun setPlayerOperationListener(playerOperation: NotifyPlayer.PlayerState){
        this.playerOperation = playerOperation
    }

    fun getCurrentDuration() : Long{
        return player.currentPosition
    }

    /**
     * Private Function
     */
    private fun initializePlayer(){
        player = SimpleExoPlayer.Builder(this).build()
        player.addListener(object : Player.EventListener{
            override fun onPlayerError(error: ExoPlaybackException) {
                super.onPlayerError(error)
                handleError(error)
            }

            override fun onPlayWhenReadyChanged(playWhenReady: Boolean, reason: Int) {
                super.onPlayWhenReadyChanged(playWhenReady, reason)
                Log.d(TAG, "onPlayWhenReadyChanged : ${playWhenReady} ${reason}")
            }

            override fun onIsPlayingChanged(isPlaying: Boolean) {
                super.onIsPlayingChanged(isPlaying)
                Log.d(TAG, "onIsPlayingChanged : ${isPlaying}")
                if (isPlaying)
                    makeNotification()
            }
        })
        player.addAnalyticsListener(object: AnalyticsListener {

            override fun onAudioSessionId(eventTime: AnalyticsListener.EventTime,
                audioSessionId: Int) {
                super.onAudioSessionId(eventTime, audioSessionId)
                Log.d(TAG, "Service Audio Session : ${audioSessionId}")
                val intent: Intent = Intent(PlayerProducer.ACTION_AUDIO_SESSION)
                intent.putExtra(PlayerProducer.AUDIO_SESSION_ID, audioSessionId)
                LocalBroadcastManager.getInstance(this@MediaPlayerService).sendBroadcast(intent)
            }
        })
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            val audioAttributes: AudioAttributes = AudioAttributes.Builder()
                .setUsage(C.USAGE_MEDIA)
                .setContentType(C.CONTENT_TYPE_MOVIE)
                .build()
            player.setAudioAttributes(audioAttributes, true)
        } else {

        }
    }


    private fun handleError(error: ExoPlaybackException) {
        Log.e(TAG, error.message, error)
    }

    private fun releasePlayer() {
        if (player != null) {
            player.stop()
            player.seekTo(0)
            player.release();
            stopSelf()
        }
    }
    fun play(){
        Log.d(TAG, "onStartCommand : onPlay")
        player.let {
            // reset player before play
            if (it.isPlaying) {
                it.seekTo(0)
                it.stop()
            }

            val mediaItem = MediaItem.fromUri(uri!!)
            it.setMediaItem(mediaItem)
            it.playWhenReady = true
            it.prepare()
        }
    }

    private fun pause(){
        Log.d(TAG, "onStartCommand : onPause")
        player.let {
            it.pause()
        }
    }
    private fun resume(){
        Log.d(TAG, "onStartCommand : onResume")
        player.let {
            it.play()
        }
    }
    private fun stop(){
        Log.d(TAG, "onStartCommand : onStop")
        releasePlayer()
    }
    private fun seekTo(millisecond: Long){
        Log.d(TAG, "onStartCommand : onSeek - ${player.contentPosition} $millisecond")
        player.let {
            it.seekTo(player.contentPosition+millisecond)
        }
    }
    private fun reverseSeekTo(millisecond: Long){
        Log.d(TAG, "onStartCommand : reverseSeekTo - ${player.contentPosition} $millisecond")
        player.let {
            it.seekTo(player.contentPosition - Math.abs(millisecond))
        }
    }

    private fun syncTo(millisecond: Long){
        Log.d(TAG, "onStartCommand : syncTo - $millisecond")
        player.let {
            if (it.currentPosition != millisecond)
                it.seekTo(millisecond+100)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getNotificationChannel(notificationManager: NotificationManager): String? {
        val channelId = "channelide"
        val channelName = "Media Player Service"
        val channel =
            NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW)
        channel.importance = NotificationManager.IMPORTANCE_LOW
        channel.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        channel.setSound(null, null)
        notificationManager.createNotificationChannel(channel)
        return channelId
    }


    private fun makeNotification() {
        Log.d(TAG, "makeNotification")
        //For creating the Foreground Service
        var contentView = RemoteViews(packageName, R.layout.layout_media_control)
        contentView.setTextViewText(R.id.tvSongName, File(uri).name);

        if (player.isPlaying){
            contentView.setImageViewResource(R.id.iv_play_pause, R.drawable.exo_controls_pause)
            val pauseIntent  = Intent(applicationContext,MediaPlayerService::class.java)
            pauseIntent.action = ACTION_PAUSE
            val pause: PendingIntent =
                PendingIntent.getService(applicationContext, 0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            contentView.setOnClickPendingIntent(R.id.iv_play_pause, pause)

        } else {
            contentView.setImageViewResource(R.id.iv_play_pause, R.drawable.exo_controls_play)
            val resumeIntent  = Intent(applicationContext,MediaPlayerService::class.java)
            resumeIntent.action = ACTION_RESUME
            val resume: PendingIntent =
                PendingIntent.getService(applicationContext, 0, resumeIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            contentView.setOnClickPendingIntent(R.id.iv_play_pause, resume)
        }

        val seekMinus10Intent  = Intent(applicationContext, MediaPlayerService::class.java)

        seekMinus10Intent.putExtra(PARAM_EXTRA_SEEK, 10000L)
        seekMinus10Intent.action = ACTION_REVERSE_SEEK_TO
        val seekMinus10: PendingIntent =
            PendingIntent.getService(applicationContext, 0, seekMinus10Intent, PendingIntent.FLAG_UPDATE_CURRENT)
        contentView.setOnClickPendingIntent(R.id.iv_seek_minus_10, seekMinus10)

        val seekPlus10Intent  = Intent(applicationContext, MediaPlayerService::class.java)
        seekPlus10Intent.putExtra(PARAM_EXTRA_SEEK, 10000L)
        seekPlus10Intent.action = ACTION_SEEK_TO
        val seekPlus10: PendingIntent =
            PendingIntent.getService(applicationContext, 0, seekPlus10Intent, PendingIntent.FLAG_UPDATE_CURRENT)
        contentView.setOnClickPendingIntent(R.id.iv_seek_plus_10, seekPlus10)

        val notificationManager =
            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) getNotificationChannel(
                notificationManager
            ) else ""

        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId!!)
        val notification: Notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.drawable.ic_stat_library_music)
            .setCategory(NotificationCompat.CATEGORY_SERVICE)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setAutoCancel(true)
            .setContent(contentView)
            .setSound(null)
            .build()
        startForeground(110, notification)
        notificationManager.notify(110, notification)
    }


    /**
     * Binder Class
     */
    inner class PlayerServiceBinder : Binder(){
        val service: MediaPlayerService
            get() =// Return this instance of MyService so clients can call public methods
                this@MediaPlayerService
    }

}