package com.play.music_player.local

import android.app.Activity
import android.content.Intent
import android.util.Log
import com.play.music_player.PlayerProducer
import com.play.music_player.base.NotifyPlayer
import com.play.music_player.base.Player
import com.play.networking.api.sync.SyncApi

class LocalPlayer(private val activity: Activity) : NotifyPlayer() {

    private val TAG = "LocalPlayer"
    private lateinit var uri : String
    private val service by lazy {
        Intent(activity, MediaPlayerService::class.java)
    }

    override fun setDataSource(uri: String) {
        super.setDataSource(uri)
        Log.d(TAG, "setDataSource : $uri")
        this.uri = uri
        activity.runOnUiThread {
            service.putExtra(MediaPlayerService.PARAM_EXTRA_URI, uri)
            service.action = MediaPlayerService.ACTION_URI
            activity.startService(service)
        }
    }

    override fun play() {
        super.play()
        Log.d(TAG, "play")
        activity.runOnUiThread {
            service.action = MediaPlayerService.ACTION_PLAY
            activity.startService(service)
        }
    }

    override fun pause() {
        super.pause()
        Log.d(TAG, "pause")
        activity.runOnUiThread {
            service.action = MediaPlayerService.ACTION_PAUSE
            activity.startService(service)
        }
    }

    override fun resume() {
        super.resume()
        Log.d(TAG, "resume")
        activity.runOnUiThread {
            service.action = MediaPlayerService.ACTION_RESUME
            activity.startService(service)
        }
    }

    override fun seekTo(millisecond: Long) {
        super.seekTo(millisecond)
        Log.d(TAG, "seekTo $millisecond")
        activity.runOnUiThread {
            service.putExtra(MediaPlayerService.PARAM_EXTRA_SEEK, millisecond)
            if (millisecond > 0){
                service.action = MediaPlayerService.ACTION_SEEK_TO
            } else {
                service.action = MediaPlayerService.ACTION_REVERSE_SEEK_TO
            }
            activity.startService(service)
        }
    }

    override fun stop() {
        super.stop()
        Log.d(TAG, "stop")
        activity.runOnUiThread {
            service.action = MediaPlayerService.ACTION_STOP
            activity.startService(service)
        }
    }

    override fun nextTrack() {
        Log.d(TAG, "nextTrack")
    }

    override fun previousTrack() {
        Log.d(TAG, "prevoisTrack")
    }

    override fun getCurrentDuration(): String {
        return ""
    }

    override fun syncTime(millisecond: String) {
        activity.runOnUiThread {
            service.putExtra(MediaPlayerService.PARAM_EXTRA_SEEK, millisecond.toLong())
            service.action = MediaPlayerService.ACTION_SYNC
            activity.startService(service)
        }
    }
}