package com.play.music_player

import android.app.Activity
import com.play.music_player.base.AbstractPlayer
import com.play.music_player.base.Player
import com.play.music_player.local.LocalPlayer
import com.play.music_player.spotify.SpotifyPlayer
import com.play.music_player.youtube.YoutubePlayer

class PlayerProducer{

    class PlayerFactory(var activity: Activity) : AbstractPlayer() {

        override fun getPlayer(type: String): Player? {
            return when (type) {
                LOCAL_PLAYER -> {
                    LocalPlayer(activity)
                }
                SPOTIFY_PLAYER -> {
                    SpotifyPlayer(activity)
                }
                YOUTUBE_PLAYER -> {
                    YoutubePlayer()
                }
                else -> null
            }
        }

    }

    companion object{
        const val LOCAL_PLAYER = "local_player"
        const val SPOTIFY_PLAYER = "spotify_player"
        const val YOUTUBE_PLAYER = "youtube_player"

        const val AUDIO_SESSION_ID = "audio_session_id"
        const val ACTION_AUDIO_SESSION = "action_audio_session"

        fun getPlayer(activity: Activity) : AbstractPlayer{
            return PlayerFactory(activity)
        }
    }
}

