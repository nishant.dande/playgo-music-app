package com.play.music_player.youtube

import android.util.Log
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerTracker
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.play.music_player.base.NotifyPlayer
import kotlin.math.abs
import kotlin.math.roundToLong

class YoutubePlayer : NotifyPlayer() {

    private val TAG = "YoutubePlayer"
    var youtubePlayerControl : YouTubePlayer? = null
    private var playerTracker = YouTubePlayerTracker()
    private lateinit var youTubePlayerView : YouTubePlayerView
    private lateinit var uri : String

    fun setYoutubeView(youTubePlayerView: YouTubePlayerView){
        this.youTubePlayerView = youTubePlayerView
        this.youTubePlayerView.getYouTubePlayerWhenReady(object : YouTubePlayerCallback {
            override fun onYouTubePlayer(youTubePlayer: YouTubePlayer) {
                youtubePlayerControl = youTubePlayer
                youtubePlayerControl!!.addListener(playerTracker)
            }
        })
    }

    override fun setDataSource(uri: String) {
        super.setDataSource(uri)
        Log.d(TAG, "setDataSource : $uri")
        this.uri = uri
    }

    override fun play() {
        super.play()
        Log.d(TAG, "play")
        youtubePlayerControl.let {
            it?.loadVideo(uri, 0f)
        }
    }

    override fun pause() {
        super.pause()
        Log.d(TAG, "pause")
        youtubePlayerControl.let {
            it?.pause()
        }
    }

    override fun resume() {
        super.resume()
        youtubePlayerControl.let {
            it?.play()
        }
    }

    override fun seekTo(millisecond: Long) {
        super.seekTo(millisecond)
        Log.d(TAG, "seekTo")
        youtubePlayerControl.let {
            var seconds = playerTracker.currentSecond.plus(millisecond/1000)
            it?.seekTo(seconds)
        }
    }

    override fun stop() {
        super.stop()
        youTubePlayerView.let {
            pause()
            it?.release()
        }
    }

    override fun nextTrack() {
        Log.d(TAG, "nextTrack")
    }

    override fun previousTrack() {
        Log.d(TAG, "prevoisTrack")
    }

    override fun getCurrentDuration(): String {
        return playerTracker.currentSecond.toString()
    }

    override fun syncTime(millisecond: String){
        if (playerTracker.currentSecond != millisecond.toFloat()) {
            youtubePlayerControl.let {
                it?.seekTo(1F+millisecond.toFloat())
            }
        }
    }
}