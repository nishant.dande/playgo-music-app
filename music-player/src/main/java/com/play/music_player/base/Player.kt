package com.play.music_player.base

interface Player {
    fun setDataSource(uri: String)
    fun play()
    fun pause()
    fun resume()
    fun seekTo(millisecond: Long)
    fun stop()
    fun nextTrack()
    fun previousTrack()
    fun getCurrentDuration(): String
    fun syncTime(millisecond: String)
}