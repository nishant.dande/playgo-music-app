package com.play.music_player.base

import com.play.networking.api.sync.SyncApi

open class NotifyPlayer() : Player {

    private var playerOperation: PlayerState? = null

    interface PlayerState{
        fun state(command : String, isPlaying: Boolean)
    }

    fun setPlayerStateListener(playerOperation: PlayerState){
        this.playerOperation = playerOperation
    }

    override fun setDataSource(uri: String) {
        playerOperation.let {
            it?.state(SyncApi.CMD_SET_URI, false)
        }
    }

    override fun play() {
        playerOperation.let {
            it?.state(SyncApi.CMD_PLAY, true)
        }
    }

    override fun pause() {
        playerOperation.let {
            it?.state(SyncApi.CMD_PAUSE, true)
        }
    }

    override fun resume() {
        playerOperation.let {
            it?.state(SyncApi.CMD_RESUME, true)
        }
    }

    override fun seekTo(millisecond: Long) {
        playerOperation.let {
            if (millisecond > 0) {
                it?.state(SyncApi.CMD_SEEK_TO, true)
            } else {
                it?.state(SyncApi.CMD_SEEK_REVERSE, true)
            }
        }
    }

    override fun stop() {
        playerOperation.let {
            it?.state(SyncApi.CMD_STOP, false)
        }
    }

    override fun nextTrack() {

    }

    override fun previousTrack() {

    }

    override fun getCurrentDuration(): String {
        return ""
    }

    override fun syncTime(millisecond: String) {
    }
}