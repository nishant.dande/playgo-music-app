package com.play.music_player.base

abstract class AbstractPlayer {
    abstract fun getPlayer(type : String) : Player?
}