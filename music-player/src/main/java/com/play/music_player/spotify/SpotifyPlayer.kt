package com.play.music_player.spotify

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.play.music_player.base.NotifyPlayer
import com.play.networking.api.spotify.SpotifyApi
import com.spotify.android.appremote.api.ConnectionParams
import com.spotify.android.appremote.api.Connector
import com.spotify.android.appremote.api.SpotifyAppRemote
import com.spotify.android.appremote.api.error.CouldNotFindSpotifyApp

class SpotifyPlayer(val activity: Activity)  : NotifyPlayer() {

    private val TAG = "SpotifyPlayer"
    private var mSpotifyAppRemote: SpotifyAppRemote? = null
    private lateinit var uri : String

    init {
        configureSpotify()
    }

    private fun configureSpotify(){
        if (mSpotifyAppRemote == null) {
            val connectionParams = ConnectionParams.Builder(SpotifyApi.CLIENT_ID)
                .setRedirectUri(SpotifyApi.REDIRECT_URI)
                .showAuthView(true)
                .build()

            SpotifyAppRemote.connect(activity, connectionParams,
                object : Connector.ConnectionListener {
                    override fun onConnected(spotifyAppRemote: SpotifyAppRemote) {
                        Log.d(TAG, "SpotifyAppRemote OnConnect")
                        mSpotifyAppRemote = spotifyAppRemote
                        play()
                    }

                    override fun onFailure(throwable: Throwable) {
                        if (mSpotifyAppRemote != null)
                            SpotifyAppRemote.disconnect(mSpotifyAppRemote)
                        if (throwable is CouldNotFindSpotifyApp) {
                            try {
                                activity.startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("market://details?id=com.spotify.music")
                                    )
                                )
                            } catch (anfe: ActivityNotFoundException) {
                                activity.startActivity(
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://play.google.com/store/apps/details?id=com.spotify.music")
                                    )
                                )
                            }
                        } else {
                            Toast.makeText(
                                activity,
                                throwable.message.toString(),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })
        }
    }

    override fun setDataSource(uri: String) {
        super.setDataSource(uri)
        Log.d(TAG, "setDataSource : $uri")
        this.uri = uri
        configureSpotify()
    }

    override fun play() {
        super.play()
        Log.d(TAG, "play")
        mSpotifyAppRemote.let {
            it?.playerApi?.play(uri)
        }
    }

    override fun pause() {
        super.pause()
        Log.d(TAG, "pause")
        mSpotifyAppRemote.let {
            it?.playerApi?.pause()
        }
    }

    override fun resume() {
        super.resume()
        Log.d(TAG, "resume")
        mSpotifyAppRemote.let {
            it?.playerApi?.resume()
        }
    }

    override fun seekTo(millisecond: Long) {
        super.seekTo(millisecond)
        Log.d(TAG, "seekTo")
        mSpotifyAppRemote.let {
            it?.playerApi?.seekToRelativePosition(millisecond)
        }
    }

    override fun stop() {
        super.stop()
        mSpotifyAppRemote.let {
            SpotifyAppRemote.disconnect(it)
        }
    }

    override fun nextTrack() {
        Log.d(TAG, "nextTrack")
    }

    override fun previousTrack() {
        Log.d(TAG, "prevoisTrack")
    }

    override fun getCurrentDuration(): String {
        return ""
    }

    override fun syncTime(millisecond: String) {

    }
}