package com.play.models.playlist

import com.play.models.music.Playlist
import com.play.models.music.Song

interface IPlaylist {
    fun getCurrentTrack() : Song?
    fun getCurrentIndex() : Int
    fun getCurrentPlaylist() : Playlist?
    fun getNextTrack() : Song?
    fun getPreviousTrack() : Song?
}