package com.play.models.playlist

import com.play.models.music.Playlist
import com.play.models.music.Song

class ManagePlaylist(private val playlist: Playlist,
                        private var mCurrentIndex: Int = 0) : IPlaylist {

    private val tracks = playlist.songs ?: arrayListOf()

    override fun getCurrentTrack() : Song?{
        if (tracks.isEmpty()){
            return tracks[getCurrentIndex()]
        }
        return null
    }

    override fun getCurrentIndex() : Int {
        return mCurrentIndex
    }

    override fun getCurrentPlaylist() : Playlist? {
        return playlist
    }

    override fun getNextTrack() : Song? {
        if (tracks.isNotEmpty()) {
            if (mCurrentIndex < (tracks.size-1)) {
                ++mCurrentIndex
                return tracks[getCurrentIndex()]
            } else {
                return tracks[tracks.size-1]
            }
        }
        return null
    }

    override fun getPreviousTrack() : Song? {
        if (tracks.isNotEmpty()) {
            if (mCurrentIndex > 0) {
                --mCurrentIndex
                return tracks[getCurrentIndex()]
            } else {
                return tracks[0]
            }
        }
        return null
    }
}