package com.play.models.account

data class User(val name: String,
                var userId: String?,
                var pushId: String?){
    override fun toString(): String {
        return name
    }
}
