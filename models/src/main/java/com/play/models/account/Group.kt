package com.play.models.account

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Group(
    val name: String,
    val ownerId: String?,
    var userInfo: ArrayList<UserInfo>? = null) : Parcelable

@Parcelize
data class UserInfo(val userId: String?, val status: String?) : Parcelable{
    companion object{
        const val NO_ACTION = "no_action"
        const val JOIN = "join"
        const val WAITING = "waiting"
        const val REQUESTED = "requested"
        const val CANCELLED = "cancelled"
    }
}
