package com.play.models.auth


import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("message")
    val message: String? = "",
    var loggedIn: Boolean = false,
    var onboarding: Boolean = false,
    var continueAsGuest: Boolean = false,
    var genre: ArrayList<String> = arrayListOf(),
    var languages: ArrayList<String> = arrayListOf()
)

data class User(var name: String, var imageUrl: String? = null)