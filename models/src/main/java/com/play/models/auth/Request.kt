package com.play.models.auth


import com.google.gson.annotations.SerializedName

data class Request(
    @SerializedName("identity")
    val identity: String?,
    @SerializedName("password")
    val password: String?,
    @SerializedName("type")
    val type: String?
)