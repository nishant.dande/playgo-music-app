package com.play.models.youtube

class YoutubeParams(val query: String? = "",
                    val part: String? = "snippet") {
}