package com.play.models.sync

class MediaControlPayload(
    val message: Message,
    val registrationToken: List<String>
)  {
    data class Message(
        val `data`: Data
    ) {
        data class Data(
            val payload_type: String,
            val owner: String,
            val command: String,
            val song: String,
            val timestamp: String = System.currentTimeMillis().toString()
        )
    }
}