package com.play.models.sync

data class MessagePayload(
    val message: Message,
    val registrationToken: List<String>
){
    data class Message(
        val `data`: Data
    ) {
        data class Data(
            val payload_type: String,
            val payload: String,
        )
    }

    companion object{
        const val UPDATE_GROUP_SESSION = "update_session_push_ids"
        const val STOP_GROUP_SESSION = "stop_session_push_ids"
    }
}

