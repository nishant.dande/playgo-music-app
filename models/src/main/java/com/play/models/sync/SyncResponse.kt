package com.play.models.sync

data class SyncResponse(
    val canonicalRegistrationTokenCount: Int,
    val failureCount: Int,
    val multicastId: Long,
    val results: List<Result>,
    val successCount: Int
) {
    data class Result(
        val messageId: String
    )
}