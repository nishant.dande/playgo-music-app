package com.play.models.sync

data class SyncPayload(
    val message: Message,
    val registrationToken: List<String>
){
    data class Message(
        val `data`: Data
    ) {
        data class Data(
            val payload_type: String,
            val millisec: String,
            val provider: String?
        )
    }
}

