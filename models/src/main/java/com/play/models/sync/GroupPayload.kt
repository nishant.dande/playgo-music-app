package com.play.models.sync

data class GroupPayload(
    val message: Message,
    val registrationToken: List<String>
){
    data class Message(
        val `data`: Data
    ) {
        data class Data(
            val payload_type: String,
            val owner: String?,
            val name: String?,
            var userId: String? = null
        )
    }
}

data class Group(
    val owner: String?,
    val name: String?,
    var userId: List<String>? = null
)