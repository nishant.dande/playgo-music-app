package com.play.models.music

import androidx.room.*

@Entity(indices = [Index(value = ["id"], unique = true)])
data class Playlist(@PrimaryKey(autoGenerate = true) var id: Int,
                    @ColumnInfo(name = "name")  var name: String,
                    @Ignore  var songs: List<Song>,
                    @ColumnInfo(name = "songId")  var songId: Int = -1,
                    @ColumnInfo(name = "orientation")  var orientation: Int = Orientation.HORIZONTAL_SQUARE){

    constructor(name: String) : this(0, name, arrayListOf(), -1, Orientation.HORIZONTAL_SQUARE) {

    }
}

@Entity(indices = [Index(value = ["id", "playlistId", "songId"], unique = true)])
data class ManagePlaylist(@PrimaryKey(autoGenerate = true) var id: Int,
                    @ColumnInfo(name = "playlistId")  var playlistId: Int,
                    @ColumnInfo(name = "songId")  var songId: Int){

    constructor(playlistId: Int, songId: Int) : this(0, playlistId, songId) {

    }
}

object Orientation{
    const val VERTICAL = 0
    const val HORIZONTAL_SQUARE = 1
    const val HORIZONTAL_WIDE = 2
}