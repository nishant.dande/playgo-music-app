package com.play.models.music

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = [Index(value = ["uri"], unique = true)])
open class Song(@PrimaryKey(autoGenerate = true) val id: Int,
                @ColumnInfo(name = "title") val title: String,
                @ColumnInfo(name = "artist") @Transient var artist: String = "",
                @ColumnInfo(name = "uri") val uri: String,
                @ColumnInfo(name = "provider") val provider: String,
                @ColumnInfo(name = "like") val like: Boolean = false,
                @ColumnInfo(name = "image") val image: String){


    constructor(artist: String, title: String, uri: String, provider: String, image: String) :
            this(0, title, artist, uri, provider, false, image )
}