package com.play.models.music

import androidx.room.Entity


@Entity
data class Artist(
    var externalUrls: ExternalUrls?,
    var href: String?,
    var id: String?,
    var name: String?,
    var type: String?,
    var uri: String?
) {
    constructor() :
            this(ExternalUrls(""), "","","","","") {

    }
}

@Entity
data class ExternalUrls(
    var spotify: String?
){
    constructor() : this("") {

    }
}