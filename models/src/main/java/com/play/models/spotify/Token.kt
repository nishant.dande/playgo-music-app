package com.play.models.spotify

data class Token(
    val access_token: String,
    val expires_in: Int,
    val scope: String,
    val token_type: String
)