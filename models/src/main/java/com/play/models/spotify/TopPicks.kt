package com.play.models.spotify


import com.google.gson.annotations.SerializedName

data class TopPicks(
    @SerializedName("playlists")
    val playlists: Playlists?
) {
    data class Playlists(
        @SerializedName("href")
        val href: String?,
        @SerializedName("items")
        val items: List<Item?>?,
        @SerializedName("limit")
        val limit: Int?,
        @SerializedName("next")
        val next: Any?,
        @SerializedName("offset")
        val offset: Int?,
        @SerializedName("previous")
        val previous: Any?,
        @SerializedName("total")
        val total: Int?
    ) {
        data class Item(
            @SerializedName("collaborative")
            val collaborative: Boolean?,
            @SerializedName("description")
            val description: String?,
            @SerializedName("external_urls")
            val externalUrls: ExternalUrls?,
            @SerializedName("href")
            val href: String?,
            @SerializedName("id")
            val id: String?,
            @SerializedName("images")
            val images: List<Image?>?,
            @SerializedName("name")
            val name: String?,
            @SerializedName("owner")
            val owner: Owner?,
            @SerializedName("primary_color")
            val primaryColor: Any?,
            @SerializedName("public")
            val `public`: Any?,
            @SerializedName("snapshot_id")
            val snapshotId: String?,
            @SerializedName("tracks")
            val tracks: Tracks?,
            @SerializedName("type")
            val type: String?,
            @SerializedName("uri")
            val uri: String?
        ) {
            data class ExternalUrls(
                @SerializedName("spotify")
                val spotify: String?
            )

            data class Image(
                @SerializedName("height")
                val height: Any?,
                @SerializedName("url")
                val url: String?,
                @SerializedName("width")
                val width: Any?
            )

            data class Owner(
                @SerializedName("display_name")
                val displayName: String?,
                @SerializedName("external_urls")
                val externalUrls: ExternalUrls?,
                @SerializedName("href")
                val href: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("type")
                val type: String?,
                @SerializedName("uri")
                val uri: String?
            ) {
                data class ExternalUrls(
                    @SerializedName("spotify")
                    val spotify: String?
                )
            }

            data class Tracks(
                @SerializedName("href")
                val href: String?,
                @SerializedName("total")
                val total: Int?
            )
        }
    }
}