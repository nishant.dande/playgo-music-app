package com.play.models.spotify


import com.google.gson.annotations.SerializedName

data class Trending(
    @SerializedName("collaborative")
    val collaborative: Boolean?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("external_urls")
    val externalUrls: ExternalUrls?,
    @SerializedName("followers")
    val followers: Followers?,
    @SerializedName("href")
    val href: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("images")
    val images: List<Image?>?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("owner")
    val owner: Owner?,
    @SerializedName("primary_color")
    val primaryColor: Any?,
    @SerializedName("public")
    val `public`: Boolean?,
    @SerializedName("snapshot_id")
    val snapshotId: String?,
    @SerializedName("tracks")
    val tracks: Tracks?,
    @SerializedName("type")
    val type: String?,
    @SerializedName("uri")
    val uri: String?
) {
    data class ExternalUrls(
        @SerializedName("spotify")
        val spotify: String?
    )

    data class Followers(
        @SerializedName("href")
        val href: Any?,
        @SerializedName("total")
        val total: Int?
    )

    data class Image(
        @SerializedName("height")
        val height: Any?,
        @SerializedName("url")
        val url: String?,
        @SerializedName("width")
        val width: Any?
    )

    data class Owner(
        @SerializedName("display_name")
        val displayName: String?,
        @SerializedName("external_urls")
        val externalUrls: ExternalUrls?,
        @SerializedName("href")
        val href: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("type")
        val type: String?,
        @SerializedName("uri")
        val uri: String?
    ) {
        data class ExternalUrls(
            @SerializedName("spotify")
            val spotify: String?
        )
    }

    data class Tracks(
        @SerializedName("href")
        val href: String?,
        @SerializedName("items")
        val items: List<Item?>?,
        @SerializedName("limit")
        val limit: Int?,
        @SerializedName("next")
        val next: Any?,
        @SerializedName("offset")
        val offset: Int?,
        @SerializedName("previous")
        val previous: Any?,
        @SerializedName("total")
        val total: Int?
    ) {
        data class Item(
            @SerializedName("added_at")
            val addedAt: String?,
            @SerializedName("added_by")
            val addedBy: AddedBy?,
            @SerializedName("is_local")
            val isLocal: Boolean?,
            @SerializedName("primary_color")
            val primaryColor: Any?,
            @SerializedName("track")
            val track: Track?,
            @SerializedName("video_thumbnail")
            val videoThumbnail: VideoThumbnail?
        ) {
            data class AddedBy(
                @SerializedName("external_urls")
                val externalUrls: ExternalUrls?,
                @SerializedName("href")
                val href: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("type")
                val type: String?,
                @SerializedName("uri")
                val uri: String?
            ) {
                data class ExternalUrls(
                    @SerializedName("spotify")
                    val spotify: String?
                )
            }

            data class Track(
                @SerializedName("album")
                val album: Album?,
                @SerializedName("artists")
                val artists: List<Artist?>?,
                @SerializedName("available_markets")
                val availableMarkets: List<String?>?,
                @SerializedName("disc_number")
                val discNumber: Int?,
                @SerializedName("duration_ms")
                val durationMs: Int?,
                @SerializedName("episode")
                val episode: Boolean?,
                @SerializedName("explicit")
                val explicit: Boolean?,
                @SerializedName("external_ids")
                val externalIds: ExternalIds?,
                @SerializedName("external_urls")
                val externalUrls: ExternalUrls?,
                @SerializedName("href")
                val href: String?,
                @SerializedName("id")
                val id: String?,
                @SerializedName("is_local")
                val isLocal: Boolean?,
                @SerializedName("name")
                val name: String?,
                @SerializedName("popularity")
                val popularity: Int?,
                @SerializedName("preview_url")
                val previewUrl: Any?,
                @SerializedName("track")
                val track: Boolean?,
                @SerializedName("track_number")
                val trackNumber: Int?,
                @SerializedName("type")
                val type: String?,
                @SerializedName("uri")
                val uri: String?
            ) {
                data class Album(
                    @SerializedName("album_type")
                    val albumType: String?,
                    @SerializedName("artists")
                    val artists: List<Artist?>?,
                    @SerializedName("available_markets")
                    val availableMarkets: List<String?>?,
                    @SerializedName("external_urls")
                    val externalUrls: ExternalUrls?,
                    @SerializedName("href")
                    val href: String?,
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("images")
                    val images: List<Image?>?,
                    @SerializedName("name")
                    val name: String?,
                    @SerializedName("release_date")
                    val releaseDate: String?,
                    @SerializedName("release_date_precision")
                    val releaseDatePrecision: String?,
                    @SerializedName("total_tracks")
                    val totalTracks: Int?,
                    @SerializedName("type")
                    val type: String?,
                    @SerializedName("uri")
                    val uri: String?
                ) {
                    data class Artist(
                        @SerializedName("external_urls")
                        val externalUrls: ExternalUrls?,
                        @SerializedName("href")
                        val href: String?,
                        @SerializedName("id")
                        val id: String?,
                        @SerializedName("name")
                        val name: String?,
                        @SerializedName("type")
                        val type: String?,
                        @SerializedName("uri")
                        val uri: String?
                    ) {
                        data class ExternalUrls(
                            @SerializedName("spotify")
                            val spotify: String?
                        )
                    }

                    data class ExternalUrls(
                        @SerializedName("spotify")
                        val spotify: String?
                    )

                    data class Image(
                        @SerializedName("height")
                        val height: Int?,
                        @SerializedName("url")
                        val url: String?,
                        @SerializedName("width")
                        val width: Int?
                    )
                }

                data class Artist(
                    @SerializedName("external_urls")
                    val externalUrls: ExternalUrls?,
                    @SerializedName("href")
                    val href: String?,
                    @SerializedName("id")
                    val id: String?,
                    @SerializedName("name")
                    val name: String?,
                    @SerializedName("type")
                    val type: String?,
                    @SerializedName("uri")
                    val uri: String?
                ) {
                    data class ExternalUrls(
                        @SerializedName("spotify")
                        val spotify: String?
                    )
                }

                data class ExternalIds(
                    @SerializedName("isrc")
                    val isrc: String?
                )

                data class ExternalUrls(
                    @SerializedName("spotify")
                    val spotify: String?
                )
            }

            data class VideoThumbnail(
                @SerializedName("url")
                val url: Any?
            )
        }
    }
}