package com.play.models.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.play.models.music.Playlist
import java.lang.reflect.Type

class Converters {
    @TypeConverter
    fun fromPlaylistToString(value: List<Playlist>): String = value.toJson()
    @TypeConverter
    fun fromStringToPlaylist(value: String): List<Playlist> = value.toList()
}

fun <T> T.toJson(): String = gson().toJson(this, object : TypeToken<T>() {}.type)
fun <T> String.toInstance(): T = getInstanceFromJson(this, object : TypeToken<T>() {}.type)
fun <T> List<T>.toJson(): String = gson().toJson(this, object : TypeToken<List<T>>() {}.type)
fun <T> String.toList(): List<T> = getInstanceFromJson(this, object : TypeToken<List<T>>() {}.type)


/**
 * added - <code> GsonBuilder.serializeSpecialFloatingPointValues() </code> -
 * to avoid "infinity is not a valid double value as per JSON specification exception"
 */
private fun gson(): Gson {
    return GsonBuilder().serializeSpecialFloatingPointValues().create()
}

private inline fun <reified T> toJson(obj: T, typeToken: Type): String {
    return gson().toJson(obj, typeToken)
}

private fun <T> getInstanceFromJson(json: String?, typeToken: Type): T {
    return gson().fromJson<T>(json, typeToken)
}
