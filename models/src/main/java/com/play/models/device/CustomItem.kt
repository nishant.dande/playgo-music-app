package com.play.models.device

open class CustomItem(val name: String,
                      var subTitle: String? = null,
                      val checked: Boolean = false,
                      val isClickable: Boolean = true,
                      val showSettingIcon: Boolean = false,
                      val showSwitch: Boolean = false,
                      val ShowArrow: Boolean = false,
                      val ShowTextArrow: Boolean = false,
                      val ShowText: Boolean = false)
