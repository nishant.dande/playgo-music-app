package com.play.persistance

import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song

class Library(private val dao: LibraryDao) {

    suspend fun selectAllPlaylist() : List<Playlist>{
        return dao.selectAllPlaylist()
    }

    suspend fun createPlaylist(playlist: Playlist) {
        dao.upsert(playlist)
    }

    suspend fun getRecentAdded() : List<Song>{
        return dao.selectAllRecents()
    }

    suspend fun addRecentSong(song: Song) {
        song.apply {
            if (artist == null)
                artist = ""
        }
        dao.upsert(song)
    }

    suspend fun addSongToPlaylist(recent: ManagePlaylist){
        dao.upsert(recent)
    }

    suspend fun getPlaylistById(id: Int) : Playlist{
        return dao.getPlaylistById(id)
    }

    suspend fun getSongById(id: Int) : Song{
        return dao.getSongById(id)
    }
    suspend fun getSongByUri(uri: String) : Song {
        return dao.getSongByUri(uri)
    }
}