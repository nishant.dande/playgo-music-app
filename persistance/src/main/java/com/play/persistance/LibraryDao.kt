package com.play.persistance

import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song

interface LibraryDao {
    suspend fun selectAllRecents(): List<Song>
    suspend fun selectAllPlaylist() : List<Playlist>
    suspend fun getPlaylistById(id: Int) : Playlist
    suspend fun getSongById(id: Int) : Song
    suspend fun getSongByUri(uri: String) : Song
    suspend fun upsert(vararg recent: Playlist)
    suspend fun upsert(vararg recent: Song)
    suspend fun upsert(vararg recent: ManagePlaylist)
}