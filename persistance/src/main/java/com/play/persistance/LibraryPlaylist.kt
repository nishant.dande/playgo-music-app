package com.play.persistance

import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song

class LibraryPlaylist(private val database: MusicLibraryDatabase) : LibraryDao {
    override suspend fun selectAllRecents(): List<Song> {
        return database.libraryDao().selectAllRecents()
    }

    override suspend fun selectAllPlaylist(): List<Playlist> {
        return database.libraryDao().selectAllPlaylist()
    }

    override suspend fun getPlaylistById(id: Int): Playlist {
        // Get Manage Playlist
        var managePlaylist = database.libraryDao().getManagePlaylistById(id)
        //Get Song Details
        var songs : ArrayList<Song> = arrayListOf()
        managePlaylist.forEach {
            var song  = database.libraryDao().getSongById(it.songId)

            songs.add(song)
        }
        // Get Playlist
        var playlist = database.libraryDao().getPlaylistById(id)
        playlist.songs = songs

        return playlist
    }

    override suspend fun getSongById(id: Int): Song {
        return database.libraryDao().getSongById(id)
    }

    override suspend fun getSongByUri(uri: String): Song {
        return database.libraryDao().getSongById(uri)
    }

    override suspend fun upsert(vararg recent: Playlist) {
        database.libraryDao().upsert(* recent)
    }

    override suspend fun upsert(vararg recent: Song) {
        database.libraryDao().upsert(* recent)
    }

    override suspend fun upsert(vararg recent: ManagePlaylist) {
        database.libraryDao().upsert(* recent)
    }
}