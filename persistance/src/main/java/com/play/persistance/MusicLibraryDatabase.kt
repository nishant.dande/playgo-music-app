package com.play.persistance

import androidx.room.Database
import androidx.room.RoomDatabase
import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song

@Database(entities = [Playlist::class, Song::class, ManagePlaylist::class], version = 1)
abstract class MusicLibraryDatabase : RoomDatabase() {
    abstract fun libraryDao() : LibraryPlaylistDAO
}