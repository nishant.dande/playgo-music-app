package com.play.persistance

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.play.models.music.ManagePlaylist
import com.play.models.music.Playlist
import com.play.models.music.Song

@Dao
interface LibraryPlaylistDAO {

    @Query("SELECT * FROM Song")
    fun selectAllRecents(): List<Song>

    @Query("SELECT * FROM Playlist")
    fun selectAllPlaylist() : List<Playlist>

    @Query("SELECT * FROM Playlist  WHERE id =:id")
    fun getPlaylistById(id: Int) : Playlist

    @Query("SELECT * FROM Song  WHERE id =:id")
    fun getSongById(id: Int): Song

    @Query("SELECT * FROM Song  WHERE uri =:uri")
    fun getSongById(uri: String): Song

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(vararg recent: Playlist)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(vararg song: Song)

    @Query("SELECT * FROM ManagePlaylist WHERE playlistId =:id")
    suspend fun getManagePlaylistById(id: Int) : List<ManagePlaylist>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(vararg recent: ManagePlaylist)

}