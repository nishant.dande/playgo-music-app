package com.play.ble_service

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Handler
import android.util.Log
import com.play.ble_service.model.CustomDevice
import com.play.ble_service.receiver.BREDRDiscoveryReceiver
import com.play.ble_service.receiver.BluetoothStateReceiver
import com.play.ble_service.receiver.BondStateReceiver
import com.play.ble_service.service.BluetoothService
import com.play.ble_service.utils.Consts
import com.play.ble_service.utils.Consts.ACTION_REQUEST_ENABLE_BLUETOOTH
import java.util.*

const val TAG = "BluetoothWrapper"

class BluetoothWrapper() : BREDRDiscoveryReceiver.BREDRDiscoveryListener,
    BluetoothStateReceiver.BroadcastReceiverListener, BondStateReceiver.BondStateListener {

    private val mDiscoveryReceiver: BREDRDiscoveryReceiver = BREDRDiscoveryReceiver(this)

    /**
     * To keep the instance of the bond state receiver in order to unregister it.
     */
    private val mBondStateReceiver: BondStateReceiver = BondStateReceiver(this)

    /**
     * The instance of the Bluetooth adapter used to retrieve paired Bluetooth devices.
     */
    var mBtAdapter: BluetoothAdapter? = null

    /**
     * The Broadcast receiver we used to have information about the Bluetooth state on the device.
     */
    private var mBluetoothStateReceiver: BroadcastReceiver? = null
    private lateinit var context : Activity

    /**
     * The handler to use to postpone some actions.
     */
    private val mHandler = Handler()

    /**
     * To know if the scan is running.
     */
    private var mIsScanning = false

    /**
     * The callback called when a device has been scanned by the LE scanner.
     */
    private val mLeScanCallback: LeScanCallback = LeScanCallback(this)

    /**
     * The runnable to trigger to stop the scan once the scanning time is finished.
     */
    private val mStopScanRunnable = Runnable { stopScan() }

    private var deviceListener : DeviceListener? = null

    constructor(context: Activity) : this() {
        this.context = context
        init()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        this.context.registerReceiver(mBluetoothStateReceiver, filter)
        checkEnableBt()
    }

    /**
     * To initialise objects used in this activity.
     */
    private fun init() {
        Log.d(TAG, "Init Bluetooth Adapter and Receiver")
        // Bluetooth adapter
        mBtAdapter = (this.context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter

        // Register for broadcasts on BluetoothAdapter state change so that we can tell if it has been turned off.
        mBluetoothStateReceiver = BluetoothStateReceiver(this)


    }

    /**
     *
     * To register the bond state receiver to be aware of any bond state change.
     */
    fun registerReceiver() {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        context.registerReceiver(mDiscoveryReceiver, filter)
    }

    /**
     *
     * To unregister the bond state receiver when the application is stopped or we don't need it anymore.
     */
    fun unregisterReceiver() {
        context.unregisterReceiver(mDiscoveryReceiver)
    }

    override fun onDeviceFound(device: BluetoothDevice?) {
        if (device != null && device.name != null) {
            Log.d(TAG, "onDeviceFound " + device?.name + " " + device?.address + " " + device.type)
            if (deviceListener != null){
                deviceListener!!.getDevices(
                    CustomDevice(
                        UNBONDED_DEVICE, 0, device, getBluetoothTransport(
                            device.type
                        )
                    )
                )
            }
        }
    }

    /**
     *
     * This method checks if Bluetooth is enabled on the phone.
     *
     * @return true is the Bluetooth is enabled, false otherwise.
     */
    private fun isBluetoothEnabled(): Boolean {
        return mBtAdapter == null || !mBtAdapter!!.isEnabled
    }

    fun getBluetoothTransport(transport: Int): Int {
        return when (transport) {
            BluetoothDevice.DEVICE_TYPE_CLASSIC -> {
                BluetoothService.Transport.BR_EDR
            }
            BluetoothDevice.DEVICE_TYPE_LE -> {
                BluetoothService.Transport.BLE
            }
            else -> {
                BluetoothService.Transport.UNKNOWN
            }
        }
    }

    /**
     * Display a dialog requesting Bluetooth to be enabled if it isn't already. Otherwise this method updates the
     * list to the list view. The list view needs to be ready when this method is called.
     */
    private fun checkEnableBt() {
        Log.d(TAG, "checkEnableBt")
        if (isBluetoothEnabled()) {
            Log.d(TAG, "ACTION_REQUEST_ENABLE Bluetooth")
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            context.startActivityForResult(enableBtIntent, ACTION_REQUEST_ENABLE_BLUETOOTH)
        } else {
            onBluetoothEnabled()
        }
    }

    override fun onBluetoothDisabled() {
        Log.d(TAG, "onBluetoothDisabled")
    }

    override fun onBluetoothEnabled() {
        Log.d(TAG, "onBluetoothEnabled")
        // Scan For Devices
        scanDevices(true)
        getBondedDevices()
    }

    public fun scanDevices(scan: Boolean) {
        assert(mBtAdapter != null)
        if (scan && !mIsScanning) {
            mIsScanning = true
            mHandler.postDelayed(mStopScanRunnable, Consts.SCANNING_TIME.toLong())
            val isScanning = mBtAdapter!!.startLeScan(mLeScanCallback)
            val isDiscovering = mBtAdapter!!.startDiscovery()
            if (BuildConfig.DEBUG) Log.i(
                TAG,
                "Start scan of LE devices: " + isScanning + " - start discovery of BR/EDR devices: " +
                        isDiscovering
            )
        } else if (mIsScanning) {
            mIsScanning = false
            mHandler.removeCallbacks(mStopScanRunnable)
            mBtAdapter!!.stopLeScan(mLeScanCallback)
            val isDiscovering = mBtAdapter!!.cancelDiscovery()
            if (BuildConfig.DEBUG) Log.i(
                TAG,
                "Stop scan of LE devices - stop discovery of BR/EDR devices: $isDiscovering"
            )
        }
    }

    private fun stopScan() {
        Log.d(TAG, "Stop Scan")
        scanDevices(false)
    }

    /**
     * Callback for scan results.
     */
    private class LeScanCallback(val bluetoothWrapper: BluetoothWrapper) : BluetoothAdapter.LeScanCallback {


        override fun onLeScan(device: BluetoothDevice, rssi: Int, scanRecord: ByteArray) {
            device.let {
                if (it.name != null) {
                    Log.d(TAG, "onLeScan : " + it.name + " " + it.address + " " + it.type)
                    if (bluetoothWrapper.deviceListener != null){
                        bluetoothWrapper.deviceListener!!.getDevices(
                            CustomDevice(
                                UNBONDED_DEVICE, 0, device,
                                bluetoothWrapper.getBluetoothTransport(device.type)
                            )
                        )
                    }
                }
            }
        }
    }

    private fun getBondedDevices(){

        val listDevices: Set<BluetoothDevice> = if (mBtAdapter != null && mBtAdapter!!.isEnabled) {
            mBtAdapter!!.bondedDevices
        } else {
            emptySet()
        }

        val listBLEDevices = ArrayList<BluetoothDevice>()

        for (device in listDevices) {
            if (device.type == BluetoothDevice.DEVICE_TYPE_DUAL ||
                device.type == BluetoothDevice.DEVICE_TYPE_CLASSIC ||
                device.type == BluetoothDevice.DEVICE_TYPE_LE) {

                device.let {
                    if (it.name != null) {
                        Log.d(TAG, "onBondedDevice : " + it.name + " " + it.address + " " + it.type)
                        if (deviceListener != null){
                            deviceListener!!.getDevices(
                                CustomDevice(
                                    BONDED_DEVICE, 0, device, getBluetoothTransport(
                                        device.type
                                    )
                                )
                            )
                        }
                    }
                }

                listBLEDevices.add(device)
            }
        }
    }


    fun getScannedDevices(deviceListener: DeviceListener){
        this.deviceListener = deviceListener
        getBondedDevices()
    }


    /**
     *
     * To register the bond stat receiver in order to be informed of any bond state change.
     */
    fun registerBondReceiver() {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST)
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        context.registerReceiver(mBondStateReceiver, filter)
    }

    /**
     *
     * To unregister the bond stat receiver when the application is stopped or it is not needed anymore.
     */
    fun unregisterBondReceiver() {
        context.unregisterReceiver(mBondStateReceiver)
    }


    companion object{
        const val BONDED_DEVICE = 0
        const val UNBONDED_DEVICE = 1
    }

    interface DeviceListener{
        fun getDevices(customDevice: CustomDevice)
        fun onConnectionStateChange(device: BluetoothDevice?, state: String)
    }

    override fun onBondStateChange(device: BluetoothDevice?, state: Int) {
        Log.d(TAG, device?.name+" "+state)
    }

    override fun onConnectionStateChange(device: BluetoothDevice?, state: String) {
        if (this.deviceListener != null){
            this.deviceListener!!.onConnectionStateChange(device, state)
        }
    }
}