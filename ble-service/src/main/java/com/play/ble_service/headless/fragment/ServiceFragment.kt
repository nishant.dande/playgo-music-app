package com.play.ble_service.headless.fragment

import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.Context.BIND_AUTO_CREATE
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Message
import android.util.Log
import android.view.MenuItem
import com.play.ble_service.service.BluetoothService
import com.play.ble_service.service.GAIABREDRService
import com.play.ble_service.service.GAIAGATTBLEService
import com.play.ble_service.utils.Consts
import java.lang.ref.WeakReference

abstract class ServiceFragment : BluetoothFragment() {
    /**
     * The tag to use for the logs.
     */
    private val TAG = "ServiceActivity"

    /**
     * The BLE service to communicate with any device.
     */
    var mService: BluetoothService? = null

    /**
     * The service connection object to manage the service bind and unbind.
     */
    private val mServiceConnection: ServiceConnection = ActivityServiceConnection(this)

    /**
     * The handler used by the service to be linked to this activity.
     */
    private var mHandler: ActivityHandler? = null

    /**
     * To know if this activity is in the pause state.
     */
    private var mIsPaused = false

    /**
     * The type of Bluetooth transport use to communicate with a Bluetooth device.
     */
    @BluetoothService.Transport
    private var mTransport: Int = BluetoothService.Transport.UNKNOWN


    // ====== ACTIVITY METHODS =====================================================================

    // ====== ACTIVITY METHODS =====================================================================
    // When the activity is created.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    // When the activity is resumed.
    override fun onResume() {
        super.onResume()
        mIsPaused = false
        if (mService != null) {
            initService()
        } else {
            Log.d(TAG, "BluetoothLEService not bound yet.")
        }
    }

    // When the activity is paused.
    override fun onPause() {
        super.onPause()
        mIsPaused = true
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mService != null) {
            mService!!.removeHandler(mHandler)
            mService = null
            requireContext().unbindService(mServiceConnection)
        }
    }

    // to implement the back navigation for all classes which have a parent
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.home -> {
//                onBackPressed()
//                return true
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }


    // ====== PROTECTED METHODS ====================================================================

    // ====== PROTECTED METHODS ====================================================================
    /**
     *
     * To get the type of Bluetooth transport uses to communicate with a BluetoothDevice.
     *
     * @return The transport, one of the followings:
     * [BR_EDR][com.qualcomm.qti.gaiacontrol.services.BluetoothService.Transport.BR_EDR],
     * [BLE][com.qualcomm.qti.gaiacontrol.services.BluetoothService.Transport.BLE] or
     * [UNKNOWN][com.qualcomm.qti.gaiacontrol.services.BluetoothService.Transport.UNKNOWN].
     */
    /*package*/
    @BluetoothService.Transport
    fun getTransport(): Int {
        return mTransport
    }

    // ====== PUBLIC METHODS =======================================================================

    // ====== PUBLIC METHODS =======================================================================
    override fun onBluetoothEnabled() {
        super.onBluetoothEnabled()
        if (mService == null) {
            startService()
        }
    }


    // ====== PRIVATE METHODS ======================================================================
    /**
     *
     * This method allows to init the bound service by defining this activity as a handler listening its messages.
     */
    private fun initService() {

        mService?.addHandler(mHandler)
        if (mService?.getDevice() == null) {
            // get the bluetooth information
            val sharedPref: SharedPreferences =
                requireContext().getSharedPreferences(Consts.PREFERENCES_FILE, Context.MODE_PRIVATE)

            // get the device Bluetooth address
            val address = sharedPref.getString(Consts.BLUETOOTH_ADDRESS_KEY, "")
            val done: Boolean = mService?.connectToDevice(address) ?: false
            if (!done) Log.w(TAG, "connection failed")
        }
    }

    /**
     * To initialise objects used in this activity.
     */
    private fun init() {
        // the Handler to receive messages from the GAIAGATTBLEService once attached
        mHandler = ActivityHandler(this)
    }

    /**
     *
     * To start the Android Service which will allow this application to communicate with a BluetoothDevice.
     *
     * This method will start the [GAIABREDRService] or the
     * [GAIAGATTBLEService] depending on the content of the SharedPreferences file
     * [Consts.PREFERENCES_FILE].
     */
    private fun startService(): Boolean {
        // get the bluetooth information
        val sharedPref: SharedPreferences =
            requireContext().getSharedPreferences(Consts.PREFERENCES_FILE, Context.MODE_PRIVATE)

        // get the device Bluetooth address
        val address = sharedPref.getString(Consts.BLUETOOTH_ADDRESS_KEY, "")
        if (address!!.length == 0 || !BluetoothAdapter.checkBluetoothAddress(address)) {
            // no address, not possible to establish a connection
            return false
        }

        // get the transport type
        val transport = sharedPref.getInt(Consts.TRANSPORT_KEY, BluetoothService.Transport.UNKNOWN)
        mTransport =
            if (transport == BluetoothService.Transport.BLE) BluetoothService.Transport.BLE else if (transport == BluetoothService.Transport.BR_EDR) BluetoothService.Transport.BR_EDR else BluetoothService.Transport.UNKNOWN
        if (mTransport == BluetoothService.Transport.UNKNOWN) {
            // transport unknown, not possible to establish a connection
            return false
        }

        // get the service class to bind
        val serviceClass: Class<*> =
            if (mTransport == BluetoothService.Transport.BLE)
                GAIAGATTBLEService::class.java else GAIABREDRService::class.java // mTransport can only be BLE or BR EDR

        // bind the service
        val gattServiceIntent: Intent = Intent(requireActivity(), serviceClass)
        gattServiceIntent.putExtra(
            Consts.BLUETOOTH_ADDRESS_KEY,
            address
        ) // give address to the service
        return requireActivity().bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE)
    }


    // ====== ABSTRACT METHODS =====================================================================

    // ====== ABSTRACT METHODS =====================================================================
    /**
     *
     * This method is called when the connected service is sending a message to the activity.
     *
     * @param msg
     * The message received from the service.
     */
    protected abstract fun handleMessageFromService(msg: Message?)

    /**
     *
     * This method is called when the service has been bound to this activity.
     */
    protected abstract fun onServiceConnected()

    /**
     *
     * This method is called when the service has been unbound from this activity.
     */
    protected abstract fun onServiceDisconnected()


    // ====== INNER CLASS ==========================================================================

    // ====== INNER CLASS ==========================================================================
    /**
     *
     * This class is used to be informed of the connection state of the BLE service.
     */
    private class ActivityServiceConnection internal constructor(activity: ServiceFragment) :
        ServiceConnection {
        /**
         * The reference to this activity.
         */
        val mActivity: WeakReference<ServiceFragment>
        override fun onServiceConnected(componentName: ComponentName, service: IBinder) {
            val parentActivity: ServiceFragment? =
                mActivity.get()
            if (componentName.className == GAIAGATTBLEService::class.java.getName()) {
                parentActivity?.mService = (service as GAIAGATTBLEService.LocalBinder).getService()
            } else if (componentName.className == GAIABREDRService::class.java.getName()) {
                parentActivity?.mService = (service as GAIABREDRService.LocalBinder).getService()
            }
            if (parentActivity?.mService != null) {
                parentActivity.initService()
                parentActivity.onServiceConnected() // to inform subclass
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            if (componentName.className == GAIAGATTBLEService::class.java.getName()) {
                val parentActivity: ServiceFragment? =
                    mActivity.get()
                parentActivity?.mService = null
                parentActivity?.onServiceDisconnected() // to inform subclass
            }
        }

        /**
         * The constructor for this activity service connection.
         *
         * @param activity
         * this activity.
         */
        init {
            mActivity =
                WeakReference<ServiceFragment>(activity)
        }
    }

    /**
     *
     * This class is for receiving and managing messages from a [GAIAGATTBLEService].
     */
    private class ActivityHandler internal constructor(activity: ServiceFragment) :
        Handler() {
        /**
         * The reference to this activity.
         */
        val mReference: WeakReference<ServiceFragment>
        override fun handleMessage(msg: Message) {
            val activity: ServiceFragment? =
                mReference.get()
            if (!activity?.mIsPaused!!) {
                activity?.handleMessageFromService(msg)
            }
        }

        /**
         * The constructor for this activity handler.
         *
         * @param activity
         * this activity.
         */
        init {
            mReference =
                WeakReference<ServiceFragment>(activity)
        }
    }
}