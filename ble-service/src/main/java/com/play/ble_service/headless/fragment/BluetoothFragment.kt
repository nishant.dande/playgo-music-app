package com.play.ble_service.headless.fragment

import android.app.Activity.RESULT_OK
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.play.ble_service.receiver.BluetoothStateReceiver
import com.play.ble_service.utils.Consts.ACTION_REQUEST_ENABLE_BLUETOOTH

open class BluetoothFragment : Fragment(), BluetoothStateReceiver.BroadcastReceiverListener {

    /**
     * The Broadcast receiver we used to have information about the Bluetooth state on the device.
     */
    private var mBluetoothStateReceiver: BroadcastReceiver? = null

    /**
     * To know if we are using the application in debug mode.
     */
    val DEBUG: Boolean = true

    /**
     * The instance of the Bluetooth adapter used to retrieve paired Bluetooth devices.
     */
    var mBtAdapter: BluetoothAdapter? = null


    // ====== ACTIVITY METHODS =====================================================================

    // ====== ACTIVITY METHODS =====================================================================
    // When the activity is created.
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    // Callback activated after the user responds to the enable Bluetooth dialogue.
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            ACTION_REQUEST_ENABLE_BLUETOOTH -> {
                if (resultCode == RESULT_OK) {
                    onBluetoothEnabled()
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    // When the activity is resumed.
    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        requireContext().registerReceiver(mBluetoothStateReceiver, filter)
        checkEnableBt()
    }

    // When the activity is paused.
    override fun onPause() {
        super.onPause()
        requireContext().unregisterReceiver(mBluetoothStateReceiver)
    }


    // ====== PROTECTED METHODS ====================================================================

    // ====== PROTECTED METHODS ====================================================================
    /**
     * To display a long toast inside this activity
     *
     * @param textID
     * The ID of the text to display from the strings file.
     */
    fun displayLongToast(textID: Int) {
        Toast.makeText(requireContext(), textID, Toast.LENGTH_LONG).show()
    }

    /**
     * To display a long toast inside this activity
     *
     * @param text
     * The text to display.
     */
    fun displayLongToast(text: String?) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
    }

    /**
     * To display a short toast inside this activity
     *
     * @param text
     * The text to display.
     */
    fun displayShortToast(text: String?) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
    }

    /**
     *
     * This method checks if Bluetooth is enabled on the phone.
     *
     * @return true is the Bluetooth is enabled, false otherwise.
     */
    private fun isBluetoothEnabled(): Boolean {
        return mBtAdapter == null || !mBtAdapter!!.isEnabled
    }


    // ====== PUBLIC METHODS =======================================================================

    // ====== PUBLIC METHODS =======================================================================
    override fun onBluetoothDisabled() {
        checkEnableBt()
    }

    override fun onBluetoothEnabled() {}


    // ====== PRIVATE METHODS ======================================================================

    // ====== PRIVATE METHODS ======================================================================
    /**
     * To initialise objects used in this activity.
     */
    private fun init() {
        // Bluetooth adapter
        mBtAdapter = (requireActivity().getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter

        // Register for broadcasts on BluetoothAdapter state change so that we can tell if it has been turned off.
        mBluetoothStateReceiver = BluetoothStateReceiver(this)
    }

    /**
     * Display a dialog requesting Bluetooth to be enabled if it isn't already. Otherwise this method updates the
     * list to the list view. The list view needs to be ready when this method is called.
     */
    private fun checkEnableBt() {
        if (isBluetoothEnabled()) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, ACTION_REQUEST_ENABLE_BLUETOOTH)
        } else {
            onBluetoothEnabled()
        }
    }


}