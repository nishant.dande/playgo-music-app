package com.play.ble_service.utils;

import java.text.DecimalFormat;

/**
 * <p>This class contains all useful methods for this module.</p>
 */
@SuppressWarnings("WeakerAccess")
public class Utils {

    /**
     * The number of bits in a byte.
     */
    private static final int BITS_IN_BYTE = 8;
    /**
     * <p>The number of bytes contains in a int.</p>
     */
    public static final int BYTES_IN_INT = 4;
    /**
     * <p>The number of bytes contains in a short.</p>
     */
    private static final int BYTES_IN_SHORT = 2;
    /**
     * The number of bits contains in an hexadecimal value.
     */
    public static final int BITS_IN_HEXADECIMAL = 4;
    /**
     * To display a number in a specific decimal format.
     */
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat();

    /**
     * Convert a byte array to a human readable String.
     *
     * @param value
     *            The byte array.
     * @return String object containing values in byte array formatted as hex.
     */
    public static String getStringFromBytes(byte[] value) {
        if (value == null)
            return "null";
        final StringBuilder stringBuilder = new StringBuilder(value.length*2);
        //noinspection ForLoopReplaceableByForEach // the for loop is more efficient than the foreach one
        for (int i = 0; i < value.length; i++) {
            stringBuilder.append(String.format("0x%02x ", value[i]));
        }
        return stringBuilder.toString();
    }

    /**
     * Get 16-bit hexadecimal string representation of byte.
     *
     * @param i
     *            The value.
     *
     * @return Hex value as a string.
     */
    @SuppressWarnings("unused")
    public static String getIntToHexadecimal(int i) {
        return String.format("0x%04X", i & 0xFFFF);
    }

    /**
     * <p>This method allows to copy a int value into a byte array from the specified <code>offset</code> location to
     * the <code>offset + length</code> location.</p>
     *
     * @param sourceValue
     *         The <code>int</code> value to copy in the array.
     * @param target
     *         The <code>byte</code> array to copy in the <code>int</code> value.
     * @param targetOffset
     *         The targeted offset in the array to copy the first byte of the <code>int</code> value.
     * @param length
     *         The number of bytes in the array to copy the <code>int</code> value.
     * @param reverse
     *         True if bytes should be interpreted in reverse (little endian) order.
     */
    @SuppressWarnings("SameParameterValue")
    public static void copyIntIntoByteArray(int sourceValue, byte [] target, int targetOffset, int length, boolean reverse) {
        if (length < 0 | length > BYTES_IN_INT) {
            throw new IndexOutOfBoundsException("Length must be between 0 and " + BYTES_IN_INT);
        }

        if (reverse) {
            int shift = 0;
            int j = 0;
            for (int i = length-1; i >= 0; i--) {
                int mask = 0xFF << shift;
                target[j+targetOffset] = (byte)((sourceValue & mask) >> shift);
                shift += BITS_IN_BYTE;
                j++;
            }
        }
        else {
            int shift = (length-1) * BITS_IN_BYTE;
            for (int i = 0; i < length; i++) {
                int mask = 0xFF << shift;
                target[i+targetOffset] = (byte)((sourceValue & mask) >> shift);
                shift -= BITS_IN_BYTE;
            }
        }
    }

    /**
     * Extract a <code>short</code> field from an array.
     * @param source The array to extract from.
     * @param offset Offset within source array.
     * @param length Number of bytes to use (maximum 2).
     * @param reverse True if bytes should be interpreted in reverse (little endian) order.
     * @return The extracted integer.
     */
    @SuppressWarnings("SameParameterValue")
    public static short extractShortFromByteArray(byte [] source, int offset, int length, boolean reverse) {
        if (length < 0 | length > BYTES_IN_SHORT)
            throw new IndexOutOfBoundsException("Length must be between 0 and " + BYTES_IN_SHORT);
        short result = 0;
        int shift = (length-1) * BITS_IN_BYTE;

        if (reverse) {
            for (int i = offset+length-1; i >= offset; i--) {
                result |= ((source[i] & 0xFF) << shift);
                shift -= BITS_IN_BYTE;
            }
        }
        else {
            for (int i = offset; i < offset+length; i++) {
                result |= ((source[i] & 0xFF) << shift);
                shift -= BITS_IN_BYTE;
            }
        }
        return result;
    }

    /**
     * <p>Extract an <code>int</code> value from a <code>bytes</code> array.</p>
     *
     * @param source
     *         The array to extract from.
     * @param offset
     *         Offset within source array.
     * @param length
     *         Number of bytes to use (maximum 4).
     * @param reverse
     *         True if bytes should be interpreted in reverse (little endian) order.
     *
     * @return The extracted <code>int</code>.
     */
    @SuppressWarnings("SameParameterValue")
    public static int extractIntFromByteArray(byte[] source, int offset, int length, boolean reverse) {
        if (length < 0 | length > BYTES_IN_INT)
            throw new IndexOutOfBoundsException("Length must be between 0 and " + BYTES_IN_INT);
        int result = 0;
        int shift = (length - 1) * BITS_IN_BYTE;

        if (reverse) {
            for (int i = offset + length - 1; i >= offset; i--) {
                result |= ((source[i] & 0xFF) << shift);
                shift -= BITS_IN_BYTE;
            }
        } else {
            for (int i = offset; i < offset + length; i++) {
                result |= ((source[i] & 0xFF) << shift);
                shift -= BITS_IN_BYTE;
            }
        }
        return result;
    }

    /**
     * <p>To get the percentage as a String, formatted with the % type as follow.</p>
     *
     * @return the percentage as a formatted String value as follow: <code>value %</code>
     */
    public static String getStringForPercentage(double percentage) {
        if (percentage <= 1) {
            DECIMAL_FORMAT.setMaximumFractionDigits(2);
        }
        else {
            DECIMAL_FORMAT.setMaximumFractionDigits(1);
        }

        return DECIMAL_FORMAT.format(percentage) + " " + Consts.PERCENTAGE_CHARACTER;
    }

}