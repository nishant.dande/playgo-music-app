package com.play.ble_service.model

import android.bluetooth.BluetoothDevice

data class CustomDevice(
    var bonded: Int,
    var connected: Int = 0,
    val device: BluetoothDevice,
    var transport: Int) {
}