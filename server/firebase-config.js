var admin = require("firebase-admin");

var serviceAccount = require("./music-world-6fc4a-firebase-adminsdk-lkh1u-59dbce1d6c.json");


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

module.exports.admin = admin