# firebase-nodejs
A tutorial to show how to send Firebase push notifications to mobile devices
To run this code, clone the repository then run npm install to install all necessary dependencies
then run the command node index.js to start the server file

1. open ngnix
2. node .\index.js
3. ngrok http 3000
For Win : ./ngrok http 3000

# Lyrics API
https://www.npmjs.com/package/genius-lyrics-api