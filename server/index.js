//import { getLyrics, getSong } from 'genius-lyrics-api';
const express = require('express')
const bodyparser = require('body-parser')

//const admin = require('./firebase-config')
const genius_lyrics_api = require('genius-lyrics-api')
const getSong = require('genius-lyrics-api')
const player = require('url-song-utilities');

var admin = require("firebase-admin");

var serviceAccount = require("./music-world-6fc4a-firebase-adminsdk-lkh1u-59dbce1d6c.json");
const fs = require('fs')


admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})


const app = express()
app.use(bodyparser.json())

const port = 3000
const notification_options = {
    priority: "high",
    timeToLive: 60 * 60 * 24
  };

app.get('/', function (req, res) { 
  res.send(JSON.stringify({'msg':'Client Connected Successfully to Sync Server'}))
});

app.post('/firebase/notification', (req, res)=>{
    const  registrationToken = req.body.registrationToken
    const message = req.body.message
    const options =  notification_options

     admin.messaging().sendToDevice(registrationToken, message, options)
      .then( response => {

       res.status(200).send(JSON.stringify(response))

      })
      .catch( error => {
          console.log(error);
      });

})


app.get('/lyrics', function (req, res) {
    console.log(req.query.provider+" "+req.query.title);
    if(req.query.provider == "spotify_player"){
        const options = {
            apiKey: 'CGqd36qBOBdzh2_0YpQeu2GSsTSpgiuUHOhFLf2sA1dFxT3B0Jq1qJ8C5JGiTOsl',
            title: req.query.title,
            artist: req.query.artist,
            optimizeQuery: true
        };

        genius_lyrics_api.getLyrics(options).then((lyrics) =>
            res.send(JSON.stringify({'lyrics':lyrics}))
        );
    } else {
        player.getLyrics(req.query.title).then(lyrics => {
            //If the module returns 'undefined' it means that the module did not find any lyrics
            if (!lyrics) return res.status(404).send(JSON.stringify({message:'I did not find any lyrics !'}));
            //If the module finds lyrics
            res.send(JSON.stringify({'lyrics':lyrics}))
        })
    }
});

app.get('/youtube/lyrics', function (req, res) {
    player.getLyrics('KOKA').then(lyrics => {
        //If the module returns 'undefined' it means that the module did not find any lyrics
        if (!lyrics) return res.status(404).send(JSON.stringify({message:'I did not find any lyrics !'}));
        //If the module finds lyrics
        res.send(JSON.stringify({'lyrics':lyrics}))
    })
});

app.get('/songs/language', function (req, res) {
    console.log(req.query.language);
    let query = 'files/latest_songs_in_'+req.query.language+'.json'
    try {
      const data = fs.readFileSync(query, 'utf8')
      res.send(data)
    } catch (err) {
      console.error(err)
    }
});


app.post('/auth', function (req, res) {
    console.log(req.body.type+" "+req.body.identity+" "+req.body.password);
    var type = req.body.type;
    var identity = req.body.identity;
    var password = req.body.password;

    if(identity == ""){
        var message = type == "phone" ? "Required Phone Number" : "Required Email"
        return res.status(400).send(JSON.stringify({message: message}));
    }

    if(password == ""){
        return res.status(400).send({message: 'Required Password'});
    }

    if(type == "phone" || type == "mobile"){
        if (isNaN(identity)) {
            return res.status(400).send(JSON.stringify({message: "Please enter phone number"}));
        }
        if(identity == "9860898850" && password == "1234"){
            res.send(JSON.stringify({'message':'logged in successfully via phone'}))
        } else {
            return res.status(401).send(JSON.stringify({message: "Please check credentials"}));
        }
    }else if(type == "email"){
        if(identity == "nishant.dande@worldofplay.in" && password == "1234"){
            res.send(JSON.stringify({'message':'logged in successfully via email'}))
        } else {
            return res.status(401).send(JSON.stringify({message: "Please check credentials"}));
        }
    }
});




//getSong(options).then((song) =>
//	console.log(`
//	${song.id}
//	${song.url}
//	${song.albumArt}
//	${song.lyrics}`)
//);

app.listen(port, () =>{
console.log("listening to port : "+port)
})


